//
//  SelectionScrollView.m
//  StudyPlaner
//
//  Created by Viraj Harhare on 12/29/14.
//  Copyright (c) 2014 Swatantra Singh. All rights reserved.
//

#import "SelectionScrollView.h"
#import "Merit.h"
#import "MyProfile.h"

#define kMERIT_LABEL_HEIGHT 40
#define kNAVIGATION_BUTTON_WIDTH 110
#define kMERIT_LABEL_OFFSET 20

@interface SelectionScrollView ()
{
    UIScrollView *scrollView;
    UILabel *label;
}


@end

@implementation SelectionScrollView
@synthesize arrContent = _arrContent;
@synthesize delegate;
@synthesize leftNavButton;
@synthesize rightNavButton;

- (id)initWithFrame:(CGRect)frame contentArray:(NSArray *)contentArray
{
    
    self = [super initWithFrame:frame];
    
    if(self)
    {
        _arrContent = [contentArray retain];
        
#if DEBUG
        NSLog(@"_arrContent : %@",_arrContent);
#endif
      

        [self setBackgroundColor:[UIColor clearColor]];
        CGFloat heightForButtonsAndScrollView = frame.size.height - kMERIT_LABEL_HEIGHT;
        
        leftNavButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftNavButton setFrame:CGRectMake(0, 0, kNAVIGATION_BUTTON_WIDTH, heightForButtonsAndScrollView)];
        [leftNavButton setBackgroundColor:[UIColor clearColor]];
        [leftNavButton addTarget:self action:@selector(leftNavigationAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:leftNavButton];
        
        UIImageView *leftNavImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigateLeft.png"]];
        [leftNavImageView setFrame:CGRectMake(0, 31, leftNavImageView.image.size.width, leftNavImageView.image.size.height)];
        [leftNavButton addSubview:leftNavImageView];
        [leftNavImageView release];
        
        
        CGFloat scrollViewFrameWidth = frame.size.width - (2 * kNAVIGATION_BUTTON_WIDTH);
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(leftNavButton.frame.size.width, 0, scrollViewFrameWidth, heightForButtonsAndScrollView)];
        [scrollView setPagingEnabled:YES];
        [scrollView setScrollEnabled:NO];
        [self configureScrollView];
        [self addSubview:scrollView];
        
        rightNavButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightNavButton setFrame:CGRectMake(leftNavButton.frame.size.width + scrollView.frame.size.width, 0, kNAVIGATION_BUTTON_WIDTH, heightForButtonsAndScrollView)];
        [rightNavButton setBackgroundColor:[UIColor clearColor]];
        [rightNavButton addTarget:self action:@selector(rightNavigationAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:rightNavButton];
        
        UIImageView *rightNavImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigateRight.png"]];
        [rightNavImageView setFrame:CGRectMake(kNAVIGATION_BUTTON_WIDTH - rightNavImageView.image.size.width, 31, rightNavImageView.image.size.width, rightNavImageView.image.size.height)];
        [rightNavButton addSubview:rightNavImageView];
        [rightNavImageView release];

        
        CGFloat meritLabelWidth = frame.size.width - (2 * kMERIT_LABEL_OFFSET);
        label = [[UILabel alloc] initWithFrame:CGRectMake((frame.size.width - meritLabelWidth)/2, rightNavButton.frame.size.height ,  meritLabelWidth, kMERIT_LABEL_HEIGHT)];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setText:((Merit *)[self.arrContent firstObject]).name];
        [label setFont:[UIFont systemFontOfSize:12.0]];
        [label setTextColor:[UIColor blackColor]];
        [label setBackgroundColor:[UIColor clearColor]];
        [self addSubview:label];
    }
    
    return self;
}

- (void)configureScrollView
{
    [scrollView setContentSize:CGSizeMake(scrollView.frame.size.width * self.arrContent.count, scrollView.frame.size.height)];

#if DEBUG
    NSLog(@"configureScrollView : self.arrContent : %@",self.arrContent);
#endif
    
    
    for (int i=0; i<[self.arrContent count]; i++)
    {
        Merit *merit = [self.arrContent objectAtIndex:i];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*scrollView.frame.size.width, 0, scrollView.frame.size.width, scrollView.frame.size.height)];
        [imageView setImage:merit.image];
        [scrollView addSubview:imageView];
    }
    
    [scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
    
    
}

- (void)leftNavigationAction:(UIButton *)button
{
    
        CGFloat xOffset = scrollView.contentOffset.x;
        CGFloat pageWidth = scrollView.frame.size.width;
#if DEBUG
      NSLog(@"leftNavigationAction : xOffset : %f",xOffset);
#endif
    
        
        if(xOffset > 0)
        {
            [scrollView setContentOffset:CGPointMake(xOffset - pageWidth, 0) animated:NO];
            
            // to select prev merit from array
            NSInteger pageNumber = (xOffset - pageWidth)/pageWidth;
            Merit *merit = [self.arrContent objectAtIndex:pageNumber];
#if DEBUG
            NSLog(@"new merit.name : %@",merit.name);

#endif
            [label setText:merit.name];
            [self.delegate didChangeSelectionToNewValue:merit];
        }

    
    
}

- (void)rightNavigationAction:(UIButton *)button
{
    
        CGFloat xOffset = scrollView.contentOffset.x;
        CGFloat pageWidth = scrollView.frame.size.width;
    
#if DEBUG
        NSLog(@"rightNavigationAction : xOffset : %f",xOffset);
#endif

        
        if(xOffset < (([self.arrContent count]-1) * pageWidth))
        {
            [scrollView setContentOffset:CGPointMake(xOffset + pageWidth, 0) animated:NO];
            
            // to select next merit from array
            NSInteger pageNumber = (xOffset + pageWidth)/pageWidth;
            Merit *merit = [self.arrContent objectAtIndex:pageNumber];
#if DEBUG
            NSLog(@"new merit.name : %@",merit.name);
#endif

            [label setText:merit.name];
            [self.delegate didChangeSelectionToNewValue:merit];
        }
}

- (void)navigateToSelectedMerit:(Merit *)selectedMerit
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.listofvaluesTypeid == %@",selectedMerit.listofvaluesTypeid];
    NSArray *filteredArray = [self.arrContent filteredArrayUsingPredicate:predicate];
    
#if DEBUG
    NSLog(@"filteredArray ; %@",filteredArray);
#endif
    
    if([filteredArray count] == 1)
    {
        Merit *meritFromArrContent = [filteredArray objectAtIndex:0];
#if DEBUG
        NSLog(@"meritFromArrContent ; %@",meritFromArrContent);
#endif
      
        
        NSUInteger indexOfMerit = [self.arrContent indexOfObject:meritFromArrContent];
#if DEBUG
        NSLog(@"indexOfMerit : %lu",(unsigned long)indexOfMerit);
        NSLog(@"before : xOffset : %f",scrollView.contentOffset.x);
#endif
      

        CGFloat pageWidth = scrollView.frame.size.width;
        [scrollView setContentOffset:CGPointMake(indexOfMerit * pageWidth, 0) animated:YES];

#if DEBUG
        NSLog(@"after : xOffset : %f",scrollView.contentOffset.x);
#endif
      
        //setting merit label
        [label setText:meritFromArrContent.name];

        [self.delegate didChangeSelectionToNewValue:meritFromArrContent];
    }
    else
    {
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Selected Merit can not be found" delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
}

- (void)dealloc
{
    [scrollView release];
    [label release];
    [_arrContent release];
    
    [super dealloc];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end

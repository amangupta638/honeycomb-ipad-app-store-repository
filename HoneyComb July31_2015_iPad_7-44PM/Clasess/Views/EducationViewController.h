//
//  EducationViewController.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "School_Information.h"

@interface EducationViewController : UIViewController
{
    NSInteger count;
    IBOutlet UIWebView *_webView;
    IBOutlet UILabel *_labelForPrev;
    IBOutlet UILabel *_labelForNext;
}
@property(retain,nonatomic) CATextLayer *_testlayer;
@property (nonatomic,retain)NSArray *_arrEducationContentData;
- (IBAction)PrvevousButtonAction:(id)sender;
- (IBAction)nextButtonAction:(id)sender;
-(void)searchEducationContent:(School_Information*)infoObj;
-(void)updateEducationContentDaisy;

@end

//
//  MyClassViewController.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 26/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "MyClassViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Service.h"
#import "AppHelper.h"
#import "Defines.h"
#import "MyProfile.h"
#import "MyClass.h"
#import "Student.h"
#import "DairyItemUsers.h"
#import "MainViewController.h"
#import "Service.h"
#import "DiaryItemTypes.h"

#import "FileUtils.h"

//Merit - Start
#import "Merit.h"
//Merit - End

@interface MyClassViewController ()
{
    BOOL isClassSelected;
    
    //Merit - Start
    Merit *selectedMerit;
    NSInteger notifyInteger;
    DairyItemUsers *selectedDIUser;
    Item *selectedItem;
    SelectionScrollView *selectionScrollView;
    //Merit - End
}

@end

@implementation MyClassViewController
{
    NSInteger myClassSelectedIndexPathRow;  //Aman Added EZTEST 871
}
@synthesize _popoverControler,_currentTimeTable;
@synthesize dictForClassDetail;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark viewLifeCycle
-(void)refreshCurentItemClass
{
    if(_selectedRow>=0)
    {
        MyClass *classObj=[_arrForNewClass objectAtIndex:_selectedRow];
        [AppDelegate getAppdelegate]._curentClass=classObj;
        
        //for get all Dairy item
        NSArray *arr1=[[Service sharedInstance]getStoredAllItemDataWithSubjectId:classObj.subject_name];
        NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"assignedDate"ascending:NO];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
        
        [_arrForNewTask removeAllObjects];
        for (Item *itemObj in arr1) {
            if (([itemObj.assignedtoMe integerValue]==0)) {
                [_arrForNewTask addObject:itemObj];
            }
            
        }
#if DEBUG
        NSLog(@"_arrForNewTask = %@",_arrForNewTask);
#endif

        NSMutableArray *arrfilter = [[NSMutableArray alloc] init];
        
        
        for(Item *itemObj in _arrForNewTask)
        {
            NSString *strType = [itemObj.type lowercaseString];
            if ((![strType isEqualToString:@"note"]) && (![strType isEqualToString:@"event"]))
            {
                if(!([strType isEqualToString:@"outofclass"] || [strType isEqualToString:@"OutOfClass"] || [strType isEqualToString:@"Out Of Class"] || [strType isEqualToString:k_MERIT_CHECK] || [strType isEqualToString:@"merit"]))
                {
                    // Adding object for filter all
#if DEBUG
                    NSLog(@"adding object for all ... %@",strType);
#endif

                    [arrfilter addObject:itemObj];
                }
            }
        }
        
        [_arrForNewTask removeAllObjects];
        _arrForNewTask=nil;
        _arrForNewTask = [[NSMutableArray alloc]init];
        _arrForNewTask = arrfilter;
        
        [_tableForTask reloadData];
        UITableViewCell *cell = [_tableForTask cellForRowAtIndexPath:indexPathForTask];
        [cell setSelected:YES animated:NO];
        
        [_tableForTask selectRowAtIndexPath:indexPathForTask animated:NO scrollPosition:UITableViewScrollPositionNone];

        _tableForTask.hidden=NO;
        // Commented and added [_tableForStudent reloadData] to resolve issue number EZTEST-2561
        //_tableForStudent.hidden=YES;
        [_tableForStudent reloadData];
        // Commented and added [_tableForStudent reloadData] to resolve issue number EZTEST-2561
    }
#if DEBUG
    NSLog(@"refreshCurentItemClass : [AppDelegate getAppdelegate]._currentViewController : %@",[AppDelegate getAppdelegate]._currentViewController);
#endif
    if([[AppDelegate getAppdelegate]._currentViewController.view.subviews containsObject:self.viewForMerit])
        [self.viewForMerit removeFromSuperview];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 1654
    isClassSelected = NO;
    
    myClassSelectedIndexPathRow = 0; //Aman Added EZTEST 871
    _lableForFilter.text=@"All";//////
    [AppDelegate getAppdelegate]._curentClass=nil;
    _selectedRow=-1;
    prevRowIndex=-1;
    _arrForClass=[[NSMutableArray alloc] init];
    
    _arrForNewClass=[[NSMutableArray alloc] init];
    
    _arrForNewTask=[[NSMutableArray alloc] init];
    
    _arrForStudent=[[NSMutableArray alloc]init];
    
    
    _arrForCheckImg=[[NSMutableArray alloc] init];
    UIImageView *imgView=[[UIImageView alloc]initWithFrame:CGRectMake(_tableForClass.frame.origin.x, _tableForClass.frame.origin.y, _tableForClass.frame.size.width, _tableForClass.frame.size.height+10)];
    imgView.backgroundColor=[UIColor clearColor];
    imgView.image=[UIImage imageNamed:@"myclassbase"];
    [self.view addSubview:imgView];
    [imgView release];
    [self.view bringSubviewToFront:_tableForClass];
    UIImageView *imgView1=[[UIImageView alloc]initWithFrame:CGRectMake(_tableForTask.frame.origin.x, _tableForTask.frame.origin.y, _tableForTask.frame.size.width, _tableForTask.frame.size.height+10)];
    imgView1.backgroundColor=[UIColor clearColor];
    imgView1.image=[UIImage imageNamed:@"myclassbase"];
    [self.view addSubview:imgView1];
    [imgView1 release];
    [self.view bringSubviewToFront:_tableForTask];
    _buttonCancel.hidden=YES;
    _buttonDone.hidden=YES;
    _tableForStudent.hidden=YES;
    _tableForTask.hidden=NO;
    _buttonForRemind.hidden=YES;
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    
    int index =1;
    
    for(TimeTable *time in arr){
        
        if([DELEGATE isDate:[NSDate date] inRangeFirstDate:time.startDate  lastDate:time.end_date])
            
        {
            self._currentTimeTable=time;
            
        }
        
        index++;
    }

    
    // Code added to support if current date doesn't lie between start and end date of any semester
    //--------------------------------------------------------------------------
    if(self._currentTimeTable==nil)
    {
        for(TimeTable *time in arr)
        {
#if DEBUG
            NSLog(@"time = %@",time);
            NSLog(@"NSDate = %@",[NSDate date]);
#endif
            
            if([[NSDate date] compare:time.startDate] == NSOrderedAscending)
            {
                self._currentTimeTable=time;
                break;
            }
        }
        if(self._currentTimeTable == nil)
        {
            self._currentTimeTable = [arr lastObject];
        }
    }
    //--------------------------------------------------------------------------

    
    // Do any additional setup after loading the view from its nib.
    
    //Merit - Start
    notifyInteger = 1;
    [self.notifyParentsButton setSelected:YES];
    //Merit - End
}

-(void)viewWillAppear:(BOOL)animated
{
     _buttonForRemind.hidden = YES;
    [AppHelper saveToUserDefaults:NULL withKey:@"isMyClass"];
    
    [self addClassItem];
}


//Aman Added EZTEST 871
-(void)loadLastSelectedIndex
{
    if (myClassSelectedIndexPathRow)
    {
        _lableForFilter.text=@"All";
         [_tableForClass selectRowAtIndexPath:[NSIndexPath indexPathForRow:myClassSelectedIndexPathRow inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    }
}

// Campus Based Start

-(void)addClassItem{
    [_arrForNewClass removeAllObjects];
    
    /****************************************************************/
    
    NSArray *arr1=[[Service sharedInstance]getStoredAllTimeTableData];
    NSMutableArray *arrofTimeTabelID = [[[NSMutableArray alloc] init] autorelease];
    /////
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:kDATEFORMAT_Slash];
    
    NSDate *dDate = [NSDate date];
    [format setDateFormat:@"yyyy-MM-dd"];
    
    NSString *strinDate = [format stringFromDate:dDate];
#if DEBUG
    NSLog(@"strinDate : %@",strinDate);
#endif
    
    NSDate *date = nil;
    
    date = [format dateFromString:strinDate];

    /////
    
    for(TimeTable *timetable in arr1)
    {
        if([DELEGATE isDate:date inRangeFirstDate:timetable.startDate  lastDate:timetable. end_date])
        {
#if DEBUG
            NSLog(@"in range of dates %@",timetable);
#endif

            [arrofTimeTabelID addObject:timetable];
        }
    }
    
#if DEBUG
    NSLog(@"arrofTimeTabelID %@",arrofTimeTabelID);
#endif

    // Code added to support if current date doesn't lie between start and end date of any semester
    //----------------------------------------------------------------------
    //TODO: Please verify the logic for multi campus
    if([arrofTimeTabelID count]==0)
    {
        for(TimeTable *time in arr1)
        {

#if DEBUG
            NSLog(@"time = %@",time);
            NSLog(@"NSDate = %@",[NSDate date]);
#endif
            
            if([[NSDate date] compare:time.startDate] == NSOrderedAscending)
            {
                [arrofTimeTabelID addObject:time];
                break;
            }
        }
        
        if([arrofTimeTabelID count]==0)
        {
            self._currentTimeTable = [arr1 lastObject];
        }
    }
    //----------------------------------------------------------------------

    
    NSMutableArray *arrOfClassesOfAllCampus = [[[NSMutableArray alloc] init] autorelease];
    
#if DEBUG
    NSLog(@"arrofTimeTabelID : %@",arrofTimeTabelID);
#endif

    for(TimeTable *timetable in arrofTimeTabelID)
    {
        NSArray *arr=[[Service sharedInstance]getStoredAllClassDataInApp:timetable.timeTable_id];
        for(MyClass *classes in arr)
        {
            [arrOfClassesOfAllCampus addObject:classes];
        }
    }
#if DEBUG
    NSLog(@"arrOfClassesOfAllCampus = %@",arrOfClassesOfAllCampus);
#endif
    
    NSArray *arr=arrOfClassesOfAllCampus;
    
//------------------------------------------------------------------------------
    
    
    
    
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"class_name"ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr =[NSMutableArray arrayWithArray:[arr sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    NSMutableArray *arrData=[[NSMutableArray alloc]init];
    
#if DEBUG
    NSLog(@"arr==%@",arr);
#endif
    
    for (MyClass *classObj in arr)
    {
#if DEBUG
        NSLog(@"classObj==%@",classObj.class_name);
#endif
      
        if(![arrData containsObject:classObj.class_id])
        {
            [arrData addObject:classObj.class_id];
            [_arrForNewClass addObject:classObj];
        }
    }
#if DEBUG
    NSLog(@"_arrForNewClass==%@",_arrForNewClass);
#endif
    

    [arrData release];
    [_picker reloadAllComponents];
    [_tableForClass reloadData];
}
// Campus Based End


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [dictForClassDetail release];
    [_currentTimeTable release];
    [_arrForClass release];
    [_arrForStudent release];
    [_arrForNewTask release];
    [_arrForNewClass release];
    [_popoverControler release];
    [_arrForCheckImg release];
    [_tableForClass release];
    [_tableForTask release];
    [_tableForStudent release];
    [_lableForFilter release];
    [_pickerView release];
    [_picker release];
    [_buttonForRemind release];
    [_buttonDone release];
    [_buttonCancel release];
    [_indicatorView release];
    [_viewForReminderText release];
    [_textViewForReminder release];
    [_lblForItemType release];
    [_lblForAssignTime release];
    [_lblForSubName release];
    [_lblForDueTime release];
    [_ViewForRemiderContent release];
    [_viewForSearchAndAsociatesClass release];
    [_itemArr release];
    
    //Merit - Start
    [_viewForMerit release];
    [_labelForClassName release];
    [_labelForStudentName release];
    [_notifyParentsButton release];
    [_labelForNotifyParents release];
    [_textViewForMeritComment release];
    [selectedItem release];
    if(selectionScrollView)
        [selectionScrollView release];
    //Merit - End
    
    [_btnMeritSaveButton release];
    [_btnDeleteMerit release];
    [_labelMeritViewTitle release];
    [_labelSelectMerittobeAssigned release];
    [super dealloc];
}
- (void)viewDidUnload {
    [_tableForClass release];
    _tableForClass = nil;
    [_tableForTask release];
    _tableForTask = nil;
    [_tableForStudent release];
    _tableForStudent = nil;
    [_lableForFilter release];
    _lableForFilter = nil;
    [_pickerView release];
    _pickerView = nil;
    [_picker release];
    _picker = nil;
    [_buttonForRemind release];
    _buttonForRemind = nil;
    [_buttonDone release];
    _buttonDone = nil;
    [_buttonCancel release];
    _buttonCancel = nil;
    [_indicatorView release];
    _indicatorView = nil;
    [_viewForReminderText release];
    _viewForReminderText = nil;
    [_textViewForReminder release];
    _textViewForReminder = nil;
    [_lblForItemType release];
    _lblForItemType = nil;
    [_lblForAssignTime release];
    _lblForAssignTime = nil;
    [_lblForSubName release];
    _lblForSubName = nil;
    [_lblForDueTime release];
    _lblForDueTime = nil;
    [_ViewForRemiderContent release];
    _ViewForRemiderContent = nil;
    [_viewForSearchAndAsociatesClass release];
    _viewForSearchAndAsociatesClass = nil;
    [_itemArr release];
    _itemArr = nil;
    [super viewDidUnload];
}

#pragma mark - Merits

//Merit - Start
- (void)addMeritAction:(UIButton *)button
{
    
    [DELEGATE set_isClassBasedOrTaskBasedMeritDelete:YES]; // NO means Class Based Merit is going to delete YES means TaskBasedMerit
    
#if DEBUG
    NSLog(@"button.tag : %ld",(long)button.tag);
    NSLog(@"button.title : %@",button.currentTitle);
#endif

    //Class name
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    NSInteger totalCampusFromDB = [[schoolDetailObj noOfCampus] integerValue];
    
    if (totalCampusFromDB > 1 )
    {
        NSString *classNameWithCampusCode = [NSString stringWithFormat:@"%@(%@)",[AppDelegate getAppdelegate]._curentClass.class_name,[AppDelegate getAppdelegate]._curentClass.campusCode];
        self.labelForClassName.text=classNameWithCampusCode;
    }
    else
    {
        self.labelForClassName.text = [AppDelegate getAppdelegate]._curentClass.class_name;
    }
    
    //student name
    DairyItemUsers *stuObj= [_marrDiaryItemUsers objectAtIndex:button.tag];
    self.labelForStudentName.text = stuObj.studentName;
    selectedDIUser = [stuObj retain];

    if ([stuObj.listofvaluesTypeid integerValue] != 0 && [[stuObj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
    {
        self.textViewForMeritComment.text = stuObj.meritDescription;
    }
    else
    {
        //comments
        self.textViewForMeritComment.text = @"";
    }
    
    NSArray *meritsAsMeritModelsArr = [[Service sharedInstance] getAllMeritsAsMeritModels];

#if DEBUG
    NSLog(@"meritsAsMeritModelsArr : %@",meritsAsMeritModelsArr);
    NSLog(@"meritsAsMeritModelsArr count : %lu",(unsigned long)meritsAsMeritModelsArr.count);
#endif

    if(selectedMerit)
    {
        [selectedMerit release]; selectedMerit = nil;
    }
    
    if([button.currentTitle isEqualToString:@"Add Merit"])
    {
        selectedMerit = [[meritsAsMeritModelsArr firstObject] retain];
        notifyInteger = 0;
        [self.notifyParentsButton setSelected:NO];
    }
    else
    {
        for (Merit *merit in meritsAsMeritModelsArr)
        {
            if([merit.listofvaluesTypeid integerValue]== [selectedDIUser.listofvaluesTypeid integerValue])
            {
                selectedMerit = [merit retain];
            }
        }

        notifyInteger = [selectedDIUser.isNotify integerValue];
        if (notifyInteger == 0)
        {
            [self.notifyParentsButton setSelected:NO];
        }
        else
        {
            [self.notifyParentsButton setSelected:YES];
        }
    }
    
    if(selectionScrollView)
    {
        [selectionScrollView removeFromSuperview];
        [selectionScrollView release];
        selectionScrollView = nil;
    }
    
    UIView *meritView = [self.viewForMerit viewWithTag:500];
    selectionScrollView = [[SelectionScrollView alloc] initWithFrame:CGRectMake(25, 255, 350, 135) contentArray:meritsAsMeritModelsArr];
    selectionScrollView.delegate = self;
    
    [selectionScrollView navigateToSelectedMerit:selectedMerit];

    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];

    
    if([button.currentTitle isEqualToString:@"Add Merit"]){
        
        _isEdit = NO;

        [_labelSelectMerittobeAssigned setText:@"Select Merit/Concern to be assigned"];
        [selectionScrollView.rightNavButton setUserInteractionEnabled:YES];
        [selectionScrollView.leftNavButton setUserInteractionEnabled:YES];
        [selectionScrollView.rightNavButton setHidden:NO];
        [selectionScrollView.leftNavButton setHidden:NO];

        [self.textViewForMeritComment setUserInteractionEnabled:YES];
        [self.textViewForMeritComment setEditable:YES];
        [self.notifyParentsButton setUserInteractionEnabled:YES];
        [self.btnMeritSaveButton setHidden:NO];
        self.textViewForMeritComment.text = @"";
        [self.btnMeritSaveButton setTitle:@"Assign Merit" forState:UIControlStateNormal];
        [self.labelMeritViewTitle setText:@"Add Item"];

    }
    else
    {
        _isEdit = YES;

        ListofValues *lov = [[Service sharedInstance] getListOfValuesInfoStored:selectedMerit.listofvaluesTypeid];
        
        [_labelSelectMerittobeAssigned setText:@"Merit Assigned"];

        if([lov.type isEqualToString: @"Demerit"]|| [lov.type isEqualToString: @"demerit"])
        {
            [_labelSelectMerittobeAssigned setText:@"Concern"];
            
        }

        [selectionScrollView.rightNavButton setUserInteractionEnabled:NO];
        [selectionScrollView.leftNavButton setUserInteractionEnabled:NO];
        [selectionScrollView.rightNavButton setHidden:YES];
        [selectionScrollView.leftNavButton setHidden:YES];
        [self.textViewForMeritComment setUserInteractionEnabled:YES];
        [self.textViewForMeritComment setEditable:NO];

        [self.notifyParentsButton setUserInteractionEnabled:NO];
        [self.btnMeritSaveButton setHidden:YES];
        if ([profobj.type isEqualToString:@"Teacher"])
        {
            [self.labelMeritViewTitle setText:@"Merit"];
        }
        // Student
        else
        {
            [self.labelMeritViewTitle setText:@"Assigned Item"];
        }
    }

    // Teacher
    if ([profobj.type isEqualToString:@"Teacher"])
    {
        if([button.currentTitle isEqualToString:@"Add Merit"])
        {
            [_btnDeleteMerit setHidden:YES];
        }
        else
        {
            [_btnDeleteMerit setHidden:NO];
        }
    }
    // Student
    else
    {
        [_btnDeleteMerit setHidden:YES];
    }
    
    
    if ([schoolDetailObj.allow_parent isEqualToNumber:[NSNumber numberWithInteger:1]] && [profobj.type isEqualToString:@"Teacher"] )
    {
        _labelForNotifyParents.hidden = NO;
        _notifyParentsButton.hidden = NO;
    }
    else
    {
        _labelForNotifyParents.hidden = YES;
        _notifyParentsButton.hidden = YES;
    }
    [meritView addSubview:selectionScrollView];

    [[AppDelegate getAppdelegate]._currentViewController.view addSubview:self.viewForMerit];
}


- (void)didChangeSelectionToNewValue:(id)newSelection
{
    if(selectedMerit)
    {
        [selectedMerit release];
        selectedMerit = nil;
    }
    selectedMerit = [(Merit *)newSelection retain];
#if DEBUG
    NSLog(@"newly selected Merit : %@",selectedMerit);
#endif
    
}

- (IBAction)cancelButtonForMerit:(UIButton *)sender
{
    [self.viewForMerit removeFromSuperview];
}

- (IBAction)notifyButtonAction:(id)sender {
    if(notifyInteger == 0)
    {
        notifyInteger = 1;
        [sender setSelected:YES];
    }
    else
    {
        notifyInteger = 0;
        [sender setSelected:NO];
    }
}

- (IBAction)saveMeritAction:(UIButton *)button
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
        
        NSNumber *lastUpdatedNumber = [NSNumber numberWithDouble:[[[Service sharedInstance] miliSecondFromDateMethod:selectedItem.lastUpdated] doubleValue]];
        
        NSNumber *passTypeId = selectedMerit.listofvaluesTypeid;//[NSNumber numberWithInteger:16];
        
        //selected students' id should be set in assignUserId
        NSString *asssignedUserIdStr = selectedDIUser.student_id;
        
        // Assign And Due of Item
        //Assign
        NSDate *assignedDate = selectedItem.assignedDate;//dueDate
        
        NSDateFormatter *df2 = [[NSDateFormatter alloc] init];
        [df2 setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
        NSString *assignedDateStr = [df2 stringFromDate:assignedDate];
        NSArray *assignedDateArr = [assignedDateStr componentsSeparatedByString:@" "];
        
        NSString *onlyAssignedDateStr = [assignedDateArr objectAtIndex:0];
        NSString *onlyAssignedTimeStr = [assignedDateArr objectAtIndex:1];

        //Due
        NSDate *dueDate = selectedItem.dueDate;
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
        NSString *dueDateStr = [df stringFromDate:dueDate];
        NSArray *dueDateArr = [dueDateStr componentsSeparatedByString:@" "];
        
        NSString *onlyDueDateStr = [dueDateArr objectAtIndex:0];
        NSString *onlyDueTimeStr = [dueDateArr objectAtIndex:1];

        //////
        
        
        //TODO add OBSERVER
        MainViewController *mainVC=(MainViewController*)[AppDelegate getAppdelegate]._currentViewController;

        [[NSNotificationCenter defaultCenter] addObserver:mainVC selector:@selector(diaryItemDidEdit:) name:NOTIFICATION_EditItem object:nil];

       
        [[Service sharedInstance] editTaskBasedMeritOnServerWithDIType:selectedItem.type
                                                                typeid:[NSNumber numberWithInteger:[[[Service sharedInstance] getItemTypeID:selectedItem.type] integerValue]]
                                                              schoolId:schoolDetailObj.school_Id
                                                               classId:[AppDelegate getAppdelegate]._curentClass.class_id
                                                                 title:selectedItem.title
                                                           description:selectedItem.itemDescription
                                                          assignedDate:onlyAssignedDateStr
                                                               dueDate:onlyDueDateStr
                                                            assignTime:onlyAssignedTimeStr
                                                               dueTime:onlyDueTimeStr
                                                           createdDate:[lastUpdatedNumber stringValue]                                                             createdBy:selectedItem.createdBy
                                                           lastUpdated:[lastUpdatedNumber stringValue]
                                                          assignedtoMe:selectedItem.assignedtoMe
                                                                isSync:selectedItem.isSync
                                                              onTheDay:selectedItem.onTheDay
                                                            isApproved:selectedItem.isApproved
                                                           isCompelete:selectedItem.isCompelete
                                                              isDelete:selectedItem.isDelete
                                                                isRead:selectedItem.isRead
                                                                itemId:selectedItem.itemId
                                                           subjectName:selectedItem.subject_name
                                                             completed:selectedItem.isCompelete
                                                              priority:selectedItem.priority
                                                              progress:selectedItem.progress
                                                                weight:selectedItem.weight
                                                              isNotify:[NSNumber numberWithInteger:notifyInteger]
                                                          meritComment:self.textViewForMeritComment.text
                                                             meritName:selectedMerit.name
                                                             meritType:selectedMerit.subType
                                                            passTypeId:passTypeId
                                                          assignUserId:asssignedUserIdStr];

    }
    else
    {
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }

}
//Merit - End

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return height for rows.
    return 48;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows.
    if (tableView==_tableForClass)
    {
        // flurry Crash : added array count condition
        if ([_arrForNewClass count]>0)
        {
            return [_arrForNewClass count];
        }
        else
        {
            return 0;
        }
    }
    else if (tableView==_tableForTask)
    {
        // flurry Crash : added array count condition
        if ([_arrForNewTask count]>0)
        {
            return [_arrForNewTask count];
        }
        else
        {
            return 0;
        }
    }
    else if (tableView==_tableForStudent)
    {
        //Aman changed --active/inactive students
        if (_marrDiaryItemUsers.count>0)
        {
            return _marrDiaryItemUsers.count;
        }
        else
        {
            return 0;
        }
    }
    else{
        return 0;
    }
    
}
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView==_tableForStudent)
    {
        if(_buttonForRemind.hidden==YES)
            return 50;
        else
            return 0;
    }
    else
    {
        
        return 0;
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if(tableView==_tableForStudent)
    {
        if(_buttonForRemind.hidden==YES)
        {
#if DEBUG
            NSLog(@"_buttonForRemind Hidden YES");
#endif
      
            UIView *headerView=[[[UIView alloc] init] autorelease];
            headerView.frame=CGRectMake(0, 0,_tableForStudent.frame.size.width, 50);
            headerView.backgroundColor=[UIColor clearColor];
            
            UIImageView *sectionImage=[[UIImageView alloc] init];
            sectionImage.backgroundColor=[UIColor clearColor];
            sectionImage.frame=CGRectMake(0, 0,_tableForStudent.frame.size.width, 50);
            [headerView addSubview:sectionImage];
            [sectionImage release];
            
            
            UIButton *checkBtn=[UIButton buttonWithType:UIButtonTypeCustom];
            // checkbox uncheck
            [checkBtn setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
            if (isSelectAll) {
                // checkbox checked
                [checkBtn setImage:[UIImage imageNamed:@"additmcheckbox.png"] forState:UIControlStateNormal];
            }
            else{
                // checkbox unchecked
                [checkBtn setImage:[UIImage imageNamed:@"additmcheckboxuncheck.png"] forState:UIControlStateNormal];
            }
            
            [checkBtn addTarget:self action:@selector(selectAllButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            checkBtn.frame=CGRectMake(_tableForStudent.frame.size.width-33, 13,24, 24);
            [headerView addSubview:checkBtn];
            
            UILabel* selectAllLabel=[[UILabel alloc] init];
            selectAllLabel.frame=CGRectMake(5, 10,340, 30);
            selectAllLabel.font=[UIFont systemFontOfSize:15.0f];
            selectAllLabel.backgroundColor=[UIColor clearColor];
            selectAllLabel.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
            [headerView addSubview:selectAllLabel];
            [selectAllLabel release];
            
            
            sectionImage.image=[UIImage imageNamed:@"myclasstopcell"];
            
            if (isClassSelected)
            {
                 selectAllLabel.text=@"   Students enrolled in selected class";
                 checkBtn.hidden = YES;
            }
            else
            {
                selectAllLabel.text=@"Select All";
                checkBtn.hidden = NO;
            }
            
            return headerView;
        }
        else
        {
#if DEBUG
                NSLog(@"_buttonForRemind Hidden NO -> should Show");
#endif
      
            return NO;
        }
    }
    else
    {
        return nil;
    }
}


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView ==_tableForClass ){
        NSString *MyIdentifier=[NSString stringWithFormat:@"%@",@"cell"];
        UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (myCell == nil)
        {
            myCell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier]autorelease];
            myCell.backgroundColor=[UIColor clearColor ];
            myCell.selectionStyle = UITableViewCellSelectionStyleBlue;
            myCell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
        }
        
        myCell.textLabel.frame=CGRectMake(18, 0,240, 30);
        myCell.textLabel.font=[UIFont fontWithName:@"Arial-BoldMT" size:12.0];
        myCell.textLabel.backgroundColor=[UIColor clearColor];
        myCell.textLabel.textColor=[UIColor darkGrayColor];
        MyClass *class=[_arrForNewClass objectAtIndex:indexPath.row];
#if DEBUG
        NSLog(@"cellForRowAtIndexPath class %@",class);
#endif
      
        
        // Campus Based Start

        School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
        NSInteger totalCampusFromDB = [[schoolDetailObj noOfCampus] integerValue];
        
        if (totalCampusFromDB > 1 )
        {
            NSString *classNameWithCampusCode = [NSString stringWithFormat:@"%@(%@)",class.class_name,class.campusCode];
            myCell.textLabel.text=classNameWithCampusCode;
            
        }
        else
        {
            myCell.textLabel.text=class.class_name;
        }
        // Campus Based End
        
        return myCell;
    }
    else if (tableView==_tableForTask)
    {
        static NSString *CellIdentifier = @"myIdenfier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        NSArray *nib;
        if (cell == nil)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"MyClassCellforTask" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleBlue;
            cell.accessoryType=UITableViewCellAccessoryDisclosureIndicator;
            cell.backgroundColor=[UIColor clearColor ];
        }
        UIImageView *iconImgV=(UIImageView*)[cell.contentView viewWithTag:100];
        UILabel *lblForNewTask = (UILabel*)[cell.contentView viewWithTag:103];
        UILabel *lblForDate=(UILabel*)[cell.contentView viewWithTag:102];
        
        Item *itemObj=[_arrForNewTask objectAtIndex:indexPath.row];
        
        NSString *isTypeStr= itemObj.type;
        if ([isTypeStr isEqualToString:@"Assignment"]) {
            iconImgV.image=[UIImage imageNamed:@"dairygreenicon.png"];
        }
        else if([isTypeStr isEqualToString:@"Homework"]) {
            iconImgV.image=[UIImage imageNamed:@"homeredicon.png"];
        }
        
       
        NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
        [formeter setDateFormat:kDATEFORMAT_ddMMM];//@"dd MMM"
        lblForNewTask.text=itemObj.title;
        lblForDate.text=[formeter stringFromDate:itemObj.assignedDate];
      
        [formeter release];
        return cell;
        
    }
    else if (tableView==_tableForStudent)
    {
        static NSString *CellIdentifier = @"myIdenfier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSArray *nib;
        if (cell == nil)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"MyClassCellforStudent" owner:self options:nil];
            cell = [nib objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        UIImageView *imgViewForIcon=(UIImageView*)[cell.contentView viewWithTag:101];
        UIImageView *checkImg=(UIImageView*)[cell.contentView viewWithTag:106];
        UIImageView *sepratorimg=(UIImageView*)[cell.contentView viewWithTag:107];
        
        UILabel *name = (UILabel*)[cell.contentView viewWithTag:102];
        UILabel *percentage=(UILabel*)[cell.contentView viewWithTag:103];
        UILabel *remind=(UILabel*)[cell.contentView viewWithTag:104];
        UILabel *date=(UILabel*)[cell.contentView viewWithTag:105];
        
        //Merit - Start
        UIButton *addMeritButton = (UIButton *)[cell.contentView viewWithTag:108];
        [addMeritButton setFrame:CGRectMake(addMeritButton.frame.origin.x, addMeritButton.frame.origin.y, addMeritButton.frame.size.width, addMeritButton.frame.size.height)];
        [addMeritButton setTitle:@"Add Merit" forState:UIControlStateNormal];
        [addMeritButton addTarget:self action:@selector(addMeritAction:) forControlEvents:UIControlEventTouchUpInside];
        [addMeritButton setTag:indexPath.row];
        addMeritButton.hidden = YES;

        
        if ([[Service sharedInstance] isMeritActive])
        {
            addMeritButton.hidden = NO;
        }
        //Merit - End
        
               //for image rect round
        [imgViewForIcon.layer setMasksToBounds:YES];
        [imgViewForIcon.layer setCornerRadius:18.0f];
        [imgViewForIcon.layer setBorderWidth:2.0];
        [imgViewForIcon.layer setBorderColor:[[UIColor grayColor] CGColor]];
        
        if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7"))
        {
            [_tableForStudent setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            [cell.contentView setBackgroundColor:[UIColor colorWithRed:224/255.0f green:224/255.0f blue:224/255.0f alpha:1.0f]];
        }
        else
        {
            [_tableForStudent setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        }
        
        if (isClassSelected == YES)
        {
            checkImg.hidden = YES;
            percentage.hidden = YES;
            remind.hidden = YES;
            date.hidden = YES;
            sepratorimg.hidden = YES;
            //Merit - Start
            addMeritButton.hidden = YES;
            //Merit - End
        }
        else
        {
          if(_buttonForRemind.hidden==YES)
          {
#if DEBUG
              NSLog(@"_buttonForRemind.hidden = YES for CellForRow");
#endif

            checkImg.hidden=NO;
            percentage.hidden=YES;
            remind.hidden=YES;
            date.hidden=YES;
            sepratorimg.hidden=YES;
            //Merit - Start
            addMeritButton.hidden = YES;
            //Merit - End

            if(_arrForCheckImg.count>0)
            {
                if([_arrForCheckImg containsObject:[_arrForStudent objectAtIndex:indexPath.row]] )
                {
                    checkImg.image=[UIImage imageNamed:@"additmcheckbox.png"];
                }
                else{
                    checkImg.image=nil;
                }
            }
          }
          else
          {
#if DEBUG
              NSLog(@"_buttonForRemind.hidden = NO for CellForRow");
#endif

              checkImg.hidden=YES;
              percentage.hidden=NO;
              remind.hidden=NO;
              date.hidden=NO;
              sepratorimg.hidden=NO;
          }
        }
        
#if DEBUG
        NSLog(@"DiaryItemUsers %@",_marrDiaryItemUsers);
#endif

        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
        [formatter setDateFormat:kDATEFORMAT_ddMMMMyy_hhmma];//@"dd MMMM yy
        
        if (!isClassSelected)
        {
            DairyItemUsers *assinUserobj = [_marrDiaryItemUsers objectAtIndex:indexPath.row];
            
            percentage.text=[NSString stringWithFormat:@"%@%@",assinUserobj.progress,@"%"];
            if (assinUserobj.reminder!=nil)
            {
                remind.hidden = NO;
                
                NSString *reminderDateStr=[formatter stringFromDate:assinUserobj.reminder];
                date.text=[NSString stringWithFormat:@"%@",reminderDateStr];
            }
            else
            {
                date.text=@" ";
                remind.hidden = YES;
            }
            
            // show merit button without text  with icon image
            if ([assinUserobj.listofvaluesTypeid integerValue] != 0 && [[assinUserobj.meritTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet] ] length]>0  )
            {
#if DEBUG
                NSLog(@"assinUserobj.meritTitle = %@",assinUserobj.meritTitle);
#endif

                ListofValues *lov = [[Service sharedInstance] getactiveListOfValuesInfoStoredInMyclass:assinUserobj.listofvaluesTypeid];
                if (lov)
                {
                    if ([FileUtils fileExistsAtPath:[NSString stringWithFormat:@"%@/merits",[FileUtils documentsDirectoryPath]] fileName:lov.iconsmall])
                    {

                        NSString *strPath = [NSString stringWithFormat:@"%@/merits/%@",[FileUtils documentsDirectoryPath],lov.iconsmall];
                        [addMeritButton setBackgroundImage:[FileUtils getImageFromPath:strPath] forState:UIControlStateNormal];
                        [addMeritButton setFrame:CGRectMake(addMeritButton.frame.origin.x+30, addMeritButton.frame.origin.y, 40, addMeritButton.frame.size.height)];
                        [addMeritButton setTitle:@"" forState:UIControlStateNormal];
                    }
                }
            }
            // hide merit button conditinaly

            
            
            Student *stuObj= [[Service sharedInstance]getStudentDataInfoStored:assinUserobj.student_id postNotification:NO];
            
            name.text=stuObj.firstname;
            if (stuObj.image) {
                imgViewForIcon.image=[UIImage imageWithData:stuObj.image];
            }
            else{
                imgViewForIcon.image=[UIImage imageNamed:@"missing-thumb.png"];
            }
            
        }
        else
        {
            remind.hidden = YES;
            Student *stuObj= [_marrDiaryItemUsers objectAtIndex:indexPath.row];
            
            name.text=stuObj.firstname;
            if (stuObj.image) {
                imgViewForIcon.image=[UIImage imageWithData:stuObj.image];
            }
            else{
                imgViewForIcon.image=[UIImage imageNamed:@"missing-thumb.png"];
            }
        }
        
        [formatter release];
        
        return cell;
    }
    
    else{
        return nil;
    }
}
#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==_tableForClass){
        
        isClassSelected = YES;
        
        _buttonForRemind.hidden = YES;
        _buttonDone.hidden = YES;
        _buttonCancel.hidden = YES;

        myClassSelectedIndexPathRow = indexPath.row; //Aman Added EZTEST 871
        
        //for _tableForTask background view
        UIImageView *imgView1=[[UIImageView alloc]initWithFrame:CGRectMake(_tableForTask.frame.origin.x, _tableForTask.frame.origin.y, _tableForTask.frame.size.width, _tableForTask.frame.size.height+10)];
        imgView1.backgroundColor=[UIColor clearColor];
        imgView1.image=[UIImage imageNamed:@"myclassbase"];
        [self.view addSubview:imgView1];
        [imgView1 release];
        [self.view bringSubviewToFront:_tableForTask];
        
        
        MyClass *classObj=[_arrForNewClass objectAtIndex:indexPath.row];
        [AppDelegate getAppdelegate]._curentClass=classObj;
        _selectedRow=indexPath.row;
        
        
        //for get all Dairy item
        NSString *strClassId = [NSString stringWithFormat:@"%d",[classObj.class_id integerValue]];
        NSArray *arr1=[[Service sharedInstance]getStoredAllItemDataWithClassId:strClassId];
        NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"assignedDate"ascending:NO];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        arr1 =[NSMutableArray arrayWithArray:[arr1 sortedArrayUsingDescriptors:sortDescriptorArr]];
        
        [_arrForNewTask removeAllObjects];
        [_arrForClass removeAllObjects];
        for (Item *itemObj in arr1) {
            if (([itemObj.assignedtoMe integerValue]==0) && ([itemObj.isCompelete integerValue]==0))
            {
                [_arrForClass addObject:itemObj];
                
                if([_lableForFilter.text isEqualToString:@"Homework"])
                {
                    if([itemObj.type isEqualToString:@"Homework"])
                    {
                        [_arrForNewTask addObject:itemObj];
                        
                    }
                }
                else  if([itemObj.type isEqualToString:@"Assignment"])
                {
                    if([itemObj.type isEqualToString:@"Assignment"])
                    {
                        [_arrForNewTask addObject:itemObj];
                        
                    }
                }
                else
                {
#if DEBUG
                    NSLog(@"inside Else for all...");
#endif
                    NSString *strType = [itemObj.type lowercaseString];
                    if ((![strType isEqualToString:@"note"]) && (![strType isEqualToString:@"event"]))
                    {
                        if(!([strType isEqualToString:@"outofclass"] || [strType isEqualToString:@"OutOfClass"] || [strType isEqualToString:@"Out Of Class"] || [strType isEqualToString:k_MERIT_CHECK] || [strType isEqualToString:@"merit"]))
                        {
                            // Adding object for filter all
#if DEBUG
                            NSLog(@"adding object for all ... %@",strType);
#endif
                            [_arrForNewTask addObject:itemObj];
                        }
                    }
                }
            }
            
        }
        
        if (prevRowIndex!=indexPath.row)
        {
            if(classObj.class_id>0)
            {
                prevRowIndex=indexPath.row;
                
                NSArray *arr1=classObj.student.allObjects;
                if(arr1.count>0)
                {
                    // Below Line commented to fix 1653///
                }
                else
                {
                }
            }
        }
        
        [_tableForTask reloadData];
        _tableForTask.hidden=NO;
        _tableForStudent.hidden=YES;

        // -=-=-=-=--=-=-=-=-=-
        // Students should display as per Selected Class - EZEST-1654
        // Call Method to fetch students of selected class
        [self fillArrayForActiveStudentsAsPerSelectedClass:[NSString stringWithFormat:@"%ld",(long)[classObj.class_id integerValue]]];
        
        //[self fillArrayForActiveStudents];
        [_tableForStudent reloadData];
       
        
        //----
        // If Students are not available then we should remove  Remind button
        _buttonForRemind.hidden = YES;
        _buttonDone.hidden = YES;
        _buttonCancel.hidden = YES;

        if (_marrDiaryItemUsers.count > 0)
        {
            _tableForStudent.hidden=NO;
            
            if (!isClassSelected)
                 _buttonForRemind.hidden=NO;
           
        }
        else
        {
            _tableForStudent.hidden=YES;
        }
        
        [_arrForCheckImg removeAllObjects];
        isSelectAll=NO;
        //----
        // -=-=-=-=-=-=-

        //22AugChangeC
        _lableForFilter.text=@"All";//////
        [_tableForTask reloadData];
    }
    else if(_tableForTask==tableView){
        
        isClassSelected = NO;
        
        _tableForStudent.hidden=NO;
        _buttonForRemind.hidden=NO;
        
        indexPathForTask = [indexPath retain];
        
        Item *itemObj=[_arrForNewTask objectAtIndex:indexPath.row];
        selectedItem = [itemObj retain];
        
        //For reminder info
        NSDateFormatter *formeter=[[NSDateFormatter alloc]init];
        [formeter setDateFormat:kDateFormatToShowOnUI_hhmma];
        NSString *assTime=[formeter stringFromDate:itemObj.assignTimeDate];
        NSString *dueTime=nil;
        
        if([itemObj.onTheDay integerValue]==0){
            dueTime=[formeter stringFromDate:itemObj.dueTimeDate];
            
        }
        [formeter setDateFormat:kDATEFORMAT_ddMMMyyyy];//@"dd MMM yyyy"
         NSString *strFormtemplateName =[[Service sharedInstance] getNameForFormTemplateOnItemIDBasis:itemObj.itemType_id ];
        _lblForItemType.text=[NSString stringWithFormat:@"%@",strFormtemplateName];
        _lblForAssignTime.text=[NSString stringWithFormat:@"%@ \n %@",[formeter stringFromDate:itemObj.assignedDate],assTime];
        _lblForSubName.text=[NSString stringWithFormat:@"%@",itemObj.subject_name];
        
        if(dueTime!=nil){
            _lblForDueTime.text=[NSString stringWithFormat:@"%@ \n %@",[formeter stringFromDate:itemObj.dueDate],dueTime];
            
        }
        else{
            _lblForDueTime.text=[NSString stringWithFormat:@"%@",[formeter stringFromDate:itemObj.dueDate]];
            
        }
        
        [formeter release];
        
        [_arrForStudent removeAllObjects];
        
        if([itemObj.itemId integerValue]>0){
            for (DairyItemUsers *userObj in [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:itemObj.itemId]) {
                // Is isDeletedOnServer ==0 means student is no more assigned to task
                if ([userObj.isDeletedOnServer integerValue]==0 ) {
                    if(![_arrForStudent containsObject:userObj])
                        [_arrForStudent addObject:userObj];
                }
            }
        }
        else{
            for (DairyItemUsers *userObj in [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:itemObj.universal_Id]) {
                if ([userObj.isDeletedOnServer integerValue]==0) {
                    if(![_arrForStudent containsObject:userObj])
                        [_arrForStudent addObject:userObj];
                }
            }
        }
        
          // If Students are not available then we should remove  Remind button
        if (_arrForStudent.count > 0)
        {
            _buttonForRemind.hidden = NO;
            _buttonDone.hidden = YES;
            _buttonCancel.hidden = YES;
            
        }
        else
        {
           _buttonForRemind.hidden = YES;
            _buttonDone.hidden = YES;
            _buttonCancel.hidden = YES;
        }
        
        
        [_arrForCheckImg removeAllObjects];
        isSelectAll=NO;
        
        //Aman added -- active/inactive students
        [self fillArrayForActiveStudents];
        // If Students are not available then we should remove  Remind button
        if (_marrDiaryItemUsers.count > 0)
        {
            _buttonForRemind.hidden = NO;
            _buttonDone.hidden = YES;
            _buttonCancel.hidden = YES;
            
        }
        else
        {
            _buttonForRemind.hidden = YES;
            _buttonDone.hidden = YES;
            _buttonCancel.hidden = YES;
        }
        
        [_tableForStudent reloadData];
    }
    else{
        
        if (isClassSelected == NO) {
        if(_buttonForRemind.hidden==YES){
            
            if([_arrForCheckImg containsObject:[_arrForStudent objectAtIndex:indexPath.row]])
            {
                isSelectAll=NO;
                [_arrForCheckImg removeObject:[_arrForStudent objectAtIndex:indexPath.row]] ;
            }
            else{
                [_arrForCheckImg addObject:[_arrForStudent objectAtIndex:indexPath.row] ];
                
            }
            
        }
        [_tableForStudent reloadData];
        
        }
        
    }
    
}

//Aman added -- active/inactive students
-(void) fillArrayForActiveStudents
{
    _marrDiaryItemUsers = nil;
    _marrDiaryItemUsers = [[NSMutableArray alloc]init];

    int i=0;
    for (DairyItemUsers *assinUserobj in _arrForStudent)
    {
        Student *stuObj= [[Service sharedInstance]getStudentDataInfoStored:assinUserobj.student_id postNotification:NO];
        
        //Aman added --check if student is active/inactive
        if ([stuObj.status isEqualToString:@"1"])
        {
            [_marrDiaryItemUsers addObject:assinUserobj];
        }
        i++;
    }
    
    
    // Sort array according to progress
    _marrDiaryItemUsers = [[self sortGivenArray:_marrDiaryItemUsers forGivenKey:@"progress" assending:NO] mutableCopy];
    
}

//Aman added -- active/inactive students
-(void)fillArrayForActiveStudentsAsPerSelectedClass:(NSString *)classId
{
    _marrDiaryItemUsers = nil;
    _marrDiaryItemUsers = [[NSMutableArray alloc]init];
    
    NSArray *marrForStudents = [[Service sharedInstance]getStoredAllStudentDataWithClassId:[NSString stringWithFormat:@"%ld",(long)[classId integerValue]]];
    
    
    for(Student *stdnt in marrForStudents)
    {
        if ([stdnt.status isEqualToString:@"1"])
        {
            [_marrDiaryItemUsers addObject:stdnt];
        }
        
    }
    
}
#pragma mark -
#pragma mark Sorting array  -

- (NSArray *)sortGivenArray:(NSMutableArray *)marrForSorting forGivenKey:(NSString *)sortkeyName assending:(BOOL)isAssending
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for(DairyItemUsers *users in marrForSorting)
    {
        [arr addObject:[NSDictionary dictionaryWithObjectsAndKeys:users,@"User",[NSNumber numberWithInteger:[users.progress integerValue]],sortkeyName, nil]];
    }
    
    // Sort array according to progress
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:sortkeyName
                                                                   ascending:isAssending];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];

    NSArray *sortedArray = [arr sortedArrayUsingDescriptors:sortDescriptors];
    [arr removeAllObjects];
    for(DairyItemUsers *user in [sortedArray valueForKey:@"User"])
    {
        [arr addObject:user];
    }
    sortedArray = arr;
    return sortedArray;
}

#pragma mark
#pragma mark pickerView delgates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    return [_itemArr count];
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return  [_itemArr objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    [_arrForNewTask removeAllObjects];
    
    NSString *stringForOption=[_itemArr objectAtIndex:row];
    _lableForFilter.text=stringForOption;

    
    if(row==0){
        
        
        for (Item *diarItm in _arrForClass){
            NSString *strType = [diarItm.type lowercaseString];
            if ((![strType isEqualToString:@"note"]) && (![strType isEqualToString:@"event"])) {
                 if(!([strType isEqualToString:@"outofclass"] || [strType isEqualToString:@"OutOfClass"] || [strType isEqualToString:@"Out Of Class"] || [strType isEqualToString:k_MERIT_CHECK]))
                [_arrForNewTask addObject:diarItm];
            }
            
        }
        
    }
    else
    {
        for (Item *diarItm in _arrForClass){
            NSString *strformName = [[Service sharedInstance] getStoredFormTemplate:_lableForFilter.text];
            // Aman GP
            NSString *strDairyItemTypeID= [[Service sharedInstance] getDiaryItemTypeId:_lableForFilter.text];
            
            if([diarItm.type isEqualToString:strformName] && [diarItm.itemType_id integerValue] == [strDairyItemTypeID integerValue] ){
                [_arrForNewTask addObject:diarItm];
            }
        }
    }
    [_tableForTask reloadData];
    _tableForStudent.hidden=YES;
    _buttonForRemind.hidden=YES;
    [_arrForCheckImg removeAllObjects];
    isSelectAll=NO;
    _buttonCancel.hidden=YES;
    _buttonDone.hidden=YES;
    
    
}




-(void)addNewItem:(id)sender
{
    UIButton* btn=(UIButton*)sender;
    
    if (btn.tag==1)
    {
        if (![AppHelper userDefaultsForKey:@"isMyClass"])
        {
            [AppHelper saveToUserDefaults:@"myclassView" withKey:@"isMyClass"];
        }
        [[AppDelegate getAppdelegate]._currentViewController selectTimeTable];
    }
    else
    {
        if (![AppHelper userDefaultsForKey:@"isMyClass"])
        {
            [AppHelper saveToUserDefaults:@"myclassView" withKey:@"isMyClass"];
        }
        
        MainViewController *mainVC=(MainViewController*)[AppDelegate getAppdelegate]._currentViewController;
        
        [mainVC showAddViewForMyClassMethod];
    }
}
-(void)selectAllButtonAction:(id)sender
{
    if (isSelectAll==NO)
    {
        [_arrForCheckImg removeAllObjects];
        isSelectAll=YES;
        for(int i=0;i<_arrForStudent.count;i++)
        {
            [_arrForCheckImg addObject:[_arrForStudent objectAtIndex:i] ];
        }
    }
    else
    {
        isSelectAll=NO;
        [_arrForCheckImg removeAllObjects];
    }
    
    [_tableForStudent reloadData];
}

- (IBAction)remindButtonAction:(id)sender {
    _buttonCancel.hidden=NO;
    _buttonDone.hidden=NO;
    _buttonForRemind.hidden=YES;
    
    // Code added to fix 2320 Minor issue === Start ====
    isSelectAll=NO;
    [_arrForCheckImg removeAllObjects];
    // ===== End ======
    [_tableForStudent reloadData];
    
}

- (IBAction)cancelButtonAction:(id)sender {
    isSelectAll=NO;
    _buttonCancel.hidden=YES;
    _buttonDone.hidden=YES;
    _buttonForRemind.hidden=NO;
    [_tableForStudent reloadData];
    [_arrForCheckImg removeAllObjects];
    
    
}

- (IBAction)filterButtonAction:(id)sender
{
    _itemArr = [[NSMutableArray alloc]initWithArray:[[Service sharedInstance]getStoredAllActiveDiaryItemTypesWithFormTemplate]];
    
    NSMutableArray *arrayOfType = [[NSMutableArray alloc] init];
    for(DiaryItemTypes *types in _itemArr)
    {
        if(!([types.formtemplate isEqualToString:@"Note"] || [types.formtemplate isEqualToString:@"Event"] || ([types.formtemplate isEqualToString:@"OutOfClass"] || [types.formtemplate isEqualToString:@"Out Of Class"]) || [types.formtemplate isEqualToString:k_MERIT_CHECK] || [types.formtemplate isEqualToString:@"merit"]))
        {
        [arrayOfType addObject:types.name];
        }
    }
    _itemArr = nil;
    _itemArr = [[NSMutableArray alloc] initWithArray:(NSArray*)arrayOfType];
    [_itemArr insertObject:@"All" atIndex:0];
    
    self._popoverControler=nil;
    UIViewController* popoverContent = [[UIViewController alloc]init];
    
    [_pickerView removeFromSuperview];
    popoverContent.view=nil;
    _pickerView.frame=CGRectMake(_pickerView.frame.origin.x, _pickerView.frame.origin.y, 150, 176);
    popoverContent.view = _pickerView;
    
    _picker.tag=1;
    
    UIPopoverController *pop =[[UIPopoverController alloc] initWithContentViewController:popoverContent];
    self._popoverControler=pop;
    [self._popoverControler setPopoverContentSize:_pickerView.frame.size animated:YES];
    self._popoverControler.delegate = self;
    [self._popoverControler presentPopoverFromRect:_lableForFilter.frame  inView:self.view
                          permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    [pop release];
    [popoverContent release];
    
}

- (IBAction)doneButtonAction:(id)sender {
    //5 Aug 2013
    
    if (_arrForCheckImg.count>0) {
        _buttonCancel.hidden=YES;
        _buttonDone.hidden=YES;
        _buttonForRemind.hidden=NO;
        _textViewForReminder.text=@"";
        [_tableForStudent reloadData];
        
        [[AppDelegate getAppdelegate]._currentViewController.view addSubview:_viewForReminderText];
        [self initialDelayEnded:_viewForReminderText];
    }
    else{
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please select students" delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
}

- (IBAction)reminderCancelButtonAction:(id)sender {
    
    [self initialDelayEnded1:_viewForReminderText];
    [_viewForReminderText removeFromSuperview];
    [_textViewForReminder resignFirstResponder];
    
}

- (IBAction)reminderSendButtonAction:(id)sender {
    
    [self initialDelayEnded1:_viewForReminderText];
    [_viewForReminderText removeFromSuperview];
    [_textViewForReminder resignFirstResponder];
    [self sendReminderMethod];
}



#pragma mark
#pragma mark TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;
{
    return YES;
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
#pragma mark
#pragma mark TextView Delegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
     if (textView==_textViewForMeritComment) {
         if(_isEdit)
         {
             [_textViewForMeritComment setEditable:NO];
         }
         else
         {
        [UIView animateWithDuration:0.3
                         animations:^{
                             _viewForMerit.frame=CGRectMake(_viewForMerit.frame.origin.x, -250, _viewForMerit.frame.size.width, _viewForMerit.frame.size.height);
                             
                         }];
         }
    }
    
   
    return YES;

}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView==_textViewForMeritComment) {
        [UIView animateWithDuration:0.3
                         animations:^{
                             _viewForMerit.frame=CGRectMake(_viewForMerit.frame.origin.x, 0, _viewForMerit.frame.size.width, _viewForMerit.frame.size.height);
                             
                         }];
    }


}



#pragma mark
#pragma mark service calls

//for send remider to assign student
-(void)sendReminderMethod
{
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        
        if(!NSSTRING_HAS_DATA (_textViewForReminder.text))
        {
            [AppHelper showAlertViewWithTag:584 title:APP_NAME message:@"Please enter a reminder message" delegate:self cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        else
        {
            if (_arrForCheckImg.count>0)
            {
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendReminderMethodDidGot:) name:NOTIFICATION_sendReminderToStudent object:nil];
                
                [self.view addSubview:_indicatorView];
                [self.view bringSubviewToFront:_indicatorView];
                
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                
                DairyItemUsers *assinStuObj =[_arrForCheckImg objectAtIndex:0];
                [dict setObject:[NSString stringWithFormat:@"%@",assinStuObj.item_id] forKey:@"DairyitemId"];
                
                
                NSMutableArray *arrForAssinStu=[[NSMutableArray alloc]init];
                
                for (DairyItemUsers *assinStuObj in _arrForCheckImg) {
                    
                    [arrForAssinStu addObject:assinStuObj.student_id];
                }
                
                NSString *assingedUserIdStr=[arrForAssinStu componentsJoinedByString:@","];
                
                [dict setObject:assingedUserIdStr forKey:@"ReceiverIds"];
                
                NSString *str=[NSString stringWithFormat:@"<html> <body> %@<br><br><br>Type: %@ <br>Subject: %@ <br> Assigned: %@ <br> Due: %@ </body> </html>",_textViewForReminder.text,_lblForItemType.text,_lblForSubName.text,_lblForAssignTime.text,_lblForDueTime.text];
                
                [dict setObject:str forKey:@"Description"];
                [arrForAssinStu release];
                
                [[Service sharedInstance]sendRemiderToAssinStudent:dict];
                [dict release];
            }
        }
    }
    else
    {
        //21AugChange
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Sending Reminder requires a connection to the internet." delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
}

-(void)sendReminderMethodDidGot:(NSNotification*)note
{
    [_indicatorView removeFromSuperview];
    
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_sendReminderToStudent object:nil];
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Error"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
            
            
            for (NSDictionary *dictForAssignStu in [[note userInfo] valueForKey:@"assignUserId"]) {
                
                [[Service sharedInstance] parseAssignedStudentDataWithDictionary:dictForAssignStu];
            }
            
            [_arrForStudent removeAllObjects];
            if (_arrForCheckImg.count>0)
            {
                DairyItemUsers *assinStuObj =[_arrForCheckImg objectAtIndex:0];
                
                for (DairyItemUsers *userObj in [[Service sharedInstance] getStoredAllAssignedItemDataWithItemId:assinStuObj.item_id]) {
                    if (userObj) {
                        if(![_arrForStudent containsObject:userObj])
                            [_arrForStudent addObject:userObj];
                    }
                }
                
            }
            
            [_tableForStudent reloadData];
            
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Error"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
}




-(void)getSchoolUserClassServiceHit{
    //for hit service in background
    NSInvocationOperation *operation = nil;
    operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(getSchoolUserClassMethod) object:nil];
    NSOperationQueue *operationQueue = [AppDelegate getAppdelegate]._operationQueue;
    [operationQueue addOperation:operation];
    [operation release];
    
}


-(void)getSchoolUserClassMethod
{
    //service for get class user
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        [self.view addSubview:_indicatorView];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSchoolUserClassDidGot:) name:NOTIFICATION_Get_SchoolUser_Class object:nil];
        
        [[Service sharedInstance]getSchoolUserClassDetail];
        
    }
    else{
        //[AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    
}

-(void)getSchoolUserClassDidGot:(NSNotification*)note
{
    [_indicatorView removeFromSuperview];
    
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_Get_SchoolUser_Class object:nil];
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            ///////////
            NSArray *arrForUserClass=[[note userInfo] valueForKey:@"user_class"];
            for (NSDictionary *dict  in arrForUserClass)
            {
                [AppHelper saveToUserDefaults:[dict objectForKey:@"ClassId"] withKey:@"class_id"];
                for (NSDictionary *dict1 in [dict objectForKey:@"users"])
                {
                    [[Service sharedInstance]parseStudentDataWithDictionary:dict1];
                }
            }
            ///////////
            
            MyProfile *profileObj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
            [_arrForNewClass removeAllObjects];
            for(MyClass *pareniObj in profileObj.myClasses){
                [_arrForNewClass addObject:pareniObj];
                if([_arrForClass containsObject:pareniObj]){
                    [_arrForClass removeObject:pareniObj];
                }
            }
            [_picker reloadAllComponents];
            [_tableForClass reloadData];
            
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
}

//31 july
#pragma mark

#pragma mark
#pragma mark - alertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [_viewForReminderText removeFromSuperview];
    if (alertView.tag==584) {
        if (buttonIndex==0) {
            if (_arrForCheckImg.count>0) {
                [[AppDelegate getAppdelegate]._currentViewController.view addSubview:_viewForReminderText];
                [self initialDelayEnded:_viewForReminderText];
            }
        }
    }
}
#pragma mark
#pragma mark - pop up a view animated lifecycle
-(void)initialDelayEnded:(UIView*)popView
{
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.4];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped:)];
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
    [UIView commitAnimations];
}

- (void)bounce1AnimationStopped:(UIView*)popView {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped:)];
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
    [UIView commitAnimations];
}

- (void)bounce2AnimationStopped:(UIView*)popView {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    popView.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
    
}

-(void)initialDelayEnded1:(UIView*)popView
{
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.4];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce11AnimationStopped:)];
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView commitAnimations];
}

- (void)bounce11AnimationStopped:(UIView*)popView {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce12AnimationStopped:)];
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0, 1.0);
    [UIView commitAnimations];
}

- (void)bounce12AnimationStopped:(UIView*)popView {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    popView.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
}




- (IBAction)deleteTaskBasedMeritAction:(id)sender {
    
    [DELEGATE set_isClassBasedOrTaskBasedMeritDelete:YES];
    MainViewController *mainVC=(MainViewController*)[AppDelegate getAppdelegate]._currentViewController;

    [mainVC deleteTaskbasedMerit:selectedItem deletedStudentMerit:selectedDIUser];

    
}
@end

//
//  CustomCellFolderFilesList.m
//  DiaryPlannerGoogleDrive
//
//  Created by Aman Gupta on 9/16/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import "CustomCellFolderFilesList.h"

@implementation CustomCellFolderFilesList

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

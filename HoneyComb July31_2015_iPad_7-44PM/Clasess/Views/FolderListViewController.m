//
//  ViewController.m
//  DiaryPlannerGoogleDrive
//
//  Created by Apple on 15/09/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import "FolderListViewController.h"
#import "GTLDrive.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "FolderFilesListViewController.h"
#import "Utilities.h"
#import "CustomCellFolderList.h"
#import "CustomCellFolderFilesList.h"
#import "Attachment.h"
#import "FileUtils.h"
#import "Defines.h"

//#import "DrEditFileEditViewController.h"
//#import "DrEditUtilities.h"

// Condition is for App is not Daisy

// Constants used for OAuth 2.0 authorization.// Prime-HoneyComb
//static NSString *const kKeychainItemName = @"iOSDriveSample: Google Drive";
//static NSString *const kClientId = (isAppDaisy) ? @"314344546725-vvm4gnprqctb57866ark18nbur95cic0.apps.googleusercontent.com" : @"881830095836.apps.googleusercontent.com";
//
//static NSString *const kClientSecret = (isAppDaisy) ? (@"1o-SkH2z68H2QCVRivQfoNyG") : (@"MGFQDm2INj9jtmIwikpzMRcb");

// For Daisy
// Constants used for OAuth 2.0 authorization.// Daisy
//static NSString *const kKeychainItemName = @"iOSDriveSample: Google Drive";
//static NSString *const kClientId = @"314344546725-vvm4gnprqctb57866ark18nbur95cic0.apps.googleusercontent.com";
//static NSString *const kClientSecret = @"1o-SkH2z68H2QCVRivQfoNyG";

//For HoneyComb
static NSString *const kKeychainItemName = @"iOSDriveSample: Google Drive";
static NSString *const kClientId = @"881830095836.apps.googleusercontent.com";
static NSString *const kClientSecret = @"MGFQDm2INj9jtmIwikpzMRcb";

// Constants used for OAuth 2.0 authorization.// Prime
//static NSString *const kKeychainItemName = @"iOSDriveSample: Google Drive";
//static NSString *const kClientId = @"281910597855-2qsjutj0d9uj7nj853rpoqatuia1bc0a.apps.googleusercontent.com";
//static NSString *const kClientSecret = @"Q7qHx1YRFOdTaVsA3awsJMv4";



@interface FolderListViewController ()
{
    NSString *strFolderName;
    NSMutableString *strPathDB;
    NSMutableArray *marrOfFolderFiles;
    NSMutableArray *marrGDListOfSelectedDirectory;
    
}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *authButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *refreshButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (weak, readonly) GTLServiceDrive *driveService;
@property (retain) NSMutableArray *driveFiles;
@property BOOL isAuthorized;

- (IBAction)authButtonClicked:(id)sender;
- (IBAction)refreshButtonClicked:(id)sender;

- (void)toggleActionButtons:(BOOL)enabled;
- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error;
- (void)isAuthorizedWithAuthentication:(GTMOAuth2Authentication *)auth;
- (void)loadDriveFiles;

@end

@implementation FolderListViewController

@synthesize addButton = _addButton;
@synthesize authButton = _authButton;
@synthesize refreshButton = _refreshButton;
@synthesize doneButton = _doneButton;
@synthesize driveFiles = _driveFiles;
@synthesize isAuthorized = _isAuthorized;

#pragma mark -
#pragma mark View Life Cycle Methods  -

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Check for authorization.
    GTMOAuth2Authentication *auth =
    [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                          clientID:kClientId
                                                      clientSecret:kClientSecret];
    //----- Added By Mitul on 15-Dec-2013 -----//
    strPathDB = [[NSMutableString alloc] init];
    //--------- By Mitul ---------//

    if ([auth canAuthorize]) {
        [self isAuthorizedWithAuthentication:auth];
    }
}

- (void)viewDidUnload
{
    [self setAddButton:nil];
    [self setRefreshButton:nil];
    [self setAuthButton:nil];
    marrOfFolderFiles = nil;
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    NSArray *arrayOfKeys = [[DELEGATE glb_mdict_for_Attachement]allKeys];
    NSMutableArray *marrAttachedDoc = [[NSMutableArray alloc] init];
    for(NSString *str in arrayOfKeys)
    {
        for(Attachment *attachment in [[DELEGATE glb_mdict_for_Attachement] valueForKey:str])
        {
            [marrAttachedDoc addObject:attachment];
        }
    }
    
    for (Attachment *a in [DELEGATE marrListedAttachments]) {
        [marrAttachedDoc addObject:a ];
    }
    
    if ([marrAttachedDoc count]>0)
    {
        // Attachment found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) %lu",(unsigned long)[marrAttachedDoc count]]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        
        if ([marrAttachedDoc count] > 9)
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,2)];
        }
        else
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        }
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];
        
        //[[DELEGATE glb_Button_Attachment] setTitle:[NSString stringWithFormat:@"Attachment(s) %d",[marrAttachedDoc count]] forState:UIControlStateNormal];
    }
    else
    {
        // Attachment Not found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) 0"]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];

        //[[DELEGATE glb_Button_Attachment] setTitle:[NSString stringWithFormat:@"Attachment(s) 0"] forState:UIControlStateNormal];
        
    }

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // Sort Drive Files by modified date (descending order).
    
    // Disable Back button
    [self.btnBack setEnabled:NO];

    [self.driveFiles sortUsingComparator:^NSComparisonResult(GTLDriveFile *lhs,
                                                             GTLDriveFile *rhs) {
        return [rhs.modifiedDate.date compare:lhs.modifiedDate.date];
    }];
    
    marrGDListOfSelectedDirectory = nil;
    marrGDListOfSelectedDirectory = [[NSMutableArray alloc] init];
    
    if (self.isAuthorized)
    {
        [strPathDB setString:@""];
    }
    
    [self.tbl_View reloadData];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)dealloc
{
    [strPathDB release];
    [_btnBack release];
    if (marrGDListOfSelectedDirectory)
    {
        [marrGDListOfSelectedDirectory release];
    }
    
    [super dealloc];
}

#pragma mark -
#pragma mark File Attached or Not  Method -

- (BOOL)isFileAttachedOrNot:(GTLDriveFile *)inGDFile
{
    NSArray *arrayOfKeys = [[DELEGATE glb_mdict_for_Attachement] allKeys];
    NSArray *arrString  = [strPathDB componentsSeparatedByString: @"/"];
    NSString *strKeyDirName;
    if ([[arrString lastObject] isEqualToString:@""])
    {
        strKeyDirName = [NSString stringWithFormat:@"GoogleDrive"];
    }
    else
    {
        strKeyDirName = [NSString stringWithFormat:@"%@",[arrString lastObject]];
    }
    
    NSString *strFilePath = [NSString stringWithFormat:@"%@/%@",strKeyDirName,inGDFile.title];
    //NSString *strFilePath = [NSString stringWithFormat:@"%@",inGDFile.title];
    
    for(NSString *str in arrayOfKeys)
    {
        int i=0;
        for(Attachment *attachment in [[DELEGATE glb_mdict_for_Attachement] valueForKey:str])
        {
            // gdoc Implementation
            // Logic to check gDoc is attached or not
            if ([strKeyDirName isEqualToString:@"GoogleDrive"] && !inGDFile.downloadUrl)
            {
                if ([attachment.localFilePath rangeOfString:inGDFile.alternateLink].location == NSNotFound)
                {
                }
                else
                {
                    return YES;
                }
            }
            else
            {
                if ([attachment.localFilePath rangeOfString:strFilePath].location == NSNotFound)
                {
                }
                else
                {
                    return YES;
                }
            }
            i++;
        }
    }
    return NO;
}

//--------- By Mitul ---------//

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.driveFiles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CustomCellFolderFilesList *cell = (CustomCellFolderFilesList *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *cellArray = [[NSBundle mainBundle] loadNibNamed:@"CustomCellFolderFilesList" owner:self options:nil];
        cell = [cellArray lastObject];
    }

    cell.btn_Attachment.tag = indexPath.row;
    [cell.btn_Attachment addTarget:self action:@selector(clickAttachment:) forControlEvents:UIControlEventTouchUpInside];

    GTLDriveFile *file = [self.driveFiles objectAtIndex:indexPath.row];
    
    // gdoc Implementation
    if([file.mimeType isEqualToString:@"application/vnd.google-apps.folder"])
    {
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        [cell.btn_Attachment setHidden:YES];
    }
    // For Folder
    else
    {
        if([self isFileAttachedOrNot:file])
        {
            [cell.btn_Attachment setTitle:@"Attached" forState:UIControlStateNormal];
            //----- Added By Mitul on 16-Dec-2013 -----//
            [cell.btn_Attachment setEnabled:NO];
            //--------- By Mitul ---------//
        }
        else
        {
            cell.btn_Attachment.titleLabel.text = @"Attach";
        }
        [cell setAccessoryType:UITableViewCellAccessoryNone];
    }
    cell.lbl_Heading.text = file.title;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    GTLDriveFile *file = [self.driveFiles objectAtIndex:indexPath.row];
    
    // Is Folder
    if(!file.downloadUrl)
    {
        //----- Added By Mitul on 18-Dec Network Check -----//
        BOOL networkReachable = [[DELEGATE netManager]networkReachable];
        if (networkReachable)
        {
            // Back button Enabled
            [self.btnBack setEnabled:YES];

            strPathDB = [[strPathDB stringByAppendingString:[NSString stringWithFormat:@"/%@",file.title]] mutableCopy];
            //strFolderName = file.title;
            [marrGDListOfSelectedDirectory addObject:file];
            
            [self loadFileContent:file];
        }
        else
        {
            [DELEGATE hideIndicator];
            
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        //--------- By Mitul ---------//
    }
    // is File
    else
    {
        //[self downloadFile:file];
        //strFolderName = @"root";
        //[self loadDriveFiles];

        // Do Nothing
    }
}


/*- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    DrEditFileEditViewController *viewController = [segue destinationViewController];
    NSString *segueIdentifier = segue.identifier;
    viewController.driveService = [self driveService];
    viewController.delegate = self;
    
    if ([segueIdentifier isEqualToString:@"editFile"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        GTLDriveFile *file = [self.driveFiles objectAtIndex:indexPath.row];
        viewController.driveFile = file;
        viewController.fileIndex = indexPath.row;
    } else if ([segueIdentifier isEqualToString:@"addFile"]) {
        viewController.driveFile = [GTLDriveFile object];
        viewController.fileIndex = -1;
    }
}*/

/*- (NSInteger)didUpdateFileWithIndex:(NSInteger)index
                          driveFile:(GTLDriveFile *)driveFile {
    if (index == -1) {
        if (driveFile != nil) {
            // New file inserted.
            [self.driveFiles insertObject:driveFile atIndex:0];
            index = 0;
        }
    } else {
        if (driveFile != nil) {
            // File has been updated.
            [self.driveFiles replaceObjectAtIndex:index withObject:driveFile];
        } else {
            // File has been deleted.
            [self.driveFiles removeObjectAtIndex:index];
            index = -1;
        }
    }
    return index;
}
*/

#pragma mark -
#pragma mark Google Drive Methods  -

- (GTLServiceDrive *)driveService {
    static GTLServiceDrive *service = nil;
    
    if (!service) {
        service = [[GTLServiceDrive alloc] init];
        
        // Have the service object set tickets to fetch consecutive pages
        // of the feed so we do not need to manually fetch them.
        service.shouldFetchNextPages = YES;
        
        // Have the service object set tickets to retry temporary error conditions
        // automatically.
        service.retryEnabled = YES;
    }
    return service;
}

- (void)toggleActionButtons:(BOOL)enabled {
    self.addButton.enabled = enabled;
    self.refreshButton.enabled = enabled;
    self.doneButton.enabled = enabled;
}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
    if (error == nil) {
        [self isAuthorizedWithAuthentication:auth];
    }
}

- (void)isAuthorizedWithAuthentication:(GTMOAuth2Authentication *)auth {
    [[self driveService] setAuthorizer:auth];
    self.authButton.title = @"Sign out";
    self.isAuthorized = YES;
    [self toggleActionButtons:YES];
    //----- Added By Mitul on 18-Dec Network Check -----//
    BOOL networkReachable = [[DELEGATE netManager]networkReachable];
    if (networkReachable)
    {
        [self loadDriveFiles];
    }
    else
    {
        [DELEGATE hideIndicator];
        
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    //--------- By Mitul ---------//
}

- (NSString *)getLocalGoogleDriveFilePath:(NSString *)strFileName fileIndex:(NSInteger)inFileIndex
{
    //----- Added By Mitul on 17-Dec-2013 For Application DirectoryName -----//
    NSString *strGoogleDrivePath = [FileUtils createNewDirectoryAtGivenDocumentDirectoryPath:@"GoogleDrive"];
    
    NSString *dataPath = [strGoogleDrivePath stringByAppendingPathComponent:strPathDB];
    
    NSError *error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:YES attributes:nil error:&error]; //Create folder
    
    NSString *filePath = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",strFileName]];
    
    NSArray *arrFolderName = [strPathDB componentsSeparatedByString:@"/"];
    NSString *strCurrentFolder = [arrFolderName lastObject];

    if ([strCurrentFolder isEqualToString:@""])
    {
        strCurrentFolder = @"GoogleDrive";
    }
    arrFolderName = nil;
    arrFolderName = [strFileName componentsSeparatedByString:@"."];
    
    marrOfFolderFiles = nil;
    marrOfFolderFiles = [[NSMutableArray alloc] init];
    
    Attachment *attachment = [[Attachment alloc]init];
    attachment.driveName = @"GoogleDrive" ;
    attachment.displayName = strFileName ;
    attachment.localFilePath = filePath ;
    attachment.indexPath = [NSString stringWithFormat:@"%ld",(long)inFileIndex];
    attachment.localFolderName = strCurrentFolder ;
    attachment.fileType = [arrFolderName lastObject] ;
    [marrOfFolderFiles addObject:attachment];
    attachment = nil ;
    
    //--------- By Mitul ---------//
    
    return filePath;
}

// gdoc Implementation
- (void)attachDocumentsCreatedOnGDrive:(GTLDriveFile *)file index:(NSInteger)inIndex
{
    // Add Loading alert
    UIAlertView *alert = [Utilities showLoadingMessageWithTitle:@"Loading file content"
                                                       delegate:self];
    // Set Attachment Model for Google Documents
    marrOfFolderFiles = nil;
    marrOfFolderFiles = [[NSMutableArray alloc] init];
    
    Attachment *attachment = [[Attachment alloc]init];
    attachment.driveName = @"GoogleDrive" ;
    attachment.displayName = file.title;
    attachment.localFilePath = file.alternateLink;
    attachment.indexPath = [NSString stringWithFormat:@"%ld",(long)inIndex];
    attachment.localFolderName = @"GoogleDrive" ;
    attachment.fileType = @"gdoc";//[arrFolderName lastObject];
    [marrOfFolderFiles addObject:attachment];
    attachment = nil ;
    
    [self addAttachmentInGlobalArrayForKey:@"GoogleDrive"];
    
    // Dismiss Loading alert
    [alert dismissWithClickedButtonIndex:0 animated:YES];
    
}

// -----------------------------------------------------------------------------
// New Method added to generalize functionality for GDoc attachments
// gdoc Implementation

- (void)addAttachmentInGlobalArrayForKey:(NSString *)inStrKeyDirName
{
    NSMutableArray *aar =nil;
    
    aar = [[NSMutableArray alloc] init];
    
    NSArray *arrObjKeys = [[DELEGATE glb_mdict_for_Attachement] allKeys];
    
    BOOL flag = FALSE;
    
    flag = [arrObjKeys containsObject:inStrKeyDirName];
    
    if (flag)
    {
        aar =[[DELEGATE glb_mdict_for_Attachement] valueForKey:inStrKeyDirName];
        
        if([aar count]==0)
        {
            aar = nil;
            aar = [[NSMutableArray alloc] init];
        }
        [aar addObject:[marrOfFolderFiles objectAtIndex:0]];
        [[DELEGATE glb_mdict_for_Attachement] setObject:aar forKey:inStrKeyDirName];
    }
    
    if(!flag)
    {
        aar =[[DELEGATE glb_mdict_for_Attachement] valueForKey:inStrKeyDirName];
        
        if([aar count]==0)
        {
            aar = nil;
            aar = [[NSMutableArray alloc] init];
        }
        
        [aar addObject:[marrOfFolderFiles objectAtIndex:0]];
        
        [[DELEGATE glb_mdict_for_Attachement] setObject:aar forKey:inStrKeyDirName];
    }
}

- (void)downloadFile:(GTLDriveFile *)file index:(NSInteger)inIndex
{
    
    UIAlertView *alert = [Utilities showLoadingMessageWithTitle:@"Loading file content"
                                                       delegate:self];

    // Use a GTMHTTPFetcher object to download the file with authorization.
    NSURL *url = [NSURL URLWithString:file.downloadUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    GTMHTTPFetcher *fetcher = [GTMHTTPFetcher fetcherWithRequest:request];
    
    // Requests of user data from Google services must be authorized.
    fetcher.authorizer = self.driveService.authorizer;
    
    NSString *finalFilePath = [self getLocalGoogleDriveFilePath:file.title fileIndex:inIndex];
    
    // The fetcher can save data directly to a file.
    fetcher.downloadPath = finalFilePath;
    
    
    [fetcher beginFetchWithCompletionHandler:^(NSData *data, NSError *error) {
        
        // Callback
        if (error == nil) {
            
            //----- Added By Mitul on 15-Dec-2013 -----//
            NSArray *arrString  = [strPathDB componentsSeparatedByString: @"/"];
            
            NSString *strKeyDirName;
            
            if ([[arrString lastObject] isEqualToString:@""])
            {
                strKeyDirName = @"GoogleDrive";
            }
            else
            {
                strKeyDirName = [arrString lastObject];
            }
            
            [self addAttachmentInGlobalArrayForKey:strKeyDirName];

            [alert dismissWithClickedButtonIndex:0 animated:YES];
            //--------- By Mitul ---------//
        }
        else
        {
            [alert dismissWithClickedButtonIndex:0 animated:YES];
            
#if DEBUG
            NSLog(@"An error occurred: %@", error);
#endif
      
            [Utilities showErrorMessageWithTitle:@"Unable to load file"
                                         message:[error description]
                                        delegate:self];
        }
    }];
}

- (void)loadFileContent:(GTLDriveFile *)inDriveFile
{
    UIAlertView *alert = [Utilities showLoadingMessageWithTitle:@"Loading file content"
                                                       delegate:self];
    
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesList];
    query.q = [NSString stringWithFormat:@"'%@' IN parents", inDriveFile.identifier];
    // root is for root folder replace it with folder identifier in case to fetch any specific folder
    [self.driveService executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
                                                              GTLDriveFileList *files,
                                                              NSError *error) {
        if (error == nil)
        {
            if (self.driveFiles == nil)
            {
                self.driveFiles = [[NSMutableArray alloc] init];
            }
            [self.driveFiles removeAllObjects];
            [self.driveFiles addObjectsFromArray:files.items];
            
            [alert dismissWithClickedButtonIndex:0 animated:YES];
            
            [self.tbl_View reloadData];
        }
        else
        {
#if DEBUG
            NSLog(@"An error occurred: %@", error);
#endif
      
            [Utilities showErrorMessageWithTitle:@"Unable to load file"
                                         message:[error description]
                                        delegate:self];
        }
    }];
}

- (void)loadDriveFiles
{
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesList];
    // query.q = @"mimeType = 'text/plain'";
    // query.q = @"mimeType='application/vnd.google-apps.folder' and trashed=false";
    query.q = [NSString stringWithFormat:@"'%@' IN parents", @"root"];
    
    UIAlertView *alert = [Utilities showLoadingMessageWithTitle:@"Loading files"
                                                       delegate:self];
    [self.driveService executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
                                                              GTLDriveFileList *files,
                                                              NSError *error)
    {
        [alert dismissWithClickedButtonIndex:0 animated:YES];
        if (error == nil)
        {
            if (self.driveFiles == nil)
            {
                self.driveFiles = [[NSMutableArray alloc] init];
            }
            [self.driveFiles removeAllObjects];
            [self.driveFiles addObjectsFromArray:files.items];
            
            [self.tbl_View reloadData];
            [self.btnBack setEnabled:NO];
        }
        else
        {
#if DEBUG
            NSLog(@"An error occurred: %@", error);
#endif
      
            [Utilities showErrorMessageWithTitle:@"Unable to load files"
                                         message:[error description]
                                        delegate:self];
        }
    }];
}

#pragma mark -
#pragma mark IBAction Methods  -


- (IBAction)btn_back_pressed:(id)sender {
    
    //----- Added By Mitul on 18-Dec Network Check -----//
    
    BOOL networkReachable = [[DELEGATE netManager]networkReachable];
    if (networkReachable)
    {
        NSMutableArray *marr = [[NSMutableArray alloc] initWithArray:[strPathDB componentsSeparatedByString:@"/"]];
        
        [marr removeObjectAtIndex:0];
        [marr removeLastObject];
        
        
        [strPathDB setString:@""];
        for (NSString *str in marr)
        {
            strPathDB = [[strPathDB stringByAppendingString:@"/"] mutableCopy];
            strPathDB = [[strPathDB stringByAppendingString:str] mutableCopy];
        }
        if ([strPathDB length] < 1)
        {
            strPathDB = [[strPathDB stringByAppendingString:@"/"] mutableCopy];
        }
        
        [marrGDListOfSelectedDirectory removeLastObject];
        
        // load files from
        if ( [marrGDListOfSelectedDirectory count] > 0 )
        {
            [self.btnBack setEnabled:YES];
            [self loadFileContent:[marrGDListOfSelectedDirectory lastObject]];
        }
        else
        {
            [self loadDriveFiles];
        }
    }
    else
    {
        [DELEGATE hideIndicator];
        
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    //--------- By Mitul ---------//
}

- (IBAction)authButtonClicked:(id)sender {
    if (!self.isAuthorized) {
        // Sign in.
        SEL finishedSelector = @selector(viewController:finishedWithAuth:error:);
        GTMOAuth2ViewControllerTouch *authViewController =
        [[GTMOAuth2ViewControllerTouch alloc] initWithScope:kGTLAuthScopeDrive
                                                   clientID:kClientId
                                               clientSecret:kClientSecret
                                           keychainItemName:kKeychainItemName
                                                   delegate:self
                                           finishedSelector:finishedSelector];
        

        [self presentViewController:authViewController animated:YES completion:^{

        
        }];
        
    } else {
        // Sign out
        [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKeychainItemName];
        [[self driveService] setAuthorizer:nil];
        self.authButton.title = @"Sign in";
        self.isAuthorized = NO;
        [self toggleActionButtons:NO];
        [self.driveFiles removeAllObjects];
        [self.tbl_View reloadData];
        [[DELEGATE glb_mdict_for_Attachement]removeAllObjects];
        [self dismissViewControllerAnimated:YES completion:^{
        
        }];
    }
}

- (IBAction)refreshButtonClicked:(id)sender
{
    //----- Added By Mitul on 18-Dec Network Check -----//
    
    BOOL networkReachable = [[DELEGATE netManager]networkReachable];
    if (networkReachable)
    {
        [self loadDriveFiles];
    }
    else
    {
        [DELEGATE hideIndicator];
        
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    //--------- By Mitul ---------//
}

- (void)clickAttachment:(UIButton *)sender
{
    //----- Added By Mitul on 18-Dec Network Check -----//
    
    BOOL networkReachable = [[DELEGATE netManager]networkReachable];
    if (networkReachable)
    {
        GTLDriveFile *file = [self.driveFiles objectAtIndex:[sender tag]];
        
        
        if([sender.titleLabel.text isEqualToString:@"Attach"])
        {
            // gdoc Implementation
            //if ([file.mimeType isEqualToString:@""])
            if (!file.downloadUrl)
            {
                // Attach documents created on Google Drive
                [self attachDocumentsCreatedOnGDrive:file index:sender.tag];
            }
            else
            {
                [self downloadFile:file index:sender.tag];
            }
            [sender setTitle:@"Attached" forState:UIControlStateNormal];
            [sender setEnabled:NO];
        }
        else if([sender.titleLabel.text isEqualToString:@"Attached"])
        {
            //[self deleteAttachment:sender];
            
            [sender setTitle:@"Attach" forState:UIControlStateNormal];
        }
    }
    else
    {
        [DELEGATE hideIndicator];
        
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    //--------- By Mitul ---------//
}

#pragma mark -
#pragma mark Alert View Delegate Method  -

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex ==1)
    {
        [[DELEGATE glb_mdict_for_Attachement] removeAllObjects];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end

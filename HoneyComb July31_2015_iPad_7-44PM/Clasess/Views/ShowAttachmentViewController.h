//
//  ShowAttachmentViewController.h
//  StudyPlaner
//
//  Created by Mitul Trivedi on 12/10/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
// Amol

#import <UIKit/UIKit.h>
#import "Attachment.h"

@interface ShowAttachmentViewController : UIViewController

@property (retain, nonatomic) IBOutlet UIButton *btnCloseWebView;
@property (retain, nonatomic) IBOutlet UIWebView *vebViewShowAtachment;
- (IBAction)btnCloseClicked:(id)sender;
- (void)setDocumentDetail:(Attachment *)attachment;
- (void)openLinkOnWebView:(NSString*)urlwebLink;// Aman Created
- (void)openCalendarAccessOnWebView:(NSURL*)urlwebLink;// Aman Created

@end

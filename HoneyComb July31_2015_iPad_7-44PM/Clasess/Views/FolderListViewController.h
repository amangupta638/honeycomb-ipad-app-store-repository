//
//  ViewController.h
//  DiaryPlannerGoogleDrive
//
//  Created by Apple on 15/09/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FolderListViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tbl_View;
@property (retain, nonatomic) IBOutlet UIBarButtonItem *btnBack;

@end

//
//  CustomCellFolderList.h
//  DiaryPlannerGoogleDrive
//
//  Created by Apple on 15/09/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellFolderList : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_Heading;

@end

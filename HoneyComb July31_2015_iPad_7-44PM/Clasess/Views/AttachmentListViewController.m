//
//  AttachmentListViewController.m
//  StudyPlaner
//
//  Created by Mitul Trivedi on 12/9/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "CustomCellForAttachmentList.h"
#import "ShowAttachmentViewController.h"
#import "Attachment.h"
#import "AttachmentListViewController.h"


@interface AttachmentListViewController ()
{
    NSMutableArray *marrTemp;
}

@end

@implementation AttachmentListViewController

@synthesize tblViewShowAttachmentList;
@synthesize mdictSelectedAttachments;
@synthesize shouldItemLock ;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)setAttachmentListArray
{
    self.mdictSelectedAttachments = nil;
    self.mdictSelectedAttachments = [DELEGATE glb_mdict_for_Attachement];

    
    NSArray *arrayOfKeys = [self.mdictSelectedAttachments allKeys];
    
    marrTemp = nil;
    marrTemp = [[NSMutableArray alloc] init];
    
    if ([DELEGATE glb_isDiaryItemEditForAttachment] == NO) {
        for(NSString *str in arrayOfKeys)
        {
            for(Attachment *attachment in [[DELEGATE glb_mdict_for_Attachement] valueForKey:str])
            {
                if(![marrTemp containsObject:attachment])
                {
                [marrTemp addObject:attachment];
                }
            }
            
        }
    }else{
        for (Attachment *attachment in [DELEGATE marrListedAttachments]) {
            attachment.displayName = attachment.displayFileName ;
            if(![marrTemp containsObject:attachment])
            {
                [marrTemp addObject:attachment];

            }
        }
        if (arrayOfKeys.count != 0) {
            for(NSString *str in arrayOfKeys)
            {
                for(Attachment *attachment in [[DELEGATE glb_mdict_for_Attachement] valueForKey:str])
                {
                    if(![marrTemp containsObject:attachment])
                    {
                        [marrTemp addObject:attachment];

                    }
                }
                
            }
        }
    }
    
    
    
    
    
    if ( [marrTemp count] > 0)
    {
        // Attachment found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) %lu",(unsigned long)[marrTemp count]]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        
        if ([marrTemp count] > 9)
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,2)];
        }
        else
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        }
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];

    }
    else
    {
        // Attachment Not found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) 0"]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];
    }
    
    [tblViewShowAttachmentList reloadData];
}

- (void)dealloc
{
    if (self.mdictSelectedAttachments)
    {
        [self.mdictSelectedAttachments release];
    }
    [tblViewShowAttachmentList release];
    [marrTemp release];
    
    [super dealloc];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.view.backgroundColor = [UIColor yellowColor];
    // Do any additional setup after loading the view from its nib.
    [self setAttachmentListArray];
}

//----- Added by Mitul on 9 Dec -----//

- (void)viewWillDisappear:(BOOL)animated {
    
    NSArray *arrayOfKeys = [[DELEGATE glb_mdict_for_Attachement]allKeys];
    NSMutableArray *marrAttachedDoc = [[NSMutableArray alloc] init];
    for(NSString *str in arrayOfKeys)
    {
        for(Attachment *attachment in [[DELEGATE glb_mdict_for_Attachement] valueForKey:str])
        {
            if(![marrAttachedDoc containsObject:attachment])
            {
            [marrAttachedDoc addObject:attachment];
            }
        }
    }
    
    for (Attachment *a in [DELEGATE marrListedAttachments]) {
        if(![marrAttachedDoc containsObject:a])
        {
        
        [marrAttachedDoc addObject:a ];
        }
    }
    
    if ([marrAttachedDoc count]>0)
    {
        // Attachment found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) %lu",(unsigned long)[marrAttachedDoc count]]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        
        if ([marrAttachedDoc count] > 9)
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,2)];
        }
        else
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        }
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];
        
        //[[DELEGATE glb_Button_Attachment] setTitle:[NSString stringWithFormat:@"Attachment(s) %d",[marrAttachedDoc count]] forState:UIControlStateNormal];
    }
    else
    {
        // Attachment Not found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) 0"]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];
        
        //[[DELEGATE glb_Button_Attachment] setTitle:[NSString stringWithFormat:@"Attachment(s) 0"] forState:UIControlStateNormal];
    }
}
//----- by Mitul on 9 Dec -----//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UITable View Datasource Methods -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return 44;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [marrTemp count];
}
//-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 44;
//
//}

//----- Added by Mitul on 9 Dec 2013 -----//
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView=[[[UIView alloc] init] autorelease];
    headerView.frame=CGRectMake(0, 0,211, 30);
    headerView.backgroundColor=[UIColor colorWithRed:111.0/255.0 green:162.0/255.0 blue:224.0/255.0 alpha:1.0];
    
    UILabel* lblForItemType=[[UILabel alloc] init];
    lblForItemType.frame=CGRectMake(5, 2,150, 25);
    lblForItemType.font=[UIFont fontWithName:@"Arial-BoldMT" size:14];
    lblForItemType.backgroundColor=[UIColor clearColor];
    
    lblForItemType.textColor=[UIColor darkGrayColor];
    [headerView addSubview:lblForItemType];
    [lblForItemType release];
    
    lblForItemType.text=@"Attachment List";
    
    return headerView;
}
//----- by Mitul on 9 Dec 2013 -----//


-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//----- Added by Mitul on 9 Dec 2013 -----//
{
    
    static NSString *CellIdentifier = @"Cell";
    CustomCellForAttachmentList *cell = (CustomCellForAttachmentList *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *cellArray = [[NSBundle mainBundle] loadNibNamed:@"CustomCellForAttachmentList" owner:self options:nil];
        cell = [cellArray lastObject];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone ;
    Attachment *attachment = [marrTemp objectAtIndex:indexPath.row] ;
  
    if (attachment.displayName != (id)[NSNull null] || attachment.displayName.length != 0 ) {
        cell.lblAtachedFileName.text = attachment.displayName ;
    }else if (attachment.displayFileName != (id)[NSNull null] || attachment.displayFileName.length != 0 ) {
    
        cell.lblAtachedFileName.text = attachment.displayFileName ;
    }else{
        cell.lblAtachedFileName.text = @"TEST";
    }
 
    cell.btnToShowAttachment.tag = indexPath.row;
    cell.btnToDeleteAttachment.tag = indexPath.row;
    if (shouldItemLock) // lock image is hidden
    {
        cell.btnToDeleteAttachment.hidden = NO ;
    }else{
        cell.btnToDeleteAttachment.hidden = YES ;
    }
    [cell.btnToShowAttachment addTarget:self action:@selector(actionBtnViewAttachmentClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnToDeleteAttachment addTarget:self action:@selector(actionBtnDeleteAttachmentClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark -
#pragma mark IBAction Methods -

- (void)actionBtnViewAttachmentClicked:(UIButton *)button
{
    ShowAttachmentViewController *showAttachmentVC = [[ShowAttachmentViewController alloc] init];
    [self presentViewController:showAttachmentVC animated:YES completion:nil];
    Attachment *attachment = [marrTemp objectAtIndex:button.tag];
    
    [showAttachmentVC setDocumentDetail:attachment];
    
    [showAttachmentVC release];
}


- (void)actionBtnDeleteAttachmentClicked:(id)sender
{
    
    if ([DELEGATE glb_isDiaryItemEditForAttachment] == NO) {
        NSArray *arrayOfKeys = [self.mdictSelectedAttachments allKeys];
        
        for(NSString *str in arrayOfKeys)
        {
            int i=0;
            for(Attachment *attachment in [[DELEGATE glb_mdict_for_Attachement] valueForKey:str])
            {
                
                Attachment *atc = [marrTemp objectAtIndex:[sender tag]] ;
                if ( [attachment.localFilePath isEqualToString:atc.localFilePath])
                {
                    
                    Attachment *deletedAttachment = [[self.mdictSelectedAttachments valueForKey:str] objectAtIndex:i];
                    [[DELEGATE marrDeletedAttachments] addObject:deletedAttachment];
                    [[self.mdictSelectedAttachments valueForKey:str] removeObjectAtIndex:i];
                    
                    break;
                }
                i++;
            }
            
        }
        
        [DELEGATE setGlb_mdict_for_Attachement:self.mdictSelectedAttachments];
        
        [marrTemp removeObjectAtIndex:[sender tag]];
    }else{
   
        Attachment *attachment = [marrTemp objectAtIndex:[sender tag]] ;
        
        if ([[DELEGATE marrListedAttachments] containsObject:attachment]) {
            // If object contain in database array
            [[DELEGATE marrDeletedAttachments] addObject:attachment];
            [marrTemp removeObject:attachment];
            [[DELEGATE marrListedAttachments] removeObject:attachment];
            [[Service sharedInstance] updateIsDeletedFlagForAttachment:attachment];
        }else{
            // If selcted object is new
            NSArray *arrayOfKeys = [self.mdictSelectedAttachments allKeys];
            if (arrayOfKeys.count) {
                for(NSString *str in arrayOfKeys)
                {
                    int i=0;
                    for(Attachment *attachment in [[DELEGATE glb_mdict_for_Attachement] valueForKey:str])
                    {
                        Attachment *atc = [marrTemp objectAtIndex:[sender tag]] ;
                        if ( [attachment.localFilePath isEqualToString:atc.localFilePath])
                        {
                            
                            [[self.mdictSelectedAttachments valueForKey:str] removeObjectAtIndex:i];
                            
                            break;
                        }
                        i++;
                    }
                }
                
                [DELEGATE setGlb_mdict_for_Attachement:self.mdictSelectedAttachments];
                
                [marrTemp removeObjectAtIndex:[sender tag]];
        }
     }

    }
    if ([marrTemp count] > 0)
    {
        // Attachment found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) %lu",(unsigned long)[marrTemp count]]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        
        if ([marrTemp count] > 9)
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,2)];
        }
        else
        {
            [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        }
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];
    }
    else
    {
        // Attachment Not found
        NSMutableAttributedString *mutableString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Attachment(s) 0"]];
        
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0] range:NSMakeRange(0,13)];
        [mutableString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor]  range:NSMakeRange(14,1)];
        [[DELEGATE glb_Button_Attachment] setAttributedTitle:mutableString forState:UIControlStateNormal];
    }
    
    [self.tblViewShowAttachmentList reloadData];
}




@end

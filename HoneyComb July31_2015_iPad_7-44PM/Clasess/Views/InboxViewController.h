//
//  InboxViewController.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
// Chececek

#import <UIKit/UIKit.h>
#import "MainViewController.h"
#import "InboxVcListTableCell.h"

@interface InboxViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate,UISearchBarDelegate>
{

    IBOutlet UIView *_createNewMsgView;
    IBOutlet UIButton *_btnForNewMail;
    NSMutableArray *_arrForInbox;
    NSMutableArray *_arrForPicker;

    NSTimer* myTimer;
    NSInteger sct;
    NSInteger prevousSect;
    NSInteger privousForBgImgV;
    //new
    NSDateFormatter *formatter;
    IBOutlet UIView *_viewForPicker;
    IBOutlet UIPickerView *_picker;
    IBOutlet UITextField *_textfieldForReciver;
    IBOutlet UITextView *_textViewForDescription;
    IBOutlet UITextField *_textFieldSubject;
    IBOutlet UITableView *_tablViewForMsz;
    BOOL _isShowOther;
    IBOutlet UIView *_viewForLinKContent;
    IBOutlet UIWebView *_webViewForLinkContent;
    // New Inbox Functionality
    IBOutlet UIView *viewForTeacherLoginInbox;
    
    IBOutlet UITableView *tblListView;

    IBOutlet UISearchBar *listSearchBar ;
    
    NSInteger selectedIndexForFiltrationInbox;
    BOOL isStudentRadioButtonSelectedOrNot;
    IBOutlet UIImageView *imgArrowImage;
    BOOL isRowSelected;
}

/*********  Below mutable Array used for filter functionality Start ********/


@property(strong,nonatomic)NSMutableArray *arrForNews;
@property(strong,nonatomic)NSMutableArray *arrForAnnouncements;
@property(strong,nonatomic)NSMutableArray *arrForInboxMessagesOnly;
@property(strong,nonatomic)NSMutableArray *arrForAllMessages;
@property(strong,nonatomic)NSMutableArray *arrOfSelectedMessages;

/*********  Below mutable Array used for filter functionality End ********/


@property (retain, nonatomic) IBOutlet UIButton *ParentButton;
@property (retain, nonatomic) IBOutlet UILabel *ParentLabel;
@property (retain, nonatomic) IBOutlet UIButton *ClassButton;
@property (retain, nonatomic) IBOutlet UILabel *ClassLabel;
@property (retain, nonatomic) IBOutlet UILabel *AllLabel;
@property (retain, nonatomic) IBOutlet UIButton *btnAll;

@property (retain, nonatomic) IBOutlet UIButton *btnSortBydate;
@property (retain, nonatomic) IBOutlet UIButton *btnSend;
@property (retain, nonatomic) IBOutlet UIButton *btnDiscardChanges;
@property (retain, nonatomic) IBOutlet UIButton *btnSelectAll;
@property (retain, nonatomic) IBOutlet UILabel *lblSelectAll;
@property (retain, nonatomic) IBOutlet UIButton *btnSelectSpecificStudents;

@property(nonatomic,retain)NSArray *_arrformsz;
@property(nonatomic,retain) UIPopoverController *_popoverControler;
@property (retain, nonatomic) IBOutlet UITableView *_tableView;

- (IBAction)discardChangesTapped:(UIButton *)sender;

- (IBAction)sendButtonAction:(id)sender;
- (IBAction)writeNewMailButtonAction:(id)sender;
- (IBAction)savePickerButtonClick:(id)sender;
- (IBAction)cancelPickerButtonClick:(id)sender;
-(void)searchdNewsAnaAnnouncment:(id)serchObj;
-(void)sendNewMailToTeacher:(MyTeacher*)teacher;
- (IBAction)closeContentVButtonAction:(id)sender;
- (IBAction)selectSpecificStudentsTapped:(UIButton *)sender;
- (IBAction)inboxAllListTapped:(UIButton *)sender;
- (IBAction)btnSortByDateTapped:(UIButton *)sender;

// New Inbox Functionality
- (IBAction)selectallButtonTapped:(UIButton *)sender;

- (IBAction)inboxParentListTapped:(UIButton *)sender;
- (IBAction)inboxClassListTapped:(UIButton *)sender;
- (IBAction)inboxStudentListTapped:(UIButton *)sender;


- (IBAction)filterInboxImages:(UISegmentedControl *)sender;
@end

//
//  AddAttachmentViewController.h
//  StudyPlaner
//
//  Created by Aman Gupta on 11/26/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MainViewController.h"

@interface AddAttachmentViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
- (IBAction)selectGoogleDrive:(id)sender;
- (IBAction)selectDropBox:(id)sender;
- (IBAction)selectCamera:(id)sender;
- (IBAction)selectPhotoLibrary:(id)sender;
- (void)saveFileInGlobalArray:(NSMutableArray *)inMarrOfFolderFiles;

@end

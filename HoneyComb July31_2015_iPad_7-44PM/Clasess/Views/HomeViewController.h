//
//  HomeViewController.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.

// test commit

#import <UIKit/UIKit.h>
#import "OperationQueue.h"
#import "DownloadPictureOperation.h"
#import "InboxViewController.h"

#import "CountDownTimerUtility.h"
#import "ShowNotesViewController.h"

@interface HomeViewController : UIViewController<UIPopoverControllerDelegate,CountDownTimerProtocol>{
    OperationQueue *queueOperation;

    IBOutlet UIImageView *_userImgView;
    IBOutlet UILabel *_nameLbl;
    IBOutlet UITableView *_tableForProfilView;
    IBOutlet UIButton *_glanceBtn;
    IBOutlet UIButton *_newsAndAnnouncementBtn;
    IBOutlet UILabel *_newsAndAnnoCountLbl;
    IBOutlet UITableView *_tableForMainView;
    IBOutlet UILabel *_lblForOverdueAssign;
    IBOutlet UILabel *_lblForOverdueDesc;
    NSMutableArray *_arrForPrfileTable;
    NSMutableArray *_arr;
    NSMutableArray *_arrForDue;
    NSMutableArray *_arrForMainTable;
    IBOutlet UIView *_viewForDueItem;
  
    IBOutlet UIImageView *_imgViewForBanner;
    IBOutlet UIImageView *_ImgVForLeft;
    IBOutlet UIImageView *_imgVForIconBg;
    IBOutlet UIButton *_btnForViewAll;
    IBOutlet UILabel *_lblForViewAll;
    
    //Aman added -- EZMOBILE-25
    //for slider
    Item *objItemSliderSelected;
    
    /************* Out Of Class Start *****************/
    IBOutlet UIView *viewForApprovedPass;
    IBOutlet UIView *viewForApprovedPassInner;
    IBOutlet UILabel *lblPassType;
    IBOutlet UILabel *lblFromTime;
    IBOutlet UILabel *lblToTime;
    IBOutlet UILabel *lblPassAppovedBy;
    IBOutlet UILabel *lblApprovedPassTimer;
    IBOutlet UILabel *lblElapsedPassTimer;
    IBOutlet UILabel *lblPassDate;
    
    IBOutlet UILabel *lblPassApprovedByTitle;
        /************* Out Of Class End *****************/
    IBOutlet UIButton *btn_DeletePass;

    Item *selectedDiaryItem;
    
    ShowNotesViewController *viewpopup;
}



//@property(nonatomic,retain)NSArray *_arrForMainTable;
//@property(nonatomic,retain)NSDate* _curentDate;
@property(nonatomic,retain)NSDateFormatter* _curentDateFormeter;

@property(nonatomic,retain) UIPopoverController *_popover;
@property(nonatomic,retain) IBOutlet UITableView *_tableForMainView;
@property(nonatomic,retain) IBOutlet UITableView *_tableForProfilView;

// Property Created for Approved Pass
//@property(nonatomic,retain) IBOutlet UILabel *lblPassType;
//@property(nonatomic,retain) IBOutlet UILabel *lblFromTime;
//@property(nonatomic,retain) IBOutlet UILabel *lblToTime;
//@property(nonatomic,retain) IBOutlet UILabel *lblPassAppovedBy;
//@property(nonatomic,retain) IBOutlet UILabel *lblApprovedPassTimer;

@property(nonatomic,retain) UIPopoverController *_popoverControler;
- (IBAction)glanceButtonAction:(id)sender;
- (IBAction)newsAndAnnouncementBtnAction:(id)sender;
- (IBAction)crossBtuttonAction:(id)sender ;
- (IBAction)viewAllButtonAction:(id)sender;
- (IBAction)myProfileButtonAction:(id)sender;
-(void)showPopupButtonPressed:(id)sender;
- (void)addItemButtonAction;

//Aman added -- Glance Three Sections
@property (nonatomic,retain) IBOutlet UISegmentedControl *segmentedControl;
@property(nonatomic,retain) IBOutlet UILabel *lblDue;
@property(nonatomic,retain) IBOutlet UILabel *lblAssigned;
@property(nonatomic,retain) IBOutlet UISwitch *switctDueAssign;
@property (retain, nonatomic) IBOutlet UILabel *lblSortBy;


-(IBAction) segmentedControlIndexChanged;
-(IBAction) switchDueAssignToggled :(id)sender;
- (IBAction)deletePassAction:(id)sender;

// Campus Based Start
-(void)updateClock:(NSTimer *)timer;
// Campus Based End

//------------------------------------------------------------------------------
// Metod added to remove warnings

- (void)dismisPopover;

//------------------------------------------------------------------------------

@end

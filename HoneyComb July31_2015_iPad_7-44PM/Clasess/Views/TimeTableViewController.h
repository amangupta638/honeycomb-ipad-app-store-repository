//
//  TimeTableViewController.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "TimeTable.h"
#import "MyClass.h"
@interface TimeTableViewController : UIViewController<UIPopoverControllerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    //03/10/2013
    NSOperationQueue *queue;
    int whitespaceCount;
    IBOutlet UILabel *_semLbl;
    IBOutlet UILabel *_dateLbl;
    IBOutlet UITableView *_tableView;
    IBOutlet UIView *_timeTableInfoView;
    IBOutlet UITableView *_timeTableInfoTalbleView;
    IBOutlet UIButton *_timetableInfoBtn;
    IBOutlet UIView *_blackImgView;
    IBOutlet UILabel *_colorLbl;
    IBOutlet UITextField *_sujectTextField;
    IBOutlet UITextField *_roomNoTextField;
    IBOutlet UIView *_addItemBlackImgView;
    IBOutlet UIButton *_changeColorBtn;
    IBOutlet UIView *_chooseColorView;
    IBOutlet UILabel *_lblHeaderForAddNewView;
    //NSMutableArray *_arrForTimeTableData;
    NSInteger _cycle;
    NSInteger _noOfRow;
    NSInteger _noOfColom;
   // NSMutableArray *_arrForPeriod;
    IBOutlet UILabel *_ableFortime;
    BOOL _isEdit;
    IBOutlet UIButton *_deletePeroidData;
    NSDateFormatter *_globlDateFormat;
    NSInteger  _currentSem;
    NSInteger _selectedSem;
    NSTimer *timer;
NSMutableArray *_arrForSearchedSubject;

    IBOutlet UITableView *_tableViewForSearchedSub;
    //new

    IBOutlet UILabel *_lblForDay;

    IBOutlet UIImageView *_imagViewForSerchField;
    IBOutlet UILabel *_lblForSubjectName;
    IBOutlet UILabel *_lblForClassName;
    IBOutlet UILabel *_lblForPeriod;
    IBOutlet UIImageView *_imagViewForLcok;
    IBOutlet UIButton *_btnForAssoOnClassDetail;
    IBOutlet UIView *_viewForSearchAndAsociatesClass;
    IBOutlet UITextField *_textFieldForSearchClass;
    IBOutlet UIButton *_btnForSubmitOnClassDetail;
    IBOutlet UIView *_viewForClassDescription;
    IBOutlet UIButton *_saveBtn;
    
    
    NSMutableDictionary *dictFlurryParameter;
    
    // Campus Based Start
    
    NSInteger selectedTimeTableRow;

    // Campus Based End
    

}
//- (IBAction)addClassDetialButoonAction:(id)sender;
- (IBAction)closeClassDetailVButtonAction:(id)sender;
- (IBAction)associatesButtonAction:(id)sender;
- (IBAction)serchClassButtonAction:(id)sender;
@property(nonatomic,retain)  NSDictionary *dictForClassDetail;
@property(nonatomic,retain)MyClass *_cureentClass;
@property(nonatomic,retain) NSArray *_arrForSubject;
@property(nonatomic,retain) NSArray *_arrForTable;
@property(nonatomic,retain) TimeTable *_currentTimeTable;
@property(nonatomic,retain) UIPopoverController *_popoverControler;
@property (retain, nonatomic) IBOutlet UILabel *lblClassColor;

- (IBAction)deletePeroidData:(id)sender;
- (IBAction)timeTableInfoButtonAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;
- (IBAction)saveButtonAction:(id)sender;
- (IBAction)selectColorButtonAction:(id)sender;
- (IBAction)openColorPickerButtonAction:(id)sender;

@end

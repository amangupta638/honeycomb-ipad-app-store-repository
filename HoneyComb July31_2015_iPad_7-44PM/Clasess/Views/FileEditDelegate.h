//
//  FileEditDelegate.h
//  DiaryPlannerGoogleDrive
//
//  Created by Apple on 15/09/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTLDrive.h"

@protocol FileEditDelegate <NSObject>
- (NSInteger)didUpdateFileWithIndex:(NSInteger)index
                          driveFile:(GTLDriveFile *)driveFile;

@end

//
//  AboutViewController.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "School_Information.h"
@interface AboutViewController : UIViewController
{

    IBOutlet UIView *_blackImgView;
    IBOutlet UIView *_tablInfoView;
    IBOutlet UIButton *_informationBtn;
    IBOutlet UITableView *_tableView;
    NSInteger count;
    IBOutlet UIButton *_btnForPrev;
    IBOutlet UILabel *_labelForPrev;
    IBOutlet UIButton *_btnForNext;
    IBOutlet UILabel *_lableForNext;
    IBOutlet UIImageView *_tempImg;
    IBOutlet UIWebView *_webView;
}

@property (retain, nonatomic) IBOutlet UIButton *btn_youTube;
@property (retain, nonatomic) IBOutlet UIButton *btn_PDF;

@property(retain,nonatomic) CATextLayer *_testlayer;
@property (nonatomic,retain)NSArray *_arrForAboutData;
- (IBAction)informationButtonAction:(id)sender;
- (IBAction)previousButtonAction:(id)sender;
- (IBAction)nextButtonAction:(id)sender;
-(void)updateAboutDaisy;
-(void)searchAboutContent:(School_Information*)infoObj;
- (IBAction)youtubeLinkToOpen:(id)sender;
- (IBAction)pdfLinkToOpen:(id)sender;
@end

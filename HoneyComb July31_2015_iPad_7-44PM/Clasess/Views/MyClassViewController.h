//
//  MyClassViewController.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 26/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeTable.h"
//Merit - Start
#import "SelectionScrollView.h"
//Merit - End

@interface MyClassViewController : UIViewController<UIPopoverControllerDelegate,UITextFieldDelegate,UITextViewDelegate, SelectionScrollViewDelegate>{
    IBOutlet UITableView *_tableForClass;
    IBOutlet UILabel *_lableForFilter;
    IBOutlet UITableView *_tableForStudent;
    IBOutlet UITableView *_tableForTask;
    IBOutlet UIView *_pickerView;
    IBOutlet UIPickerView *_picker;
    IBOutlet UIButton *_buttonForRemind;
    IBOutlet UIButton *_buttonDone;
    IBOutlet UIButton *_buttonCancel;
    bool isSelectAll;
    NSInteger prevRowIndex;
 
    IBOutlet UIView *_viewForReminderText;

    IBOutlet UIView *_ViewForRemiderContent;
    IBOutlet UITextView *_textViewForReminder;
    IBOutlet UIView *_indicatorView;
    NSMutableArray *_arrForClass;
    NSMutableArray *_arrForNewClass;
    NSMutableArray *_arrForNewTask;
    NSMutableArray *_arrForStudent;
    NSMutableArray *_arrForCheckImg;
    NSInteger _selectedRow;
    IBOutlet UILabel *_lblForItemType;
    IBOutlet UILabel *_lblForDueTime;
    IBOutlet UILabel *_lblForSubName;
    IBOutlet UILabel *_lblForAssignTime;
    IBOutlet UIView *_viewForSearchAndAsociatesClass;
    
    NSIndexPath *indexPathForTask;
    BOOL _isEdit;

      
   
}
@property(nonatomic,retain)  NSDictionary *dictForClassDetail;

@property(nonatomic,retain) UIPopoverController *_popoverControler;
@property(nonatomic,retain) TimeTable *_currentTimeTable;
@property(nonatomic,retain) NSMutableArray *itemArr;// Aman Created


//Aman added -- active/inactive students
@property(nonatomic,retain)  NSMutableArray *marrDiaryItemUsers; //items for which students are active


- (IBAction)remindButtonAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;
- (IBAction)filterButtonAction:(id)sender;
- (IBAction)doneButtonAction:(id)sender;
- (IBAction)reminderCancelButtonAction:(id)sender;
- (IBAction)reminderSendButtonAction:(id)sender;
-(void)refreshCurentItemClass;

-(void)loadLastSelectedIndex;

//Merit - Start
@property (nonatomic, retain) IBOutlet UIView *viewForMerit;
@property (nonatomic, retain) IBOutlet UILabel *labelForClassName;
@property (nonatomic, retain) IBOutlet UILabel *labelForStudentName;
@property (nonatomic, retain) IBOutlet UIButton *notifyParentsButton;
@property (retain, nonatomic) IBOutlet UIButton *btnMeritSaveButton;
@property (nonatomic, retain) IBOutlet UILabel *labelForNotifyParents;
@property (nonatomic, retain) IBOutlet UITextView *textViewForMeritComment;
@property (retain, nonatomic) IBOutlet UIButton *btnDeleteMerit;
@property (retain, nonatomic) IBOutlet UILabel *labelMeritViewTitle;
@property (retain, nonatomic) IBOutlet UILabel *labelSelectMerittobeAssigned;
- (IBAction)deleteTaskBasedMeritAction:(id)sender;
//Merit - End

@end

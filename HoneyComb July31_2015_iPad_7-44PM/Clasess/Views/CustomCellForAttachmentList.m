//
//  CustomCellForAttachmentList.m
//  StudyPlaner
//
//  Created by Mitul Trivedi on 12/9/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "CustomCellForAttachmentList.h"

@implementation CustomCellForAttachmentList

@synthesize lblAtachedFileName;
@synthesize btnToDeleteAttachment;
@synthesize btnToShowAttachment;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [self.lblAtachedFileName release];
    [self.btnToShowAttachment release];
    [self.btnToDeleteAttachment release];
    [super dealloc];
}

@end

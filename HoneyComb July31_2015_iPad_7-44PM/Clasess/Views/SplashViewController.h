//
//  SplashViewController.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 11/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OperationQueue.h"
#import "TimeTable.h"
#import "TimeTableCycle.h"
@interface SplashViewController : UIViewController{
    NSTimer *  _myTimer1;
  OperationQueue *queueOperation;
    IBOutlet UILabel *_lableForYear;
    IBOutlet UIImageView *_userImageView;
    IBOutlet UILabel *_lableSchoolProfile;
    IBOutlet UILabel *_lableSchoolMoto;
    IBOutlet UIImageView *_imgViewForLogoImg;
    IBOutlet UIImageView *_imgViewForSchoolImg;
    IBOutlet UILabel *_lblForSchollName;
    IBOutlet UIView *_viewForIndicator;
    
    NSDateFormatter *_globlDateFormat;
    BOOL _isFirstSem;
}
@property(nonatomic,retain)NSString*_temStr;
@property (nonatomic,retain)TimeTable *_curentTimeTable;

@end

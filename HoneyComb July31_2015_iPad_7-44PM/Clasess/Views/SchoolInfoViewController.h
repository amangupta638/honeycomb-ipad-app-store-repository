//
//  SchoolInfoViewController.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
#import "School_Information.h"
@interface SchoolInfoViewController : UIViewController
{

    IBOutlet UIWebView *_webView;
    IBOutlet UIButton *_informationBtn;
    IBOutlet UIView *_infomationView;

    IBOutlet UIView *_blackImgView;
    NSInteger count;
    IBOutlet UILabel *_lableForPrev;
    IBOutlet UIButton *_btnForPrev;

    IBOutlet UILabel *_lableForNext;
    IBOutlet UIButton *_btnForNext;
}
@property(retain,nonatomic) CATextLayer *_testlayer;
@property (nonatomic,retain)NSArray *_arrForSchoolInfoData;
@property (retain, nonatomic) IBOutlet UITableView *_tableView;
- (IBAction)informationButtonAction:(id)sender;
- (IBAction)previousButtonAction:(id)sender;
- (IBAction)nextButtonAction:(id)sender;
-(void)searchSchoolContent:(School_Information*)infoObj;
-(void)updateSchoolInfoDaisy;

@end

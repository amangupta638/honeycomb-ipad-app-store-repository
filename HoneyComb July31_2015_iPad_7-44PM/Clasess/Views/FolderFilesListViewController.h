//
//  FolderFilesListViewController.h
//  DiaryPlannerGoogleDrive
//
//  Created by Apple on 15/09/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GTLDrive.h"

#import "FileEditDelegate.h"
@interface FolderFilesListViewController : UIViewController<UITextViewDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tbl_View;
@property GTLServiceDrive *driveService;
@property GTLDriveFile *driveFile;
@property id<FileEditDelegate> delegate;
@property NSInteger fileIndex;

@end

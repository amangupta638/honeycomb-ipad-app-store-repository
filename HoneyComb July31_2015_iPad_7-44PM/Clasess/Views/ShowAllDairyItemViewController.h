//
//  ShowAllDairyItemViewController.h
//  StudyPlaner
//
//  Created by Dhirendra on 24/05/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//Testing

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>
#import <QuartzCore/QuartzCore.h>
@interface ShowAllDairyItemViewController : UIViewController<UITableViewDataSource,UITableViewDataSource>{

    IBOutlet UITableView *_table;
    NSMutableArray *_arrForDairyItems;
}
-(void)getAllDairyItemMethod:(NSDate*)assignedDate selectedButton:(NSString*)selectBtn;

@property(nonatomic,retain)NSDate *assignDateForDiryItem;
@end

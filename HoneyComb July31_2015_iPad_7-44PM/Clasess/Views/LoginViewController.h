//
//  LoginViewController.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 11/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OperationQueue.h"
#import "TimeTable.h"
#import "TimeTableCycle.h"

@interface LoginViewController : UIViewController{

    IBOutlet UITextField *_testFieldForToken;
    UIView *_alertView;
    OperationQueue *queueOperation;

    IBOutlet UILabel *_alertLbl;
    IBOutlet UIView *_alertVw;
    IBOutlet UITextField *_textFieldForPassword;
    
    IBOutlet UITextField *_textFieldForForgetPassword;
    IBOutlet UITextField *_textFieldForEmail;
    IBOutlet UITextField *_textFieldForDomain;
    IBOutlet UIView *_viewForForgetPassword;
    IBOutlet UIButton *_btnForChkbox;
    NSDateFormatter *_globlDateFormat;
    BOOL _isFirstSem;


}
@property (nonatomic,retain)TimeTable *_curentTimeTable;
@property(nonatomic,retain)NSString*_temStr;
- (IBAction)registrationButtonClick:(id)sender;
- (IBAction)checkBoxButtonAction:(id)sender;
- (IBAction)cancelForgetPasswordButtonAction:(id)sender;
- (IBAction)saveForgetPasswordButtonAction:(id)sender;
- (IBAction)forgetDetailsButtonAction:(id)sender;

// Campus Based Migration Start
-(void)userLoginAgain;
// Campus Based Migration End
@end

//
//  CustomCellFolderFilesList.h
//  DiaryPlannerGoogleDrive
//
//  Created by Aman Gupta on 9/16/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellFolderFilesList : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lbl_Heading;
@property (strong, nonatomic) IBOutlet UIButton *btn_Attachment;

@end

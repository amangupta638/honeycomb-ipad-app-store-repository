//
//  TimeTableViewController.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//
#import "TimeTableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "TimeTable.h"
#import "TimeTableCycle.h" 
#import "Room.h"
#import "Period.h"
#import "MyClass.h"
#import "AppHelper.h"
#import "MyProfile.h"
#import "Service.h"
#import "AppDelegate.h"
#import "Flurry.h"

// Campus Based Start
#define kDateLblWidth 225
//complete date is not visible in the My Timetables & Subjects panel - start
#define kDateLblHeight 45
//the complete date is not visible in the My Timetables & Subjects panel. - end
// Campus Based End




@interface TimeTableViewController ()
{
    NSMutableArray *marrOfHeaderViews; //Aman added -- tableView issue
}

@end

@implementation TimeTableViewController
@synthesize _popoverControler,_currentTimeTable,_arrForTable,_arrForSubject,_cureentClass,dictForClassDetail;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark
#pragma mark Customize color Button
-(void)customizeColorButton{
    for(UIButton *btn in _chooseColorView.subviews){
        switch (btn.tag) {
            case 1:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#800000"];
                break;
            case 2:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#FF0000"];
                break;
            case 3:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#ff33ff"];
                break;
            case 4:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#800080"];
                break;
            case 5:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#000066"];
                break;
            case 6:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#87CEEB"];
                break;
            case 7:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#1f9bd5"];
                break;
            case 8:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#0f9392"];
                break;
            case 9:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#006400"];
                break;
            case 10:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#5eb902"];
                break;
            case 11:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#562b01"];
                break;
            case 12:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#ac8152"];
                break;
            case 13:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#ff6600"];
                break;
            case 14:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#FFA500"];
                break;
            case 15:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#808080"];
                break;
            case 16:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#A9A9A9"];
                break;
            case 17:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#ffffff"];
                break;
            case 18:
                btn.backgroundColor=[AppHelper colorFromHexString:@"#000000"];
                break;
                
            default:
                break;
        }
    }
}



#pragma mark View life cycle
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    [self configureFlurryParameterDictionary];
    marrOfHeaderViews = [[NSMutableArray alloc]init]; //Aman added -- tableView issue

    
    queue = [[NSOperationQueue alloc] init];
    [self customizeColorButton];
    
    
    _arrForSearchedSubject=[[NSMutableArray alloc]init];
    
    _chooseColorView.hidden=YES;
    _timeTableInfoView.frame=CGRectMake(900, _timeTableInfoView.frame.origin.y, _timeTableInfoView.frame.size.width, _timeTableInfoView.frame.size.height);
    // Do any additional setup after loading the view from its nib.
    
    
    // Campus Based Start
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    // Campus Based End
    
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    //NSArray *arr=[[Service sharedInstance] getTimeTableDataBasedOnUniqueCampusCode];
  
#if DEBUG
    NSLog(@"arr %@",arr);
#endif
    
    
    // Campus Based Start
    NSMutableArray *logicaltimetable = [[NSMutableArray alloc] init];
    
    NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
    
    // Multi Campus Support
    if (totalCampusCount > 1 )
    {
        //////////////
        NSMutableArray *arrOfTimeTable = nil;
        arrOfTimeTable = [[NSMutableArray alloc] init];

        //////////////
        NSMutableArray *arrOfTimeTablebasedOnCampuscode = nil;
        arrOfTimeTablebasedOnCampuscode = [[NSMutableArray alloc] init];
        
        // totalCampus array is defined to fetch unique campus code
        NSArray *totalCampus = [[Service sharedInstance] getTimeTableDataBasedOnUniqueCampusCode] ;
        
        for(NSString *strCampusCode in [totalCampus valueForKey:@"campusCode"])
        {
            // arrtimtable array is defined to fetch timetable on the basis of campus Code with start date asc
            NSArray *arrtimtable= [[Service sharedInstance] getTimeTableDataFromCampusCode:strCampusCode];
            
            [arrOfTimeTablebasedOnCampuscode addObject:[NSNumber numberWithInteger:[arrtimtable count]]];
        
            int pp =0;
            ///////////////////////////////////////////////////////
            for(TimeTable *tb1  in arrtimtable)
            {
                [arrOfTimeTable addObject:[NSDictionary dictionaryWithObjectsAndKeys:tb1,@"TimeTableObj",[NSNumber numberWithInteger:pp],@"IndexOfObject", nil]];
                pp++;
            }
            ///////////////////////////////////////////////////////
        }
        
        NSSortDescriptor *highestToLowest = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:NO];
        [arrOfTimeTablebasedOnCampuscode sortUsingDescriptors:[NSArray arrayWithObject:highestToLowest]];
        NSInteger totalRows =[[arrOfTimeTablebasedOnCampuscode firstObject] integerValue];
        

        
        // get array acording campus code wise semester from all timetable records
        
        for(int k = 0;k<totalRows;k++)
        {
            
            NSMutableArray *timetableObj = [[NSMutableArray alloc] init];
            ///////////////////////////////////////////////////////
                for(int jj = 0;jj<[arrOfTimeTable count];jj++)
                {
                    if(k == [[[arrOfTimeTable objectAtIndex:jj]valueForKey:@"IndexOfObject"] integerValue] )
                    {
                       [timetableObj addObject:[[arrOfTimeTable objectAtIndex:jj]valueForKey:@"TimeTableObj"]  ];
                    }
                }
            [logicaltimetable addObject:timetableObj];

            ///////////////////////////////////////////////////////
        }
       
        self._arrForTable=logicaltimetable;
        
        // Campus Based Start
        
        for(int k = 0; k<[self._arrForTable count];k++)
        {
            NSArray *arr = [self._arrForTable objectAtIndex:k];
#if DEBUG
            NSLog(@"[[arr objectAtIndex:0]startDate] %@",[[arr firstObject]startDate]);
#endif
      
            if([DELEGATE isDate:[NSDate date] inRangeFirstDate:[[arr firstObject]startDate]  lastDate:[[arr lastObject] end_date]])
            {
#if DEBUG
                NSLog(@"inside Loop000");
#endif
      
                _currentSem=k;
                _selectedSem=k;
                selectedTimeTableRow = k;
                break;
            }
            // Code added to support single campus based timetable
            // condition added as per desktop
            // StartDate < Today
            else if ([[[arr firstObject]startDate] compare:[NSDate date]] == NSOrderedAscending )
            {
#if DEBUG
                NSLog(@"inside ElseIf000 NSOrderedAscending");
#endif
      
                _currentSem=k;
                _selectedSem=k;
                selectedTimeTableRow = k;
            }
            // Code added to support single campus based timetable
        }
        
        
        NSArray *arrr1 = [ self._arrForTable objectAtIndex:selectedTimeTableRow];
        TimeTable *table;
        TimeTable *previousTable;
        BOOL isAllDatesAreSame = NO;
        NSString *semName =@"";
        NSString *semdates =@"from ";
        
        NSInteger totalCampusFromDB = [[schoolDetailObj noOfCampus] integerValue];
        
        _globlDateFormat = [[NSDateFormatter alloc] init];
        //[_globlDateFormat setDateFormat:kDATEFORMAT_with_day];
        [_globlDateFormat setDateFormat:KDATEFORMAT_Day_ddMMMyyyy];
        
        
        for(int k = 1 ; k<[arrr1 count];k++)
        {
            table=[ arrr1 objectAtIndex:k];
            
            previousTable = [ arrr1 objectAtIndex:(k-1)];
            
            if ([table.startDate compare:previousTable.startDate] == NSOrderedSame)
            {
                isAllDatesAreSame = YES;
            }
            else
            {
                isAllDatesAreSame = NO;
                break;
            }
        }
        
        
        for(int k = 0 ; k<[arrr1 count];k++)
        {
            table=[ arrr1 objectAtIndex:k];
            semName = [semName stringByAppendingString:table.semester_name];
            
            if (totalCampusFromDB > 1 )
            {
                semName = [semName stringByAppendingString:[NSString  stringWithFormat:@"(%@)",table.campusCode]];
            }
            semName = [semName stringByAppendingString:@"/"];
            semdates = [semdates stringByAppendingString:[NSString stringWithFormat:@"%@",[_globlDateFormat stringFromDate:table.startDate]]];
            semdates = [semdates stringByAppendingString:@"/ "];
            
        }
        if (isAllDatesAreSame)
        {
            semdates = [NSString stringWithFormat:@"from %@",[_globlDateFormat stringFromDate:table.startDate]];
            semdates = [semdates stringByAppendingString:@"/ "];
        }
        semName = [semName substringToIndex:[semName length] - 1];
        semdates = [semdates substringToIndex:[semdates length] - 2];
        
        [_globlDateFormat setDateFormat:kDATEFORMAT_with_day];
        
        _semLbl.text=semName;
        _noOfColom=[self maxSemesterCycleLength:arrr1];
        
        NSInteger maxRowval = 0;
        NSInteger maxRowPerTimeTable;
        NSInteger maxRowCount = 0;
        for(int k = 0 ; k<[arrr1 count];k++)
        {
            table=[ arrr1 objectAtIndex:k];
            maxRowPerTimeTable = 0;
            for(int i=0 ;i<=_noOfColom;i++){
                NSArray *arr1=[[Service sharedInstance]getStoredAllPeriodTableData:table.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                
                if (arr1.count > maxRowPerTimeTable)
                {
                    maxRowPerTimeTable = arr1.count;
                }
                if(arr1.count>maxRowval)
                {
                    maxRowval=arr1.count;
                }
            }
            maxRowCount = maxRowPerTimeTable + maxRowCount;
        }
        _noOfRow = maxRowval;
        self._currentTimeTable=table;
        
        
        _dateLbl.text=semdates;
        
        // Campus Based End
        
        
        
        _noOfRow=_noOfRow+1;
    }
    // Single Campus Support -- Existing Code uncommented
    else
    {
        NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
        int index =1;
        
        for(TimeTable *time in arr){
            if([DELEGATE isDate:[NSDate date] inRangeFirstDate:time.startDate  lastDate:time.end_date])
            {
                _currentSem=index;
                self._currentTimeTable=time;
                
                _selectedSem=index;
            }
            index++;
        }
        
        // Code added to support if current date doesn't lie between start and end date of any semester
        //----------------------------------------------------------------------
        if(self._currentTimeTable==nil)
        {
            for(TimeTable *time in arr)
            {
#if DEBUG
                NSLog(@"time = %@",time);
                NSLog(@"NSDate = %@",[NSDate date]);
#endif

                if([[NSDate date] compare:time.startDate] == NSOrderedAscending)
                {
                    self._currentTimeTable=time;
                    break;

                }
                
            }
            if(self._currentTimeTable == nil)
            {
                self._currentTimeTable = [arr lastObject];

            }
        }
        //----------------------------------------------------------------------
        
        self._arrForTable=arr;
        _noOfColom=[self._currentTimeTable.cycle_Length integerValue];
        _noOfRow=0;
        _semLbl.text=self._currentTimeTable.semester_name;
        _globlDateFormat = [[NSDateFormatter alloc] init];
        [_globlDateFormat setDateFormat:@"EEEE dd MMMM"];
        
        _dateLbl.text=[NSString stringWithFormat:@"from %@",[_globlDateFormat stringFromDate:self._currentTimeTable.startDate]];
        
        for(int i=0 ;i<=_noOfColom;i++){
            NSArray *arr1=[[Service sharedInstance]getStoredAllPeriodTableData:self._currentTimeTable.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];

            if(arr1.count>_noOfRow){
                _noOfRow=arr1.count;
            }
        }
        _noOfRow=_noOfRow+1;
    }

    // Campus Based End

    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        _semLbl.frame=CGRectMake(_semLbl.frame.origin.x,  _semLbl.frame.origin.y+3,  _semLbl.frame.size.width,  _semLbl.frame.size.height);
        _dateLbl.frame=CGRectMake(_dateLbl.frame.origin.x,  _dateLbl.frame.origin.y+3,  _dateLbl.frame.size.width,  _dateLbl.frame.size.height);
        _timeTableInfoTalbleView = nil;
        _timeTableInfoTalbleView = [[UITableView alloc] initWithFrame:CGRectMake(10, 10, 220, 460) style:UITableViewStylePlain];
        [_timeTableInfoTalbleView setFrame:CGRectMake(10, 10, 220, 460)];
        [_timeTableInfoTalbleView setDelegate:self];
        [_timeTableInfoTalbleView setDataSource:self];
        [_timeTableInfoTalbleView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_timeTableInfoTalbleView setBackgroundColor:[UIColor clearColor]];

        [_timeTableInfoView addSubview:_timeTableInfoTalbleView];

        
    }
    else
    {
        _semLbl.frame=CGRectMake(25,  _semLbl.frame.origin.y,  _semLbl.frame.size.width,  _semLbl.frame.size.height);
        _dateLbl.frame=CGRectMake(25,  _dateLbl.frame.origin.y,  _dateLbl.frame.size.width,  _dateLbl.frame.size.height);
        _timeTableInfoTalbleView = nil;
        _timeTableInfoTalbleView = [[UITableView alloc] initWithFrame:CGRectMake(10, 10, 220, 460) style:UITableViewStyleGrouped];
        [_timeTableInfoTalbleView setDelegate:self];
        [_timeTableInfoTalbleView setDataSource:self];
        [_timeTableInfoTalbleView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_timeTableInfoTalbleView setBackgroundColor:[UIColor clearColor]];

        [_timeTableInfoView addSubview:_timeTableInfoTalbleView];

    }

    // Multi Campus Support
    if (totalCampusCount > 1 )
    {
        [self addClassItemForMultiCampus];
        [self refillPeriodsForTimetableForMultiCampus];//Aman added -- tableView issue
    }
    // Single Campus Support
    else
    {
        [self addClassItemForSingleCampus];
        [self refillPeriodsForTimetableForSingleCampus];//Aman added -- tableView issue
    }
    
    [_tableView reloadData];

}

// Campus Based Start
-(void)addClassItemForSingleCampus
{
    
    // 17/10/2013
    NSArray *arr=[[Service sharedInstance]getStoredAllClassDataInApp:self._currentTimeTable.timeTable_id];
    NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"subject_name"ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
    arr =[NSMutableArray arrayWithArray:[arr sortedArrayUsingDescriptors:sortDescriptorArr]];
    
    NSMutableArray *arrData=[[NSMutableArray alloc]init];
    NSMutableArray *arrData1=[[NSMutableArray alloc]init];
    
    for (MyClass *classObj in arr) {
        if(![arrData1 containsObject:classObj.class_id]){
            [arrData addObject:classObj];
            [arrData1 addObject:classObj.class_id];
        }
    }
    self._arrForSubject =arrData;
    [arrData release];
    [arrData1 release ];
    
}

-(void)addClassItemForMultiCampus
{
#if DEBUG
    NSLog(@"addClassItemForMultiCampus");
#endif
    
    
    NSArray *selectedTimeTable = [self._arrForTable objectAtIndex:selectedTimeTableRow];
    NSMutableArray *arrData=[[NSMutableArray alloc]init];
    NSMutableArray *arrData1=[[NSMutableArray alloc]init];
#if DEBUG
    NSLog(@"selectedTimeTable %@",selectedTimeTable);
#endif
    
    for(TimeTable *time in selectedTimeTable)
    {
        NSArray *arr=[[Service sharedInstance]getStoredAllClassDataInApp:time.timeTable_id];
        
        NSSortDescriptor*  sortByName = [NSSortDescriptor sortDescriptorWithKey:@"subject_name"ascending:YES];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        arr =[NSMutableArray arrayWithArray:[arr sortedArrayUsingDescriptors:sortDescriptorArr]];
        
        for (MyClass *classObj in arr) {
            
            if(![arrData1 containsObject:classObj.class_id]){
                [arrData addObject:classObj];
                [arrData1 addObject:classObj.class_id];
            }
        }
        
    }
    
    self._arrForSubject =arrData;
    [arrData release];
    [arrData1 release ];
    
}

// Campus Based End




-(void)viewWillAppear:(BOOL)animated
{

    // For Daisy
    if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
    {
        self.lblClassColor.text = @"Color";
    }
    // For Prime
    else
    {
        self.lblClassColor.text = @"Colour";
    }

    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    if([profobj.type isEqualToString:@"Student"]){
        [AppHelper saveToUserDefaults:NULL withKey:@"isMyClass"];
    }
    
    
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    [queue release];
    [dictForClassDetail release];
    if(timer!=nil){
        [timer invalidate];
        timer=nil;
    }
    [_arrForSearchedSubject release];
    [_cureentClass release];
    [_globlDateFormat release];
    [_arrForSubject release];
    [_arrForTable release];
    self._currentTimeTable=nil;
    [_semLbl release];
    [_dateLbl release];
    [_tableView release];
    [_timeTableInfoView release];
    [_timeTableInfoTalbleView release];
    [_timetableInfoBtn release];
    [_blackImgView release];
    [_colorLbl release];
    [_sujectTextField release];
    [_roomNoTextField release];
    [_addItemBlackImgView release];
    [_changeColorBtn release];
    [_chooseColorView release];
    [_popoverControler release];
    [_lblHeaderForAddNewView release];
    [_ableFortime release];
    [_deletePeroidData release];
    [_viewForSearchAndAsociatesClass release];
    [_textFieldForSearchClass release];
    [_btnForSubmitOnClassDetail release];
    [_btnForAssoOnClassDetail release];
    [_viewForClassDescription release];
    [_lblForClassName release];
    [_lblForSubjectName release];
    [_lblForPeriod release];
    [_lblForDay release];
    [_imagViewForLcok release];
    [_saveBtn release];
    [_imagViewForSerchField release];
    [_lblClassColor release];
    [super dealloc];
}
-(void)viewDidDisappear:(BOOL)animated{
    if(timer!=nil){
        [timer invalidate];
        timer=nil;
    }
    
}
- (void)viewDidUnload {
    if(timer!=nil){
        [timer invalidate];
        timer=nil;
    }
    [_semLbl release];
    _semLbl = nil;
    [_dateLbl release];
    _dateLbl = nil;
    [_tableView release];
    _tableView = nil;
    [_timeTableInfoView release];
    _timeTableInfoView = nil;
    [_timeTableInfoTalbleView release];
    _timeTableInfoTalbleView = nil;
    [_timetableInfoBtn release];
    _timetableInfoBtn = nil;
    [_blackImgView release];
    _blackImgView = nil;
    [_colorLbl release];
    _colorLbl = nil;
    [_sujectTextField release];
    _sujectTextField = nil;
    [_roomNoTextField release];
    _roomNoTextField = nil;
    [_addItemBlackImgView release];
    _addItemBlackImgView = nil;
    [_changeColorBtn release];
    _changeColorBtn = nil;
    [_chooseColorView release];
    _chooseColorView = nil;
    [_lblHeaderForAddNewView release];
    _lblHeaderForAddNewView = nil;
    [_ableFortime release];
    _ableFortime = nil;
    [_deletePeroidData release];
    _deletePeroidData = nil;
    [_viewForSearchAndAsociatesClass release];
    _viewForSearchAndAsociatesClass = nil;
    [_textFieldForSearchClass release];
    _textFieldForSearchClass = nil;
    [_btnForSubmitOnClassDetail release];
    _btnForSubmitOnClassDetail = nil;
    [_btnForAssoOnClassDetail release];
    _btnForAssoOnClassDetail = nil;
    [_viewForClassDescription release];
    _viewForClassDescription = nil;
    [_lblForClassName release];
    _lblForClassName = nil;
    [_lblForSubjectName release];
    _lblForSubjectName = nil;
    [_lblForPeriod release];
    _lblForPeriod = nil;
    [_lblForDay release];
    _lblForDay = nil;
    [_imagViewForLcok release];
    _imagViewForLcok = nil;
    [_saveBtn release];
    _saveBtn = nil;
    [_imagViewForSerchField release];
    _imagViewForSerchField = nil;
    [super viewDidUnload];
}
#pragma mark Table
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if ((tableView==_tableView)||(tableView==_tableViewForSearchedSub)) {
        return 1;
    }
    else{
        return 2;
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexinPath
{
    // Campus Based Start
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    // Campus Based End
    NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
    
    // Return height for rows.
    if (tableView==_tableView)
    {
        // Campus Based Start
        if (totalCampusCount > 1)
        {
            return 60*_noOfRow;
        }
        else
        {
          return 60; //Aman added -- tableView issue
        }
        
        // Campus Based End
    }
    else if (tableView==_tableViewForSearchedSub)
    {
            return 30;
    }
    else{
        if (indexinPath.section==0) {
            //the complete date is not visible in the My Timetables & Subjects panel. - start
            if (totalCampusCount > 1)
            {
                return 60;
            }
            else
            {
                return 45;
            }
            //the complete date is not visible in the My Timetables & Subjects panel. - end
        }
        else
        {
            return 27;
        }
        
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows.
    NSInteger count = 0;
    if (tableView==_tableView)
    {
        // Campus Based Start
        School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
        
        // Campus Based End
        NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
        
        if (totalCampusCount > 1 )
        {
            // Campus Based Start
            if ([marrOfHeaderViews count]>0)
            {
                count = 1;
            }
            // Campus Based End
        }
        else
        {
            // flurry Crash : added array count condition
            if (_noOfRow>0)
            {
                count = _noOfRow;
            }
        }
    }
    else if (tableView==_tableViewForSearchedSub)
    {
        // flurry Crash : added array count condition
        if ([_arrForSearchedSubject count]>0)
        {
            count = _arrForSearchedSubject.count;
        }
    }
    else
    {
        if (section==0)
        {
            // flurry Crash : added array count condition
            if ([self._arrForTable count]>0)
            {
                count = self._arrForTable.count;
            }
        }
        else
        {
            // flurry Crash : added array count condition
            if ([self._arrForSubject count]>0)
            {
                count = self._arrForSubject.count;
            }
        }
    }

    return count;
    
    
}
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ((tableView==_tableView)||(tableView==_tableViewForSearchedSub)) {
        return 0;
    }
    else{
        return 30;
    }
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ((tableView==_tableView)||(tableView==_tableViewForSearchedSub)) {
        return nil;
    }
    else{
        UIView *headerView=[[[UIView alloc] init] autorelease];
        headerView.backgroundColor=[UIColor clearColor];
        UIImageView *sectionImage=[[UIImageView alloc] init];
        
        sectionImage.backgroundColor=[UIColor clearColor];
        [headerView addSubview:sectionImage];
        
        headerView.frame=CGRectMake(0, 0,220, 30);
        sectionImage.frame=CGRectMake(1, 1,218, 30);
        
        [sectionImage release];
        
        
        UILabel* sectionLabel=[[UILabel alloc] init];
        sectionLabel.frame=CGRectMake(12, 5,200, 25);
        sectionLabel.textAlignment=NSTextAlignmentCenter;
        sectionLabel.font=[UIFont boldSystemFontOfSize:14.0f];
        sectionLabel.backgroundColor=[UIColor clearColor];
        sectionLabel.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
        [headerView addSubview:sectionLabel];
        [sectionLabel release];
        
        sectionImage.image=[UIImage imageNamed:@"popuptopheader.png"];
        
        //08-08-13
        if (section==0) {
            sectionLabel.text=@"Timetables";
        }
        else{
            sectionLabel.text=@"Classes";
        }

        return headerView;
    }
    
}

// Campus Based Start



//Aman added -- tableView issue ----------------------------------------------------------------------------------------
-(void)refillPeriodsForTimetableForSingleCampus
{
    [marrOfHeaderViews removeAllObjects];
    
    for (int j=0; j<=_noOfRow; j++)
    {
        NSMutableArray *marrOfHeaders= nil;
        marrOfHeaders = [[NSMutableArray alloc]init]; //array to hold headers for one row
        
        if (j==0)
        {
            int x=0;
            
            [marrOfHeaders removeAllObjects];
            
            for(int i=1;i<=_noOfColom;i++)
            {
                
                UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(x,0, _tableView.frame.size.width/_noOfColom, 60)];
                headerView.backgroundColor=[UIColor clearColor];
                
                UIImageView *bgImage=[[UIImageView alloc] init];
                bgImage.frame=CGRectMake(0, 0, headerView.frame.size.width,headerView.frame.size.height);
                bgImage.backgroundColor=[UIColor clearColor];
                
                UILabel *period=[[UILabel alloc] init];
                period.frame=CGRectMake(0, 0,headerView.frame.size.width, headerView.frame.size.height);
                period.font=[UIFont fontWithName:@"Arial" size:13];
                period.backgroundColor=[UIColor clearColor];
                period.textAlignment=NSTextAlignmentCenter;
                period.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                
                TimeTableCycle *cycle=[[Service sharedInstance]getCurentTimeTableCycle:self._currentTimeTable.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                if(cycle){
                    period.text=cycle.cycle_name;
                }
                else
                    period.text=@"Day";
                if(i==1){
                    bgImage.image=[UIImage imageNamed:@"timetabletopheadelefttcell"];
                }
                else if(i ==_noOfColom){
                    bgImage.image=[UIImage imageNamed:@"timetabletopheaderrightcell"];
                    
                }
                else{
                    bgImage.image=[UIImage imageNamed:@"timetabletopheademidcell"];
                    
                }
                
                [headerView addSubview:bgImage];
                [headerView addSubview:period];
               
                [marrOfHeaders addObject:headerView];

                x=x+headerView.frame.size.width;
                
                [bgImage release];
                [period release];
                [headerView release];
                headerView=nil;
            }
            
            [marrOfHeaderViews addObject:marrOfHeaders];
        }
        else
        {
            int x=0;
            
            [marrOfHeaders removeAllObjects];
            
            for(int i=1;i<=_noOfColom;i++)
            {
                UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(x, 0, _tableView.frame.size.width/_noOfColom, 60)];
                headerView.backgroundColor=[UIColor clearColor];
                
                UIImageView *sectionImage=[[UIImageView alloc] init];
                sectionImage.frame=CGRectMake(5, 5, headerView.frame.size.width-10,5 );
                sectionImage.backgroundColor=[UIColor clearColor];

                UIImageView *bgImage=[[UIImageView alloc] init];
                bgImage.frame=CGRectMake(0, 0, headerView.frame.size.width,headerView.frame.size.height);
                bgImage.backgroundColor=[UIColor clearColor];
                bgImage.image=[UIImage imageNamed:@"periodbase"];

                UIImageView *iconImgView=[[UIImageView alloc] init];
                iconImgView.frame=CGRectMake(5, 5, headerView.frame.size.width-10,headerView.frame.size.height-10);
                iconImgView.backgroundColor=[UIColor clearColor];
                
                UILabel* period=[[UILabel alloc] init];
                period.frame=CGRectMake(5, 10,iconImgView.frame.size.width, iconImgView.frame.size.height/3);
                period.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
                period.backgroundColor=[UIColor clearColor];
                period.textAlignment=NSTextAlignmentCenter;
                period.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                period.tag = 100;
                
                UILabel* class=[[UILabel alloc] init];
                class.frame=CGRectMake(5, 10+period.frame.size.height-5,iconImgView.frame.size.width, iconImgView.frame.size.height/3);
                class.font=[UIFont fontWithName:@"Arial" size:8];
                class.backgroundColor=[UIColor clearColor];
                class.textAlignment=NSTextAlignmentCenter;
                class.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                class.tag=200;
                
                UILabel* room=[[UILabel alloc] init];
                room.frame=CGRectMake(5, class.frame.origin.y+period.frame.size.height-4,iconImgView.frame.size.width, iconImgView.frame.size.height/3);
                room.font=[UIFont fontWithName:@"Arial" size:7];
                room.textAlignment=NSTextAlignmentCenter;
                room.backgroundColor=[UIColor clearColor];
                room.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                room.tag=300;
                
                UIButton *addBtn1=[[UIButton alloc]initWithFrame:bgImage.frame];
                [addBtn1 addTarget:self action:@selector(addNewItemButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                [addBtn1 setBackgroundColor:[UIColor clearColor]];
                addBtn1.tag=i;
                
                NSArray *arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:self._currentTimeTable.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                if(!arrOfp.count>0){
                    arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:self._currentTimeTable.timeTable_id cycle:[NSString stringWithFormat:@"%d",0]];
                }
                
                NSSortDescriptor* sortByName = [NSSortDescriptor sortDescriptorWithKey:@"start_time"ascending:YES];
                NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
                arrOfp =[NSMutableArray arrayWithArray:[arrOfp sortedArrayUsingDescriptors:sortDescriptorArr]];
                
                Period *perObj=nil;
                
                if(arrOfp.count>j-1)
                    perObj=  [arrOfp objectAtIndex:j-1];
                if(perObj!=nil)
                {
                    
                    period.text=perObj.title;
                    
                    MyClass *classCycleDay0=[[Service sharedInstance]getStoredFilterClass:perObj.period_id cycle:@"0" timeTable:perObj.timeTable_id] ;
                    
                    MyClass *class1=[[Service sharedInstance]getStoredFilterClass:perObj.period_id cycle:[NSString stringWithFormat:@"%d",i] timeTable:perObj.timeTable_id] ;
                    
                    if (classCycleDay0)
                    {
                        iconImgView.image=[UIImage imageNamed:@"timetableperiodcell"];
                        iconImgView.hidden=NO;
                        
                        class.text=classCycleDay0.class_name;
                        
                        if(classCycleDay0.code!=nil)
                            sectionImage.backgroundColor=[AppHelper colorFromHexString:classCycleDay0.code];
                        else
                            sectionImage.backgroundColor=[UIColor whiteColor];
                        
                        room.text=classCycleDay0.room_name;
                        
                        addBtn1.tag=0;

                    }
                    
                    else if(class1)
                    {
                        iconImgView.image=[UIImage imageNamed:@"timetableperiodcell"];
                        iconImgView.hidden=NO;
                        
                        class.text=class1.class_name;
                        
                        if(class1.code!=nil)
                            sectionImage.backgroundColor=[AppHelper colorFromHexString:class1.code];
                        else
                            sectionImage.backgroundColor=[UIColor whiteColor];
                        
                        room.text=class1.room_name;
                    }
                
                    else
                    {
                        iconImgView.hidden=YES;
                    }
                    
                }
                
                /// Aman Added Code ///
                [addBtn1 setTitle:period.text forState:UIControlStateNormal];
                [addBtn1 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                
                /// Aman Added Code ///
                
                [headerView addSubview:bgImage];
                [headerView addSubview:iconImgView];
                [headerView addSubview:sectionImage];
                [headerView addSubview:period];
                [headerView addSubview:class];
                [headerView addSubview:room];
                [headerView addSubview:addBtn1];
                
                [marrOfHeaders addObject:headerView];
                x=x+headerView.frame.size.width;
                
                [sectionImage release];
                sectionImage = nil;
                [bgImage release];
                bgImage=  nil;
                [period release];
                period = nil;
                [class release];
                class = nil;
                [room release];
                room = nil;
                [iconImgView release];
                iconImgView = nil;
                [addBtn1 release];
                addBtn1 = nil;
                [headerView release];
                headerView = nil;
            }
            
            [marrOfHeaderViews addObject:marrOfHeaders];
        }
    }
}

//Aman added -- tableView issue ----------------------------------------------------------------------------------------
// New Logic Column And Row - Mitul
-(void)refillPeriodsForTimetableForMultiCampus
{
    [marrOfHeaderViews removeAllObjects];
    
    int x=0;
    for (int i=1; i<=_noOfColom; i++)
    {
#if DEBUG
        NSLog(@"_noOfColom with i Value = %d",i);
#endif
      
        NSMutableArray *marrOfHeaders= nil;
        marrOfHeaders = [[NSMutableArray alloc]init]; //array to hold headers for one row
        
        ////////////////////////////////
        // Adding Timetable Ids for no of columns to find which column/cycleDay is for witch Timetable
        NSMutableArray *marrTimetableIdsForGivenCycleDay = [[NSMutableArray alloc] init];
        for (int j=0; j<=_noOfColom; j++)
        {
            [marrTimetableIdsForGivenCycleDay addObject:@"0"];
        }
        
        ///////////////////////////////
        
        //----------------------------------------------------------------------
        // New Code to show exact cells in all coulumns
        //----------------------------------------------------------------------
        NSMutableArray *marrBlankCells = nil;
        marrBlankCells = [[NSMutableArray alloc] init];
        
        //----------------------------------------------------------------------
        int y=0;
        int width = _tableView.frame.size.width/_noOfColom;
        
        //_noOfRow=1;
        for(int j=0;j<_noOfRow;j++)
        {
            
            if ( j==0)
            {
                UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(x,y, _tableView.frame.size.width/_noOfColom, 60)];
                
                headerView.backgroundColor=[UIColor clearColor];
                
                UIImageView *bgImage=[[UIImageView alloc] init];
                bgImage.frame=CGRectMake(0, 0, headerView.frame.size.width,headerView.frame.size.height);
                bgImage.backgroundColor=[UIColor clearColor];
                
                UILabel *period=[[UILabel alloc] init];
                period.frame=CGRectMake(0, 0,headerView.frame.size.width, headerView.frame.size.height);
                period.font=[UIFont fontWithName:@"Arial" size:13];
                period.backgroundColor=[UIColor clearColor];
                period.textAlignment=NSTextAlignmentCenter;
                period.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                period.text=@"Day";
                
                
                //--------------------------------------
                School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
                NSInteger totalCampusFromDB = [[schoolDetailObj noOfCampus] integerValue];
                //--------------------------------------------------
                
                //------------------------------------------
                
                NSArray *arrOfp =nil;
                
                
                NSArray *selectedTimeTable = [self._arrForTable objectAtIndex:selectedTimeTableRow];
                
                MyClass *cccc = nil;
                for(TimeTable *time in selectedTimeTable)
                {
                    cccc = [[Service sharedInstance] getStoredAllClassesTableData:time.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                    if(cccc)
                        break;
                }
                
                if(cccc)
                {
                    arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:cccc.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                    
                }
                
                if(!arrOfp.count>0){
                    
                   
                    if (totalCampusFromDB > 1 )
                    {
                        arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:cccc.timeTable_id cycle:[NSString stringWithFormat:@"%d",0]];
                        
                    }
                    else if (totalCampusFromDB ==1 )
                    {
                        arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:[[selectedTimeTable objectAtIndex:0] timeTable_id] cycle:[NSString stringWithFormat:@"%d",0]];
                    }
                    
                }
                //------------------------------------------
                
                
                NSSortDescriptor* sortByName = [NSSortDescriptor sortDescriptorWithKey:@"start_time"ascending:YES];
                NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
                arrOfp =[NSMutableArray arrayWithArray:[arrOfp sortedArrayUsingDescriptors:sortDescriptorArr]];
                
#if DEBUG
                NSLog(@"after sorting arrOfp = %@",arrOfp);
#endif
      

                
                Period *perObj=nil;
                
                if(arrOfp.count>0)
                {
                    for(perObj in arrOfp)
                    {
                        if(perObj!=nil)
                        {

                            MyClass *classCycleDay0=[[Service sharedInstance]getStoredFilterClass:perObj.period_id cycle:@"0" timeTable:perObj.timeTable_id] ;
                            
                            MyClass *class1=[[Service sharedInstance]getStoredFilterClass:perObj.period_id cycle:[NSString stringWithFormat:@"%d",i] timeTable:perObj.timeTable_id] ;
                            
                            if (classCycleDay0)
                            {

                                TimeTableCycle *cycle;
                                cycle =[[Service sharedInstance]getCurentTimeTableCycle:classCycleDay0.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                                period.text=cycle.short_lable;
                                
                              
                                
                                [marrTimetableIdsForGivenCycleDay replaceObjectAtIndex:(i-1) withObject:[NSString stringWithFormat:@"%d",[perObj.timeTable_id integerValue]]];
                            }
                            else if(class1)
                            {

                                TimeTableCycle *cycle;
                                cycle =[[Service sharedInstance]getCurentTimeTableCycle:class1.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                                period.text=cycle.short_lable;
                                
                                
                                
                                [marrTimetableIdsForGivenCycleDay replaceObjectAtIndex:(i-1) withObject:[NSString stringWithFormat:@"%d",[perObj.timeTable_id integerValue]]];
                            }
                            // To manage Single And Multiple CapusBased Timetable
                            else
                            {
                                TimeTableCycle *cycle;
                                cycle =[[Service sharedInstance]getCurentTimeTableCycle:perObj.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                                period.text=cycle.short_lable;
                                
                                [marrTimetableIdsForGivenCycleDay replaceObjectAtIndex:(i-1) withObject:[NSString stringWithFormat:@"%d",[perObj.timeTable_id integerValue]]];
                                
                            }
                            // To manage Single And Multiple CapusBased Timetable
                        }
                    }
                }
                else
                {
                    
                    NSString *strPeriodLbl=@"";
                    period.numberOfLines = 3;
                    period.font=[UIFont fontWithName:@"Arial" size:11];
                    for(TimeTable *time in selectedTimeTable)
                    {
                        TimeTableCycle *cycle;
                        cycle =[[Service sharedInstance]getCurentTimeTableCycle:time.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                        if (cycle)
                        {
                            if (totalCampusFromDB > 1 )
                            {
                                TimeTable *tble = [[Service sharedInstance] getTimeTableDataInfoStored:[NSString stringWithFormat:@"%d",[time.timeTable_id integerValue]] postNotification:NO];
#if DEBUG
                                NSLog(@"cycle.short_lable %@",cycle.short_lable);
#endif
                                
                                strPeriodLbl = [strPeriodLbl stringByAppendingFormat:@"%@",cycle.short_lable];
                                strPeriodLbl = [strPeriodLbl stringByAppendingFormat:@"%@",[NSString stringWithFormat:@"(%@)",tble.campusCode]];
                                strPeriodLbl = [strPeriodLbl stringByAppendingFormat:@"/ "];
                            }
                        }
                    }
                    if (strPeriodLbl.length > 2 )
                    {
                        strPeriodLbl = [strPeriodLbl substringToIndex:[strPeriodLbl length] - 2];
                        
                        period.text=strPeriodLbl;
                    }
                    
                }
                
                
                if(i==0){
                    bgImage.image=[UIImage imageNamed:@"timetabletopheadelefttcell"];
                }
                else if(i ==_noOfColom){
                    bgImage.image=[UIImage imageNamed:@"timetabletopheaderrightcell"];
                    
                }
                else{
                    bgImage.image=[UIImage imageNamed:@"timetabletopheademidcell"];
                    
                }
                
                [headerView addSubview:bgImage];
                [headerView addSubview:period];
                
                [marrOfHeaders addObject:headerView];
                
                y=y+headerView.frame.size.height;
                [bgImage release];
                [period release];
                [headerView release];
                headerView=nil;
                
                
            }
            else
            {
                
                UIView *headerView=[[UIView alloc] initWithFrame:CGRectMake(x,y, _tableView.frame.size.width/_noOfColom, 60)];
                headerView.backgroundColor=[UIColor clearColor];
                
                UIImageView *sectionImage=[[UIImageView alloc] init];
                sectionImage.frame=CGRectMake(5, 5, headerView.frame.size.width-10,5 );
                sectionImage.backgroundColor=[UIColor clearColor];
                
                UIImageView *bgImage=[[UIImageView alloc] init];
                bgImage.frame=CGRectMake(0, 0, headerView.frame.size.width,headerView.frame.size.height);
                bgImage.backgroundColor=[UIColor clearColor];
                bgImage.image=[UIImage imageNamed:@"periodbase"];
                
                UIImageView *iconImgView=[[UIImageView alloc] init];
                iconImgView.frame=CGRectMake(5, 5, headerView.frame.size.width-10,headerView.frame.size.height-10);
                iconImgView.backgroundColor=[UIColor clearColor];
                
                UILabel* period=[[UILabel alloc] init];
                period.frame=CGRectMake(5, 10,iconImgView.frame.size.width, iconImgView.frame.size.height/3);
                period.font=[UIFont fontWithName:@"Arial-BoldMT" size:10];
                period.backgroundColor=[UIColor clearColor];
                period.textAlignment=NSTextAlignmentCenter;
                period.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                period.tag = 100;
                iconImgView.hidden=YES;
                
                
                UILabel* class=[[UILabel alloc] init];
                class.frame=CGRectMake(5, 10+period.frame.size.height-5,iconImgView.frame.size.width, iconImgView.frame.size.height/3);
                class.font=[UIFont fontWithName:@"Arial" size:8];
                class.backgroundColor=[UIColor clearColor];
                class.textAlignment=NSTextAlignmentCenter;
                class.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                class.tag=200;
                
                UILabel* room=[[UILabel alloc] init];
                room.frame=CGRectMake(5, class.frame.origin.y+period.frame.size.height-4,iconImgView.frame.size.width, iconImgView.frame.size.height/3);
                
                room.font=[UIFont fontWithName:@"Arial" size:7];
                room.textAlignment=NSTextAlignmentCenter;
                room.backgroundColor=[UIColor clearColor];
                room.textColor=[UIColor colorWithRed:79.0/255.0 green:79.0/255.0 blue:80.0/255.0 alpha:1.0];
                room.tag=300;
                
                UIButton *addBtn1=[UIButton buttonWithType:UIButtonTypeCustom];
                addBtn1.frame =CGRectMake(0, 0, headerView.frame.size.width,headerView.frame.size.height);
                [addBtn1 addTarget:self action:@selector(addNewItemButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                [addBtn1 setBackgroundColor:[UIColor clearColor]];
                addBtn1.tag=j;
                //------------------------------------------
                
                NSArray *arrOfp;
                
                
                NSArray *selectedTimeTable = [self._arrForTable objectAtIndex:selectedTimeTableRow];

                MyClass *cccc = nil;
                for(TimeTable *time in selectedTimeTable)
                {
                    cccc = [[Service sharedInstance] getStoredAllClassesTableData:time.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                    if(cccc)
                        break;
                }
                
                
                arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:cccc.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                if(!arrOfp.count>0){
                    
                    // To manage Single And Multiple CapusBased Timetable
                    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
                    NSInteger totalCampusFromDB = [[schoolDetailObj noOfCampus] integerValue];
                    if (totalCampusFromDB > 1 )
                    {
                        arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:cccc.timeTable_id cycle:[NSString stringWithFormat:@"%d",0]];

                    }
                    else if (totalCampusFromDB ==1 )
                    {
                    arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:[[selectedTimeTable objectAtIndex:0] timeTable_id] cycle:[NSString stringWithFormat:@"%d",0]];
                    }
                    // To manage Single And Multiple CapusBased Timetable

                }
                
                
                //------------------------------------------
                
                
                NSSortDescriptor* sortByName = [NSSortDescriptor sortDescriptorWithKey:@"start_time"ascending:YES];
                NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
                arrOfp =[NSMutableArray arrayWithArray:[arrOfp sortedArrayUsingDescriptors:sortDescriptorArr]];
                
                Period *perObj=nil;
                
                if(arrOfp.count>0)
                {
                    if(j<=arrOfp.count)
                    {
                        perObj=  [arrOfp objectAtIndex:j-1];
                    }
                    
                    
                    if(perObj!=nil)
                    {
#if DEBUG
                        NSLog(@"if(perObj!=nil)");
#endif
      
                        MyClass *classCycleDay0=[[Service sharedInstance]getStoredFilterClass:perObj.period_id cycle:@"0" timeTable:perObj.timeTable_id] ;
                            period.text = perObj.title;
                        
                       
                        
                        MyClass *class1=[[Service sharedInstance]getStoredFilterClass:perObj.period_id cycle:[NSString stringWithFormat:@"%d",i] timeTable:perObj.timeTable_id] ;
                        
                        if ([perObj.timeTable_id integerValue] == [[marrTimetableIdsForGivenCycleDay objectAtIndex:i-1] integerValue])
                        {
#if DEBUG
                            NSLog(@"condition match for Tid %@",[marrTimetableIdsForGivenCycleDay objectAtIndex:i-1]);
#endif
                            
                            //------------- conditionalbase ----------------
                            School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
                            NSInteger totalCampusFromDB = [[schoolDetailObj noOfCampus] integerValue];
                            if (totalCampusFromDB > 1 )
                            {
                                NSString *classNameWithCampusCode = period.text;
                                
                                if (classCycleDay0)
                                {
                                    classNameWithCampusCode = [classNameWithCampusCode stringByAppendingFormat:@" (%@)",[[Service sharedInstance]getTimeTableCodeOnthebasisofClass:classCycleDay0]];
                                }
                                else if(class1)
                                {
                                    classNameWithCampusCode = [classNameWithCampusCode stringByAppendingFormat:@" (%@)",[[Service sharedInstance]getTimeTableCodeOnthebasisofClass:class1]];
                                }
                                period.text = classNameWithCampusCode;
                            }
                            //------------- conditionalbase ----------------
                        }
                        
                        if (classCycleDay0)
                        {
                            
                            
                            iconImgView.image=[UIImage imageNamed:@"timetableperiodcell"];
                            iconImgView.hidden=NO;
                            
                            class.text=classCycleDay0.class_name;
                            
                            if(classCycleDay0.code!=nil)
                                sectionImage.backgroundColor=[AppHelper colorFromHexString:classCycleDay0.code];
                            else
                                sectionImage.backgroundColor=[UIColor whiteColor];
                            
                            room.text=classCycleDay0.room_name;
                            
                            addBtn1.tag=0;
                            
                        }
                        
                        else if(class1)
                        {
                            
                            iconImgView.image=[UIImage imageNamed:@"timetableperiodcell"];
                            iconImgView.hidden=NO;
                            
                            class.text=class1.class_name;
                            
                            if(class1.code!=nil)
                                sectionImage.backgroundColor=[AppHelper colorFromHexString:class1.code];
                            else
                                sectionImage.backgroundColor=[UIColor whiteColor];
                            
                            room.text=class1.room_name;
                        }
                    }
                }
                
                
                /// Aman Added Code ///
                [addBtn1 setTitle:period.text forState:UIControlStateNormal];
                [addBtn1 setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
                
                /// Aman Added Code ///
                
                [headerView addSubview:bgImage];
                [headerView addSubview:iconImgView];
                [headerView addSubview:sectionImage];
                [headerView addSubview:period];
                [headerView addSubview:class];
                [headerView addSubview:room];
                [headerView addSubview:addBtn1];
                
                // if Period Label is blank then there is no period and class on that cell
                if ([period.text isEqualToString:@""] || [[period.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length]==0)
                {
                    // if Cycle Day is not followed please add blank Cell
                    if([arrOfp count]==0)
                    {
                        [marrOfHeaders addObject:headerView];
                        y=y+headerView.frame.size.height;
                    }
                    else
                    {
                        [marrBlankCells addObject:headerView];
                    }
                }
                // else period label is having some value so add the header View
                else
                {
                    [marrOfHeaders addObject:headerView];
                    y=y+headerView.frame.size.height;
                }
                
                
                [sectionImage release];
                sectionImage = nil;
                [bgImage release];
                bgImage=  nil;
                [period release];
                period = nil;
                [class release];
                class = nil;
                [room release];
                room = nil;
                [iconImgView release];
                iconImgView = nil;

                [headerView release];
                headerView = nil;
            }
            // adding blank cells in Table for that column where Cycle Day is followed
            if ([marrBlankCells count] > 0 )
            {
                UIView *lastHeaderView = [marrBlankCells lastObject];
                int xPoint = lastHeaderView.frame.origin.x;
                int yPoint = lastHeaderView.frame.origin.y;
                
                for (UIView *blankHeaderViews in marrBlankCells)
                {
                    [blankHeaderViews setFrame:CGRectMake(xPoint,yPoint, _tableView.frame.size.width/_noOfColom, 60)];
                    [marrOfHeaders addObject:blankHeaderViews];
                    yPoint=yPoint+blankHeaderViews.frame.size.height;
                }
            }
            [marrOfHeaderViews addObject:marrOfHeaders];
        }
        x=x+width;
    }
}


// Campus Based End

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Campus Based Start
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    // Campus Based End
    NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
    
    
    if (tableView==_tableView)
    {
        
        static NSString *CellIdentifier = @"myIdenfier1";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
          cell = nil;
        if(cell == nil)
        {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        
        if (totalCampusCount > 1)
        {
            // Campus Based Start
            
            for(NSArray *arr in marrOfHeaderViews )
            {
                for(UIView *headerView in arr)
                {
                    [cell.contentView addSubview:headerView];
                }
            }
            [cell.contentView setBackgroundColor:[UIColor colorWithRed:237/255.0f green:237/255.0f blue:237/255.0f alpha:1.0]];
            
            // Campus Based End
        }
        else
        {
            if (marrOfHeaderViews.count>0)
            {
                for(UIView *headerView in [marrOfHeaderViews objectAtIndex:indexPath.row])
                {
                    [cell.contentView addSubview:headerView];
                }
            }
        }
        
        return cell;
    }
    
    else if (tableView==_tableViewForSearchedSub)
    {
        NSString *MyIdentifier=[NSString stringWithFormat:@"%@",@"cell"];
        UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (myCell == nil){
            myCell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier]autorelease];
            myCell.backgroundColor=[UIColor clearColor ];
            myCell.selectionStyle = UITableViewCellSelectionStyleBlue;
            myCell.accessoryType=UITableViewCellAccessoryNone;
            UILabel* lblForSubject=[[UILabel alloc] init];
            lblForSubject.frame=CGRectMake(1, 4,150, 20);
            lblForSubject.font=[UIFont fontWithName:@"Arial" size:12];
            lblForSubject.backgroundColor=[UIColor clearColor];
            lblForSubject.tag=748;
            
           [myCell.contentView addSubview:lblForSubject];
            [lblForSubject release];
            
        }
        UILabel *lblForSubject=(UILabel*)[myCell viewWithTag:748];
        lblForSubject.textColor=[UIColor whiteColor];
        lblForSubject.text=[_arrForSearchedSubject objectAtIndex:indexPath.row];
        return myCell;

    }
    else
    {
        NSString *MyIdentifier=[NSString stringWithFormat:@"%@",@"cell"];
        UITableViewCell *myCell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        if (myCell == nil){
            myCell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier]autorelease];
            myCell.backgroundColor=[UIColor clearColor ];
            myCell.selectionStyle = UITableViewCellSelectionStyleNone;
            myCell.accessoryType=UITableViewCellAccessoryNone;
            UILabel* lblForDate=[[UILabel alloc] init];
            lblForDate.frame=CGRectMake(1, 16,kDateLblWidth, 30);
            lblForDate.font=[UIFont fontWithName:@"Arial" size:12];
            lblForDate.backgroundColor=[UIColor clearColor];
            lblForDate.tag=4;
            lblForDate.textColor=[UIColor grayColor];
            //complete date is not visible in the My Timetables & Subjects panel - start
            lblForDate.numberOfLines = 0;
            lblForDate.lineBreakMode = NSLineBreakByWordWrapping;
            //the complete date is not visible in the My Timetables & Subjects panel. - end
            UILabel* lblForSubject=[[UILabel alloc] init];
            lblForSubject.frame=CGRectMake(1, 20,150, 30);
            lblForSubject.font=[UIFont fontWithName:@"Arial-BoldMT" size:12];
            lblForSubject.backgroundColor=[UIColor clearColor];
            lblForSubject.tag=5;
            lblForSubject.textColor=[UIColor darkGrayColor];
            
            UIImageView *cellBgImgV=[[UIImageView alloc]initWithFrame:CGRectMake(-9, 0, 218, 60)];
            cellBgImgV.backgroundColor=[UIColor clearColor];
            cellBgImgV.tag=8;
            
            UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(10, 20, 10, 10)];
            imageView.tag=6;
            
            UIImageView *BgimageView=[[UIImageView alloc]initWithFrame:CGRectMake(-9, 0, 218, 60)];
            BgimageView.backgroundColor=[UIColor clearColor];
            BgimageView.tag=7;
            
            
            [myCell.contentView addSubview:cellBgImgV];
            [myCell.contentView addSubview:BgimageView];
            [myCell.contentView addSubview:imageView];
            [myCell.contentView addSubview:lblForDate];
            [myCell.contentView addSubview:lblForSubject];
            
            [cellBgImgV release];
            [lblForDate release];
            [lblForSubject release];
            [imageView release];
            [BgimageView release];
            
        }
        UILabel *dateLb=(UILabel*)[myCell viewWithTag:4];
        UILabel *subject=(UILabel*)[myCell viewWithTag:5];
        UIImageView *imgview=(UIImageView*)[myCell viewWithTag:6];
        UIImageView *Bgimgview=(UIImageView*)[myCell viewWithTag:7];
        UIImageView *cellBgImgV=(UIImageView*)[myCell viewWithTag:8];
        
        if (indexPath.section==0)
        {
            if (totalCampusCount > 1)
            {
                // Campus Based Start
                
                imgview.hidden=YES;
                Bgimgview.hidden=NO;
                
                NSArray *arrr1 = [ self._arrForTable objectAtIndex:indexPath.row];
                NSDate *dateSelectedSame = [[ arrr1 objectAtIndex:0] startDate];
                
                TimeTable *table;
                TimeTable *previousTable;
                BOOL isAllDatesAreSame = NO;
                NSString *semName =@"";
                NSString *semdates =@"from ";

                [_globlDateFormat setDateFormat:KDATEFORMAT_Day_ddMMMyyyy];
                for(int k = 1 ; k<[arrr1 count];k++)
                {
                    table=[ arrr1 objectAtIndex:k];
                    
                    previousTable = [ arrr1 objectAtIndex:(k-1)];
                    
                    if ([table.startDate compare:previousTable.startDate] == NSOrderedSame)
                    {
                        isAllDatesAreSame = YES;
                    }
                    else
                    {
                        isAllDatesAreSame = NO;
                        break;
                    }
                }
                for(int k = 0 ; k<[arrr1 count];k++)
                {
                    table=[ arrr1 objectAtIndex:k];
                    semName = [semName stringByAppendingString:table.semester_name];
                    semName = [semName stringByAppendingString:@"/"];
                    semdates = [semdates stringByAppendingString:[NSString stringWithFormat:@"%@",[_globlDateFormat stringFromDate:table.startDate]]];
                    semdates = [semdates stringByAppendingString:@"/ "];
                    
                    dateSelectedSame=table.startDate;
                    
                }
                
                if (isAllDatesAreSame)
                {
                    semdates = [NSString stringWithFormat:@"from %@",[_globlDateFormat stringFromDate:table.startDate]];
                    semdates = [semdates stringByAppendingString:@"/ "];
                }
                
                semName = [semName substringToIndex:[semName length] - 1];
                semdates = [semdates substringToIndex:[semdates length] - 2];
                
                subject.text=semName;
                [_globlDateFormat setDateFormat:kDATEFORMAT_global_day];
                
                dateLb.text=semdates;
                
                if(_currentSem>0 && _currentSem==indexPath.row+1){
                    imgview.backgroundColor=[UIColor redColor];
                }
                else{
                    imgview.backgroundColor=[UIColor clearColor];
                }
                
                if (indexPath.row==self._arrForTable.count-1) {
                    Bgimgview.image=[UIImage imageNamed:@"popupbigbuttomcellselected.png"];
                }
                else{
                    Bgimgview.image=[UIImage imageNamed:@"popupbigcellseletecd.png"];
                }
                
                if(_selectedSem==indexPath.row+1){
                    Bgimgview.backgroundColor=[UIColor clearColor];
                    Bgimgview.hidden=NO;
                }
                else{
                    Bgimgview.backgroundColor=[UIColor clearColor];
                    Bgimgview.hidden=YES;
                }
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    subject.frame=CGRectMake(9, 0,240, 30);
                    imgview.frame=CGRectMake(9, 9,10, 10);
                    //the complete date is not visible in the My Timetables & Subjects panel. - start
                    Bgimgview.frame = CGRectMake(0, 0, 218, 60);
                    dateLb.frame = CGRectMake(9, 16,kDateLblWidth, kDateLblHeight);
                    cellBgImgV.frame=CGRectMake(0, 0, 218, 60);
                    //the complete date is not visible in the My Timetables & Subjects panel. - end
                }
                else
                {
                    subject.frame=CGRectMake(1, 0,240, 30);
                    imgview.frame=CGRectMake(1, 9,10, 10);
                    //the complete date is not visible in the My Timetables & Subjects panel. - start
                    Bgimgview.frame = CGRectMake(-9, 0, 218, 60);
                    dateLb.frame = CGRectMake(1, 16,kDateLblWidth, kDateLblHeight);
                    cellBgImgV.frame=CGRectMake(-9, 0, 218, 60);
                    //the complete date is not visible in the My Timetables & Subjects panel. - end
                }
                
                if (indexPath.row==self._arrForTable.count-1) {
                    cellBgImgV.image=[UIImage imageNamed:@"popupbigbuttomcell.png"];
                }
                else{
                    cellBgImgV.image=[UIImage imageNamed:@"popupbigcellnromal.png"];
                }
                
                // Campus Based End
            }
            else
            {
            
                imgview.hidden=YES;
                Bgimgview.hidden=NO;
                
                
                TimeTable *table=[ self._arrForTable objectAtIndex:indexPath.row];
                subject.text=table.semester_name;
                [_globlDateFormat setDateFormat:kDATEFORMAT_global_day];

                dateLb.text=[NSString stringWithFormat:@"from %@",[_globlDateFormat stringFromDate:table.startDate]];
                if(_currentSem>0 && _currentSem==indexPath.row+1){
                    imgview.backgroundColor=[UIColor redColor];
                }
                else{
                    imgview.backgroundColor=[UIColor clearColor];
                }
                
                if (indexPath.row==self._arrForTable.count-1) {
                    Bgimgview.image=[UIImage imageNamed:@"popupbigbuttomcellselected.png"];
                }
                else{
                    Bgimgview.image=[UIImage imageNamed:@"popupbigcellseletecd.png"];
                }
                
                if(_selectedSem==indexPath.row+1){
                    Bgimgview.backgroundColor=[UIColor clearColor];
                    Bgimgview.hidden=NO;
                }
                else{
                    Bgimgview.backgroundColor=[UIColor clearColor];
                    Bgimgview.hidden=YES;
                }
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    subject.frame=CGRectMake(9, 0,240, 30);
                    imgview.frame=CGRectMake(9, 9,10, 10);
                    Bgimgview.frame = CGRectMake(0, 0, 218, 44);
                    dateLb.frame = CGRectMake(9, 16,150, 30);
                    cellBgImgV.frame=CGRectMake(0, 0, 218, 45);
                }
                else
                {
                    subject.frame=CGRectMake(1, 0,240, 30);
                    imgview.frame=CGRectMake(1, 9,10, 10);
                    Bgimgview.frame = CGRectMake(-9, 0, 218, 44);
                    dateLb.frame = CGRectMake(1, 16,150, 30);
                    cellBgImgV.frame=CGRectMake(-9, 0, 218, 45);
                }
                
                if (indexPath.row==self._arrForTable.count-1) {
                    cellBgImgV.image=[UIImage imageNamed:@"popupbigbuttomcell.png"];
                }
                else{
                    cellBgImgV.image=[UIImage imageNamed:@"popupbigcellnromal.png"];
                }
            }
        }
        else
        {
            
            if (totalCampusCount > 1)
            {
                // Campus Based Start
               
                Bgimgview.hidden=YES;
                imgview.hidden=NO;
                dateLb.text=@"";
                
                //for image rect round
                [imgview.layer setMasksToBounds:YES];
                [imgview.layer setCornerRadius:5.5f];
                [imgview.layer setBorderWidth:1.0];
                [imgview.layer setBorderColor:[[UIColor clearColor] CGColor]];
                
                MyClass *sb = nil;
                if([self._arrForSubject count]>0){
                    sb=[ self._arrForSubject objectAtIndex:indexPath.row];
                }
                
                //------------- conditionalbase ----------------
                School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
                NSInteger totalCampusFromDB = [[schoolDetailObj noOfCampus] integerValue];
                if (totalCampusFromDB > 1 )
                {
                    NSString *classNameWithCampusCode = [NSString stringWithFormat:@"%@(%@)",sb.class_name,sb.campusCode];
                    subject.text=classNameWithCampusCode;
                }
                else
                {
                    subject.text=sb.class_name;
                }
                // Campus Based End
                
                
                if(sb.code!=nil)
                    imgview.backgroundColor=[AppHelper colorFromHexString:sb.code ];
                
                else{
                    imgview.backgroundColor=[UIColor whiteColor];
                }
                
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    subject.frame=CGRectMake(25, 2,170, 25);
                    imgview.frame=CGRectMake(9, 8,12, 12);
                    cellBgImgV.frame=CGRectMake(0, 0, 218, 27);
                }
                else
                {
                    subject.frame=CGRectMake(17, 2,170, 25);
                    imgview.frame=CGRectMake(1, 8,12, 12);
                    cellBgImgV.frame=CGRectMake(-9, 0, 218, 27);
                }
                
                if (indexPath.row==self._arrForSubject.count-1) {
                    cellBgImgV.image=[UIImage imageNamed:@"popupbuttomcell.png"];
                }
                else
                {
                    cellBgImgV.image=[UIImage imageNamed:@"popupcell.png"];
                }

            }
            else
            {
                Bgimgview.hidden=YES;
                imgview.hidden=NO;
                dateLb.text=@"";
                
                //for image rect round
                [imgview.layer setMasksToBounds:YES];
                [imgview.layer setCornerRadius:5.5f];
                [imgview.layer setBorderWidth:1.0];
                [imgview.layer setBorderColor:[[UIColor clearColor] CGColor]];
                
                MyClass *sb = nil;
                if([self._arrForSubject count]>0){
                    sb=[ self._arrForSubject objectAtIndex:indexPath.row];
                }
                subject.text=sb.class_name;
                
                
                if(sb.code!=nil)
                    imgview.backgroundColor=[AppHelper colorFromHexString:sb.code ];
                
                else{
                    imgview.backgroundColor=[UIColor whiteColor];
                }
                
                if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
                {
                    subject.frame=CGRectMake(25, 2,170, 25);
                    imgview.frame=CGRectMake(9, 8,12, 12);
                    cellBgImgV.frame=CGRectMake(0, 0, 218, 27);
                }
                else
                {
                    subject.frame=CGRectMake(17, 2,170, 25);
                    imgview.frame=CGRectMake(1, 8,12, 12);
                    cellBgImgV.frame=CGRectMake(-9, 0, 218, 27);
                }
                
                if (indexPath.row==self._arrForSubject.count-1) {
                    cellBgImgV.image=[UIImage imageNamed:@"popupbuttomcell.png"];
                }
                else
                {
                    cellBgImgV.image=[UIImage imageNamed:@"popupcell.png"];
                }
            }
        }

        return myCell;
    }
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Campus Based Start
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    // Campus Based End
    NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
    
    if(_tableView==tableView){
        
    }
    else if(tableView==_tableViewForSearchedSub){
        _sujectTextField.text=[_arrForSearchedSubject objectAtIndex:indexPath.row];
        _tableViewForSearchedSub.hidden=YES;
    }
    else
    {
            // Campus Based Start
            
            School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
            NSInteger totalCampusFromDB = [[schoolDetailObj noOfCampus] integerValue];

        if(indexPath.section==0)
        {
            // Multi Campus
            if (totalCampusCount > 1 )
            {
                _selectedSem=indexPath.row+1;
                NSArray *arrr1 = [ self._arrForTable objectAtIndex:indexPath.row];
                TimeTable *table;
                TimeTable *previousTable;
                BOOL isAllDatesAreSame = NO;
                NSString *semName =@"";
                NSString *semdates =@"from ";
                
                [_globlDateFormat setDateFormat:KDATEFORMAT_Day_ddMMMyyyy];
                
                for(int k = 1 ; k<[arrr1 count];k++)
                {
                    table=[ arrr1 objectAtIndex:k];
                    
                    previousTable = [ arrr1 objectAtIndex:(k-1)];
                    
                    if ([table.startDate compare:previousTable.startDate] == NSOrderedSame)
                    {
                        isAllDatesAreSame = YES;
                    }
                    else
                    {
                        isAllDatesAreSame = NO;
                        break;
                    }
                }
                
                for(TimeTable *tbl in arrr1)
                {
                    semName = [semName stringByAppendingString:tbl.semester_name];
                    if (totalCampusFromDB>1)
                    {
                        semName = [semName stringByAppendingString:[NSString  stringWithFormat:@"(%@)",tbl.campusCode]];
                    }
                    semName = [semName stringByAppendingString:@"/"];
                    
                    semdates = [semdates stringByAppendingString:[NSString stringWithFormat:@"%@",[_globlDateFormat stringFromDate:tbl.startDate]]];
                    semdates = [semdates stringByAppendingString:@"/ "];
                    
                }
                if (isAllDatesAreSame)
                {
                    semdates = [NSString stringWithFormat:@"from %@",[_globlDateFormat stringFromDate:table.startDate]];
                    semdates = [semdates stringByAppendingString:@"/ "];
                }
                
                semName = [semName substringToIndex:[semName length] - 1];
                semdates = [semdates substringToIndex:[semdates length] - 2];
                [_globlDateFormat setDateFormat:kDATEFORMAT_with_day];
                selectedTimeTableRow = indexPath.row;
                _semLbl.text=semName;
                _noOfColom=[self maxSemesterCycleLength:arrr1];
                
                NSInteger maxRowval = 0;
                NSInteger maxRowPerTimeTable;
                NSInteger maxRowCount = 0;
                for(int k = 0 ; k<[arrr1 count];k++)
                {
                    table=[ arrr1 objectAtIndex:k];
                    maxRowPerTimeTable = 0;
                    for(int i=0 ;i<=_noOfColom;i++)
                    {
                        NSArray *arr1=[[Service sharedInstance]getStoredAllPeriodTableData:table.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];
                        
                        if (arr1.count > maxRowPerTimeTable)
                        {
                            maxRowPerTimeTable = arr1.count;
                        }
                        if(arr1.count>maxRowval)
                        {
                            maxRowval=arr1.count;
                        }
                    }
                    maxRowCount = maxRowPerTimeTable + maxRowCount;
                }
                _noOfRow = maxRowval;
                
                self._currentTimeTable=table;
                _dateLbl.text=semdates;
                
                [_globlDateFormat setDateFormat:kDATEFORMAT_with_day];
                
                [self addClassItemForMultiCampus];
                _noOfRow =_noOfRow+1;
                [_timeTableInfoTalbleView reloadData];
                
                [self refillPeriodsForTimetableForMultiCampus]; //Aman added -- tableView issue
                
                [_tableView reloadData];
            }
            // Single Campus
            else
            {
                _selectedSem=indexPath.row+1;
                TimeTable *table=[self._arrForTable objectAtIndex:indexPath.row];
                self._currentTimeTable=table;
                _noOfColom=[table.cycle_Length integerValue];
                
                _semLbl.text=table.semester_name;
                [_globlDateFormat setDateFormat:kDATEFORMAT_with_day];
                
                _noOfRow=0;
                
                _dateLbl.text=[NSString stringWithFormat:@"from %@",[_globlDateFormat stringFromDate:table.startDate]];
                for(int i=0 ;i<=_noOfColom;i++){
                    NSArray *arr1=[[Service sharedInstance]getStoredAllPeriodTableData:self._currentTimeTable.timeTable_id cycle:[NSString stringWithFormat:@"%d",i]];

                    if(arr1.count>_noOfRow){
                        _noOfRow=arr1.count;
                    }
                }
                [self addClassItemForSingleCampus];
                _noOfRow =_noOfRow+1;
                [_timeTableInfoTalbleView reloadData];
                
                [self refillPeriodsForTimetableForSingleCampus]; //Aman added -- tableView issue
                [_tableView reloadData];
            }

            }
            // Campus Based End
    }
}

// Campus Based Start
-(NSInteger)maxSemesterCycleLength:(NSArray *)arrofTimetable
{
    
    NSInteger maxVal = [[[arrofTimetable objectAtIndex:0]cycle_Length] integerValue];
    for(TimeTable *ttable in arrofTimetable)
    {
        if([ttable.cycle_Length integerValue]>maxVal)
        {
            maxVal =[ttable.cycle_Length integerValue];
        }
        
    }
    
    return maxVal;
}

-(NSInteger)maxRowForPeriod:(NSArray *)arrofperiod
{
    
    NSInteger maxVal = [arrofperiod objectAtIndex:0];
    
    for(int k = 0; k<[arrofperiod count];k++)
    {
        if([[arrofperiod objectAtIndex:k] integerValue] > maxVal)
        {
            maxVal =[arrofperiod objectAtIndex:k];
        }
        
    }
    
    return maxVal;
}
// Campus Based End




#pragma mark
#pragma mark Button Action
- (IBAction)deletePeroidData:(id)sender
{   //5julyBUG
   
    // Campus Based Start
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    // Campus Based End
    NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
    
    MyClass *classItem=nil;
    
    classItem =  self._cureentClass;
    if(self._cureentClass==nil){
    }
    else
    {
        if(classItem.class_id.integerValue==0)//Means class in not sync with the server
        {
            [[Service sharedInstance] deleteSubjectForClass:classItem];
            
            [[Service sharedInstance]deleteClassObject:classItem];

            // Multi Campus
            if (totalCampusCount > 1 )
            {
                [self addClassItemForMultiCampus];
                [_timeTableInfoTalbleView reloadData];
                self._cureentClass=nil;
                
                [self refillPeriodsForTimetableForMultiCampus];//Aman added -- tableView issue
                [_tableView reloadData];
            }
            // Single Campus
            else
            {
                [self addClassItemForSingleCampus];
                [_timeTableInfoTalbleView reloadData];
                self._cureentClass=nil;
                
                [self refillPeriodsForTimetableForSingleCampus];//Aman added -- tableView issue
                [_tableView reloadData];

            }
        }
        else//already on the server
        {
            BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
            if (networkReachable)
            {
                [dictFlurryParameter setValue:_roomNoTextField.text forKey:@"roomName"];

                [dictFlurryParameter setValue:_sujectTextField.text forKey:@"Name"];
                
                 [Flurry logEvent:FLURRY_EVENT_Class_Deleted withParameters:dictFlurryParameter];
                
                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePeroidDataGet:) name:NOTIFICATION_DeletePeriodData object:nil];
                if([[Service sharedInstance] netManager]){ //Stop Previous Calls
                    [[[Service sharedInstance] netManager] stopRequest];
                }
                NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                [dict setObject:[NSString stringWithFormat:@"%ld",(long)_cycle] forKey:@"cycle"];
                [dict setObject:self._cureentClass.class_id forKey:@"ClassId"];
                [dict setObject:self._cureentClass.room_Id forKey:@"roomId"];
                [dict setObject:self._cureentClass.period_Id forKey:@"periodId"];
                
                [[Service sharedInstance]deltePeriodOnTimeTable:dict];
                [dict release];
            }
            else
            {
                classItem.isDelete=[NSNumber numberWithBool:YES];
                classItem.isSync=[NSNumber numberWithBool:NO];
                NSError *error=nil;
                if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
                {
                    
                }
                // Multi Campus
                if (totalCampusCount > 1 )
                {
                    [self addClassItemForMultiCampus];
                    self._cureentClass=nil;
                    [_timeTableInfoTalbleView reloadData];
                    
                    [self refillPeriodsForTimetableForMultiCampus];//Aman added -- tableView issue
                    [_tableView reloadData];
                }
                // Single Campus
                else
                {
                    [self addClassItemForSingleCampus];
                    self._cureentClass=nil;
                    [_timeTableInfoTalbleView reloadData];
                    
                    [self refillPeriodsForTimetableForSingleCampus];//Aman added -- tableView issue
                    [_tableView reloadData];
                }
            }
            
        }
    }
    [_addItemBlackImgView removeFromSuperview];
}
- (IBAction)timeTableInfoButtonAction:(id)sender {
    
    if (_timetableInfoBtn.frame.origin.x==861) {
        [[AppDelegate getAppdelegate]._currentViewController.view addSubview:_blackImgView];
        [UIView animateWithDuration:0.3
                         animations:^{
                             _timetableInfoBtn.frame=CGRectMake(746, _timetableInfoBtn.frame.origin.y, _timetableInfoBtn.frame.size.width, _timetableInfoBtn.frame.size.height);
                             _timeTableInfoView.frame=CGRectMake(775, _timeTableInfoView.frame.origin.y, _timeTableInfoView.frame.size.width, _timeTableInfoView.frame.size.height);
                             
                         }];
        [_timetableInfoBtn setImage:[UIImage imageNamed:@"mytime2.png"] forState:UIControlStateNormal];
    }
    else
    {
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             _timetableInfoBtn.frame=CGRectMake(861, _timetableInfoBtn.frame.origin.y, _timetableInfoBtn.frame.size.width, _timetableInfoBtn.frame.size.height);
                             _timeTableInfoView.frame=CGRectMake(900, _timeTableInfoView.frame.origin.y, _timeTableInfoView.frame.size.width, _timeTableInfoView.frame.size.height);
                         }];
        [_blackImgView removeFromSuperview];
        [_timetableInfoBtn setImage:[UIImage imageNamed:@"mytime1.png"] forState:UIControlStateNormal];
        
    }
    
}

- (IBAction)cancelButtonAction:(id)sender
{
    [_addItemBlackImgView removeFromSuperview];
}

- (IBAction)saveButtonAction:(id)sender
{
    if(!NSSTRING_HAS_DATA (_sujectTextField.text))
    {
       
    [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please enter subject name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
    }
    else if (!NSSTRING_HAS_DATA (_roomNoTextField.text))
    {
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please enter room name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
    }
    else
    {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setObject:[_sujectTextField.text uppercaseString] forKey:@"sub_name"];
        [dict setObject:[_roomNoTextField.text uppercaseString] forKey:@"room_name"];
        NSString *str=[self hexFromUIColor:_changeColorBtn.backgroundColor];
        [dict setObject:str forKey:@"Code"];
        [dict setObject:[NSString stringWithFormat:@"%ld",(long)_roomNoTextField.tag] forKey:@"PeriodId"];
        [dict setObject:[NSString stringWithFormat:@"%ld",(long)_cycle] forKey:@"CycleDays"];
        //
        [dict setObject:[NSNumber numberWithBool:NO]forKey:@"isSync"];
       
        [dictFlurryParameter setValue:_roomNoTextField.text forKey:@"roomName"];
        [dictFlurryParameter setValue:str forKey:@"classColor"];
        [dictFlurryParameter setValue:_sujectTextField.text forKey:@"Name"];

        
        if(_isEdit)
        {
             [Flurry logEvent:FLURRY_EVENT_Class_Updated withParameters:dictFlurryParameter];
            
            if(self._cureentClass==nil){
            }
            else{
                MyClass *classItem=self._cureentClass;
                
                if( [classItem.class_id integerValue]==0){
                    [dict setObject:classItem.app_id forKey:@"AppId"];
                }
                else
                {
                    [dict setObject:classItem.app_id forKey:@"AppId"];
                    [dict setObject:classItem.class_id forKey:@"Id"];
                }
                [dict setObject:[[Service sharedInstance] miliSecondFromDateMethod:classItem.createdTime] forKey:@"Created"];
                
                [dict setObject:[[Service sharedInstance] miliSecondFromDateMethod:classItem.updatedTime] forKey:@"Updated"];
                [dict setObject:self._cureentClass.createdBy forKey:@"CreatedBy"];

                [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(editPeroidData:) name:NOTIFICATION_EditPeriodData object:nil];
                
                [[Service sharedInstance]editPeriodOnTimeTable:dict];
            }
            
        }
        else
        {    [dictFlurryParameter setValue:self._cureentClass.createdBy forKey:@"Sender"];
             [dictFlurryParameter setValue:self._cureentClass.createdTime forKey:@"Created Date"];
             [Flurry logEvent:FLURRY_EVENT_Class_Created withParameters:dictFlurryParameter];
            
            [dict setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"CreatedBy"];

            [dict setObject:[AppHelper userDefaultsForKey:@"ipadd"]forKey:@"IpAddress"];
            
            NSNumber *appId=[[Service sharedInstance] nextAvailble:@"app_id" forEntityName:@"MyClass"];
            
            [dict setObject:appId forKey:@"AppId"];
            
            [dict setObject:[[Service sharedInstance] miliSecondFromDateMethod:[NSDate date]] forKey:@"Created"];
            
            [dict setObject:[[Service sharedInstance] miliSecondFromDateMethod:[NSDate date]] forKey:@"Updated"];
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addPeroidData:) name:NOTIFICATION_AddPeriod object:nil];
            
            [[Service sharedInstance]addPeriodOnTimeTable:dict];
        }
        [dict release];
        [_addItemBlackImgView removeFromSuperview];
        
    }
       
}
- (NSString *) hexFromUIColor:(UIColor *)color
{
    if (CGColorGetNumberOfComponents(color.CGColor) < 4) {
        const CGFloat *components = CGColorGetComponents(color.CGColor);
        color = [UIColor colorWithRed:components[0] green:components[0] blue:components[0] alpha:components[1]];
    }
    if (CGColorSpaceGetModel(CGColorGetColorSpace(color.CGColor)) != kCGColorSpaceModelRGB) {
        return [NSString stringWithFormat:@"#FFFFFF"];
    }
    return [NSString stringWithFormat:@"#%02X%02X%02X", (int)((CGColorGetComponents(color.CGColor))[0]*255.0), (int)((CGColorGetComponents(color.CGColor))[1]*255.0), (int)((CGColorGetComponents(color.CGColor))[2]*255.0)];
}
- (IBAction)selectColorButtonAction:(id)sender
{
    
    UIButton *btn=(UIButton*)sender;
    [_changeColorBtn setBackgroundColor:btn.backgroundColor];
    //dismiss Popover
    if([self._popoverControler isPopoverVisible]){
        [self._popoverControler dismissPopoverAnimated:YES];
        self._popoverControler=nil;
        return;
    }
    
}

- (IBAction)openColorPickerButtonAction:(id)sender {
    [_roomNoTextField resignFirstResponder];
    [_sujectTextField resignFirstResponder];
    _chooseColorView.hidden=NO;
    
    if([self._popoverControler isPopoverVisible]){
        [self._popoverControler dismissPopoverAnimated:YES];
        self._popoverControler=nil;
        return;
    }
    
    self._popoverControler=nil;
    UIViewController* popoverContent = [[UIViewController alloc]init];
    
    popoverContent.view=nil;
    popoverContent.view = _chooseColorView;
    
    
    UIPopoverController *pop =[[UIPopoverController alloc] initWithContentViewController:popoverContent];
    self._popoverControler=pop;
    [self._popoverControler setPopoverContentSize:CGSizeMake(100, 153) animated:YES];
    self._popoverControler.delegate = self;
    [self._popoverControler presentPopoverFromRect:CGRectMake(40 ,15,10,10 )inView:_changeColorBtn
                          permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
    [pop release];
    [popoverContent release];
    
}
-(void)addNewItemButtonAction:(id)sender
{
    
    // Campus Based Start
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    // Campus Based End
    NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
    // Multi Campus
    if (totalCampusCount > 1 )
    {
        /// Aman Added Code ///
#if DEBUG
        NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif

        if([[sender currentTitle] isEqualToString:@"Recess"] ||[[sender currentTitle] isEqualToString:@"Lunch"] )
        {
            UIAlertView *alertHasBreak = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"You cannot add a class during %@",[sender currentTitle]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertHasBreak show];
            
            return;
        }
        /// Aman Added Code ///
        _tableViewForSearchedSub.hidden=YES;
        UIButton *btn=(UIButton*)sender;
        
        //    UITableViewCell*cell=(UITableViewCell*) btn.superview.superview.superview;
        
        //Aman added -- iOS7 optimisation --------------------------------------
        
        UITableViewCell*cell = nil;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            cell=(UITableViewCell*) btn.superview.superview.superview.superview;
        }
        else
        {
            cell=(UITableViewCell*) btn.superview.superview.superview;
        }
        
        //--------------------------------------
//        School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
        NSInteger totalCampusFromDB = [[schoolDetailObj noOfCampus] integerValue];
        //--------------------------------------------------
        
        /////////////////////////////////////////////////////////////////////////////
        
        CGPoint headerViewPosition = [btn.superview convertPoint:CGPointZero toView:_tableView];
        NSIndexPath *indexPath = [_tableView indexPathForRowAtPoint:headerViewPosition];
        if (indexPath != nil)
        {
#if DEBUG
            NSLog(@" headerViewPosition %@",NSStringFromCGPoint(headerViewPosition));
#endif
        }
        
        int cycleDay1 = 4545445;
        if(headerViewPosition.x==0)
            cycleDay1 = 1;
        else
        {
            // Code modified to Fix issue for CycleDay Calculation dynamically
            //------------------------------------------------------------------
            //cycleDay1 = (headerViewPosition.x/82)+1;
            int val =  _tableView.frame.size.width/_noOfColom;
            
            cycleDay1 = headerViewPosition.x/val;
            
            cycleDay1 = cycleDay1+1;
            
#if DEBUG
            NSLog(@"cycleDay1 = %d",cycleDay1);
#endif
            //------------------------------------------------------------------
            
        }
        
        NSInteger row=btn.tag;
        
        //------------------------------------------
        
        NSArray *arrOfp;
        
        
        NSArray *selectedTimeTable = [self._arrForTable objectAtIndex:selectedTimeTableRow];
        
        MyClass *cccc = nil;
        for(TimeTable *time in selectedTimeTable)
        {
            cccc = [[Service sharedInstance] getStoredAllClassesTableData:time.timeTable_id cycle:[NSString stringWithFormat:@"%d",cycleDay1]];
            if(cccc)
                break;
        }
        
        arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:cccc.timeTable_id cycle:[NSString stringWithFormat:@"%d",cycleDay1]];
        if(!arrOfp.count>0){
            arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:cccc.timeTable_id cycle:[NSString stringWithFormat:@"%d",0]];
        }
        
        NSSortDescriptor* sortByName = [NSSortDescriptor sortDescriptorWithKey:@"start_time"ascending:YES];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        arrOfp =[NSMutableArray arrayWithArray:[arrOfp sortedArrayUsingDescriptors:sortDescriptorArr]];
        Period *perObj=nil;
        if(arrOfp.count>0)
        {

        if(row<=arrOfp.count)
        {
            perObj=  [arrOfp objectAtIndex:row-1];
        }
        }
        
        //    if ([perObj.has_Break intValue] == 1) //check if period has break. If break then return
        //    {
        //        UIAlertView *alertHasBreak = [[UIAlertView alloc]initWithTitle:@"" message:@"This period has a break. You cannot edit a class." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        //        [alertHasBreak show];
        //        return;
        //    }
        
        
        _cycle=cycleDay1;
        if(perObj){
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
            [formatter setDateFormat:@"hh:mm a"];
            
            _ableFortime.text=[NSString stringWithFormat:@"%@-%@",[formatter stringFromDate:perObj.start_time],[formatter stringFromDate:perObj.end_time]];
            TimeTableCycle *cycle=[[Service sharedInstance]getCurentTimeTableCycle:cccc.timeTable_id cycle:[NSString stringWithFormat:@"%ld",(long)_cycle]];
            
            
            
            [formatter release];
            
            /////////////////////////////////////////////////////////////////////////////
            
            MyClass *class1=[[Service sharedInstance]getStoredFilterClass:perObj.period_id cycle:[NSString stringWithFormat:@"%d",cycleDay1]timeTable:perObj.timeTable_id] ;
            
            if(class1)
            {
                
                self._cureentClass=class1;
                //Important
                //07/08/2013
                
                if (!cycle.cycle_name)
                {
                    _lblHeaderForAddNewView.text=[NSString stringWithFormat:@"Edit Class:Day %@>>%@",perObj.cycle_day,perObj.title];
                }
                else
                {
                    _lblHeaderForAddNewView.text=[NSString stringWithFormat:@"Edit Class:%@>>%@",cycle.cycle_name,perObj.title];
                }
                
                _isEdit=YES;
                if ([class1.createdBy isEqualToString:[AppHelper userDefaultsForKey:@"user_id"]])
                {
                    _deletePeroidData.hidden=NO;
                    
                    _saveBtn.hidden=NO;
                    _roomNoTextField.enabled=YES;
                    _sujectTextField.enabled=YES;
                    _imagViewForLcok.hidden=YES;
                    _changeColorBtn.enabled=YES;
                }
                else
                {
                    _imagViewForLcok.hidden=NO;
                    //_changeColorBtn.enabled=NO;
                    _roomNoTextField.enabled=NO;
                    _sujectTextField.enabled=NO;
                    //_isEdit=NO;
                    // _saveBtn.hidden=YES;
                    _deletePeroidData.hidden=YES;
                }
                if (totalCampusFromDB > 1 )
                {
                    _sujectTextField.text=[NSString stringWithFormat:@"%@ (%@)",class1.class_name,class1.campusCode];
                }
                else
                {
                    _sujectTextField.text=class1.class_name;
                }
                
                
                
                if(self._cureentClass.code!=nil)
                    [_changeColorBtn setBackgroundColor:[AppHelper colorFromHexString:self._cureentClass.code]];
                else{
                    [_changeColorBtn setBackgroundColor:[UIColor redColor]];
                }
                
                
                
                _roomNoTextField.text=class1.room_name;
                _sujectTextField.tag=[class1.class_id integerValue];
                _roomNoTextField.tag=[perObj.period_id integerValue];
                _deletePeroidData.tag=[class1.app_id integerValue];
                
                if ([perObj.has_Break integerValue] == 1) //check if period has break. If break then return
                {
                    UIAlertView *alertHasBreak = [[UIAlertView alloc]initWithTitle:@"" message:@"This period has a break. You cannot edit a class." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alertHasBreak show];
                    return;
                }
                else
                {
                    
                    [[AppDelegate getAppdelegate]._currentViewController.view addSubview:_addItemBlackImgView];
                }
            }
            else
            {
                
                
                _lblHeaderForAddNewView.text=[NSString stringWithFormat:@"Add New Class:%@>>%@",cycle.cycle_name,perObj.title];
                _isEdit=NO;
                _deletePeroidData.hidden=YES;
                _sujectTextField.text=@"";
                _roomNoTextField.text=@"";
                [_changeColorBtn setBackgroundColor:[UIColor redColor]];
                _roomNoTextField.tag=[perObj.period_id integerValue];
                
                _imagViewForLcok.hidden=YES;
                _changeColorBtn.enabled=YES;
                _roomNoTextField.enabled=YES;
                _sujectTextField.enabled=YES;
                _saveBtn.hidden=NO;
                _deletePeroidData.hidden=YES;
                
            }
            
        }
        else{
            
        }
        // Campus Based End

    }
    // Single Campus
    else
    {
        // Campus Based Start
        
        
        /// Aman Added Code ///
        if([[sender currentTitle] isEqualToString:@"Recess"] ||[[sender currentTitle] isEqualToString:@"Lunch"] )
        {
            UIAlertView *alertHasBreak = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"You cannot add a class during %@",[sender currentTitle]] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertHasBreak show];
            
            return;
        }
        /// Aman Added Code ///
        _tableViewForSearchedSub.hidden=YES;
        UIButton *btn=(UIButton*)sender;
        
        //    UITableViewCell*cell=(UITableViewCell*) btn.superview.superview.superview;
        
        //Aman added -- iOS7 optimisation --------------------------------------
        
        UITableViewCell*cell = nil;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")&& SYSTEM_VERSION_LESS_THAN(@"8.0"))
        {
            cell=(UITableViewCell*) btn.superview.superview.superview.superview;
        }
        else
        {
            cell=(UITableViewCell*) btn.superview.superview.superview;
        }
        
        
        
        
        
        //Aman added -- iOS7 optimisation --------------------------------------
        
        NSInteger row=[_tableView indexPathForCell:cell].row;
        
        NSArray *arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:self._currentTimeTable.timeTable_id cycle:[NSString stringWithFormat:@"%ld",(long)btn.tag]];
        
        if(!arrOfp.count>0){
            arrOfp=[[Service sharedInstance]getStoredAllPeriodTableData:self._currentTimeTable.timeTable_id cycle:[NSString stringWithFormat:@"%d",0]];
        }
        NSSortDescriptor* sortByName = [NSSortDescriptor sortDescriptorWithKey:@"start_time"ascending:YES];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortByName];
        arrOfp =[NSMutableArray arrayWithArray:[arrOfp sortedArrayUsingDescriptors:sortDescriptorArr]];
        Period *perObj=nil;
        if([arrOfp count]>0)
        {
            if(row-1<=arrOfp.count-1)
                perObj=[arrOfp objectAtIndex:row-1];

        }
        
        //Aman added
        if ([perObj.has_Break integerValue] == 1) //check if period has break. If break then return
        {
            UIAlertView *alertHasBreak = [[UIAlertView alloc]initWithTitle:@"" message:@"This period has a break. You cannot add a class." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertHasBreak show];
            return;
        }
        
        
        _cycle=btn.tag;
        if(perObj){
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
            [formatter setDateFormat:kDateFormatToShowOnUI_hhmma];
            
            _ableFortime.text=[NSString stringWithFormat:@"%@-%@",[formatter stringFromDate:perObj.start_time],[formatter stringFromDate:perObj.end_time]];
            TimeTableCycle *cycle=[[Service sharedInstance]getCurentTimeTableCycle:self._currentTimeTable.timeTable_id cycle:[NSString stringWithFormat:@"%ld",(long)_cycle]];
            
            
            
            [formatter release];
            
            MyClass *class1=[[Service sharedInstance]getStoredFilterClass:perObj.period_id cycle:[NSString stringWithFormat:@"%ld",(long)btn.tag]timeTable:perObj.timeTable_id] ;
            
            if(class1)
            {
                
                self._cureentClass=class1;
                //Important
                //07/08/2013
                
                if (!cycle.cycle_name)
                {
                    _lblHeaderForAddNewView.text=[NSString stringWithFormat:@"Edit Class:Day %@>>%@",perObj.cycle_day,perObj.title];
                }
                else
                {
                    _lblHeaderForAddNewView.text=[NSString stringWithFormat:@"Edit Class:%@>>%@",cycle.cycle_name,perObj.title];
                }
                
                _isEdit=YES;
                if ([class1.createdBy isEqualToString:[AppHelper userDefaultsForKey:@"user_id"]])
                {
                    _deletePeroidData.hidden=NO;
                    
                    _saveBtn.hidden=NO;
                    _roomNoTextField.enabled=YES;
                    _sujectTextField.enabled=YES;
                    _imagViewForLcok.hidden=YES;
                    _changeColorBtn.enabled=YES;
                }
                else
                {
                    _imagViewForLcok.hidden=NO;
                    //_changeColorBtn.enabled=NO;
                    _roomNoTextField.enabled=NO;
                    _sujectTextField.enabled=NO;
                    //_isEdit=NO;
                    // _saveBtn.hidden=YES;
                    _deletePeroidData.hidden=YES;
                }
                //   Subjects *new=[[Service sharedInstance]getSubjectsDataInfoStored:class1.subject_name postNotification:NO];
                _sujectTextField.text=class1.class_name;
                
                if(self._cureentClass.code!=nil)
                    [_changeColorBtn setBackgroundColor:[AppHelper colorFromHexString:self._cureentClass.code]];
                else{
                    [_changeColorBtn setBackgroundColor:[UIColor redColor]];
                }
                
                
                
                _roomNoTextField.text=class1.room_name;
                _sujectTextField.tag=[class1.class_id integerValue];
                _roomNoTextField.tag=[perObj.period_id integerValue];
                _deletePeroidData.tag=[class1.app_id integerValue];
                
                if ([perObj.has_Break integerValue] == 1) //check if period has break. If break then return
                {
                    UIAlertView *alertHasBreak = [[UIAlertView alloc]initWithTitle:@"" message:@"This period has a break. You cannot edit a class." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alertHasBreak show];
                    return;
                }
                else
                {
                    
                    [[AppDelegate getAppdelegate]._currentViewController.view addSubview:_addItemBlackImgView];
                }
                
            }
            else
            {
                _lblHeaderForAddNewView.text=[NSString stringWithFormat:@"Add New Class:%@>>%@",cycle.cycle_name,perObj.title];
                _isEdit=NO;
                _deletePeroidData.hidden=YES;
                _sujectTextField.text=@"";
                _roomNoTextField.text=@"";
                [_changeColorBtn setBackgroundColor:[UIColor redColor]];
                _roomNoTextField.tag=[perObj.period_id integerValue];
                
                _imagViewForLcok.hidden=YES;
                _changeColorBtn.enabled=YES;
                _roomNoTextField.enabled=YES;
                _sujectTextField.enabled=YES;
                _saveBtn.hidden=NO;
                _deletePeroidData.hidden=YES;
            }
            //[[AppDelegate getAppdelegate]._currentViewController.view addSubview:_addItemBlackImgView];
            
        }
        else{
            
        }
    }
    
    
    
}

#pragma mark Service CallBack
-(void)addPeroidData:(NSNotification*)note
{
    
    [[AppDelegate getAppdelegate]hideIndicator];
    
    if (note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_AddPeriod object:nil];
        
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            
            if([[note userInfo] valueForKey:@"Class_details"])
            {
                //new changes ß
                [[Service sharedInstance ]SyncMyClassDataWithServer:[[note userInfo] valueForKey:@"Class_details"]];
                
                //for classInfo Table
                //17/10/2013
                NSArray *arr=[[Service sharedInstance]getStoredAllClassDataInApp:self._currentTimeTable.timeTable_id];
                for(MyClass *newOne in arr){
                    if([[NSString stringWithFormat:@"%d",[newOne.class_id integerValue]] isEqualToString:[[[note userInfo] valueForKey:@"Class_details"] objectForKey:@"Id"]]){
                        newOne.code=[[[note userInfo] valueForKey:@"Class_details"] objectForKey:@"Code"];
                        NSError *error;
                        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
                        {
#if DEBUG
                            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
                        }
                    }
                }

            }
            
            if([[note userInfo] valueForKey:@"Room_details"])
                [[Service sharedInstance ]parseRoomDataWithDictionary:[[note userInfo] valueForKey:@"Room_details"]];
            
            if([[note userInfo] valueForKey:@"Subject_details"])
                [[Service sharedInstance ]parseSubjectDataWithDictionary:[[note userInfo] valueForKey:@"Subject_details"]];
            
            // Campus Based Start
            School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
            
            // Campus Based End
            NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
            // Multi Campus
            if (totalCampusCount > 1 )
            {
                [self addClassItemForMultiCampus];
                [_timeTableInfoTalbleView reloadData];
                
                [self refillPeriodsForTimetableForMultiCampus];//Aman added -- tableView issue
                [_tableView reloadData];
                
                if ([[AppHelper userDefaultsForKey:@"isMyClass"] isEqualToString:@"myclassView"])
                {
                    [[AppDelegate getAppdelegate]._currentViewController selectMyClass];
                    [AppHelper saveToUserDefaults:NULL withKey:@"isMyClass"];
                    _isEdit=NO;
                }
                else{
                }
            }
            // Single Campus
            else
            {
                [self addClassItemForSingleCampus];
                [_timeTableInfoTalbleView reloadData];
                
                [self refillPeriodsForTimetableForSingleCampus];//Aman added -- tableView issue
                [_tableView reloadData];
                
                if ([[AppHelper userDefaultsForKey:@"isMyClass"] isEqualToString:@"myclassView"])
                {
                    [[AppDelegate getAppdelegate]._currentViewController selectMyClass];
                    [AppHelper saveToUserDefaults:NULL withKey:@"isMyClass"];
                    _isEdit=NO;
                }
                else{
                }

            }
            
        }
        else{
            
            //[AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Error"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
}

#pragma mark edit period data
-(void)editPeroidData:(NSNotification*)note
{
    // Campus Based Start
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    // Campus Based End
    NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
    
    [[AppDelegate getAppdelegate]hideIndicator];
    if (note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_EditPeriodData object:nil];
        
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            MyClass *classes=self._cureentClass;

            if(classes!=nil)
            {

                if((![classes.subject_name isEqualToString:[[[note userInfo] valueForKey:@"Class_details"] objectForKey:@"sub_name"]] )|| (![classes.room_name isEqualToString:[[[note userInfo] valueForKey:@"Class_details"] objectForKey:@"room_name"]] ) || ([classes.cycle_day integerValue ]!=[[[[note userInfo] valueForKey:@"Class_details"] objectForKey:@"CycleDays"]integerValue ] )){
               
                if(classes.class_id.integerValue==0)//Means class in not sync with the server
                {
                    [[Service sharedInstance] deleteSubjectForClass:classes];
                    
                    [[Service sharedInstance]deleteClassObject:classes];
                    
                    
                    // Multi Campus
                    if (totalCampusCount > 1 )
                    {
                        [self addClassItemForMultiCampus];
                        [_timeTableInfoTalbleView reloadData];
                        self._cureentClass=nil;
                        
                        [self refillPeriodsForTimetableForMultiCampus];//Aman added -- tableView issue
                        [_tableView reloadData];
                    }
                    // Single Campus
                    else
                    {
                        [self addClassItemForSingleCampus];
                        [_timeTableInfoTalbleView reloadData];
                        self._cureentClass=nil;
                        
                        [self refillPeriodsForTimetableForSingleCampus];//Aman added -- tableView issue
                        [_tableView reloadData];
                    }
                    
                }
                else{
                    
                    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
                    if (networkReachable)
                    {
                        
                        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deletePeroidDataGet:) name:NOTIFICATION_DeletePeriodData object:nil];
//                        if([[Service sharedInstance] netManager]){ //Stop Previous Calls
//                            [[[Service sharedInstance] netManager] stopRequest];
//                        }
                        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                        [dict setObject:[NSString stringWithFormat:@"%ld",(long)_cycle] forKey:@"cycle"];
                        [dict setObject:self._cureentClass.class_id forKey:@"ClassId"];
                        [dict setObject:self._cureentClass.room_Id forKey:@"roomId"];
                        [dict setObject:self._cureentClass.period_Id forKey:@"periodId"];
//                        [[Service sharedInstance]deltePeriodOnTimeTable:dict];
                        [[Service sharedInstance]deleteClassObject:classes];

                        [dict release];
                    }
                    else
                    {
                        classes.isDelete=[NSNumber numberWithBool:YES];
                        classes.isSync=[NSNumber numberWithBool:NO];
                        NSError *error=nil;
                        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
                        {
#if DEBUG
                            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
                        }
                        // Multi Campus
                        if (totalCampusCount > 1 )
                        {
                            [self addClassItemForMultiCampus];
                            self._cureentClass=nil;
                            [_timeTableInfoTalbleView reloadData];
                            
                            [self refillPeriodsForTimetableForMultiCampus];//Aman added -- tableView issue
                            [_tableView reloadData];
                        }
                        // Single Campus
                        else
                        {
                            [self addClassItemForSingleCampus];
                            self._cureentClass=nil;
                            [_timeTableInfoTalbleView reloadData];
                            
                            [self refillPeriodsForTimetableForSingleCampus];//Aman added -- tableView issue
                            [_tableView reloadData];
                        }
                    }
                }
            }

            }
            
            if([[note userInfo] valueForKey:@"Class_details"])
            {
                [[Service sharedInstance ]SyncMyClassDataWithServer:[[note userInfo] valueForKey:@"Class_details"]];
                //for classInfo Table
                //17/10/2013
                NSArray *arr = nil;
                // Campus Based Start
                // Multi Campus
                if (totalCampusCount > 1 )
                {
                    arr =[[Service sharedInstance]getStoredAllClassData];
                }
                // Single Campus
                else
                {
                    arr=[[Service sharedInstance]getStoredAllClassDataInApp:self._currentTimeTable.timeTable_id];
                    // Campus Based End
                }

                for(MyClass *newOne in arr){
                    if([[NSString stringWithFormat:@"%d",[newOne.class_id integerValue]] isEqualToString:[[[note userInfo] valueForKey:@"Class_details"] objectForKey:@"Id"]]){
                        if (![[[[note userInfo] valueForKey:@"Class_details"] objectForKey:@"Code"] isKindOfClass:[NSNull class]])
                        newOne.code=[[[note userInfo] valueForKey:@"Class_details"] objectForKey:@"Code"];
                       
                        NSError *error;
                        if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
                        {
#if DEBUG
                            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
                        }
                    }
                }
            }
            if([[note userInfo] valueForKey:@"Room_details"])
                [[Service sharedInstance ]parseRoomDataWithDictionary:[[note userInfo] valueForKey:@"Room_details"]];
            if([[note userInfo] valueForKey:@"Subject_details"])
                [[Service sharedInstance ]parseSubjectDataWithDictionary:[[note userInfo] valueForKey:@"Subject_details"]];
            
            // Multi Campus
            if (totalCampusCount > 1 )
            {
                [self addClassItemForMultiCampus];
                [_timeTableInfoTalbleView reloadData];
                
                [self refillPeriodsForTimetableForMultiCampus];//Aman added -- tableView issue
                [_tableView reloadData];
            }
            // Single Campus
            else
            {
                [self addClassItemForSingleCampus];
                [_timeTableInfoTalbleView reloadData];
                
                [self refillPeriodsForTimetableForSingleCampus];//Aman added -- tableView issue
                [_tableView reloadData];
            }
            
            
            if ([[AppHelper userDefaultsForKey:@"isMyClass"] isEqualToString:@"myclassView"]) {
                [[AppDelegate getAppdelegate]._currentViewController selectMyClass];
                [AppHelper saveToUserDefaults:NULL withKey:@"isMyClass"];
                _isEdit=NO;
            }
            else{
            }
            
        }
        else{
            
           // [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Error"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
    
    [_addItemBlackImgView removeFromSuperview];
}
-(void)deletePeroidDataGet:(NSNotification*)note
{
    // Campus Based Start
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    // Campus Based End
    NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
    
    
    [[AppDelegate getAppdelegate]hideIndicator];
    if (note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_DeletePeriodData object:nil];
        
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            MyClass *classes=self._cureentClass;
            if(classes!=nil)
            {
                [[Service sharedInstance] deleteSubjectForClass:classes];
            }
            NSError *error;
            NSManagedObject *eventToDelete = self._cureentClass;
            [[AppDelegate getAppdelegate].managedObjectContext deleteObject:eventToDelete];
            
            if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Error deleting  - error:%@",error);
#endif
            }
            
            // Multi Campus
            if (totalCampusCount > 1 )
            {
                [self addClassItemForMultiCampus];
                [_timeTableInfoTalbleView reloadData];
                self._cureentClass=nil;//5julyBUG
                
                [self refillPeriodsForTimetableForMultiCampus];//Aman added -- tableView issue
                [_tableView reloadData];
            }
            // Single Campus
            else
            {
                [self addClassItemForSingleCampus];
                [_timeTableInfoTalbleView reloadData];
                self._cureentClass=nil;//5julyBUG
                
                [self refillPeriodsForTimetableForSingleCampus];//Aman added -- tableView issue
                [_tableView reloadData];
            }
            
        }
        else{
            
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Error"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
}




#pragma mark
#pragma mark TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    
    
    if([self._popoverControler isPopoverVisible]){
        [self._popoverControler dismissPopoverAnimated:YES];
        self._popoverControler=nil;
        return YES;
        
    }
    [_sujectTextField resignFirstResponder];
    [_roomNoTextField resignFirstResponder];
    return YES;
    
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    }
- (void)textFieldDidEndEditing:(UITextField *)textField
{
  
}



- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *searcText=nil;
    if (textField==_sujectTextField) {
        if (textField.text.length>0) {
             searcText=[NSString stringWithFormat:@"%@",textField.text];
        }
        else{
            searcText=[NSString stringWithFormat:@"%@",string];
        }
    }
    
    const char * _char = [string cStringUsingEncoding:NSUTF8StringEncoding];
    int isBackSpace = strcmp(_char, "\b");
    
    if(isBackSpace==-8)
    {
        
        if (textField==_sujectTextField) {
            _tableViewForSearchedSub.hidden=NO;
            [_roomNoTextField resignFirstResponder];
            
            
            NSArray *arrForSubject=[[Service sharedInstance]getStoredAllSubjectData];
            if(arrForSubject.count>0){
                
                [_arrForSearchedSubject removeAllObjects];
                for (Subjects *subObj in arrForSubject) {
                    
                    NSString *str= subObj.subject_name;
                    NSRange rangeVar = [str rangeOfString:searcText options:NSCaseInsensitiveSearch];
                    if(rangeVar.location != NSNotFound)
                    {
                        [_arrForSearchedSubject addObject:subObj.subject_name];
                    }
                    
                }
                
                
            }
            if (_arrForSearchedSubject.count>0) {
                _tableViewForSearchedSub.hidden=NO;
            }
            else{
                _tableViewForSearchedSub.hidden=YES;
            }
            [_tableViewForSearchedSub reloadData];
        }
        else{
            _tableViewForSearchedSub.hidden=YES;
        }
        return YES;
    }
    
    if ((range.location == 0 && [string isEqualToString:@" "])) {
        return NO;
    }
    else{
        if (textField.text.length >= 30)
        {
            return NO;
        }
        else{
                      
            if (textField==_sujectTextField) {
                _tableViewForSearchedSub.hidden=NO;
                [_roomNoTextField resignFirstResponder];
                
                
                NSArray *arrForSubject=[[Service sharedInstance]getStoredAllSubjectData];
                if(arrForSubject.count>0){
                    
                    [_arrForSearchedSubject removeAllObjects];
                    for (Subjects *subObj in arrForSubject) {
                        
                        NSString *str= subObj.subject_name;
                        NSRange rangeVar = [str rangeOfString:searcText options:NSCaseInsensitiveSearch];
                        if(rangeVar.location != NSNotFound)
                        {
                            //23/09/2013
                            if(str!=nil){
                            [_arrForSearchedSubject addObject:subObj.subject_name];
                            }
                        }
                        
                    }
                                        
                 
                }
                if (_arrForSearchedSubject.count>0) {
                    _tableViewForSearchedSub.hidden=NO;
                }
                else{
                    _tableViewForSearchedSub.hidden=YES;
                }
                [_tableViewForSearchedSub reloadData];
        }
        else{
            _tableViewForSearchedSub.hidden=YES;
        }
           
                    
            return YES;
        }
    }
    
         
}




#pragma mark background Sync
-(void)BackGroundMethoForClassSync
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        
        NSInvocationOperation *operation1 = [[NSInvocationOperation alloc]
                                             initWithTarget:self
                                             selector:@selector(synchonizeClassItem)
                                             object:nil];
        
        [queue addOperation:operation1];
        [operation1 release];
        
        [[NSNotificationCenter defaultCenter] addObserverForName:NOTIFICATION_syncronizeClass object:nil queue:nil
                                                      usingBlock:^(NSNotification *note)
         {
             
             if (note.userInfo)
             {
                 [[AppDelegate getAppdelegate]hideIndicator];
                 
                 [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_syncronizeClass object:nil];
                 
               //  TableUpdateTime *tabl=[[Service sharedInstance] getlastUpdateTableDataInfoStored];
                 
                 //TODO: Update last sync time
                // [[Service sharedInstance]updateClassTime:tabl];
                 
                 if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
                 {
                     TableUpdateTime *tabl=[[Service sharedInstance] getlastUpdateTableDataInfoStored];
                     tabl.classUpdateTime=[NSNumber numberWithDouble:[[[note userInfo] valueForKey:@"LastSynchTime"]doubleValue]];
                     
                     for(NSDictionary *dict in [[note userInfo] valueForKey:@"deleted"])
                     {
                         MyClass *delClassobj=[[Service sharedInstance]getClassDataFromClassId:[dict objectForKey:@"uniqueId"]];
                         
                         //check whether the subject os this class exist in another class or not.....if not then delete subject also
                         if(delClassobj)
                             [[Service sharedInstance]deleteSubjectForClass:delClassobj];
                         
                         //finally delete the class
                         if(delClassobj)
                             [[Service sharedInstance]deleteClassObject:delClassobj];
                     }

                     for (NSDictionary *dict  in [[note userInfo] valueForKey:@"details"])
                     {
                         [[Service sharedInstance]SyncMyClassDataWithServer:dict];
                     }
                     
                     
                     // Campus Based Start
                     School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
                     
                     // Campus Based End
                     NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
                     
                     // Multi Campus
                     if (totalCampusCount > 1 )
                     {
                         [self addClassItemForMultiCampus];
                         [_timeTableInfoTalbleView reloadData];
                         
                         [self refillPeriodsForTimetableForMultiCampus];//Aman added -- tableView issue
                         [_tableView reloadData];
                     }
                     // Single Campus
                     else
                     {
                         [self addClassItemForSingleCampus];
                         [_timeTableInfoTalbleView reloadData];
                         
                         [self refillPeriodsForTimetableForSingleCampus];//Aman added -- tableView issue
                         [_tableView reloadData];
                     }
                 }
                 else
                 {
                     [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
                 }
                 
             }
         }];
    }
}

-(void)synchonizeClassItem
{
    NSArray *arr=[[Service sharedInstance]getClassDataInfoStoredwithNoSync:[NSNumber numberWithBool:NO]];
    NSArray *delArray=[[Service sharedInstance]getClassDeleteObject:[NSNumber numberWithBool:YES]];
    
    NSMutableArray *details=[[NSMutableArray alloc]init];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    NSMutableArray *deletedArray=[[NSMutableArray alloc]init];
    if(arr.count>0)
    {
        for(MyClass *class in arr){
            [details addObject:[self returnClassDictonaryForSync:class]];
        }
    }
    
    
    if(delArray.count>0)
    {
        for(MyClass *class in delArray){
            [deletedArray addObject:[self returnDataForDeleteClassOnServer:class]];
        }
        
    }
    
    [dict setObject:details forKey:@"details"];
    [dict setObject:deletedArray forKey:@"deleteClasses"];
    
    [[Service sharedInstance]synchronizeClass:dict];
    
    [details release];
    [deletedArray release];
    [dict release];
    
}
-(NSMutableDictionary*)returnClassDictonaryForSync:(MyClass*)classData
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    
    if (classData.cycle_day) {
        [dict setObject:classData.cycle_day forKey:@"CycleDays"];
    }
    if (classData.code) {
        [dict setObject:classData.code forKey:@"code"];
        
    }
    if (classData.period_Id) {
        [dict setObject:classData.period_Id forKey:@"PeriodId"];
        
    }
    if (classData.room_name) {
        [dict setObject:classData.room_name forKey:@"room_name"];
        
    }
    if (classData.subject_name) {
        [dict setObject:classData.subject_name forKey:@"subject_name"];
        
    }
    
    if (classData.createdTime) {
        [dict setObject:[[Service sharedInstance] miliSecondFromDateMethod:classData.createdTime] forKey:@"created"];
        
    }
    
    return [dict autorelease];
}
-(NSMutableDictionary*)returnDataForDeleteClassOnServer:(MyClass*)classData
{
    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    
    [dict setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
    [dict setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
    
    if (classData.cycle_day) {
        [dict setObject:classData.cycle_day forKey:@"CycleDays"];
    }
    if (classData.class_id) {
        [dict setObject:classData.class_id forKey:@"ClassId"];
        
    }
    if (classData.period_Id) {
        [dict setObject:classData.period_Id forKey:@"periodId"];
        
    }
    if (classData.room_Id) {
        [dict setObject:classData.room_Id forKey:@"roomId"];
        
    }
    
    return [dict autorelease];
}
/*
#pragma mark new
- (IBAction)addClassDetialButoonAction:(id)sender {
    
    _viewForClassDescription.hidden=YES;
    _btnForAssoOnClassDetail.hidden=YES;
    _imagViewForSerchField.hidden=NO;
    _textFieldForSearchClass.text=nil;
    _textFieldForSearchClass.hidden=NO;
    //[[AppDelegate getAppdelegate]._currentViewController.view addSubview:_viewForSearchAndAsociatesClass];
    //[self initialDelayEnded:_viewForSearchAndAsociatesClass];
    _viewForSearchAndAsociatesClass.frame=CGRectMake(200, -15, _viewForSearchAndAsociatesClass.frame.size.width, _viewForSearchAndAsociatesClass.frame.size.height);
    [self.view addSubview:_viewForSearchAndAsociatesClass];
   // _semLbl.frame=CGRectMake(3,  _semLbl.frame.origin.y,  _semLbl.frame.size.width,  _semLbl.frame.size.height);
   //  _dateLbl.frame=CGRectMake(-4,  _dateLbl.frame.origin.y,  _dateLbl.frame.size.width,  _dateLbl.frame.size.height);
}*/

- (IBAction)closeClassDetailVButtonAction:(id)sender {
    [_viewForSearchAndAsociatesClass removeFromSuperview];
  //  _semLbl.frame=CGRectMake(338,  _semLbl.frame.origin.y,  _semLbl.frame.size.width,  _semLbl.frame.size.height);
   // _dateLbl.frame=CGRectMake(345,  _dateLbl.frame.origin.y,  _dateLbl.frame.size.width,  _dateLbl.frame.size.height);
}

- (IBAction)serchClassButtonAction:(id)sender {
    
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
          if (_textFieldForSearchClass.text.length>0) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchclClassesDidGot:) name:NOTIFICATION_Search_Classes object:nil];
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        
        [dict setObject:_textFieldForSearchClass.text forKey:@"code"];
        [dict setObject:[AppHelper userDefaultsForKey:@"school_Id"]forKey:@"SchoolId"];
        //[dict setObject:@"5" forKey:@"SchoolId"];
      
            [[Service sharedInstance]getSearchClasses:dict];
              [dict release];

        }
        else{
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Please enter class name" delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
        
        
        
        
    }
    else{
        //21AugChange
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Adding a new class requires a connection to the internet." delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    
}

- (IBAction)associatesButtonAction:(id)sender {
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(associatesClassesDidGot:) name:NOTIFICATION_Associate_Class object:nil];
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        
        //ClassId ,user_id,user_type and created
        
        
        [dict setObject:[self.dictForClassDetail objectForKey:@"Id"] forKey:@"ClassId"];
        [dict setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [dict setObject:[AppHelper userDefaultsForKey:@"user_type"] forKey:@"user_type"];
        [dict setObject:[[Service sharedInstance] miliSecondFromDateMethod:[NSDate date]] forKey:@"created"];
        
        [[Service sharedInstance]getAssociatesClasses:dict];
        [dict release];
        
        
        
    }
    else{
        //21AugChange
        [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Adding a new class requires a connection to the internet." delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    
}
#pragma mark - Search and associates classes
-(void)searchclClassesDidGot:(NSNotification*)note
{
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_Search_Classes object:nil];
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
                        self.dictForClassDetail=[[note userInfo] valueForKey:@"result"];
            
            
            NSArray *arr= [self.dictForClassDetail objectForKey:@"ClassDays"];
            NSString *strDay=@"";
               NSString *strPeriod=@"";
            if (arr.count>0) {
                _textFieldForSearchClass.hidden=YES;
                _imagViewForSerchField.hidden=YES;

                _viewForClassDescription.hidden=NO;
                _btnForAssoOnClassDetail.hidden=NO;
                for(NSDictionary *dict in arr){
                    strDay=[strDay stringByAppendingFormat:@"%@,",[dict objectForKey:@"CycleDays"]];
                    strPeriod=[strPeriod stringByAppendingFormat:@"%@,",[dict objectForKey:@"period_name"]];

                }
                _lblForDay.text=strDay;
                _lblForPeriod.text=strPeriod;
                _lblForSubjectName.text=[NSString stringWithFormat:@"%@",[self.dictForClassDetail objectForKey:@"sub_name"]];
                _lblForClassName.text=[NSString stringWithFormat:@"%@",[self.dictForClassDetail objectForKey:@"Name"]];
                
            }
            else{
                
                [AppHelper showAlertViewWithTag:0 title:APP_NAME message:@"Class Not Found" delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
            }
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Error"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
}



-(void)associatesClassesDidGot:(NSNotification*)note
{
    //5 Aug 2013
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_Associate_Class object:nil];
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            [_viewForSearchAndAsociatesClass removeFromSuperview];

            _semLbl.frame=CGRectMake(338,  _semLbl.frame.origin.y,  _semLbl.frame.size.width,  _semLbl.frame.size.height);
            _dateLbl.frame=CGRectMake(345,  _dateLbl.frame.origin.y,  _dateLbl.frame.size.width,  _dateLbl.frame.size.height);

            if([[[note userInfo] valueForKey:@"result"]valueForKey:@"Class_details"])
            {
                //new changes ß
                for(NSDictionary *dict in [[[note userInfo] valueForKey:@"result"]valueForKey:@"Class_details"]){
                [[Service sharedInstance ]SyncMyClassDataWithServer:dict];
                }
                
                //for classInfo Table,Subject_details
                
            }
            
            if([[[note userInfo] valueForKey:@"result"]valueForKey:@"Room_details"]){
                 for(NSDictionary *dict in [[[note userInfo] valueForKey:@"result"]valueForKey:@"Room_details"]){
                [[Service sharedInstance ]parseRoomDataWithDictionary:dict];
                 }
            }
            
            
            if([[[note userInfo] valueForKey:@"result"]valueForKey:@"Subject_details"])
                [[Service sharedInstance ]parseSubjectDataWithDictionary:[[[note userInfo] valueForKey:@"result"]valueForKey:@"Subject_details"]];
            if([[[note userInfo] valueForKey:@"result"]valueForKey:@"classuserDetails"]){
            for (NSDictionary *dict  in [[[note userInfo] valueForKey:@"result"]valueForKey:@"classuserDetails"])
            {
                [[Service sharedInstance]parseStudentDataWithDictionary:dict];
            }
        }
        
            // Campus Based Start
            School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
            
            // Campus Based End
            NSInteger totalCampusCount = [[schoolDetailObj noOfCampus] integerValue];
            
            // Multi Campus
            if (totalCampusCount > 1 )
            {
                [self addClassItemForMultiCampus];
                
                [self refillPeriodsForTimetableForMultiCampus];//Aman added -- tableView issue
            }
            // Single Campus
            else
            {
                [self addClassItemForSingleCampus];
                
                [self refillPeriodsForTimetableForSingleCampus];//Aman added -- tableView issue
            }

            [_tableView reloadData];
            [_timeTableInfoTalbleView reloadData];

           
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Error"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
}




#pragma mark - pop up a view animated lifecycle
-(void)initialDelayEnded:(UIView*)popView
{
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.4];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped:)];
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0, 1.0);
    [UIView commitAnimations];
}

- (void)bounce1AnimationStopped:(UIView*)popView {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped:)];
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
    [UIView commitAnimations];
}

- (void)bounce2AnimationStopped:(UIView*)popView {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    popView.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
    
}

-(void)initialDelayEnded1:(UIView*)popView
{
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.4];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce11AnimationStopped:)];
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView commitAnimations];
}

- (void)bounce11AnimationStopped:(UIView*)popView {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce12AnimationStopped:)];
    popView.transform = CGAffineTransformScale(CGAffineTransformIdentity,1.0, 1.0);
    [UIView commitAnimations];
}

- (void)bounce12AnimationStopped:(UIView*)popView {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    popView.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
}

-(void)configureFlurryParameterDictionary
{
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    dictFlurryParameter = [[NSMutableDictionary alloc]init];
    
    [dictFlurryParameter setValue:profobj.profile_id forKey:@"userid"];
    [dictFlurryParameter setValue:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"schoolId"];
    [dictFlurryParameter setValue:profobj.type forKey:@"Role"];
    if ([profobj.type isEqualToString:@"Teacher"])
        [dictFlurryParameter setValue:@"0" forKey:@"grade"];
    else
        [dictFlurryParameter setValue:[NSString stringWithFormat:@"%@",profobj.year] forKey:@"grade"];
    [dictFlurryParameter setValue:@"iPad" forKey:@"platform"];
    
}



@end

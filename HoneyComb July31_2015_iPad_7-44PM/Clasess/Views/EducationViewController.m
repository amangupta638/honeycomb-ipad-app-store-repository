//
//  EducationViewController.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 12/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "EducationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "School_Information.h"
#import "Service.h"

#import "FileUtils.h"
#import "SchoolInformation.h"

@interface EducationViewController ()
{
}
@end

@implementation EducationViewController
@synthesize _arrEducationContentData;
@synthesize _testlayer;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    count=0;
    
    
    [_webView.layer setMasksToBounds:YES];
    [_webView.layer setCornerRadius:5.0f];
    [_webView.layer setBorderWidth:1.0];
    [_webView.layer setBorderColor:[[UIColor grayColor] CGColor]];
    

    // Do any additional setup after loading the view from its nib.
    [self updateEducationContentDaisy];
    
}

-(void)updateEducationContentDaisy
{
    //Important
    self._arrEducationContentData=[[Service sharedInstance]getStoredDaisyAboutData:@"Empower"];
    
    // Get the path of Bundle To fetch HTML files array
//    NSString *strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];

    // Get the path of Bundle To fetch HTML files array
    NSString *EducationContentFolder = [[Service sharedInstance] getContentTypeForEducationContent];
    NSString *strBundlePath;
    // For Daisy
    if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
    {
        strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];
    }
    // For Prime
    // For Teacher And Student Same content
    else
    {
        strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content"];
        strBundlePath = [strBundlePath stringByAppendingPathComponent:EducationContentFolder];
    }
#if DEBUG
    NSLog(@"strBundlePath %@",strBundlePath);
#endif
    
    count=0;
    if(_arrEducationContentData.count>0){
        School_Information *about = [self._arrEducationContentData objectAtIndex:count];
        [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:strBundlePath isDirectory:YES]];
    }
    
    // Previous Button Logic
    if(_arrEducationContentData.count>0){
        School_Information *about2=[self._arrEducationContentData objectAtIndex:self._arrEducationContentData.count-1];
        School_Information *about3=nil;
        
        // Next Button Logic
        if (_arrEducationContentData.count>1) {
            about3=[self._arrEducationContentData objectAtIndex:1];;
        }
        else if (_arrEducationContentData.count==1){
            about3=[self._arrEducationContentData objectAtIndex:self._arrEducationContentData.count-1];
        }

        _labelForPrev.text=about2.title;
        
        NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
        NSArray *arrItem=[str componentsSeparatedByString:@"  "];
        NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                             initWithString:str];
        NSString *str1=[arrItem objectAtIndex:0];
        NSString *str2=[arrItem objectAtIndex:1];
        CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
        CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
        [string addAttribute:(id)kCTFontAttributeName
                       value:(id)helveticaBold
                       range:[str rangeOfString:str1]];
        [string addAttribute:(id)kCTFontAttributeName
                       value:(id)helveticaBold1
                       range:[str rangeOfString:str2]];
        
        [string addAttribute:(id)kCTForegroundColorAttributeName
                       value:(id)[UIColor darkGrayColor].CGColor
                       range:[str rangeOfString:str1]];
        [string addAttribute:(id)kCTForegroundColorAttributeName
                       value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                       range:[str rangeOfString:str2]];
        
        if (self._testlayer!=nil){
            
            [self._testlayer removeFromSuperlayer];
        }
        CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
        normalTextLayer_.string =string;
        normalTextLayer_.wrapped = YES;
        normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
        normalTextLayer_.alignmentMode = kCAAlignmentRight;
        normalTextLayer_.frame = CGRectMake(0, 0, _labelForNext.frame.size.width,_labelForNext.frame.size.height);
        self._testlayer=normalTextLayer_;
        [_labelForNext.layer addSublayer: self._testlayer];
        [_labelForNext setNeedsDisplay];
        [normalTextLayer_ release];
        [string release];
    }
    ////////////////////////////////////////
    
    
}


//for search
-(void)searchEducationContent:(School_Information*)infoObj
{
    //----- Added By Mitul on 23-Dec For Education Content -----//
    
    
     self._arrEducationContentData=[[Service sharedInstance]getStoredDaisyAboutData:@"Empower"];
    NSInteger indexOfObject = 0;
    NSInteger Aindex = 0;
    if(self._arrEducationContentData.count>0)
    {
       // NSInteger Aindex = [self._arrEducationContentData indexOfObject:infoObj];
        for(SchoolInformation *school in self._arrEducationContentData)
        {
            if ([infoObj.title isEqualToString:school.title])
            {
                 Aindex = indexOfObject;
                break;
            }
            indexOfObject ++;
        }
        count=Aindex;
        
//        NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
        // Get the path of Bundle To fetch HTML files array
//        NSString *strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];
        
        // Get the path of Bundle To fetch HTML files array
        NSString *EducationContentFolder = [[Service sharedInstance] getContentTypeForEducationContent];
        NSString *strBundlePath;
        // For Daisy
        if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
        {
            strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];
        }
        // For Prime
        // For Teacher And Student Same content
        else
        {
            strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content"];
            strBundlePath = [strBundlePath stringByAppendingPathComponent:EducationContentFolder];
        }

        //--------- By Mitul ---------//
        if(_arrEducationContentData.count>0){
            School_Information *about=[self._arrEducationContentData objectAtIndex:Aindex];
            
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:strBundlePath isDirectory:YES]];
        }
        
        if(_arrEducationContentData.count>0)
        {
            
            School_Information *about2=nil;
            School_Information *about3=nil;
            if (count==0)
            {
                
                about2=[self._arrEducationContentData objectAtIndex:self._arrEducationContentData.count-1];
                about3=[self._arrEducationContentData objectAtIndex:count+1];
                
            }
            else if (count==self._arrEducationContentData.count-1)
            {
                about2=[self._arrEducationContentData objectAtIndex:count-1];
                about3=[self._arrEducationContentData objectAtIndex:0];
            }
            else{
                about2=[self._arrEducationContentData objectAtIndex:count-1];
                about3=[self._arrEducationContentData objectAtIndex:count+1];
            }
            
            _labelForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _labelForNext.frame.size.width,_labelForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_labelForNext.layer addSublayer: self._testlayer];
            [_labelForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
        }
        
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark Button Action

//----- Added By Mitul on 23 Dec -----//

- (IBAction)PrvevousButtonAction:(id)sender
{
    if (self._arrEducationContentData.count)
    {
        // Get the path of Bundle To fetch HTML files array
//        NSString *strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];
        
        // Get the path of Bundle To fetch HTML files array
        NSString *EducationContentFolder = [[Service sharedInstance] getContentTypeForEducationContent];
        NSString *strBundlePath;
        // For Daisy
        if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
        {
            strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];
        }
        // For Prime
        // For Teacher And Student Same content
        else
        {
            strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content"];
            strBundlePath = [strBundlePath stringByAppendingPathComponent:EducationContentFolder];
        }

        if (count>1)
        {
            count=count-1;
            
            School_Information *about= [self._arrEducationContentData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:strBundlePath isDirectory:YES]];
            School_Information *about2= [self._arrEducationContentData objectAtIndex:count-1];
            School_Information *about3= [self._arrEducationContentData objectAtIndex:count+1];
            
            _labelForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _labelForNext.frame.size.width,_labelForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_labelForNext.layer addSublayer: self._testlayer];
            [_labelForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
        }
        else if(count==1){
            count=count-1;
//            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about= [self._arrEducationContentData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:strBundlePath isDirectory:YES]];
            School_Information *about2= [self._arrEducationContentData objectAtIndex:self._arrEducationContentData.count-1];
            School_Information *about3= [self._arrEducationContentData objectAtIndex:count+1];
            
            
            _labelForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _labelForNext.frame.size.width,_labelForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_labelForNext.layer addSublayer: self._testlayer];
            [_labelForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
            
            
        }
        else if(count==0){
            count=self._arrEducationContentData.count-1;
//            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about= [self._arrEducationContentData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:strBundlePath isDirectory:YES]];

            //21AugChange
            School_Information *about2=nil;
            if (count-1==-1) {
                about2= [self._arrEducationContentData objectAtIndex:0];
            }
            else{
                about2= [self._arrEducationContentData objectAtIndex:count-1];
            }
            
            School_Information *about3= [self._arrEducationContentData objectAtIndex:0];
            
            
            _labelForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _labelForNext.frame.size.width,_labelForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_labelForNext.layer addSublayer: self._testlayer];
            [_labelForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
            
            
        }
        
    }
    
}



- (IBAction)nextButtonAction:(id)sender
{
    if (self._arrEducationContentData.count)
    {
        // Get the path of Bundle To fetch HTML files array
//        NSString *strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];

        // Get the path of Bundle To fetch HTML files array
        NSString *EducationContentFolder = [[Service sharedInstance] getContentTypeForEducationContent];
        NSString *strBundlePath;
        // For Daisy
        if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
        {
            strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];
        }
        // For Prime
        // For Teacher And Student Same content
        else
        {
            strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content"];
            strBundlePath = [strBundlePath stringByAppendingPathComponent:EducationContentFolder];
        }

        if (count==self._arrEducationContentData.count-1) {
            count=0;
//            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about= [self._arrEducationContentData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:strBundlePath isDirectory:YES]];
            School_Information *about2= [self._arrEducationContentData objectAtIndex:self._arrEducationContentData.count-1];
            //21AugChange
            School_Information *about3=nil;
            if (self._arrEducationContentData.count==1) {
                about3= [self._arrEducationContentData objectAtIndex:count];
            }
            else{
                about3= [self._arrEducationContentData objectAtIndex:count+1];
            }
            
            _labelForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _labelForNext.frame.size.width,_labelForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_labelForNext.layer addSublayer: self._testlayer];
            [_labelForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
        }
        else if (count==self._arrEducationContentData.count-2) {
            count=count+1;
//            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about= [self._arrEducationContentData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:strBundlePath isDirectory:YES]];
            School_Information *about2= [self._arrEducationContentData objectAtIndex:count-1];
            School_Information *about3= [self._arrEducationContentData objectAtIndex:0];
            
            
            _labelForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _labelForNext.frame.size.width,_labelForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_labelForNext.layer addSublayer: self._testlayer];
            [_labelForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
            
        }
        else {
            count=count+1;
//            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
            School_Information *about= [self._arrEducationContentData objectAtIndex:count];
            [_webView loadHTMLString:about.content baseURL:[NSURL fileURLWithPath:strBundlePath isDirectory:YES]];
            School_Information *about2= [self._arrEducationContentData objectAtIndex:count-1];
            School_Information *about3= [self._arrEducationContentData
                                        objectAtIndex:count+1];
            
            
            _labelForPrev.text=about2.title;
            
            NSString *str=[NSString stringWithFormat:@"%@  %@",@"Next:",about3.title];
            NSArray *arrItem=[str componentsSeparatedByString:@"  "];
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc]
                                                 initWithString:str];
            NSString *str1=[arrItem objectAtIndex:0];
            NSString *str2=[arrItem objectAtIndex:1];
            CTFontRef helveticaBold = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            CTFontRef helveticaBold1 = CTFontCreateWithName(CFSTR("Arial"), 10.0, NULL);
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTFontAttributeName
                           value:(id)helveticaBold1
                           range:[str rangeOfString:str2]];
            
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor darkGrayColor].CGColor
                           range:[str rangeOfString:str1]];
            [string addAttribute:(id)kCTForegroundColorAttributeName
                           value:(id)[UIColor colorWithRed:140.0/255.0 green:140.0/255.0 blue:140.0/255.0 alpha:1].CGColor
                           range:[str rangeOfString:str2]];
            
            if (self._testlayer!=nil){
                
                [self._testlayer removeFromSuperlayer];
            }
            CATextLayer *normalTextLayer_ = [[CATextLayer alloc] init];
            normalTextLayer_.string =string;
            normalTextLayer_.wrapped = YES;
            normalTextLayer_.foregroundColor = [[UIColor clearColor] CGColor];
            normalTextLayer_.alignmentMode = kCAAlignmentRight;
            normalTextLayer_.frame = CGRectMake(0, 0, _labelForNext.frame.size.width,_labelForNext.frame.size.height);
            self._testlayer=normalTextLayer_;
            [_labelForNext.layer addSublayer: self._testlayer];
            [_labelForNext setNeedsDisplay];
            [normalTextLayer_ release];
            [string release];
            
        }
    }
}

//--------- By Mitul ---------//


-(void)dealloc
{
    
    [_testlayer release];
    [_arrEducationContentData release];
    [_webView release];
    [_labelForPrev release];
    [_labelForNext release];
    [super dealloc];
}
- (void)viewDidUnload {
    [_webView release];
    _webView = nil;
    [_labelForPrev release];
    _labelForPrev = nil;
    [_labelForNext release];
    _labelForNext = nil;
    
    [super viewDidUnload];
}
@end

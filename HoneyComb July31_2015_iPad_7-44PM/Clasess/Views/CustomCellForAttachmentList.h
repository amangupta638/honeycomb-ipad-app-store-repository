//
//  CustomCellForAttachmentList.h
//  StudyPlaner
//
//  Created by Mitul Trivedi on 12/9/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellForAttachmentList : UITableViewCell
{
    
}
@property (retain, nonatomic) IBOutlet UILabel *lblAtachedFileName;
@property (retain, nonatomic) IBOutlet UIButton *btnToShowAttachment;
@property (retain, nonatomic) IBOutlet UIButton *btnToDeleteAttachment;


@end

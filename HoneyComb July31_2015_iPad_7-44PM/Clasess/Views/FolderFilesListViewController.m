//
//  FolderFilesListViewController.m
//  DiaryPlannerGoogleDrive
//
//  Created by Apple on 15/09/13.
//  Copyright (c) 2013 Apple. All rights reserved.
//

#import "FolderFilesListViewController.h"
#import "Utilities.h"
#import "CustomCellFolderFilesList.h"
#import "Attachment.h"

@interface FolderFilesListViewController ()
{

    NSMutableArray *marrToCheckAttachedOrDetached;
    NSMutableArray *marrOfFolderFiles;

}
@property (strong, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (strong, nonatomic) IBOutlet UITextView *textView;

@property (strong) NSString *originalContent;
@property (strong) NSString *updatedTitle;

/*- (IBAction)saveButtonClicked:(id)sender;
- (IBAction)deleteButtonClicked:(id)sender;
- (IBAction)renameButtonClicked:(id)sender;*/
- (void)clickAttachment:(id)sender;

- (void)loadFileContent;
/*- (void)saveFile;
- (void)deleteFile;
- (void)toggleSaveButton;
*/
@end

@implementation FolderFilesListViewController
@synthesize driveService = _driveService;
@synthesize driveFile = _driveFile;
@synthesize delegate = _delegate;
@synthesize saveButton = _saveButton;
@synthesize textView = _textView;
@synthesize originalContent = _originalContent;
@synthesize updatedTitle = _updatedTitle;
@synthesize fileIndex = _fileIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - Managing the detail item
- (void)viewDidLoad
{
    
    self.navigationController.navigationBarHidden = NO;

    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.textView.delegate = self;
    
    NSString *fileTitle;
    if (self.fileIndex == -1) {
        fileTitle = @"New file";
    } else {
        fileTitle = self.driveFile.title;
    }
    
    self.title = [[NSString alloc] initWithFormat:@"%@", fileTitle];
    self.updatedTitle = [self.driveFile.title copy];
    
    // In case of new file, show the title dialog.
    if (self.fileIndex == -1) {
        [self renameButtonClicked:nil];
    } else {
        [self loadFileContent];
    }
}

- (void)viewDidUnload
{
    [self setSaveButton:nil];
    [self setTextView:nil];
    marrToCheckAttachedOrDetached =nil;
    marrOfFolderFiles = nil;
    [super viewDidUnload];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    UIBarButtonItem *doneButton =
    [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                  target:self
                                                  action:@selector(doneEditing:)];
    self.navigationItem.leftBarButtonItem = doneButton;
}

/*- (void)textViewDidChange:(UITextView *)textView {
    [self toggleSaveButton];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        self.updatedTitle = [[[alertView textFieldAtIndex:0] text] copy];
        self.title = [[NSString alloc] initWithFormat:@"Edit: %@", self.updatedTitle];
    }
    [self toggleSaveButton];
}

- (IBAction)doneEditing:(id)sender {
    [self.view endEditing:YES];
    self.navigationItem.leftBarButtonItem = nil;
}

- (IBAction)saveButtonClicked:(id)sender {
    [self saveFile];
}

- (IBAction)deleteButtonClicked:(id)sender {
    [self deleteFile];
}
*/
- (IBAction)renameButtonClicked:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Edit title"
                                                     message:@""
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"Ok", nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField * alertTextField = [alert textFieldAtIndex:0];
    alertTextField.placeholder = @"File's title";
    alertTextField.text = self.updatedTitle;
    [alert show];
}

- (void)loadFileContent {
    UIAlertView *alert = [Utilities showLoadingMessageWithTitle:@"Loading file content"
                                                             delegate:self];
    
    
    GTLQueryDrive *query = [GTLQueryDrive queryForFilesList];
    query.q = [NSString stringWithFormat:@"'%@' IN parents", self.driveFile.identifier];
    // root is for root folder replace it with folder identifier in case to fetch any specific folder
    __block  NSMutableArray *driveFiles = nil;
    [self.driveService executeQuery:query completionHandler:^(GTLServiceTicket *ticket,
                                                              GTLDriveFileList *files,
                                                              NSError *error) {
        if (error == nil)
        {
            marrOfFolderFiles = nil;
            marrOfFolderFiles = [[NSMutableArray alloc] init];
            marrToCheckAttachedOrDetached = nil;
            marrToCheckAttachedOrDetached = [[NSMutableArray alloc] init];
            
            driveFiles = [[NSMutableArray alloc] init];
            [driveFiles addObjectsFromArray:files.items];
            
            if([driveFiles count] ==0)
            {
             [alert dismissWithClickedButtonIndex:0 animated:YES];
            }
            
            NSInteger kk = 0;
            // Now you have all files of root folder
            for (GTLDriveFile *file in driveFiles)
            {
                
                NSString  *filename=file.title;
                
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
                NSString *strGoogleDrivePath = [documentsDirectory stringByAppendingPathComponent:@"GoogleDrive"];
                
                if (![[NSFileManager defaultManager] fileExistsAtPath:strGoogleDrivePath])
                    [[NSFileManager defaultManager] createDirectoryAtPath:strGoogleDrivePath withIntermediateDirectories:NO attributes:nil error:nil];
                
                NSString *strFinalPath = [documentsDirectory stringByAppendingPathComponent:@"GoogleDrive"];
                
                NSString *dataPath = [strFinalPath stringByAppendingPathComponent:self.driveFile.title];
                
                BOOL success = [[NSFileManager defaultManager] removeItemAtPath:dataPath error:nil];
                if(success)
                {
#if DEBUG
                    NSLog(@"Directory Removed Successfully");
#endif
                }
                
                if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                    [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
                
                filename = [dataPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",file.title]];
                
                
                
                Attachment *attachment = [[Attachment alloc]init];
                attachment.driveName = @"GoogleDrive" ;
                attachment.displayName = file.title ;
                attachment.localFilePath = filename ;
                attachment.indexPath = [NSString stringWithFormat:@"%d",kk];
                //attachment.localFolderName = strCurrentFolder ;
                //attachment.fileType = [arrFolderName lastObject] ;
                [marrOfFolderFiles addObject:attachment];
                attachment = nil ;
                
                
                [marrOfFolderFiles addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"GoogleDrive",@"DriveName",file.title,@"Title",filename,@"FilePath",[NSNumber numberWithInteger:kk],@"IndexPath", nil]];
                kk++;
                
                // Use a GTMHTTPFetcher object to download the file with authorization.
                NSURL *url = [NSURL URLWithString:file.downloadUrl];
                NSURLRequest *request = [NSURLRequest requestWithURL:url];
                GTMHTTPFetcher *fetcher = [GTMHTTPFetcher fetcherWithRequest:request];
                
                // Requests of user data from Google services must be authorized.
                fetcher.authorizer = self.driveService.authorizer;
                
                // The fetcher can save data directly to a file.
                fetcher.downloadPath = filename;
                
                
                [fetcher beginFetchWithCompletionHandler:^(NSData *data, NSError *error) {
                    
                    // Callback
                    if (error == nil) {
                        [alert dismissWithClickedButtonIndex:0 animated:YES];
                        
                        
                        // Successfully saved the file.
                        //
                        // Since a downloadPath property was specified, the data argument is
                        // nil, and the file data has been written to disk.
                        
                        
                        
                    } else {
                        [alert dismissWithClickedButtonIndex:0 animated:YES];
                        
#if DEBUG
                        NSLog(@"An error occurred: %@", error);
#endif
                        [Utilities showErrorMessageWithTitle:@"Unable to load file"
                                                           message:[error description]
                                                          delegate:self];
                        
                    }
                }];
            }
           
            if([marrOfFolderFiles count]>0)
            {
                for(int i =0 ; i<[marrOfFolderFiles count];i++)
                {
                    [marrToCheckAttachedOrDetached addObject:@"0"]; // Here Zero means not selected and One means selected//
                }
                
                NSArray *arrayOfKeys = [[DELEGATE glb_mdict_for_Attachement]allKeys];
                NSMutableArray *arrayOfValues = [[NSMutableArray alloc] init];
                for(NSString *str in arrayOfKeys)
                {
                if([str isEqualToString: self.driveFile.title])
                {
                    for(NSDictionary *dict in [[DELEGATE glb_mdict_for_Attachement]valueForKey:self.driveFile.title])
                    {
                        
                        [arrayOfValues addObject:dict];
                        
                    }
                }
                }
                
                
                for(int j =0; j<[arrayOfValues count];j++)
                {
                    NSString *str = [[arrayOfValues objectAtIndex:j]valueForKey:@"FilePath"];
                    for(int i =0; i<[marrOfFolderFiles count];i++)
                    {
                        NSString *strPath = [[marrOfFolderFiles objectAtIndex:i]valueForKey:@"FilePath" ];
                        if([str isEqualToString:strPath])
                        {
                            [marrToCheckAttachedOrDetached replaceObjectAtIndex:[[NSString stringWithFormat:@"%@",[[arrayOfValues objectAtIndex:j] valueForKey:@"IndexPath"] ]integerValue ] withObject:@"1"];
                        }
                    }
                    
                }
                [self.tbl_View reloadData];
            }
            
        }
        else
        {
            [Utilities showErrorMessageWithTitle:@"Unable to load file"
                                               message:[error description]
                                              delegate:self];
        }
    }];
    
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [marrOfFolderFiles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CustomCellFolderFilesList *cell = (CustomCellFolderFilesList *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil)
    {
        NSArray *cellArray = [[NSBundle mainBundle] loadNibNamed:@"CustomCellFolderFilesList" owner:self options:nil];
        cell = [cellArray lastObject];
    }
    cell.lbl_Heading.text = [[marrOfFolderFiles objectAtIndex:indexPath.row]valueForKey:@"Title"];
    cell.btn_Attachment.tag = indexPath.row;
    [cell.btn_Attachment addTarget:self action:@selector(clickAttachment:) forControlEvents:UIControlEventTouchUpInside];

    
    if([[marrToCheckAttachedOrDetached objectAtIndex:indexPath.row] isEqualToString:@"0"])
    {
        [cell.btn_Attachment setTitle:@"Attach" forState:UIControlStateNormal];
        [cell.btn_Attachment setSelected:NO];
    }
    else
    {
        [cell.btn_Attachment setTitle:@"Detached" forState:UIControlStateSelected];
        [cell.btn_Attachment setSelected:YES];
    }
    return cell;
}

- (void)clickAttachment:(UIButton *)sender
{
    
    NSMutableArray *arrOfTemp = nil;
    arrOfTemp = [[NSMutableArray alloc] init];

    
    if(![sender isSelected])
    {
    
        [sender setTitle:@"Detached" forState:UIControlStateNormal];
        [marrToCheckAttachedOrDetached replaceObjectAtIndex:[sender tag] withObject:@"1"];
        [sender setSelected:YES];

        [arrOfTemp removeAllObjects];

        int j =0;
        for(NSString *str in marrToCheckAttachedOrDetached)
        {
            if([str isEqualToString:@"1"])
            {
                [arrOfTemp addObject:[marrOfFolderFiles objectAtIndex:j]];
            }
            j++;
            
        }
        if([arrOfTemp count]>0)
        {
        [[DELEGATE glb_mdict_for_Attachement] setObject:arrOfTemp forKey:self.driveFile.title];
        }
    }
    else
    {
        [sender setTitle:@"Attach" forState:UIControlStateNormal];
        [sender setSelected:NO];

        [marrToCheckAttachedOrDetached replaceObjectAtIndex:[sender tag] withObject:@"0"];
        [arrOfTemp removeAllObjects];

        int j =0;
        for(NSString *str in marrToCheckAttachedOrDetached)
        {
            if([str isEqualToString:@"1"])
                {
                    [arrOfTemp addObject:[marrOfFolderFiles objectAtIndex:j]];
                }
            j++;
        
        }
        if([arrOfTemp count]>0)
        {
        [[DELEGATE glb_mdict_for_Attachement] setObject:arrOfTemp forKey:self.driveFile.title];
        }
        else
        {
            [[DELEGATE glb_mdict_for_Attachement] removeObjectForKey:self.driveFile.title];
        }
    }
}

@end

//
//  SplashViewController.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 11/04/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "SplashViewController.h"
#import "MainViewController.h"
#import "MyProfile.h"
#import "DownloadPictureOperation.h"
#import "School_Information.h"
#import "Defines.h"
#import "AppHelper.h"
#import "Service.h"
#import "School_Images.h"
@interface SplashViewController ()

@end

@implementation SplashViewController
@synthesize _temStr;
@synthesize _curentTimeTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //  NSNumber *appId=[[Service sharedInstance] nextAvailble:@"itemId" forEntityName:@"Item"];
        queueOperation = [[OperationQueue alloc] init];
        [queueOperation setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
        // Custom initialization
    }
    return self;
}
#pragma mark TimeTable
-(void)getTimeTableDidGot:(NSNotification*)note
{
#if DEBUG
    NSLog(@" Splash Screen  getTimeTableDidGot%@",note.userInfo);
#endif
    
    // [[AppDelegate getAppdelegate]hideIndicator];
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_TimeTable object:nil];
        
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            TableUpdateTime *tabl=[[Service sharedInstance] getlastUpdateTableDataInfoStored];
            tabl.timeTble=[NSNumber numberWithDouble:[[[note userInfo] valueForKey:@"LastSynchTime"]doubleValue]];
            for(NSDictionary *dict in [[[note userInfo] valueForKey:@"detials"] valueForKey:@"deleted"])
            {
                //MyClass *delClassobj=[[Service sharedInstance]getClassDataFromClassId:[dict objectForKey:@"Id"]];
                
                MyClass *delClassobj = [[Service sharedInstance] getClassDataRoom:[NSNumber numberWithInteger:[[dict objectForKey:@"RoomId"]integerValue]] period:[NSNumber numberWithInteger:[[dict objectForKey:@"PeriodId"]integerValue]] cycle:[NSNumber numberWithInteger:[[dict objectForKey:@"CycleDays"]integerValue]]classID:[NSNumber numberWithInteger:[[dict objectForKey:@"Id"]integerValue]] ];
                
                //check whether the subject os this class exist in another class or not.....if not then delete subject also
                if(delClassobj)
                    [[Service sharedInstance]deleteSubjectForClass:delClassobj];
                
                //finally delete the class
                if(delClassobj)
                    [[Service sharedInstance]deleteClassObject:delClassobj];
            }
            for(NSDictionary *dict in [[[note userInfo] valueForKey:@"detials"] valueForKey:@"deletedPeriods"])
            {
                Period *periodData=[[Service sharedInstance] getPeriodDataInfoStored:[dict objectForKey:@"Id"] postNotification:NO];
                NSError *error;
                
                if(periodData){
                    [[AppDelegate getAppdelegate].managedObjectContext deleteObject:periodData];
                    
                    if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
                    {
#if DEBUG
                        NSLog(@"Error deleting  - error:%@",error);
#endif
                    }
                }
            }

            for(NSDictionary *dict in [[note userInfo] valueForKey:@"timetable"]){
                [[Service sharedInstance ]parseTimeTableDataWithDictionary:dict];
            }
            for(NSDictionary *dict in [[note userInfo] valueForKey:@"timeTableCyle"]){
                [[Service sharedInstance ]parseTimeTableCycleDataWithDictionary:dict];
            }
            for(NSDictionary *dict in [[note userInfo] valueForKey:@"DayofNotes"]){
                [[Service sharedInstance ]parseDayOfNotesWithDictionary:dict];
                [[Service sharedInstance]updateTableForCycle:dict];

            }
            for(NSDictionary *dict in [[[note userInfo] valueForKey:@"detials"] valueForKey:@"periods"]){
                [[Service sharedInstance ]parsePeriodDataWithDictionary:dict];
            }
            for(NSDictionary *dict in [[[note userInfo] valueForKey:@"detials"] valueForKey:@"rooms"]){
                [[Service sharedInstance ]parseRoomDataWithDictionary:dict];
            }
            for(NSDictionary *dict in [[[note userInfo] valueForKey:@"detials"] valueForKey:@"sujects"]){
                [[Service sharedInstance ]parseSubjectDataWithDictionary:dict];
            }
            
#if DEBUG
            NSLog(@"getTimeTableDidGot [[[note userInfo] valueForKey:@detials] valueForKey:@class] %@",[[[note userInfo] valueForKey:@"detials"] valueForKey:@"class"]);
#endif

            for(NSDictionary *dict in [[[note userInfo] valueForKey:@"detials"] valueForKey:@"class"]){
                [[Service sharedInstance ]SyncMyClassDataWithServer:dict];
                //for classInfo Table
                
            }
            //            for(NSDictionary *dict in [[[note userInfo] valueForKey:@"detials"] valueForKey:@"deleted"])
            //            {
            //                MyClass *delClassobj=[[Service sharedInstance]getClassDataFromClassId:[dict objectForKey:@"Id"]];
            //
            //                //check whether the subject os this class exist in another class or not.....if not then delete subject also
            //                if(delClassobj)
            //                    [[Service sharedInstance]deleteSubjectForClass:delClassobj];
            //
            //                //finally delete the class
            //                if(delClassobj)
            //                    [[Service sharedInstance]deleteClassObject:delClassobj];
            //            }
         
        }
        else{
            
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Error"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
    else{
        [[AppDelegate getAppdelegate]hideIndicator];
        
    }
       [self getImageOfSchool];
}

-(void)getTimeTableDetals{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getTimeTableDidGot:) name:NOTIFICATION_TimeTable object:nil];
        
        
        [[Service sharedInstance]getTimeTableDetailsofSchool];
        
    }
    else{
    }
    
}

#pragma mark Download School Image
-(void)downloadSchoolImage{
    NSArray *arr=[[Service sharedInstance]getNotDownloadedImage:nil];
    
    if (arr.count>0) {
        for(School_Images *image in arr){
            DownloadPictureOperation *pictOp = [[DownloadPictureOperation alloc] initWithMode:UIViewContentModeScaleAspectFill];
            
            
            NSString *strng=image.image_url;
            //07/08/2013
            strng=[strng stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [pictOp setThumbURL:[NSURL URLWithString:strng]];
            
            NSMutableDictionary *storedObject = [[NSMutableDictionary alloc] init];
            // [storedObject setObject:nil forKey:@"cellImageView"];
            [storedObject setObject:image forKey:@"model"];
            //[storedObject setObject:nil forKey:@"imageKey"];
            NSRange rang=[image.image_url rangeOfString:@"data"];
            NSRange rang1=[image.image_url rangeOfString:@"images"];
            NSString *   str=[image.image_url substringWithRange:NSMakeRange( rang.location, rang1.location+rang1.length-rang.location)];
            str=[NSString stringWithFormat:@"Documents/%@",str];
            
            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:str];
            [storedObject setObject:dataPath1 forKey:@"str"];
            [pictOp setStoredObject:storedObject];
            [storedObject release];
            [queueOperation addOperation:pictOp];
            [pictOp release];
        }
        
    }
}
-(void)getImageOfSchool{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSchoolImageDidGot:) name:NOTIFICATION_SchoolImage object:nil];
        
        [[Service sharedInstance]getSchoolImage];
        
    }
    else{
        
    }
    
}

-(void)getSchoolImageDidGot:(NSNotification*)note
{
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_SchoolImage object:nil];
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            TableUpdateTime *tabl=[[Service sharedInstance] getlastUpdateTableDataInfoStored];
            tabl.empowerContent=[NSNumber numberWithDouble:[[[note userInfo] valueForKey:@"LastSynchTime"]doubleValue]];
            if (![[[note userInfo] valueForKey:@"images"] isKindOfClass:[NSNull class]]) {
                for(NSDictionary *dict in [[note userInfo] valueForKey:@"images"]){
                    [[Service sharedInstance]ParseSchoolImageData:dict];
                }
              
            }
            
            
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
    else{
        [[AppDelegate getAppdelegate]hideIndicator];
        
    }
#if DEBUG
    NSLog(@"Called From [self downloadSchoolImage] Splash View controller Method- getSchoolImageDidGot ");
#endif

    [self downloadSchoolImage];
    //----- Modified By Mitul To Restrict Content Webservice Call -----//
//    self._temStr=@"Panel";
//    [self getDetailshOfSchool:@"Panel"];
    self._temStr=@"SchoolContent";
    [self getDetailshOfSchool:@"SchoolContent"];
    //--------- By Mitul ---------//
}

#pragma mark ShcoolContent
-(void)getDetailshOfSchool:(NSString*)Type
{

#if DEBUG
    NSLog(@"Called from Splash View Controller  getDetailshOfSchool");
#endif
    

    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getSchoolInfoDidGot:) name:NOTIFICATION_SchoolInfo object:nil];
        
        
        [[Service sharedInstance]getSchoolInformation:Type];
        
    }
    else{
        [[AppDelegate getAppdelegate] hideIndicator];
        
        //[AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
    
}

-(void)getSchoolInfoDidGot:(NSNotification*)note
{

#if DEBUG
    NSLog(@"Splash Screen getSchoolInfoDidGot %@",note.userInfo);
#endif
    
    if (note.userInfo) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_SchoolInfo object:nil];
        for(NSDictionary *dict in [[note userInfo] valueForKey:@"deleted"]){
            School_Information *school= [[Service sharedInstance]getSchoolInfoDataInfoStored:[dict objectForKey:@"Id"] postNotification:NO];
            
#if DEBUG
            NSLog(@"Dictionary %@",dict);
#endif
            
            NSError *error;
            
            if(school)
                [[AppDelegate getAppdelegate].managedObjectContext deleteObject:school];
            
            if (![[AppDelegate getAppdelegate].managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Error deleting  - error:%@",error);
#endif
            }
        }

        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            for(NSDictionary *dict in [[note userInfo] valueForKey:@"result"])
            {
                [[Service sharedInstance]ParseSchoolInformatio:dict];
            }
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Status"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
        
        
        //----- Modified By Mitul To Restrict Content Webservice Call -----//
        /*if([self._temStr isEqualToString:@"Panel"]){
            self._temStr=@"SchoolContent";
            [self getDetailshOfSchool:@"SchoolContent"];
            
        }
        else if([self._temStr isEqualToString:@"SchoolContent"]){
            self._temStr=@"Empower";
            [self getDetailshOfSchool:@"Empower"];
            
        }
        //        else if([self._temStr isEqualToString:@"Empower"]){
        //            self._temStr=@"About";
        //
        //            [self getDetailshOfSchool:@"About"];
        ////            TableUpdateTime *tabl=[[Service sharedInstance] getlastUpdateTableDataInfoStored];
        ////            tabl.about=[NSNumber numberWithDouble:[[[Service sharedInstance]miliSecondFromDateMethod:[NSDate date]] doubleValue]];
        //
        //
        //        }
        else {*/
            TableUpdateTime *tabl=[[Service sharedInstance] getlastUpdateTableDataInfoStored];
            tabl.about=[NSNumber numberWithDouble:[[[note userInfo] valueForKey:@"LastSynchTime"]doubleValue]];
            _viewForIndicator.hidden=YES;
            [[AppDelegate getAppdelegate] hideIndicator];
            if (_myTimer1 != nil){
                [_myTimer1 invalidate];
                _myTimer1= nil;
            }
            _myTimer1 = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                         target: self
                                                       selector:@selector(updateProgress:)
                                                       userInfo: nil repeats:NO];
        //}
        //--------- By Mitul ---------//

    }
    }
#pragma mark Userlogin Again
-(void)userLoginAgain{
    if (([AppHelper userDefaultsForKey:kEmailKey])&&([AppHelper userDefaultsForKey:kPasswordKey]))
    {
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidRegister:) name:NOTIFICATION_Login object:nil];
    
    [dict setObject:[AppHelper userDefaultsForKey:kEmailKey] forKey:@"email_id"];
    [dict setObject:[AppHelper userDefaultsForKey:kPasswordKey] forKey:@"password"];
    if([AppHelper userDefaultsForKey:@"user_id"])
    {
    [dict setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
    }
        
        NSString *str_Domain = [AppHelper userDefaultsForKey:kDomain];
        if([str_Domain length]>0)
        {
    [dict setObject:[AppHelper userDefaultsForKey:kDomain] forKey:@"domain"];
        }
    [dict setObject:@"iPad" forKey:@"device_type"];
        
        
    // [[AppDelegate getAppdelegate] showIndicator:self.view];
    [[Service sharedInstance]registrationWithUser:dict];
    [dict release];
    }
    else{
          [self getTimeTableDetals];
    }
    
}
-(void)userDidRegister:(NSNotification*)note
{
    if (note.userInfo)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_Login object:nil];
        
        if ([[[note userInfo] valueForKey:@"ErrorCode"] integerValue]==1)
        {
            //for parse profile data in core data user_id
            
            if([[NSUserDefaults standardUserDefaults] objectForKey:vserionUpdateCampusBasedSchool] == nil)
            {
                // Campus Based Migration Start
                [[Service sharedInstance]deleteAllTablesData];
                [[Service sharedInstance] deleteAllObjects:@"School_detail"];
                [[Service sharedInstance] deleteAllObjects:@"MyProfile"];

                [[Service sharedInstance]parseProfileDataWithDictionary:[note userInfo]];

                [[Service sharedInstance]parseSchoolDetailDataWithDictionary:[[note userInfo]objectForKey:@"school_details" ]];

                // Campus Based Migration End
                
                
                NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
                
                if (standardUserDefaults) {
                    [standardUserDefaults setObject:@"DBUpdated" forKey:vserionUpdateCampusBasedSchool];
                    [standardUserDefaults synchronize];
                }
            }
            else
            {
                //for parse school detail
                [[Service sharedInstance]parseSchoolDetailDataWithDictionary:[[note userInfo]objectForKey:@"school_details" ]];
                //12/AUG/2013
                TableUpdateTime *tabl=[[Service sharedInstance] getlastUpdateTableDataInfoStored];
                tabl.schoolInfo=[NSNumber numberWithDouble:[[[Service sharedInstance]miliSecondFromDateMethod:[NSDate date]] doubleValue]];

            
            }
            
            
        }
        else{
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:[[note userInfo] valueForKey:@"Error"] delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
//       dispatch_async( dispatch_get_main_queue(), ^{
//        // Add code here to update the UI/send notifications based on the
//        // results of the background processing
//        [self getTimeTableDetals];
//    });
    
    [self getImageOfSchool];
}

-(void)getCureentTimeTable:(NSDate*)fromDate{
    [_globlDateFormat setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    
    NSArray *arr=[[Service sharedInstance]getStoredAllTimeTableData];
    NSMutableArray *arrFortime=[[NSMutableArray alloc]init];
    for(TimeTable *time in arr){
        int timeDiff=(int)[fromDate timeIntervalSinceDate:time.startDate];
        if(timeDiff>=0)
        {
            [arrFortime addObject:time];
        }
    }
    if(arrFortime.count>0){
        if(arrFortime.count>1){
            _isFirstSem=NO;
            self._curentTimeTable=[arr objectAtIndex:1];
        }
        else{
            self._curentTimeTable=[arr firstObject];
            
            _isFirstSem=YES;
        }
    }
    else{
        self._curentTimeTable=nil;
    }
    
    [arrFortime release];
   
}



#pragma mark ViewLifeCycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    if(schoolDetailObj.school_BgImge){
        _imgViewForSchoolImg.image=[UIImage imageWithData:schoolDetailObj.school_BgImge];
        _imgViewForSchoolImg.backgroundColor=[UIColor clearColor];
    }
    _globlDateFormat = nil;
    _globlDateFormat=[[NSDateFormatter alloc]init ];
}

-(void)userSyncAgain{
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
       
        [self performSelector:@selector(userLoginAgain) withObject:nil afterDelay:0.1];
    }
    else{
        if (_myTimer1 != nil){
            [_myTimer1 invalidate];
            _myTimer1= nil;
        }
        _myTimer1 = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                     target: self
                                                   selector:@selector(updateProgress:)
                                                   userInfo: nil repeats:NO];
        
    }
}
-(void)viewDidAppear:(BOOL)animated{
    
    if([AppHelper userDefaultsForKey:@"local"])
    {
        _viewForIndicator.hidden=NO;
        [self performSelector:@selector(userSyncAgain) withObject:nil afterDelay:2.0];
    }
    else
    {
        if (_myTimer1 != nil){
            [_myTimer1 invalidate];
            _myTimer1= nil;
        }
        _myTimer1 = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                     target: self
                                                   selector:@selector(updateProgress:)
                                                   userInfo: nil repeats:NO];
    }
    
    [AppHelper saveToUserDefaults:[AppHelper userDefaultsForKey:@"user_id"] withKey:@"local"];
    
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    //08-08-13
    NSString *yearStr=@"";
    if (profobj.year!=nil) {
        yearStr=profobj.year;
    }
    if(profobj.last_name!=nil){
      _lableSchoolProfile.text=[NSString stringWithFormat:@"%@ %@ \n%@\n%@",profobj.name,profobj.last_name,profobj.email,yearStr];
    }
    else{
        _lableSchoolProfile.text=[NSString stringWithFormat:@"%@\n%@\n%@",profobj.name,profobj.email,yearStr]; 
    }
   
    // Do any additional setup after loading the view from its nib.
    //  self._infolable.text=[NSString stringWithFormat:@"Select your city location from \n\n the drop down list. This will \n \n affect the prayer timings and \n \n your compass Qibla direction.",nil];
    _lableSchoolProfile.numberOfLines =0;
    CGSize constraint = CGSizeMake(_lableSchoolProfile.frame.size.width, 1000.0f);
    
    _lableSchoolProfile.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize size = [_lableSchoolProfile.text sizeWithFont:_lableSchoolProfile.font constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    
    [_lableSchoolProfile setFrame:CGRectMake(_lableSchoolProfile.frame.origin.x, _lableSchoolProfile.frame.origin.y, _lableSchoolProfile.frame.size.width,MAX(round(size.height), 40.0))];
    
    // Flurry : Crash Log Fix for setObjectForKey: object cannot be nil (key: model)
    if (profobj)
    {
        if(profobj.image==nil)
        {
            DownloadPictureOperation *pictOp = [[DownloadPictureOperation alloc] initWithMode:UIViewContentModeScaleAspectFill];
            
            NSString *strng =[NSString stringWithFormat:@"%@%@",pic_BaseUrl,profobj.image_url];
            //NSString *strng=@"http://www.productdynamics.expensetrackingapplication.com/UserProfileImages/pic.jpg";
            
            [pictOp setThumbURL:[NSURL URLWithString:strng]];
            
            NSMutableDictionary *storedObject = [[NSMutableDictionary alloc] init];
            [storedObject setObject:_userImageView forKey:@"cellImageView"];
            [storedObject setObject:profobj forKey:@"model"];
            [storedObject setObject:@"image" forKey:@"imageKey"];
            
            [pictOp setStoredObject:storedObject];
            [storedObject release];
            [queueOperation addOperation:pictOp];
            [pictOp release];
        }
        else{
            _userImageView.image=[UIImage imageWithData:profobj.image];
        }
    }
    
    
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    if(schoolDetailObj.school_BgImge){
        _imgViewForSchoolImg.image=[UIImage imageWithData:schoolDetailObj.school_BgImge];
        _imgViewForSchoolImg.backgroundColor=[UIColor clearColor];
    }
    else{
        
        // Flurry : Crash Log Fix for setObjectForKey: object cannot be nil (key: model)
        if (schoolDetailObj)
        {
            if(schoolDetailObj.theme_color!=nil){
                _imgViewForSchoolImg.backgroundColor=[AppHelper colorFromHexString:schoolDetailObj.theme_color];
            }
            
            DownloadPictureOperation *pictOp = [[DownloadPictureOperation alloc] initWithMode:UIViewContentModeScaleAspectFill];

            NSString *strng =[NSString stringWithFormat:@"%@%@",pic_BaseUrlForSchoolLogo,schoolDetailObj.school_BgImageUrl];
            
            //NSString *strng=@"http://www.productdynamics.expensetrackingapplication.com/UserProfileImages/pic.jpg";
            [pictOp setThumbURL:[NSURL URLWithString:strng]];
            
            NSMutableDictionary *storedObject = [[NSMutableDictionary alloc] init];
            [storedObject setObject:_imgViewForSchoolImg forKey:@"cellImageView"];
            [storedObject setObject:schoolDetailObj forKey:@"model"];
            [storedObject setObject:@"school_BgImge" forKey:@"imageKey"];
            
            [pictOp setStoredObject:storedObject];
            [storedObject release];
            [queueOperation addOperation:pictOp];
            [pictOp release];
        }
    }
    
    _lableForYear.text=schoolDetailObj.dairy_Titled;
    _lblForSchollName.text=schoolDetailObj.display_Name;
    _lableSchoolMoto.text=schoolDetailObj.motto;
    
}


-(void)updateProgress:(NSString*)time
{
    if (_myTimer1 != nil){
        [_myTimer1 invalidate];
        _myTimer1= nil;
    }
    MainViewController *viewController=[[MainViewController alloc]initWithNibName:@"MainViewController" bundle:nil];
    [AppDelegate getAppdelegate]._currentViewController=viewController;
    [self.navigationController pushViewController:viewController animated:NO];
    [viewController release];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    if (_myTimer1 != nil){
        [_myTimer1 invalidate];
        _myTimer1= nil;
    }
    [_temStr release];
    [queueOperation release];
    [_lableSchoolMoto release];
    [_lableSchoolProfile release];
    [_userImageView release];
    [_lableForYear release];
    [_imgViewForLogoImg release];
    [_imgViewForSchoolImg release];
    [_lblForSchollName release];
    [_viewForIndicator release];
    if(_globlDateFormat)
    {
        [_globlDateFormat release];
        _globlDateFormat = nil;
    }
    if(self._curentTimeTable)
    {
        _curentTimeTable = nil;
    }
    
    [super dealloc];
}
- (void)viewDidUnload {
    [_lableSchoolMoto release];
    _lableSchoolMoto = nil;
    [_lableSchoolProfile release];
    _lableSchoolProfile = nil;
    [_userImageView release];
    _userImageView = nil;
    [_lableForYear release];
    _lableForYear = nil;
    [_imgViewForLogoImg release];
    _imgViewForLogoImg = nil;
    [_imgViewForSchoolImg release];
    _imgViewForSchoolImg = nil;
    [_lblForSchollName release];
    _lblForSchollName = nil;
    [_viewForIndicator release];
    _viewForIndicator = nil;
    if(_globlDateFormat)
    {
        [_globlDateFormat release];
        _globlDateFormat = nil;
    }
    if(self._curentTimeTable)
    {
        _curentTimeTable = nil;
    }
    [super viewDidUnload];
}
#pragma mark
#pragma mark autorotation delegate

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeRight ;
}

-(BOOL)shouldAutorotate{
    return NO;
}



///
@end

//
//  Operation.m
//  Pepsi
//
//  Created by Admin on 31.03.10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "Operation.h"


@implementation Operation

@synthesize isExecuting = _isExecuting;
@synthesize isFinished = _isFinished;
@synthesize ownerQueue;

- (BOOL)isConcurrent {
    return YES;
}

- (void)cancelOperations:(NSString *)errorMessage 
{
	[self.ownerQueue cancelAllOperations];
	[self.ownerQueue setDidFailInQueueOperation:YES];
	[self.ownerQueue setFailOperationMessage:errorMessage];
}

- (void)start
{
	if ([self isCancelled]) {
		[self finish];
		return;
	}

    if (![NSThread isMainThread]) {
        [self performSelectorOnMainThread:@selector(start) withObject:nil waitUntilDone:NO];
        return;
    }
    
    [self willChangeValueForKey:@"isExecuting"];
    _isExecuting = YES;
    [self didChangeValueForKey:@"isExecuting"];
	
	[self createTask];
}

- (void)finish {	
	[self.ownerQueue setNumberExpectedOperations:self.ownerQueue.numberExpectedOperations - 1];
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];
	
    _isExecuting = NO;
    _isFinished = YES;
	
    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];

	
}

- (void)createTask 
{
}


- (void)dealloc {
    [super dealloc];
}

@end

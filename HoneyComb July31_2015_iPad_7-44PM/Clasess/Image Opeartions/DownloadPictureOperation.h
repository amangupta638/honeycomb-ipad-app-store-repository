//
//  DownloadPictureOperation.h
//

#import <Foundation/Foundation.h>
#import "Operation.h"

@interface DownloadPictureOperation : Operation 
{
	NSURL *thumbURL;
	id storedObject;
	NetManager *globalNetManager;
	UIViewContentMode mode;
	int num;
}

@property (nonatomic, retain) NSURL *thumbURL;
@property (nonatomic, retain) id storedObject;
@property (nonatomic, assign) UIViewContentMode mode;
- (id) initWithMode:(UIViewContentMode)contentMode;
- (id) initWithMode:(UIViewContentMode)contentMode whichImage:(int)code;
@end

@interface UIImage (imageWithShadow) 

- (UIImage *)imageWithShadow;	
- (UIImage *)imageWithRoundedRect:(double)radius;

@end

//
//  CalendarViewControllerDelegate.h
//  Sorted
//
//  Created by Lloyd Bottomley on 30/04/10.
//  Copyright 2010 Savage Media Pty Ltd. All rights reserved.
//

@class calenderVC;

@protocol CalendarViewControllerDelegate

- (void)calendarViewController:(calenderVC *)aCalendarViewController dateDidChange:(NSDate *)aDate;

@end

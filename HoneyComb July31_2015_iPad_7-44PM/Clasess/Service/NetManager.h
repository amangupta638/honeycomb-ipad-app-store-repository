//
//  NetManager.h
//  PlacesYellHybrid
//
//  
//

#import <Foundation/Foundation.h>

#import "Reachability.h"
@class ASIFormDataRequest;
@interface NetManager : NSObject {
	
	id acceptTarget;
	NSString *finishLoadingSelector;
	
	NSURLConnection *actualConnection;
    id request;
	
	bool shouldShowErrorMessage;
	
	//some object, that you want to update after finish loading
	id storedObject;
	NSError *error;
	
	NSDictionary *parameters;
	NSURL *url;
	Reachability *reachability;
}
@property (nonatomic, retain)NSMutableDictionary *aDic;

@property (nonatomic, retain) id request;
@property (nonatomic, assign) id acceptTarget;
@property (nonatomic, retain) NSString *finishLoadingSelector;
@property (nonatomic, retain) NSURLConnection *actualConnection;
@property (nonatomic, retain) NSDictionary *parameters;

@property (nonatomic, readonly) NSError *error;

@property (nonatomic, retain) id storedObject;

@property (nonatomic, retain) NSURL *url;

- (id)initWithURL:(NSURL *)url 
				 target:(id)thisTarget 
  finishLoadingSelector:(NSString *)thisSelector 
		 postParameters:(NSDictionary *)parameters
	   showErrorMessage:(BOOL)thisShowErrorMessage 
		   storedObject:(id)thisObject;

- (id)initWithURL:(NSURL *)url 
					 target:(id)thisTarget 
	  finishLoadingSelector:(NSString *)thisSelector 
			 postParameters:(NSDictionary *)parameters
		   showErrorMessage:(BOOL)thisShowErrorMessage;

- (void) startRequest;
-(void)stopRequest;
- (void)showLoadErrorMessage:(NSString *)thisMessage;

- (void)updateReachabilityStates;

- (void) enableNotifications;
- (NSError *)makeErrorLocalizable:(NSError *) netError;

- (BOOL)networkReachable;


@end

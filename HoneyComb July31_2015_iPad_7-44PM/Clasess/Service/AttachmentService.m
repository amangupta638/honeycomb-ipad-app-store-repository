//
//  AttachmentService.m
//  StudyPlaner
//
//  Created by Nandini Tomke on 27/12/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//




 //http://54.252.159.156/zend/html/index.php/webservicev2/attachmentsync?
#import "AttachmentService.h"
#import "ASIFormDataRequest.h"
#import "Attachment.h"
#import "JSON.h"
#import "Attachment.h"

@implementation AttachmentService

+ (AttachmentService*) sharedInstance
{
	static AttachmentService* singleton;
    
	if (!singleton)
	{
		singleton = [[AttachmentService alloc] init];
		
	}
	return singleton;
}

- (void) postAttachmentsForDiaryItem:(NSString *)diaryItemId schoolId :(NSString *)sId userId :(NSString *)uId andAttachmentDictionary :(Attachment *) attachment isCopyDI:(BOOL)isCopyDI {
    
    NSString *strURL = @"";
    if (diaryItemId != nil) {
        strURL = [@"diaryItemId=" stringByAppendingString:diaryItemId] ;
    }
    
    if (sId != nil) {
        strURL = [strURL stringByAppendingString:@"&schoolId="] ;
        strURL = [strURL stringByAppendingString:sId] ;
        
    }
    
    if (uId != nil) {
        strURL = [strURL stringByAppendingString:@"&userId="] ;
        strURL = [strURL stringByAppendingString:uId] ;
        
    }
    
    if ([attachment.type isEqualToString:@"gdoc"] && attachment.filePath != nil )
    {
        strURL = [strURL stringByAppendingString:@"&url="] ;
        strURL = [strURL stringByAppendingString:attachment.filePath] ;
    }
    else
    {
        strURL = [strURL stringByAppendingString:@"&url="] ;
        strURL = [strURL stringByAppendingString:@""] ;
    }
    
    strURL = [AttachmentURL stringByAppendingString:strURL];
    
    if( [attachment.itemtype length]!=0 && ![attachment.itemtype isEqualToString:@""] && ![attachment.itemtype isKindOfClass:[NSNull class]] && attachment.itemtype !=nil)
    {
        strURL = [strURL stringByAppendingString:@"&itemType="] ;
        strURL = [strURL stringByAppendingString:attachment.itemtype] ;
    }
    /// AppType new parameter added for WebLog
    strURL = [strURL stringByAppendingString:@"&AppType="] ;
    strURL = [strURL stringByAppendingString:APP_Key];
    
    if (isCopyDI)
    {
        NSString *localAttachmentId = [[NSString stringWithFormat:@"%d",[attachment.localAttachmentId integerValue]] stringByAppendingString:@"_"];
        
        localAttachmentId = [localAttachmentId stringByAppendingString:diaryItemId];

        
        strURL = [strURL stringByAppendingString:@"&localAttachmentId="];
        strURL = [strURL stringByAppendingString:localAttachmentId];
        
        strURL = [strURL stringByAppendingString:@"&remoteFilePath="];
        if (attachment.filePath.length)
        {
            strURL = [strURL stringByAppendingString:attachment.filePath];
        }
        else
        {
            strURL = [strURL stringByAppendingString:attachment.remoteFilePath];

        }
        
    }
    
    NSLog(@"strURL = %@ ",strURL);
    
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:strURL]];
    [request setNumberOfTimesToRetryOnTimeout:2];
    [request setDelegate:self];
    [request setTimeOutSeconds:60];
    [request setPostValue:@"This is sample text..." forKey:@"text"];
    NSString *contentType = @"image/jpeg";
    
    //for (Attachment *attachment in marrToAttach) {
    if ([attachment.type isEqualToString:@"jpg"] || [attachment.type isEqualToString:@"png"])
    {
        contentType=@"image/jpeg";
    }
    else if ([attachment.type isEqualToString:@"xlsx"] || [attachment.type isEqualToString:@"xls"])
    {
        contentType=@"application/excel";
    }
    else if ([attachment.type isEqualToString:@"docx"] || [attachment.type isEqualToString:@"doc"])
    {
        contentType=@"application/msword";
    }
    else if ([attachment.type isEqualToString:@"txt"])
    {
        contentType=@"text/plain";
    }
    else if ([attachment.type isEqualToString:@"pdf"])
    {
        contentType=@"application/pdf";
    }
    // gdoc Implementation
    // gDoc included to support google documents and all other google drive doc,excel,etc. files
    else if ([attachment.type isEqualToString:@"gdoc"]|| [attachment.type isEqualToString:@".gdoc"])
    {
        contentType=@"gdoc";
    }
    
    
    NSString *key = [[NSString stringWithFormat:@"%d",[attachment.localAttachmentId integerValue]] stringByAppendingString:@"_"];
   
    key = [key stringByAppendingString:diaryItemId];
    [request addData:attachment.fileData withFileName:attachment.displayFileName andContentType:contentType forKey:key];
    // }
    
    [request startAsynchronous];
    
}


#pragma mark
#pragma mark ASIHttpDelegate Methods

- (void)requestFinished:(ASIHTTPRequest *)request {
	
	NSString *receivedString = [request responseString];

    NSDictionary *jsonDictD = [receivedString JSONValue];
#if DEBUG
    NSLog(@"jsonDictD %@",jsonDictD);
#endif

    dispatch_async(dispatch_get_main_queue(), ^{
        [[Service sharedInstance] parseAttachmentData:jsonDictD];

    });
}

- (void)requestFailed:(ASIHTTPRequest *)request {
	
	NSString *receivedString = [request responseString];

//    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Message" message:receivedString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//	[alertView show];
//	[alertView release];
}


@end

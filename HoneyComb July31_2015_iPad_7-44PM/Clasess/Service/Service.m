


//
//  Service.m
//  Modizo
//
//  Created by AppStudioz on 13/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//Aman
#import "Service.h"

#import "OperationQueue.h"

#import "NetManager.h"
#import "StringTransformer.h"
#import "AppDelegate.h"
#import "JSON.h"
#import "Item.h"
#import "Defines.h"
#import "AppHelper.h"
#import "MyProfile.h"
#import "MyParent.h"
#import "MyTeacher.h"
#import "Country.h"
#import "States.h"
#import "Annoncements.h"
#import "EventItem.h"
#import "Subjects.h"
#import "School_detail.h"
#import "MyClass.h"
#import "Student.h"
#import "School_Images.h"
#import "AppDelegate.h"
#import "Grade.h"
#import "News_Attachment.h"
#import "DiaryItemTypes.h"
#import "Inbox_Users.h"
#import "Attachment.h"
#import "SchoolInformation.h"
#import "Flurry.h"
#import "Report.h"
#import "DownloadPictureOperation.h"
// Campus Based Migration Start
#import "LoginViewController.h"
// Campus Based Migration End

//Merit - Start
#import "Merit.h"
#import "FileUtils.h"
#import "SSKeychain.h"
//Merit - End

@implementation Service

@synthesize netManager;
+ (Service*) sharedInstance
{
	static Service* singleton;
    
	if (!singleton)
	{
		singleton = [[Service alloc] init];
		
	}
	return singleton;
}

- (id)init
{
    self = [super init];
    if (self) {
        _managedObjectContext = [[AppDelegate getAppdelegate] managedObjectContext];
    }
    
    return self;
}


#pragma mark for appid
-(NSDate*)dateFromMilliSecond:(NSNumber*)milliSeconds
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:([milliSeconds doubleValue] / 1000)];
    
    return date;
}
-(NSString*)miliSecondFromDateMethod:(NSDate*)date
{
    
    double milliseconds = 1000.0 * [date timeIntervalSince1970];
    
    
    NSString *strcre=[NSString stringWithFormat:@"%f",milliseconds];
    NSArray *aa=[strcre componentsSeparatedByString:@"."];

    return [aa objectAtIndex:0];
}
-(NSNumber *)nextAvailble:(NSString *)idKey forEntityName:(NSString *)entityName{
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *moc = _managedObjectContext;
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:moc];
    
    [request setEntity:entity];
    
    NSArray *propertiesArray = [[NSArray alloc] initWithObjects:idKey, nil];
    [request setPropertiesToFetch:propertiesArray];
    [propertiesArray release], propertiesArray = nil;
    
    NSSortDescriptor *indexSort = [[NSSortDescriptor alloc] initWithKey:idKey ascending:YES];
    NSArray *array = [[NSArray alloc] initWithObjects:indexSort, nil];
    [request setSortDescriptors:array];
    [array release], array = nil;
    [indexSort release], indexSort = nil;
    
    NSError *error = nil;
    NSArray *results = [moc executeFetchRequest:request error:&error];
    NSManagedObject *maxIndexedObject = [results lastObject];
    [request release], request = nil;
    
    NSInteger myIndex = 1;
    if (maxIndexedObject)
    {
        myIndex = [[maxIndexedObject valueForKey:idKey] integerValue] + 1;
    }

    return [NSNumber numberWithInteger:myIndex];
}
- (NSEntityDescription *) makeEntityDescription: (NSString *)name
{
    NSEntityDescription* descr = [NSEntityDescription entityForName:name
											 inManagedObjectContext:_managedObjectContext];
    return descr;
    
}


- (NSFetchRequest *)getBasicRequestForEntityName: (NSString *)entityName
{
	NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    
	NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:_managedObjectContext];
	[request setEntity:entity];
    
	return request;
}


#pragma mark-Delete Dairy Item


- (void) deleteObjectOfDairyItem:(NSNumber*)Item_id
{
    NSError *error;
    NSManagedObject *eventToDelete = [self getItemDataInfoStored:Item_id  postNotification:NO];
    if (eventToDelete)
    {
        [_managedObjectContext deleteObject:eventToDelete];
    }
    
    
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif
    }
}
- (EventItem *)getEventItemDataInfoStoredForDelete:(NSNumber*)itemId postNotification:(BOOL)postNotification
{
   
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"EventItem"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_Id == %@", itemId];
        [request setPredicate:predicate];
        
        NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        results =  [[NSSet setWithArray:results] allObjects];
    
        if([results count]==0)
        {
            NSFetchRequest *request = [self getBasicRequestForEntityName:@"EventItem"];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"universal_id == %@", itemId];
            [request setPredicate:predicate];
            
            NSError *error = nil;
            results = [_managedObjectContext executeFetchRequest:request error:&error];
            results =  [[NSSet setWithArray:results] allObjects];

        
        
        }
    
    
        
        NSMutableArray* filterResults = [[NSMutableArray alloc] init];
        BOOL copy;
        
        
        if (![results count] == 0) {
            for (EventItem *a1 in results) {
                copy = YES;
                for (EventItem *a2 in filterResults) {
                    if ( [a1.universal_id integerValue] == [a2.universal_id integerValue] ) {
                        copy = NO;
                        break;
                    }
                }
                if (copy) {
                    [filterResults addObject:a1];
                }
            }
        }

         results = [self getActiveEvents:filterResults];

        
        
        EventItem  * addEventItemData  = nil;
        if (!error && [results count] > 0) {
            
            addEventItemData = [results objectAtIndex:0];
        }
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
        }
        
        return addEventItemData;
}

#pragma mark-Delete Newz and deleted Announcment
- (void) deleteObjectOfNewz:(NSNumber*)Item_id
{
    NSError *error;
    NSManagedObject *eventToDelete = [self getNewsDataInfoStored:Item_id postNotification:NO];
    if (eventToDelete) {
        [_managedObjectContext deleteObject:eventToDelete];
    }
    
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif
      
    }
}

- (void) deleteObjectOfAnnouncment:(NSNumber*)Item_id
{
    NSError *error;
    NSManagedObject *eventToDelete = [self getAnnoncementsDataInfoStored:Item_id postNotification:NO];
    if (eventToDelete)
    {
        [_managedObjectContext deleteObject:eventToDelete];
    }
    
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif
    }
}


- (void) deleteObjectOfEventDairyItem:(NSNumber*)Item_id
{
    NSError *error;
    NSManagedObject *eventToDelete = [self getEventItemDataInfoStoredForDelete:Item_id postNotification:NO];
    if (eventToDelete)
    {
        [_managedObjectContext deleteObject:eventToDelete];
    }
    
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif
      
    }
}

-(void)deleteDairyItems:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_deletDairyItem;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        if ([dict objectForKey:@"id"] ) {
            [parameters setObject:[dict objectForKey:@"id"]forKey:@"Id"];
        }
        
        if ([dict objectForKey:@"type"] ) {
            [parameters setObject:[dict objectForKey:@"type"]forKey:@"type"];
        }
        
        if ([dict objectForKey:@"isdeleteTaskBasedMerit"] ) {
            [parameters setObject:[dict objectForKey:@"isdeleteTaskBasedMerit"]forKey:@"isdeleteTaskBasedMerit"];
        }
        
        if ([dict objectForKey:@"deletedTaskBasedMerits"] ) {
            [parameters setObject:[dict objectForKey:@"deletedTaskBasedMerits"]forKey:@"deletedTaskBasedMerits"];
        }
        
        
        // Webservice Logging
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        
        // New
        [parameters setObject:APP_Key forKey:kKeyAppType];
#if DEBUG
        NSLog(@"urlString %@",urlString);
        NSLog(@"deleteDairyItems -parameters-%@",[parameters JSONRepresentation]);
#endif
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishDeleteDiaryTtems:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
		//[AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
	}
}

- (void)netManagerDidFinishDeleteDiaryTtems:(NetManager *)thisNetManager
                                   response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
        
          [Flurry logError:FLURRY_EVENT_Error_in_deleting_Diary_Item message:FLURRY_EVENT_Error_in_deleting_Diary_Item error:thisNetManager.error];
        
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_DeleteDairyItem object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}







#pragma mark- Diary Item
-(NSArray*)getStoredAllDueItemDataTime:(NSDate*)fromdate toDate:(NSDate*)todate assign:(NSDate*)dueDtae
{
    if(fromdate!=nil && todate!=nil && dueDtae!=nil)
    {
        
   
        NSDate *startDueDate = dueDtae;
        NSDate *endDueDate = dueDtae;
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:dueDtae];
        [components setHour:0];
        [components setMinute:0];
        [components setSecond:0];
        startDueDate = [calendar dateFromComponents:components];
        
        [components setHour:23];
        [components setMinute:59];
        [components setSecond:59];
        endDueDate = [calendar dateFromComponents:components];
    
    //1july done
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
    [request setPropertiesToFetch :[NSArray arrayWithObjects:@"class_id",@"universal_Id",@"itemId",nil]];
      
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@ AND assignedDate == %@ AND assignTimeDate BETWEEN %@",[NSNumber numberWithBool:NO],assignDate,limits];
    
        //---- Predicate modified to support 12/24 format -----//
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@ AND dueDate>=%@ AND dueDate<=%@ AND dueTimeDate>=%@ AND dueTimeDate<%@ AND onTheDay==%@ AND type!=%@",[NSNumber numberWithBool:NO],startDueDate,endDueDate,fromdate,todate,[NSNumber numberWithBool:NO],@"Note"];

//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@ AND dueDate==%@ AND dueTimeDate>=%@ AND dueTimeDate<%@ AND onTheDay==%@ AND type!=%@",[NSNumber numberWithBool:NO],dueDtae,fromdate,todate,[NSNumber numberWithBool:NO],@"Note"];
        //---- Predicate modified to support 12/24 format -----//
        
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
    }
    
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.class_id integerValue] == [a2.class_id integerValue] && [a1.universal_Id integerValue] == [a2.universal_Id integerValue] && [a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    NSArray *activeItems = [self getActiveItems:filterResults];
        
#if DEBUG
        NSLog(@"getStoredAllDueItemDataTime %@",activeItems);
#endif
      
    return activeItems;
    }
    else
    {
        return [NSArray array];
    }
    ////////////////////////////
}

-(void)editEventDairyItems:(NSMutableDictionary*)dict
{
#if DEBUG
    NSLog(@"editEventDairyItems %@",dict);
#endif

    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_EditEventItem ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        //        > http://productdynamics.expensetrackingapplication.com/Webservice/editevent
        //
        //        and keys is  Id,SchoolId,Name,Details,OnTheDay,StartDate,StartTime,EndDate,EndTime,
        
        if ([dict objectForKey:@"Id"] ) {
            [parameters setObject:[dict objectForKey:@"Id"]forKey:@"Id"];
        }
        
        if ([dict objectForKey:@"Type"] ) {
            [parameters setObject:[dict objectForKey:@"Type"] forKey:@"Type"];
        }
        if ([dict objectForKey:@"Name"]) {
            [parameters setObject:[dict objectForKey:@"Name"]forKey:@"Name"];
        }
        if ([dict objectForKey:@"Details"]) {
            [parameters setObject:[dict objectForKey:@"Details"]forKey:@"Details"];
        }
        if ([dict objectForKey:@"StartDate"]) {
            [parameters setObject:[dict objectForKey:@"StartDate"]forKey:@"StartDate"];
        }
        if ([dict objectForKey:@"StartTime"] ) {
            [dict setObject:[self convert12to24format:[dict objectForKey:@"StartTime"]] forKey:@"StartTime"];

            [parameters setObject:[dict objectForKey:@"StartTime"]forKey:@"StartTime"];
        }
        if ([dict objectForKey:@"StartDate"]) {
            [parameters setObject:[dict objectForKey:@"StartDate"]forKey:@"EndDate"];
        }
        if ([dict objectForKey:@"EndTime"]) {
            [dict setObject:[self convert12to24format:[dict objectForKey:@"EndTime"]] forKey:@"EndTime"];

            [parameters setObject:[dict objectForKey:@"EndTime"]forKey:@"EndTime"];
        }
        
        if ([dict objectForKey:@"OnTheDay"]) {
            [parameters setObject:[dict objectForKey:@"OnTheDay"]forKey:@"OnTheDay"];
        }
        
        if ([dict objectForKey:@"assignUserId"] ) {
            [parameters setObject:[dict objectForKey:@"assignUserId"]forKey:@"AssingedUserId"];
        }
        
        // Class Id Parameter added to avoid issue of retaining the values of selected students
        //----------------------------------------------------------------------
        if ([dict objectForKey:@"ClassId"]) {
            [parameters setObject:[dict objectForKey:@"ClassId"]forKey:@"ClassId"];
        }
        //----------------------------------------------------------------------
        
        if([dict objectForKey:@"deletedAttachmentId"]){
            [parameters setObject:[dict objectForKey:@"deletedAttachmentId"]forKey:@"deletedAttachmentId"];
        }
        [parameters setObject:[dict objectForKey:@"created"] forKey:@"created"];
        [parameters setObject:[dict objectForKey:@"updated"] forKey:@"updated"];
        
        //////
        [parameters setObject:[dict objectForKey:@"AppId"] forKey:@"AppId"];
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"]forKey:@"SchoolId"];

        // Webservice Logging existing
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        //uniqueIdentifier
        if ([dict objectForKey:@"IpAddress"]) {
            [parameters setObject:[dict objectForKey:@"IpAddress"]forKey:@"ipadd"];
        }
        
        
        if ([dict objectForKey:@"publishToClass"] ) {
            [parameters setObject:[dict objectForKey:@"publishToClass"]forKey:@"publishToClass"];
        }
        
#if DEBUG
        NSLog(@"urlString %@",urlString);
        NSLog(@"editEventDairyItems %@",[parameters JSONRepresentation]);
#endif

        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishEditEventDiaryTtems:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
        [dictionary setObject:dict forKey:@"event"];
        [dictionary setObject:@"1" forKey:@"ErrorCode"];
        [dictionary setObject:@"Event items updated successfully." forKey:@"Status"];
        NSArray *arr=[[dict objectForKey:@"assignUserId"] componentsSeparatedByString:@","];

        if(arr.count>0){
            NSMutableArray *arr1=[[NSMutableArray alloc]init];
            for(NSString *str in arr){
                NSMutableDictionary *dict1=[[NSMutableDictionary alloc]init];
                [dict1 setObject:str forKey:@"UserId"];
                [dict1 setObject:[dict objectForKey:@"AppId"] forKey:@"itemId"];
                [arr1 addObject:dict1];
                [dict1 release];
            }
            [dict setObject:arr1 forKey:@"assignUserId"];
            [arr1 release];
        }
        [dictionary setObject:dict forKey:@"event_items"];        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EditEventItem object:nil userInfo:dictionary];
        [dictionary release];
	}
}

- (void)netManagerDidFinishEditEventDiaryTtems:(NetManager *)thisNetManager
                                      response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EditEventItem object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

//for add event item
-(void)addEventItems:(NSMutableDictionary*)dict
{
#if DEBUG
    NSLog(@"addEventItems %@",dict);
#endif
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        
        NSString *serviceName = Method_AddEventItem ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        if ([dict objectForKey:@"Type"] ) {
            [parameters setObject:[dict objectForKey:@"Type"] forKey:@"Type"];
        }
        // Aman GP
        if (![[dict objectForKey:@"type_id"] isKindOfClass:[NSNull class]]) {
            [parameters setObject:[dict objectForKey:@"type_id"]forKey:@"type_id"];
        }
        
        if (![[dict objectForKey:@"type_name"] isKindOfClass:[NSNull class]]) {
            [parameters setObject:[dict objectForKey:@"type_name"]forKey:@"type_name"];
        }
        
        if ([dict objectForKey:@"Name"]) {
            [parameters setObject:[dict objectForKey:@"Name"]forKey:@"Name"];
        }
        if ([dict objectForKey:@"Details"]) {
            [parameters setObject:[dict objectForKey:@"Details"]forKey:@"Details"];
        }
        if ([dict objectForKey:@"StartDate"]) {
            [parameters setObject:[dict objectForKey:@"StartDate"]forKey:@"StartDate"];
        }
        if ([dict objectForKey:@"StartTime"] ) {
            [dict setObject:[self convert12to24format:[dict objectForKey:@"StartTime"]] forKey:@"StartTime"];
            [parameters setObject:[dict objectForKey:@"StartTime"]forKey:@"StartTime"];
        }
        if ([dict objectForKey:@"StartDate"]) {
            [parameters setObject:[dict objectForKey:@"StartDate"]forKey:@"EndDate"];
        }
        if ([dict objectForKey:@"EndTime"]) {
            [dict setObject:[self convert12to24format:[dict objectForKey:@"EndTime"]] forKey:@"EndTime"];

            [parameters setObject:[dict objectForKey:@"EndTime"]forKey:@"EndTime"];
        }
        
        if ([dict objectForKey:@"OnTheDay"]) {
            [parameters setObject:[dict objectForKey:@"OnTheDay"]forKey:@"OnTheDay"];
        }
        
        // Aman added
        
        if ([dict objectForKey:@"assignUserId"] ) {
            [parameters setObject:[dict objectForKey:@"assignUserId"]forKey:@"AssingedUserId"];
        }
        if ([dict objectForKey:@"ClassId"]) {
            [parameters setObject:[dict objectForKey:@"ClassId"]forKey:@"ClassId"];
        }
        
        [parameters setObject:[dict objectForKey:@"created"] forKey:@"created"];
        [parameters setObject:[dict objectForKey:@"updated"] forKey:@"updated"];
        
        //////
        [parameters setObject:[dict objectForKey:@"AppId"] forKey:@"AppId"];
        // Webservice Logging existing
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"]forKey:@"SchoolId"];
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        
        
        //uniqueIdentifier
        [parameters setObject:[AppHelper userDefaultsForKey:@"ipadd"]forKey:@"ipadd"];
        
        // Set publish to class
        
        if ([dict objectForKey:@"publishToClass"] ) {
            [parameters setObject:[dict objectForKey:@"publishToClass"]forKey:@"publishToClass"];
        }
        
        //////
        //setdairyitems and keys is
#if DEBUG
        NSLog(@"urlString %@",urlString);
        NSLog(@"Set Event request %@",[parameters JSONRepresentation]);
#endif
      
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishAddEventDiaryTtems:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
        [dictionary setObject:dict forKey:@"event"];
        [dictionary setObject:@"1" forKey:@"ErrorCode"];
        [dictionary setObject:@"Event items add successfully." forKey:@"Status"];
        // [self parseItemDataWithDictionary:dict];
        
        NSArray *arr=[[dict objectForKey:@"assignUserId"] componentsSeparatedByString:@","];
        // [self parseItemDataWithDictionary:dict];
        if(arr.count>0){
            NSMutableArray *arr1=[[NSMutableArray alloc]init];
            for(NSString *str in arr){
                NSMutableDictionary *dict1=[[NSMutableDictionary alloc]init];
                [dict1 setObject:str forKey:@"UserId"];
                [dict1 setObject:[dict objectForKey:@"AppId"] forKey:@"itemId"];
                [arr1 addObject:dict1];
                [dict1 release];
            }
            [dict setObject:arr1 forKey:@"assignUserId"];
            [arr1 release];
        }
        [dictionary setObject:dict forKey:@"event_items"];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_Add_Event object:nil userInfo:dictionary];
        [dictionary release];
		//[AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
	}
}

- (void)netManagerDidFinishAddEventDiaryTtems:(NetManager *)thisNetManager
                                     response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
        
        [Flurry logError:FLURRY_EVENT_Error_in_creating_Diary_Item message:FLURRY_EVENT_Error_in_creating_Diary_Item error:thisNetManager.error];

        
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_Add_Event object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}


-(void)editDiaryTtems:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        //...............................................
        //http://productdynamics.expensetrackingapplication.com/Webservice/setdairyitems
        //and keys is  Type , SchoolId, SubjectId, Title,Description,  Priority ,AssignedDate, AssignedTime,DueDate,DueTime,EstimatedHours, EstimatedMins, Completed , Progress,AssignedtoMe ,All_Students and user_id
        
        NSString *serviceName = Method_EditItem ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
        if ([dict objectForKey:@"Id"] ) {
            [parameters setObject:[dict objectForKey:@"Id"]forKey:@"Id"];
        }
        if ([dict objectForKey:@"Type"] ) {
            [parameters setObject:[dict objectForKey:@"Type"] forKey:@"Type"];
        }
        
        if ([dict objectForKey:@"Title"]) {
            [parameters setObject:[dict objectForKey:@"Title"]forKey:@"Title"];
        }
        
        if ([dict objectForKey:@"WebLink"]) {
            [parameters setObject:[dict objectForKey:@"WebLink"]forKey:@"WebLink"];
        }
        if ([dict objectForKey:@"OnTheDay"]) {
            [parameters setObject:[dict objectForKey:@"OnTheDay"]forKey:@"OnTheDay"];
        }
        if ([dict objectForKey:@"Priority"]) {
            [parameters setObject:[dict objectForKey:@"Priority"]forKey:@"Priority"];
        }
        if ([dict objectForKey:@"AssignedDate"]) {
            [parameters setObject:[dict objectForKey:@"AssignedDate"]forKey:@"AssignedDate"];
        }
        if ([dict objectForKey:@"AssignedTime"] ) {
            [dict setObject:[self convert12to24format:[dict objectForKey:@"AssignedTime"]] forKey:@"AssignedTime"];
            [parameters setObject:[dict objectForKey:@"AssignedTime"]forKey:@"AssignedTime"];
        }
        ////
        if ([dict objectForKey:@"assignUserId"] ) {
            [parameters setObject:[dict objectForKey:@"assignUserId"]forKey:@"AssingedUserId"];
        }
        ////
        if ([dict objectForKey:@"DueDate"]) {
            [parameters setObject:[dict objectForKey:@"DueDate"]forKey:@"DueDate"];
        }
        if ([dict objectForKey:@"DueTime"]) {
            [dict setObject:[self convert12to24format:[dict objectForKey:@"DueTime"]] forKey:@"DueTime"];
            [parameters setObject:[dict objectForKey:@"DueTime"]forKey:@"DueTime"];
        }
        if ([dict objectForKey:@"EstimatedHours"]) {
            [parameters setObject:[dict objectForKey:@"EstimatedHours"]forKey:@"EstimatedHours"];
        }
        
        if ([dict objectForKey:@"EstimatedMins"]) {
            [parameters setObject:[dict objectForKey:@"EstimatedMins"]forKey:@"EstimatedMins"];
        }
        if ([dict objectForKey:@"Completed"]) {
            [parameters setObject:[dict objectForKey:@"Completed"]forKey:@"Completed"];
        }
        if ([dict objectForKey:@"Progress"]) {
            [parameters setObject:[dict objectForKey:@"Progress"] forKey:@"Progress"];
        }
        if ([dict objectForKey:@"AssignedtoMe"]) {
            [parameters setObject:[dict objectForKey:@"AssignedtoMe"] forKey:@"AssignedtoMe"];
        }
        if ([dict objectForKey:@"Description"]) {
            [parameters setObject:[dict objectForKey:@"Description"]forKey:@"Description"];
        }
        if ([dict objectForKey:@"sub_name"]) {
            [parameters setObject:[dict objectForKey:@"sub_name"]forKey:@"subjectName"];
        }
        
        if ([dict objectForKey:@"Weight"]) {
            [parameters setObject:[dict objectForKey:@"Weight"]forKey:@"Weight"];
        }
        
        if ([dict objectForKey:@"ClassId"]) {
            [parameters setObject:[dict objectForKey:@"ClassId"]forKey:@"ClassId"];
        }
        
        if([dict objectForKey:@"deletedAttachmentId"]){
            [parameters setObject:[dict objectForKey:@"deletedAttachmentId"]forKey:@"deletedAttachmentId"];
        }
        
        [parameters setObject:[dict objectForKey:@"created"] forKey:@"created"];
        [parameters setObject:[dict objectForKey:@"updated"] forKey:@"updated"];
        
        //////
        [parameters setObject:[dict objectForKey:@"SchoolId"] forKey:@"SchoolId"];
        [parameters setObject:[dict objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dict objectForKey:@"AppId"] forKey:@"AppId"];
        // Webservice Logging existing
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setObject:[dict objectForKey:@"IpAddress"]forKey:@"ipadd"];
        
        //Merit - Start
        if([dict objectForKey:@"isnotify"])
            [parameters setObject:[dict objectForKey:@"isnotify"] forKey:@"isnotify"];
        
        if([dict objectForKey:@"comment"])
            [parameters setObject:[dict objectForKey:@"comment"] forKey:@"comment"];
        
        if([dict objectForKey:@"passtype_name"])
            [parameters setObject:[dict objectForKey:@"passtype_name"] forKey:@"passtype_name"];
        
        if([dict objectForKey:@"passtype_id"])
            [parameters setObject:[dict objectForKey:@"passtype_id"] forKey:@"passtype_id"];

        
        if([dict objectForKey:@"merit_type"])
            [parameters setObject:[dict objectForKey:@"merit_type"] forKey:@"merit_type"];
        
        //Merit - End
#if DEBUG
        NSLog(@"urlString : %@",urlString);
        NSLog(@"editDiaryItems parameters : %@",[parameters JSONRepresentation]);
#endif
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishseditDiaryTtems:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
        [netManager startRequest];
        [parameters release];
    }
    else
    {
        //Merit - Start
        
        if(![[dict objectForKey:@"Type"] isEqualToString:k_MERIT_CHECK])     //Merit - End
        {
            
            NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
            [dictionary setObject:@"1" forKey:@"ErrorCode"];
            [dictionary setObject:[NSNumber numberWithBool:NO]forKey:@"isSync"];
            
            [dictionary setObject:@"Diaryitem updated successfully." forKey:@"Status"];
            NSArray *arr=[[dict objectForKey:@"assignUserId"] componentsSeparatedByString:@","];

#if DEBUG
            NSLog(@"Crash line number = 897");
#endif
            
            if(arr.count>0)
            {
                NSMutableArray *arr1=[[NSMutableArray alloc]init];
                for(NSString *str in arr)
                {
                    NSMutableDictionary *dict1=[[NSMutableDictionary alloc]init];
                    [dict1 setObject:str forKey:@"UserId"];
                    if ([dict objectForKey:@"Id"] )
                    {
                        [dict1 setObject:[dict objectForKey:@"Id"] forKey:@"itemId"];
                    }
                    else
                    {
                        [dict1 setObject:[dict objectForKey:@"AppId"] forKey:@"itemId"];
                    }
                    
                    [dict1 setObject:@"0000-00-00 00:00:00" forKey:@"Reminder"];
                    [dict1 setObject:@"0" forKey:@"Progress"];
                    
                    [arr1 addObject:dict1];
                    [dict1 release];
                }
                [dict setObject:arr1 forKey:@"assignUserId"];
                [arr1 release];
            }
            ///-------- logic added to set ApproverName for offline support ----

            DairyItemUsers *dairyItemUsers = [self getDiaryItemUserForDiaryItemId:[NSNumber numberWithInteger:[[dict objectForKey:@"Id"] integerValue]]];

            if(dairyItemUsers)
            {
            [dict setObject:dairyItemUsers.approverName forKey:@"ApproverName"];
            }
            ///-----------------------------------------------------------------

            [dictionary setObject:dict forKey:@"dairy_item"];

           
             // [self parseItemDataWithDictionary:dict];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EditItem object:nil userInfo:dictionary];
            [dictionary release];
            //	[AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        else
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
    }
}
- (void)netManagerDidFinishseditDiaryTtems:(NetManager *)thisNetManager
                                  response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
#if DEBUG
        NSLog(@"thisNetManager.error : %@",thisNetManager.error);
#endif
        [dictionary setObject:netManager.error forKey:@"error"];
        
           [Flurry logError:FLURRY_EVENT_Error_in_updating_Diary_Item message:FLURRY_EVENT_Error_in_updating_Diary_Item error:thisNetManager.error];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EditItem object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}


-(void)addDiaryTtems:(NSMutableDictionary*)dict
{
#if DEBUG
    NSLog(@"addDiaryTtems %@",dict);
#endif
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        //...............................................
        //http://productdynamics.expensetrackingapplication.com/Webservice/setdairyitems
        //and keys is  Type , SchoolId, SubjectId, Title,Description,  Priority ,AssignedDate, AssignedTime,DueDate,DueTime,EstimatedHours, EstimatedMins, Completed , Progress,AssignedtoMe ,All_Students and user_id
        
        NSString *serviceName = Method_setdairyitems ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        if ([dict objectForKey:@"Type"] ) {
            [parameters setObject:[dict objectForKey:@"Type"] forKey:@"Type"];
        }
        
        // Aman GP
        if (![[dict objectForKey:@"type_id"] isKindOfClass:[NSNull class]]) {
            [parameters setObject:[dict objectForKey:@"type_id"]forKey:@"type_id"];
        }
        
        if (![[dict objectForKey:@"type_name"] isKindOfClass:[NSNull class]]) {
            [parameters setObject:[dict objectForKey:@"type_name"]forKey:@"type_name"];
        }
        
        if ([dict objectForKey:@"Title"]) {
            [parameters setObject:[dict objectForKey:@"Title"]forKey:@"Title"];
        }
        
        if ([dict objectForKey:@"WebLink"]) {
            [parameters setObject:[dict objectForKey:@"WebLink"]forKey:@"WebLink"];
        }
        
        if ([dict objectForKey:@"OnTheDay"]) {
            [parameters setObject:[dict objectForKey:@"OnTheDay"]forKey:@"OnTheDay"];
        }
        if ([dict objectForKey:@"Priority"]) {
            [parameters setObject:[dict objectForKey:@"Priority"]forKey:@"Priority"];
        }
        if ([dict objectForKey:@"AssignedDate"]) {
            [parameters setObject:[dict objectForKey:@"AssignedDate"]forKey:@"AssignedDate"];
        }
        if ([dict objectForKey:@"AssignedTime"] ) {
            [dict setObject:[self convert12to24format:[dict objectForKey:@"AssignedTime"]] forKey:@"AssignedTime"];
            [parameters setObject:[dict objectForKey:@"AssignedTime"]forKey:@"AssignedTime"];
        }
        /*******************New Methods Added For Out Class********************************/
        
        if([dict objectForKey:@"passtype_id"]){
            [parameters setObject:[dict objectForKey:@"passtype_id"] forKey:@"passtype_id"];
            [parameters setObject:[dict objectForKey:@"passtype_name"] forKey:@"passtype_name"];
            
        }
        
        //Merit - Start

        if([[dict objectForKey:@"Type"]  isEqualToString:k_MERIT_CHECK])
        {
            [parameters setObject:[dict objectForKey:@"isnotify"] forKey:@"isnotify"];
            [parameters setObject:[dict objectForKey:@"comment"] forKey:@"comment"];
            [parameters setObject:[dict objectForKey:@"merit_type"] forKey:@"merit_type"];
        }

        //Merit - End
        
        if([[dict objectForKey:@"Type"]  isEqualToString:@"OutOfClass"] || [[dict objectForKey:@"Type"]  isEqualToString:@"Out Of Class"])
        {
            MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
            if ([profobj.type isEqualToString:@"Student"])
            {
                if([[dict objectForKey:@"PassCode"] length]>0)
                    [parameters setObject:[dict objectForKey:@"PassCode"] forKey:@"PassCode"];
                
            }
        }
        
        
        if([dict objectForKey:@"pass_status"]){
            [parameters setObject:[dict objectForKey:@"pass_status"] forKey:@"pass_status"];
            
        }
        ////
        if ([dict objectForKey:@"assignUserId"] ) {
            [parameters setObject:[dict objectForKey:@"assignUserId"]forKey:@"AssingedUserId"];
        }
        ////
        if ([dict objectForKey:@"DueDate"]) {
            [parameters setObject:[dict objectForKey:@"DueDate"]forKey:@"DueDate"];
        }
        if ([dict objectForKey:@"DueTime"]) {
            [dict setObject:[self convert12to24format:[dict objectForKey:@"DueTime"]] forKey:@"DueTime"];
            [parameters setObject:[dict objectForKey:@"DueTime"]forKey:@"DueTime"];
            
        }
        if ([dict objectForKey:@"EstimatedHours"]) {
            [parameters setObject:[dict objectForKey:@"EstimatedHours"]forKey:@"EstimatedHours"];
        }
        
        if ([dict objectForKey:@"EstimatedMins"]) {
            [parameters setObject:[dict objectForKey:@"EstimatedMins"]forKey:@"EstimatedMins"];
        }
        if ([dict objectForKey:@"Completed"]) {
            [parameters setObject:[dict objectForKey:@"Completed"]forKey:@"Completed"];
        }
        if ([dict objectForKey:@"Progress"]) {
            [parameters setObject:[dict objectForKey:@"Progress"] forKey:@"Progress"];
        }
        if ([dict objectForKey:@"AssignedtoMe"]) {
            [parameters setObject:[dict objectForKey:@"AssignedtoMe"] forKey:@"AssignedtoMe"];
        }
        if ([dict objectForKey:@"Description"]) {
            [parameters setObject:[dict objectForKey:@"Description"]forKey:@"Description"];
        }
        if ([dict objectForKey:@"sub_name"]) {
            [parameters setObject:[dict objectForKey:@"sub_name"]forKey:@"subjectName"];
        }
        
        if ([dict objectForKey:@"Weight"]) {
            [parameters setObject:[dict objectForKey:@"Weight"]forKey:@"Weight"];
        }
        
        if ([dict objectForKey:@"ClassId"]) {
            [parameters setObject:[dict objectForKey:@"ClassId"]forKey:@"ClassId"];
        }
        
        
        [parameters setObject:[dict objectForKey:@"created"] forKey:@"created"];
        [parameters setObject:[dict objectForKey:@"updated"] forKey:@"updated"];
        
        //////
        [parameters setObject:[dict objectForKey:@"SchoolId"] forKey:@"SchoolId"];
        [parameters setObject:[dict objectForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[dict objectForKey:@"AppId"] forKey:@"AppId"];
        // Webservice Logging existing
        [parameters setObject:APP_Key forKey:kKeyAppType];
        [parameters setObject:[AppHelper userDefaultsForKey:@"ipadd"]forKey:@"ipadd"];

#if DEBUG
        NSLog(@"addDiaryTtems urlString %@",urlString);
        
        NSLog(@"JSON Parameters : %@",[parameters JSONRepresentation]);
#endif
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishsaddDiaryTtems:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
        [netManager startRequest];
        [parameters release];
    }
    else
    {
        //Merit - Start
        if(![[dict objectForKey:@"Type"] isEqualToString:k_MERIT_CHECK])     //Merit - End
        {
            NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
            [dictionary setObject:@"1" forKey:@"ErrorCode"];
            [dictionary setObject:@"Dairy items add successfully." forKey:@"Status"];
            NSArray *arr=[[dict objectForKey:@"assignUserId"] componentsSeparatedByString:@","];
            if(arr.count>0)
            {
                NSMutableArray *arr1=[[NSMutableArray alloc]init];
                for(NSString *str in arr)
                {
                    NSMutableDictionary *dict1=[[NSMutableDictionary alloc]init];
                    [dict1 setObject:str forKey:@"UserId"];
                    [dict1 setObject:[dict objectForKey:@"AppId"] forKey:@"itemId"];
                    [dict1 setObject:@"0000-00-00 00:00:00" forKey:@"Reminder"];
                    [dict1 setObject:@"0" forKey:@"Progress"];
                    
                    [arr1 addObject:dict1];
                    [dict1 release];
                }
                [dict setObject:arr1 forKey:@"assignUserId"];
                [arr1 release];
            }
            [dictionary setObject:dict forKey:@"dairy_item"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_add_Dairy_Items object:nil userInfo:dictionary];
            [dictionary release];
        }
    }
}

- (void)netManagerDidFinishsaddDiaryTtems:(NetManager *)thisNetManager
                                 response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
#if DEBUG
        NSLog(@"netManagerDidFinishsaddDiaryTtems error dictionary %@",dictionary);
#endif
      
        [Flurry logError:FLURRY_EVENT_Error_in_creating_Diary_Item message:FLURRY_EVENT_Error_in_creating_Diary_Item error:thisNetManager.error];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
#if DEBUG
        NSLog(@"netManagerDidFinishsaddDiaryTtems %@",jsonDictD);
#endif
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
        
#if DEBUG
        NSLog(@"netManagerDidFinishsaddDiaryTtems %@",dictionary);
#endif
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_add_Dairy_Items object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}


-(void)parseItemDataWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init] ;
    [formatter1 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];

    Item *itemData=nil;
    if (![[dictionary objectForKey:@"AppType"] isEqualToString:APP_Key]) {
        itemData=[self getItemDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
        
    }
    else{
        itemData=[self getItemDataInfoStoredForParseDate:[dictionary objectForKey:@"AppId"] appid:[dictionary objectForKey:@"IpAddress"]];
    }
    
    NSDateFormatter *formatter3 = [[NSDateFormatter alloc] init] ;
    
    
    if (!itemData) {
        itemData = [NSEntityDescription
                    insertNewObjectForEntityForName:@"Item"
                    inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]])
    {
        itemData.itemId=[NSNumber numberWithInteger: [[dictionary objectForKey:@"Id"]integerValue]];
        //   itemData.universal_Id=[NSNumber numberWithInt: [[dictionary objectForKey:@"Id"]intValue]];
    }
    
    if ([dictionary objectForKey:@"isSync"])
    {
        itemData.isSync=[NSNumber numberWithBool:[[dictionary objectForKey:@"isSync"] boolValue]];
    }
    else
    {
        itemData.isSync=[NSNumber numberWithBool:YES];
    }
    
    /*******************New Methods Added For Out Class********************************/
    
    if (![[dictionary objectForKey:@"pass_status"] isKindOfClass:[NSNull class]])
    {
        itemData.isApproved=[NSNumber numberWithInteger:[[dictionary objectForKey:@"pass_status"] integerValue]];
    }
    
    if (![[dictionary objectForKey:@"passtype_id"] isKindOfClass:[NSNull class]])
    {
        itemData.passTypeId=[NSNumber numberWithInteger:[[dictionary objectForKey:@"passtype_id"] integerValue]];
    }
    /*******************New Methods Added For Out Class********************************/
    
    
    
    itemData.isDelete=[NSNumber numberWithBool:NO];
    if (![[dictionary objectForKey:@"OnTheDay"] isKindOfClass:[NSNull class]]) {
        itemData.onTheDay=[NSNumber numberWithBool:[[dictionary objectForKey:@"OnTheDay"]boolValue]];
    }
    ////17june
    if (![[dictionary objectForKey:@"AssignedtoMe"] isKindOfClass:[NSNull class]]) {
        itemData.assignedtoMe=[NSNumber numberWithBool:[[dictionary objectForKey:@"AssignedtoMe"]boolValue]];
    }
    
    if (![[dictionary objectForKey:@"Type"] isKindOfClass:[NSNull class]]) {
        itemData.type=[dictionary objectForKey:@"Type"];
        if([[dictionary objectForKey:@"Type"] isEqualToString:@"Note"]){
            if (![[dictionary objectForKey:@"AssignedDate"] isKindOfClass:[NSNull class]]) {
                itemData.dueDate=[formatter1 dateFromString:[dictionary objectForKey:@"AssignedDate"]];
            }
        }
        else{
            if (![[dictionary objectForKey:@"DueDate"] isKindOfClass:[NSNull class]]) {
                itemData.dueDate=[formatter1 dateFromString:[dictionary objectForKey:@"DueDate"]];
            }
        }
    }
    
    // Aman GP
    if (![[dictionary objectForKey:@"type_id"] isKindOfClass:[NSNull class]]) {
        if([[[dictionary objectForKey:@"type_id"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]>0)
        {
        itemData.itemType_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"type_id"]integerValue]];
        }
        else
        {
        itemData.itemType_id= [NSNumber numberWithInteger:[[self getItemTypeID:itemData.type]integerValue]];
        }
    
    }
    //    if (![[dictionary objectForKey:@"type_name"] isKindOfClass:[NSNull class]]) {
    //        itemData.type_name = [dictionary objectForKey:@"type_name"]forKey:@"type_name"];
    //    }
    

    
    if (![[dictionary objectForKey:@"IpAddress"] isKindOfClass:[NSNull class]]) {
        itemData.ipAdd=[dictionary objectForKey:@"IpAddress"];
    }
    if (![[dictionary objectForKey:@"Title"] isKindOfClass:[NSNull class]]) {
        itemData.title=[dictionary objectForKey:@"Title"];
    }
    if (![[dictionary objectForKey:@"Description"] isKindOfClass:[NSNull class]]) {
        itemData.itemDescription=[dictionary objectForKey:@"Description"];
    }
    else
    {
        itemData.itemDescription=@"";

    }
    
    if (![[dictionary objectForKey:@"WebLink"] isKindOfClass:[NSNull class]]) {
        itemData.weblink =[dictionary objectForKey:@"WebLink"];
    }
    
    if (![[dictionary objectForKey:@"Created"] isKindOfClass:[NSNull class]]) {
        itemData.created=[self dateFromMilliSecond:[NSNumber numberWithDouble:[[dictionary objectForKey:@"Created"] doubleValue]]];
    }
    if (![[dictionary objectForKey:@"AssignedDate"] isKindOfClass:[NSNull class]]) {
        itemData.assignedDate=[formatter1 dateFromString:[dictionary objectForKey:@"AssignedDate"]];

        
    }
    if (![[dictionary objectForKey:@"AssignedTime"] isKindOfClass:[NSNull class]]) {
        itemData.assignedTime=[dictionary objectForKey:@"AssignedTime"];
    }
    if (![[dictionary objectForKey:@"AssignedTime"] isKindOfClass:[NSNull class]])
    {
        // Code Modified for Daisy Data ------------------------------------
        NSDate *assigDate = nil;
        if (![[dictionary objectForKey:@"AssignedDate"] isKindOfClass:[NSNull class]])
        {
            assigDate = [formatter1 dateFromString:[dictionary objectForKey:@"AssignedDate"]];
        }
        // Code Modified End -----------------------------------------------
        
        unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:assigDate];
        NSArray *arrassigTime=nil;
        if (![[dictionary objectForKey:@"AssignedTime"] isKindOfClass:[NSNull class]]) {
            arrassigTime= [[dictionary objectForKey:@"AssignedTime"] componentsSeparatedByString:@":"];
            
        }
        if([arrassigTime count]>0)
        {
            [dateComponents setHour:[[arrassigTime objectAtIndex:0] integerValue]];
            [dateComponents setMinute:[[arrassigTime objectAtIndex:1] integerValue]];
            
            [dateComponents setSecond:[[arrassigTime objectAtIndex:2] integerValue]];
        }
        NSDate *newAssineddate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];

#if DEBUG
        NSLog(@"newAssineddate =%@",[ formatter stringFromDate:newAssineddate]);
#endif

        NSDateFormatter *lclformatter = [[NSDateFormatter alloc] init];
        [lclformatter setDateStyle:NSDateFormatterNoStyle];
        [lclformatter setTimeStyle:NSDateFormatterShortStyle];
        NSString *aasignTime = [lclformatter  stringFromDate:newAssineddate];
        itemData.assignTimeDate = [lclformatter dateFromString:aasignTime];
        [lclformatter release];
        //=====
    }
    
    if (![[dictionary objectForKey:@"DueTime"] isKindOfClass:[NSNull class]])
    {
        itemData.dueTime=[dictionary objectForKey:@"DueTime"];

        // Code Modified for Daisy Data ------------------------------------
        NSDate *dueDate = nil;
        if (![[dictionary objectForKey:@"DueDate"] isKindOfClass:[NSNull class]])
        {
            dueDate = [formatter1 dateFromString:[dictionary objectForKey:@"DueDate"]];
        }
        // Code Modified End -----------------------------------------------
        
        NSArray *dueTime= nil;
        if (![[dictionary objectForKey:@"DueTime"] isKindOfClass:[NSNull class]])
        {
            dueTime =[[dictionary objectForKey:@"DueTime"] componentsSeparatedByString:@":"];
        }
        
        unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:dueDate];
        if([dueTime count]>0)
        {
            [dateComponents setHour:[[dueTime objectAtIndex:0] integerValue]];
            [dateComponents setMinute:[[dueTime objectAtIndex:1] integerValue]];
            
            [dateComponents setSecond:[[dueTime objectAtIndex:2] integerValue]];
        }
        NSDate *newDuedate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
        NSDateFormatter *lclformatter = [[NSDateFormatter alloc] init];
        [lclformatter setDateStyle:NSDateFormatterNoStyle];
        [lclformatter setTimeStyle:NSDateFormatterShortStyle];
        NSString *dueeTime = [lclformatter  stringFromDate:newDuedate];

        itemData.dueTimeDate = [lclformatter dateFromString:dueeTime];
        [lclformatter release];
        //======
    }
    if (![[dictionary objectForKey:@"Weight"] isKindOfClass:[NSNull class]]) {
        itemData.weight=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Weight"]integerValue]];
    }
    
    
    if (![[dictionary objectForKey:@"Completed"] isKindOfClass:[NSNull class]]) {
        itemData.isCompelete=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Completed"]integerValue]];
    }
    if (![[dictionary objectForKey:@"sub_name"] isKindOfClass:[NSNull class]]) {
        itemData.subject_name=[dictionary objectForKey:@"sub_name"];
    }
    
    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
        itemData.school_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Updated"] isKindOfClass:[NSNull class]]) {
        itemData.lastUpdated=[self dateFromMilliSecond:[NSNumber numberWithDouble:[[dictionary objectForKey:@"Updated"] doubleValue]]];
    }
    if (![[dictionary objectForKey:@"EstimatedHours"] isKindOfClass:[NSNull class]]) {
        itemData.estimate_hours=[dictionary objectForKey:@"EstimatedHours"];
    }
    if (![[dictionary objectForKey:@"EstimatedMins"] isKindOfClass:[NSNull class]]) {
        itemData.estimat_Minuts=[dictionary objectForKey:@"EstimatedMins"];
    }
    if (![[dictionary objectForKey:@"Progress"] isKindOfClass:[NSNull class]]) {
        itemData.progress=[NSString stringWithFormat:@"%@",[dictionary objectForKey:@"Progress"]];
    }
    if (![[dictionary objectForKey:@"Priority"] isKindOfClass:[NSNull class]]) {
        itemData.priority=[dictionary objectForKey:@"Priority"];
    }
    if (![[dictionary objectForKey:@"ClassId"] isKindOfClass:[NSNull class]]) {
        itemData.class_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"ClassId"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"CreatedBy"] isKindOfClass:[NSNull class]]) {
        itemData.createdBy = [dictionary objectForKey:@"CreatedBy"];
    }
    
    
    if (![[dictionary objectForKey:@"AppId"] isKindOfClass:[NSNull class]]) {
        itemData.universal_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"AppId"]integerValue]];
    }
   
      itemData.isRead =[NSNumber numberWithInteger:0];
    
   
    
            //=======
    
#if DEBUG
    NSLog(@"formatter1.dateFormat : %@",formatter1.dateFormat);
    NSLog(@"dictionary objectForKey:AssignedDate : %@",[dictionary objectForKey:@"AssignedDate"]);
#endif
    
    // Code Modified for Daisy Data ------------------------------------
    NSDate *assigDate = nil;
    if (![[dictionary objectForKey:@"AssignedDate"] isKindOfClass:[NSNull class]])
    {
        assigDate = [formatter1 dateFromString:[dictionary objectForKey:@"AssignedDate"]];
    }
    // Code Modified End -----------------------------------------------
    
        unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:assigDate];

#if DEBUG
    NSLog(@"before dateComponents  :%@",dateComponents);
#endif
    
        NSArray *arrassigTime=nil;
    if (![[dictionary objectForKey:@"AssignedTime"] isKindOfClass:[NSNull class]]) {
        arrassigTime= [[dictionary objectForKey:@"AssignedTime"] componentsSeparatedByString:@":"];

    }
    if([arrassigTime count]>0)
    {
            [dateComponents setHour:[[arrassigTime objectAtIndex:0] integerValue]];
            [dateComponents setMinute:[[arrassigTime objectAtIndex:1] integerValue]];
            
            [dateComponents setSecond:[[arrassigTime objectAtIndex:2] integerValue]];
    }
    
#if DEBUG
    NSLog(@"after dateComponents  :%@",dateComponents);
#endif
    
            NSDate *newAssineddate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    
#if DEBUG
    NSLog(@"newAssineddate : %@",newAssineddate);
    NSLog(@"newAssineddate =%@",[ formatter stringFromDate:newAssineddate]);
#endif

        itemData.assignedDate = newAssineddate;
        //=======

    NSString *stringcombineDueDateAndTime = nil;
    
    if (![[dictionary objectForKey:@"OnTheDay"] isKindOfClass:[NSNull class]])
    {
        if([[dictionary objectForKey:@"OnTheDay"]boolValue])
        {
#if DEBUG
            NSLog(@"True is on the day == 1");
            NSLog(@"[dictionary objectForKey:DueDate = %@",[dictionary objectForKey:@"DueDate"]);
#endif
            stringcombineDueDateAndTime = [NSString stringWithFormat:@"%@",[dictionary objectForKey:@"DueDate"]];
            [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
            NSDateComponents *components1 = [[NSCalendar currentCalendar] components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[formatter dateFromString:stringcombineDueDateAndTime]];
            NSDate * newDuedate = [[NSCalendar currentCalendar]dateFromComponents:components1 ];
            
#if DEBUG
            NSLog(@"newDuedate =%@",[ formatter stringFromDate:newDuedate]);
#endif

            itemData.dueDate = newDuedate;
        }
        else
        {
#if DEBUG
            NSLog(@"False is on the day == 0");
#endif

            // Code Modified for Daisy Data ------------------------------------
            NSDate *dueDate = nil;
            if (![[dictionary objectForKey:@"DueDate"] isKindOfClass:[NSNull class]])
            {
                dueDate = [formatter1 dateFromString:[dictionary objectForKey:@"DueDate"]];
            }
            // Code Modified End -----------------------------------------------
            
            NSArray *dueTime= nil;
            if (![[dictionary objectForKey:@"DueTime"] isKindOfClass:[NSNull class]]) {
                dueTime =[[dictionary objectForKey:@"DueTime"] componentsSeparatedByString:@":"];

            }
            
            unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
            NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:dueDate];
            if([dueTime count]>0)
            {
            [dateComponents setHour:[[dueTime objectAtIndex:0] integerValue]];
            [dateComponents setMinute:[[dueTime objectAtIndex:1] integerValue]];
            
            [dateComponents setSecond:[[dueTime objectAtIndex:2] integerValue]];
            }
            NSDate *newDuedate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
            itemData.dueDate = newDuedate;
        }
    }
    
    
    [formatter release];
    [formatter3 release];
    
    
    

    NSError *error;
    
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	}
    
    
    if (![[dictionary objectForKey:@"attachmentData"] isKindOfClass:[NSNull class]])
    {
        for(NSDictionary *dict in [dictionary objectForKey:@"attachmentData"])
        {
            Attachment *attachmenData=[self getDiaryAttachmentstDataInfoStoredAttachments:[dict objectForKey:@"Id"]];
            
            if (!attachmenData)
            {
                attachmenData = [NSEntityDescription
                                 insertNewObjectForEntityForName:@"Attachment"
                                 inManagedObjectContext:_managedObjectContext];
            }
            
            if (![[dict objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
                attachmenData.aID =[NSNumber numberWithInteger:[[dict objectForKey:@"Id"]integerValue]];
            }
            
            if (![[dict objectForKey:@"dateAttached"] isKindOfClass:[NSNull class]]) {
                attachmenData.dateAttached = [formatter1 dateFromString:[dict objectForKey:@"dateAttached"]];
            }
            
            if (![[dict objectForKey:@"lasyupdatedDate"] isKindOfClass:[NSNull class]]) {
                attachmenData.lastupdatedDate = [formatter1 dateFromString:[dict objectForKey:@"lasyupdatedDate"]];
            }
            
            if (![[dict objectForKey:@"DisplayName"] isKindOfClass:[NSNull class]]) {
                attachmenData.displayFileName = [dict objectForKey:@"DisplayName"];
            }
            
            if (![[dict objectForKey:@"FileType"] isKindOfClass:[NSNull class]]) {
                attachmenData.type = [dict objectForKey:@"FileType"];
            }
            
            if (![[dict objectForKey:@"localFilePath"] isKindOfClass:[NSNull class]]) {
                attachmenData.filePath = [dict objectForKey:@"localFilePath"];
            }
            
            if (![[dict objectForKey:@"FilePath"] isKindOfClass:[NSNull class]]) {
                attachmenData.remoteFilePath = [dict objectForKey:@"FilePath"];
            }
            
            if (![[dict objectForKey:@"CreatedBy"] isKindOfClass:[NSNull class]]) {
                attachmenData.createdBy = [dict objectForKey:@"CreatedBy"];
                
            }
            
            if (![[dict objectForKey:@"size"] isKindOfClass:[NSNull class]]) {
                attachmenData.size = [NSNumber numberWithInteger:[[dict objectForKey:@"size"]integerValue]];
                
            }
            
            if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
                attachmenData.diaryItemId = [NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
                
            }
            
            if (![[dictionary objectForKey:@"type_id"] isKindOfClass:[NSNull class]]) {
                if([[[dictionary objectForKey:@"type_id"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]>0)
                {
                    attachmenData.itemtype = [self getFormTemplateOnItemIDBasis:[NSNumber numberWithInteger:[[dictionary objectForKey:@"type_id"]integerValue]]];
                }
                else
                {
                    attachmenData.itemtype = [self getFormTemplateOnItemIDBasis:[NSNumber numberWithInteger:[[self getItemTypeID:itemData.type]integerValue]]];
                }
                
            }
            
            
            
            //            size
            
            attachmenData.isSync=[NSNumber numberWithBool:YES];
            
            attachmenData.isDeletedOnLocal=[NSNumber numberWithBool:NO];
            
            
            if(attachmenData){
                [itemData addItemattachmentObject:attachmenData];
            }
            
            //
            
            
            
            
            NSError *error;
            if (![_managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
            }
        }
    }
    [formatter1 release];
}

//Merit - Start
- (void)addOrEditDIOnServerWithType:(NSString*)type
                             typeid:(NSNumber *)typeId
                           schoolId:(NSNumber *)schoolId
                            classId:(NSNumber *)classId
                              title:(NSString *)title
                        description:(NSString *)description
                 assignedAndDueDate:(NSString *)assignedAndDueDate
                   assignAndDueTime:(NSString *)assignAndDueTime
                        createdDate:(NSString *)createdDate
                          createdBy:(NSString *)createdBy
                        lastUpdated:(NSString *)lastUpdated
                       assignedtoMe:(NSNumber *)assignedtoMe
                       assignUserId:(NSString *)assignUserId
                             isSync:(NSNumber *)isSync
                           isNotify:(NSNumber *)isNotify
                          meritType:(NSString *)meritType
                           onTheDay:(NSNumber *)onTheDay
                         passTypeId:(NSNumber *)passTypeId
                         isApproved:(NSNumber *)isApproved
                        isCompelete:(NSNumber *)isCompelete
                           isDelete:(NSNumber *)isDelete
                             isRead:(NSNumber *)isRead
                             itemId:(NSNumber *)itemId
                        subjectName:(NSString *)subjectName
                             isEdit:(BOOL)isEdit
{

#if DEBUG
    NSLog(@"type : %@",type);
#endif

    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:type forKey:@"Type"];
    [parameters setObject:[typeId stringValue] forKey:@"type_id"];
    
#if DEBUG
    NSLog(@"title ; %@",title);
#endif

    [parameters setObject:type forKey:@"type_name"];
    [parameters setObject:title forKey:@"Title"];


#if DEBUG
    NSLog(@"onTheDay integerValue : %ld",(long)[onTheDay integerValue]);
#endif
    
    [parameters setObject:[onTheDay stringValue] forKey:@"OnTheDay"];
    
    [parameters setObject:assignedAndDueDate forKey:@"AssignedDate"];
    [parameters setObject:assignAndDueTime forKey:@"AssignedTime"];
    
    [parameters setObject:[itemId stringValue] forKey:@"Id"];
    
    [parameters setObject:[passTypeId stringValue] forKey:@"passtype_id"];
    [parameters setObject:title forKey:@"passtype_name"];
    [parameters setObject:[isNotify stringValue] forKey:@"isnotify"];
    [parameters setObject:meritType forKey:@"merit_type"];
    [parameters setObject:description forKey:@"comment"];

    [parameters setObject:assignedAndDueDate forKey:@"DueDate"];
    [parameters setObject:assignAndDueTime forKey:@"DueTime"];
    
    [parameters setObject:[assignedtoMe stringValue] forKey:@"AssignedtoMe"];
    [parameters setObject:assignUserId forKey:@"assignUserId"];
    
    [parameters setObject:description forKey:@"Description"];
    [parameters setObject:subjectName forKey:@"sub_name"];

    [parameters setObject:[classId stringValue] forKey:@"ClassId"];

    [parameters setObject:createdDate forKey:@"created"];
    [parameters setObject:lastUpdated forKey:@"updated"];
    
    //////
    [parameters setObject:[schoolId stringValue] forKey:@"SchoolId"];
    [parameters setObject:createdBy forKey:@"user_id"];
    
    NSNumber *appId=[[Service sharedInstance] nextAvailble:@"universal_Id" forEntityName:@"Item"];
    NSString *ipAddress = [AppHelper userDefaultsForKey:@"ipadd"];
    
    [parameters setObject:appId forKey:@"AppId"];
    [parameters setObject:APP_Key forKey:kKeyAppType];
    
    if(isEdit)
    {
        [parameters setObject:ipAddress forKey:@"IpAddress"];
    }
    else
    {
        [parameters setObject:ipAddress forKey:@"ipadd"];
    }
    
#if DEBUG
    NSLog(@"addOrEdit : parameters : %@",parameters);
#endif
    
    if(isEdit)
    {
        [self editDiaryTtems:parameters];
    }
    else
    {
        [self addDiaryTtems:parameters];
    }
}


- (NSArray *)getAllInActiveMeritsFromListofValues
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"ListofValues"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"diaryItemType like %@ AND active = %@", k_MERIT_CHECK,[NSNumber numberWithInteger:0]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    return results;
}


- (NSArray *)getAllActiveMeritsFromListofValues
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"ListofValues"];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"diaryItemType like %@ AND active = %@", k_MERIT_CHECK,[NSNumber numberWithInteger:1]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    return results;
}

- (NSArray *)getAllMeritsAsMeritModels
{
    //get all merits
    NSArray *meritsAsListOfValues = [[Service sharedInstance] getAllActiveMeritsFromListofValues];
    
#if DEBUG
    NSLog(@"meritsAsListOfValues count : %lu",(unsigned long)[meritsAsListOfValues count]);
#endif
    
    NSMutableArray *meritsAsMeritModels = [[NSMutableArray alloc] init];
    
    for (ListofValues *listOfValueAsMerit in meritsAsListOfValues)
    {
        Merit *merit = [[Merit alloc] init];
        
        merit.name = listOfValueAsMerit.descriptions;
        merit.subType = listOfValueAsMerit.type;
        merit.listofvaluesTypeid = listOfValueAsMerit.listofvaluesTypeid;
        
        NSString *imageName = listOfValueAsMerit.icon;
        NSString *str= @"merits";
        str=[NSString stringWithFormat:@"Documents/%@",str];
        NSString *imageDirectoryPath = [NSHomeDirectory() stringByAppendingPathComponent:str];
        
        NSString *imagePath = [imageDirectoryPath stringByAppendingPathComponent:imageName];
        
        BOOL result = [FileUtils fileExistsAtPath:imageDirectoryPath fileName:imageName];
        NSData *imageData = nil;
        
        if(result)
        {
            imageData = [FileUtils getDataFromPath:imagePath];
            merit.image = [UIImage imageWithData:imageData];
        }
        //listofvaluesTypeid
        [meritsAsMeritModels addObject:merit];
        
        [merit release];
    }

#if DEBUG
    NSLog(@"meritsAsMeritModels count : %lu",(unsigned long)meritsAsMeritModels.count);
#endif

    return [meritsAsMeritModels autorelease];
}

- (void)editTaskBasedMeritOnServerWithDIType:(NSString*)type
                                      typeid:(NSNumber *)typeId
                                    schoolId:(NSNumber *)schoolId
                                     classId:(NSNumber *)classId
                                       title:(NSString *)title
                                 description:(NSString *)description
                                assignedDate:(NSString *)assignedDate
                                     dueDate:(NSString *)dueDate
                                  assignTime:(NSString *)assignTime
                                     dueTime:(NSString *)dueTime
                                 createdDate:(NSString *)createdDate
                                   createdBy:(NSString *)createdBy
                                 lastUpdated:(NSString *)lastUpdated
                                assignedtoMe:(NSNumber *)assignedtoMe
                                      isSync:(NSNumber *)isSync
                                    onTheDay:(NSNumber *)onTheDay
                                  isApproved:(NSNumber *)isApproved
                                 isCompelete:(NSNumber *)isCompelete
                                    isDelete:(NSNumber *)isDelete
                                      isRead:(NSNumber *)isRead
                                      itemId:(NSNumber *)itemId
                                 subjectName:(NSString *)subjectName
                                   completed:(NSNumber *)completed
                                    priority:(NSString *)priority
                                    progress:(NSString *)progress
                                      weight:(NSNumber *)weight
                                    isNotify:(NSNumber *)isNotify
                                meritComment:(NSString *)meritComment
                                   meritName:(NSString *)meritName
                                   meritType:(NSString *)meritType
                                  passTypeId:(NSNumber *)passTypeId
                                assignUserId:(NSString *)assignUserId
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    [parameters setObject:type forKey:@"Type"];
    [parameters setObject:[typeId stringValue] forKey:@"type_id"];
    
#if DEBUG
    NSLog(@"title ; %@",title);
#endif
    
    [parameters setObject:type forKey:@"type_name"];
    [parameters setObject:title forKey:@"Title"];
    
    if([description length] > 0)
    {
        [parameters setObject:description forKey:@"Description"];
    }
    
    [parameters setObject:assignedDate forKey:@"AssignedDate"];
    [parameters setObject:assignTime forKey:@"AssignedTime"];
    
    [parameters setObject:dueDate forKey:@"DueDate"];
    [parameters setObject:dueTime forKey:@"DueTime"];

    
    [parameters setObject:createdDate forKey:@"created"];
    [parameters setObject:lastUpdated forKey:@"updated"];
    
    [parameters setObject:createdBy forKey:@"user_id"];
    
    [parameters setObject:[assignedtoMe stringValue] forKey:@"AssignedtoMe"];

    [parameters setObject:[onTheDay stringValue] forKey:@"OnTheDay"];

    [parameters setObject:[itemId stringValue] forKey:@"Id"];
    [parameters setObject:subjectName forKey:@"sub_name"];

    [parameters setObject:[classId stringValue] forKey:@"ClassId"];
    
    [parameters setObject:[schoolId stringValue] forKey:@"SchoolId"];
    
    NSNumber *appId=[[Service sharedInstance] nextAvailble:@"universal_Id" forEntityName:@"Item"];
    NSString *ipAddress = [AppHelper userDefaultsForKey:@"ipadd"];
    
    [parameters setObject:appId forKey:@"AppId"];
    [parameters setObject:APP_Key forKey:kKeyAppType];
    
    [parameters setObject:ipAddress forKey:@"IpAddress"];
    
    //merit specific parameters
    [parameters setObject:[isNotify stringValue] forKey:@"isnotify"];
    [parameters setObject:meritComment forKey:@"comment"];
    [parameters setObject:meritName forKey:@"passtype_name"];
    [parameters setObject:meritType forKey:@"merit_type"];
    [parameters setObject:[passTypeId stringValue] forKey:@"passtype_id"];
    [parameters setObject:assignUserId forKey:@"assignUserId"];

#if DEBUG
    NSLog(@"editTaskBased : parameters : %@",[parameters JSONRepresentation]);
#endif
    
    [self addTaskBasedMerit:parameters];

}

//Merit - End

#pragma mark- convert12to24format
-(NSString *)convert12to24format:(NSString *)intime
{
    intime = [intime lowercaseString];
    
    if([intime hasSuffix:@"am"])
    {
        intime = [intime stringByReplacingOccurrencesOfString:@" am" withString:@""];
        NSArray *arr =[intime componentsSeparatedByString:@":"];
        NSMutableArray *marr = [NSMutableArray arrayWithArray:arr];
        if([marr count]>0)
        {
            if([[marr objectAtIndex:0] isEqualToString:@"12"])
            {
                [marr replaceObjectAtIndex:0 withObject:@"00"];
                
            }
           
        }
        NSString *str = [NSString stringWithFormat:@"%@:%@:00",[marr objectAtIndex:0],[marr objectAtIndex:1]];
        return str;
    }
    
    else if([intime hasSuffix:@"pm"])
    {
        intime = [intime stringByReplacingOccurrencesOfString:@" pm" withString:@""];
        NSArray *arr =[intime componentsSeparatedByString:@":"];
        NSMutableArray *marr = [NSMutableArray arrayWithArray:arr];
        NSInteger val =12;
        if([marr count]>0)
        {
            if([[marr objectAtIndex:0] isEqualToString:@"12"])
            {
                [marr replaceObjectAtIndex:0 withObject:@"12"];
                
            }
            else
            {
                val = val +[[marr objectAtIndex:0] integerValue];
                [marr replaceObjectAtIndex:0 withObject:[NSString stringWithFormat:@"%ld",(long)val]];
            }
            
        }
        NSString *str = [NSString stringWithFormat:@"%@:%@:00",[marr objectAtIndex:0],[marr objectAtIndex:1]];
        return str;
    }

    return intime;

}

#pragma mark- ParseDiaryItemTypes
-(void)parseDiaryItemTypesDataWithDictionary:(NSMutableDictionary*)dictionary{
    
    //[self deleteAllObjects:@"DiaryItemTypes"];
    NSArray *arr = [[NSArray alloc] initWithArray:[dictionary valueForKey:@"diaryitem_types"]];
    DiaryItemTypes *diaryItemType=nil;
    for (NSDictionary *dictdiaryItem  in arr )
    {
        diaryItemType=[self getDiaryItemTypesInfoStored:[dictdiaryItem objectForKey:@"Id"]];

        if (!diaryItemType) {
            diaryItemType = [NSEntityDescription
                             insertNewObjectForEntityForName:@"DiaryItemTypes"
                             inManagedObjectContext:_managedObjectContext];
        }
        if (![[dictdiaryItem objectForKey:@"Active"] isKindOfClass:[NSNull class]]) {
            diaryItemType.active =[NSNumber numberWithInteger:[[dictdiaryItem objectForKey:@"Active"] integerValue]];
        }
        if (![[dictdiaryItem objectForKey:@"FormType"] isKindOfClass:[NSNull class]]) {
            diaryItemType.formtemplate =[dictdiaryItem objectForKey:@"FormType"];
        }
        
        if (![[dictdiaryItem objectForKey:@"Icon"] isKindOfClass:[NSNull class]]) {
            diaryItemType.icon =[dictdiaryItem objectForKey:@"Icon"];
        }
        if (![[dictdiaryItem objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
            diaryItemType.diaryitemtypeid =[NSNumber numberWithInteger:[[dictdiaryItem objectForKey:@"Id"] integerValue]];
        }
        if (![[dictdiaryItem objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
            diaryItemType.name =[dictdiaryItem objectForKey:@"Name"];
        }
        if (![[dictdiaryItem objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
            diaryItemType.school_Id =[NSNumber numberWithInteger:[[dictdiaryItem objectForKey:@"SchoolId"] integerValue]];
        }
        
        if (![[dictdiaryItem objectForKey:@"OnlyTeacher"] isKindOfClass:[NSNull class]]) {
            diaryItemType.isonlyteacher =[NSNumber numberWithInteger:[[dictdiaryItem objectForKey:@"OnlyTeacher"] integerValue]];
        }
        
        NSError *error;
        if (![_managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        }
    }
}

- (DiaryItemTypes *)getDiaryItemTypesInfoStored:(NSNumber*)diaryitemtypeid
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DiaryItemTypes"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"diaryitemtypeid =%@",diaryitemtypeid ];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    DiaryItemTypes  * diaryitemtype  = nil;
    if (!error && [results count] > 0) {
        
        diaryitemtype = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
    }
    
    return diaryitemtype;
    
}

//------------------
//Merits
//------------------



- (void)downloadMeritsImages:(NSMutableArray *)arrLOV
{
    OperationQueue *queueOperation = nil;
    queueOperation = [[[OperationQueue alloc] init] autorelease];
    [queueOperation setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];

    for(ListofValues *image in arrLOV)
    {
        
        if ([image.active integerValue] == 1 && !([image.icon isKindOfClass:[NSNull class]] && [image.iconsmall isKindOfClass:[NSNull class]] ))
        {
            DownloadPictureOperation *pictOp = [[DownloadPictureOperation alloc] initWithMode:UIViewContentModeScaleAspectFill];
            
            NSString *serviceName = [NSString stringWithFormat:@"%@%ld/%@",merits_BaseUrl,(long)[image.school_Id integerValue],image.icon];
            
            serviceName=[serviceName stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [pictOp setThumbURL:[NSURL URLWithString:serviceName]];
            
#if DEBUG
            NSLog(@"downloadMeritsImages strng  = %@",serviceName);
#endif
            
            NSMutableDictionary *storedObject = [[NSMutableDictionary alloc] init];
            [storedObject setObject:image forKey:@"model"];
            
            NSString *str= @"merits";
            str=[NSString stringWithFormat:@"Documents/%@",str];
            
            NSString *dataPath1 = [NSHomeDirectory() stringByAppendingPathComponent:str];
            
            [storedObject setObject:dataPath1 forKey:@"str"];
            [storedObject setObject:image.icon forKey:@"imageName"];
            
            [pictOp setStoredObject:storedObject];
            [storedObject release];
            
            [queueOperation addOperation:pictOp];
            [pictOp release];
            pictOp = nil;
            
            
            //--------------
            //--------------
            pictOp = [[DownloadPictureOperation alloc] initWithMode:UIViewContentModeScaleAspectFill];
            
            NSString *serviceNameSmall = [NSString stringWithFormat:@"%@%ld/%@",merits_BaseUrl,(long)[image.school_Id integerValue],image.iconsmall];
            
            serviceNameSmall=[serviceNameSmall stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            [pictOp setThumbURL:[NSURL URLWithString:serviceNameSmall]];
            
#if DEBUG
            NSLog(@"downloadMeritsImages strng  = %@",serviceNameSmall);
#endif

            
            NSMutableDictionary *storedObjectSmall = [[NSMutableDictionary alloc] init];
            [storedObjectSmall setObject:image forKey:@"model"];
            
            NSString *strSmall= @"merits";
            strSmall=[NSString stringWithFormat:@"Documents/%@",strSmall];
            
            NSString *dataPath1Small = [NSHomeDirectory() stringByAppendingPathComponent:strSmall];
            
            [storedObjectSmall setObject:dataPath1Small forKey:@"str"];
            [storedObjectSmall setObject:image.iconsmall forKey:@"imageName"];
            
            [pictOp setStoredObject:storedObjectSmall];
            [storedObjectSmall release];
            
            [queueOperation addOperation:pictOp];
            [pictOp release];
            pictOp = nil;

            
            
        }
    }
}

//------------------
/*******************New Methods Added For Out Class********************************/
#pragma  mark  parseListOfValuesDataWithDictionary

-(void)parseListOfValuesDataWithDictionary:(NSMutableDictionary*)dictionary
{
    //[self deleteAllObjects:@"ListofValues"];
    NSArray *arr = [[NSArray alloc] initWithArray:[dictionary valueForKey:@"list_of_values"]];
    ListofValues *listofValues=nil;
    for (NSDictionary *dictdiaryItem  in arr )
    {
        if([[dictdiaryItem objectForKey:@"DiaryItemType"] isEqualToString:@"Out Of Class"] || [[dictdiaryItem objectForKey:@"DiaryItemType"] isEqualToString:@"OutOfClass"] || [[dictdiaryItem objectForKey:@"DiaryItemType"] isEqualToString:k_MERIT_CHECK])
        {
            listofValues=[self getListOfValuesInfoStored:[dictdiaryItem objectForKey:@"Id"]];

            if (!listofValues) {
                listofValues = [NSEntityDescription
                                insertNewObjectForEntityForName:@"ListofValues"
                                inManagedObjectContext:_managedObjectContext];
            }
            if (![[dictdiaryItem objectForKey:@"Active"] isKindOfClass:[NSNull class]]) {
                listofValues.active =[NSNumber numberWithInteger:[[dictdiaryItem objectForKey:@"Active"] integerValue]];
            }
            
            if (![[dictdiaryItem objectForKey:@"Created"] isKindOfClass:[NSNull class]])
            {
                NSDateFormatter *format=[[NSDateFormatter alloc]init];
                [format setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                NSDate *date = [format dateFromString:[dictdiaryItem objectForKey:@"Created"]];
                listofValues.created=date;
                [format release];
                
            }
            if (![[dictdiaryItem objectForKey:@"CreatedBy"] isKindOfClass:[NSNull class]])
            {
                listofValues.createdBy=[dictdiaryItem objectForKey:@"CreatedBy"];
            }
            if (![[dictdiaryItem objectForKey:@"Description"] isKindOfClass:[NSNull class]])
            {
                listofValues.descriptions=[dictdiaryItem objectForKey:@"Description"];
            }
            if (![[dictdiaryItem objectForKey:@"Type"] isKindOfClass:[NSNull class]])
            {
                listofValues.type=[dictdiaryItem objectForKey:@"Type"];
            }
            if (![[dictdiaryItem objectForKey:@"DiaryItemType"] isKindOfClass:[NSNull class]])
            {
                listofValues.diaryItemType=[dictdiaryItem objectForKey:@"DiaryItemType"];
            }
            
            if (![[dictdiaryItem objectForKey:@"Icon"] isKindOfClass:[NSNull class]]) {
                listofValues.icon =[dictdiaryItem objectForKey:@"Icon"];
            }
            
            if (![[dictdiaryItem objectForKey:@"IconSmall"] isKindOfClass:[NSNull class]]) {
                listofValues.iconsmall =[dictdiaryItem objectForKey:@"IconSmall"];
            }
            
            if (![[dictdiaryItem objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
                listofValues.listofvaluesTypeid =[NSNumber numberWithInteger:[[dictdiaryItem objectForKey:@"Id"] integerValue]];
            }
            
            if (![[dictdiaryItem objectForKey:@"SortOrder"] isKindOfClass:[NSNull class]])
            {
                listofValues.sortedOrder=[NSNumber numberWithInteger:[[dictdiaryItem objectForKey:@"SortOrder"] integerValue]];
            }
            
            if (![[dictdiaryItem objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
                listofValues.school_Id =[NSNumber numberWithInteger:[[dictdiaryItem objectForKey:@"SchoolId"] integerValue]];
            }
            
            if (![[dictdiaryItem objectForKey:@"Updated"] isKindOfClass:[NSNull class]])
            {
                NSDateFormatter *format=[[NSDateFormatter alloc]init];
                [format setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
                NSDate *date = [format dateFromString:[dictdiaryItem objectForKey:@"Updated"]];
                listofValues.updated=date;
                [format release];
                
            }
            if (![[dictdiaryItem objectForKey:@"UpdatedBy"] isKindOfClass:[NSNull class]])
            {
                listofValues.updatedBy=[dictdiaryItem objectForKey:@"UpdatedBy"];
            }
            
            if (![[dictdiaryItem objectForKey:@"Value"] isKindOfClass:[NSNull class]])
            {
                listofValues.value=[dictdiaryItem objectForKey:@"Value"];
            }
            
            
            NSError *error;
            if (![_managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
            }
        }
    }

    // Fetching Merits from Dictionary
    NSMutableArray *marrMerits = [[NSMutableArray alloc] init];
    NSMutableArray *marrOfLOV = [[NSMutableArray alloc] init];
    
    for ( NSDictionary *dictdiaryItem  in arr )
    {
        if ([[dictdiaryItem objectForKey:@"DiaryItemType"] isEqualToString:k_MERIT_CHECK])
        {
            [marrMerits addObject:dictdiaryItem];
        }
    }
    
    if ([marrMerits count] > 0 )
    {
        for (NSString *strMeritId in [marrMerits valueForKey:@"Id"] )
        {
            NSNumber *meritId = [NSNumber numberWithInteger:[strMeritId integerValue]];
            ListofValues *lov = [self getListOfValuesInfoStored:meritId];
            [marrOfLOV addObject:lov];
        }
        [self downloadMeritsImages:marrOfLOV];
    }
    
    [marrMerits release];
    [marrOfLOV release];

}

- (ListofValues *)getListOfValuesInfoStored:(NSNumber*)listofvaluesTypeid
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"ListofValues"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"listofvaluesTypeid =%@", listofvaluesTypeid ];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    ListofValues  * listofvalues  = nil;
    if (!error && [results count] > 0) {
        
        listofvalues = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
    }
    
    return listofvalues;
    
}

- (ListofValues *)getactiveListOfValuesInfoStoredInMyclass:(NSNumber*)listofvaluesTypeid
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"ListofValues"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"listofvaluesTypeid =%@ && active=%@", listofvaluesTypeid,[NSNumber numberWithInteger:1] ];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    ListofValues  * listofvalues  = nil;
    if (!error && [results count] > 0) {
        
        listofvalues = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
    }
    
    return listofvalues;
    
}


/*******************New Methods Added For Out Class********************************/



#pragma  mark  getStoreDAllActiveDiaryItemTypes

-(NSArray*)getStoreDAllActiveDiaryItemTypes
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DiaryItemTypes"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active = %@",[NSNumber numberWithInteger:1]];
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    NSMutableArray *results2 = [NSMutableArray array];
    
    for(DiaryItemTypes *type in results)
    {
        [results2 addObject:type.formtemplate];
    }
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    NSArray * newArray = [[NSOrderedSet orderedSetWithArray:results2] array];
    return newArray;
    
    
}

// Aman GP
-(NSString *)getDiaryItemTypeId:(NSString *)selectedtype
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DiaryItemTypes"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name =%@",selectedtype];
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif

    }

    if([results count]>0)
    {
        return [[[results objectAtIndex:0]diaryitemtypeid ]stringValue
                ] ;
    }
    return @"";
    
}

// Aman GP
-(NSString *)getItemTypeID:(NSString *)selectedtype
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DiaryItemTypes"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"formtemplate =%@",selectedtype];
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    if([results count]>0)
    {
        return [[[results objectAtIndex:0]diaryitemtypeid ]stringValue
                ] ;
    }
    return @"";
    
    
    
    
}


-(NSArray*)getStoredAllActiveDiaryItemTypesWithFormTemplate
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DiaryItemTypes"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active = %@",[NSNumber numberWithInteger:1]];
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

-(NSArray*)getStoredAllActiveDiaryItemTypesWithFormTemplateWithOnlyTeacherFlag
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DiaryItemTypes"];
    NSPredicate *predicate =nil;
    
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    if ([profobj.type isEqualToString:@"Teacher"])
    {
        predicate = [NSPredicate predicateWithFormat:@"active = %@",[NSNumber numberWithInteger:1]];
        
    }
    // Student
    else
    {
        predicate = [NSPredicate predicateWithFormat:@"active = %@ && isonlyteacher = %@",[NSNumber numberWithInteger:1],[NSNumber numberWithInteger:0]];
        
    }
    
    
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}


-(NSString*)getStoredFormTemplate:(NSString*)Name
{
    Name = [Name lowercaseString];
    NSString *strtemplateName = nil;
    NSArray *allactive = [self getStoredAllActiveDiaryItemTypesWithFormTemplate];
    for(DiaryItemTypes *type in allactive)
    {
        if([[ type.name lowercaseString] isEqualToString:Name])
        {
            strtemplateName = type.formtemplate;
            break;
        }
        
    }
    
    return strtemplateName;
    
}

-(NSString*)getStoredNameForFormTemplate:(NSString*)formTemplate
{
    formTemplate = [formTemplate lowercaseString];
    NSString *strtemplateName = nil;
    NSArray *allactive = [self getStoredAllActiveDiaryItemTypesWithFormTemplate];
    for(DiaryItemTypes *type in allactive)
    {
        if([[ type.formtemplate lowercaseString] isEqualToString:formTemplate])
        {
            strtemplateName = type.name;
            break;
        }
        
    }
    
    return strtemplateName;
}
-(NSString*)getNameForFormTemplateOnItemIDBasis:(NSNumber*)formTemplateID
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DiaryItemTypes"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"diaryitemtypeid = %@",formTemplateID ];
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
    }
    
    if ([results count]>0)
    {
        
        return [[results objectAtIndex:0]name];
    }
    return @"";
    
    
}


-(NSString*)getFormTemplateOnItemIDBasis:(NSNumber*)formTemplateID
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DiaryItemTypes"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"diaryitemtypeid = %@",formTemplateID ];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    if ([results count]>0)
    {
        
        return [[results objectAtIndex:0]formtemplate];
    }
    return @"";
    
    
}


#pragma mark-deleteObjectOfDairyItem 16int 64int Dairy Item

- (void) deleteObjectOfDairyItemOnTheBasisOfUniversalID {
    
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Item" inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"universal_Id =%@",[NSNumber numberWithInteger:0]];
    [fetchRequest setPredicate:predicate];
    
    
    NSError *error;
    NSArray *items = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];
    
    
    for (NSManagedObject *managedObject in items) {
        [_managedObjectContext deleteObject:managedObject];
    }
    if (![_managedObjectContext save:&error]) {
    }
}


- (void) deleteAllObjects: (NSString *) entityDescription  {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityDescription inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSError *error;
    NSArray *items = [_managedObjectContext executeFetchRequest:fetchRequest error:&error];
    [fetchRequest release];
    
    
    for (NSManagedObject *managedObject in items) {
    	[_managedObjectContext deleteObject:managedObject];
    }
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"error : %@",[error userInfo]);
#endif
      
    }
    
}

// M here
- (Item *)getItemDataInfoStored:(NSNumber*)itemId postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"itemId==%@", itemId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    
    /**********code added to create DI in offline mode and view in oFFLine mode *****/
    
    if([results count]==0)
    {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"universal_Id==%@", itemId];
        [request setPredicate:predicate1];
        
        NSError *error = nil;
        results = nil;
        results = [_managedObjectContext executeFetchRequest:request error:&error];
        results = [[NSSet setWithArray:results] allObjects];

    
    }
    /**********code added to create DI in offline mode and view in oFFLine mode *****/

    
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.class_id integerValue] == [a2.class_id integerValue] && [a1.universal_Id integerValue] == [a2.universal_Id integerValue] && [a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    
    
    
    
    
    Item  * addItemData  = nil;
	if (!error && [filterResults count] > 0) {
		
		addItemData = [filterResults objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    
    if (![addItemData.type isEqualToString:@"Note"])
    {
        // JIRA 1679
        if ([DELEGATE glbIsStudentSelected] && ![DELEGATE glbIsSyncCalled])
        {
            // do nothing
        }
        else
        {
           // [[DELEGATE marrListedAttachments] removeAllObjects];
        }

//        for (Attachment *a in addItemData.itemattachment)
//        {
//            a.displayName = a.displayFileName ;
//            a.localFilePath = a.filePath ;
//            a.fileType = a.type ;
//            if ([a.isDeletedOnLocal integerValue] == 0)
//            {
//                [[DELEGATE marrListedAttachments] addObject:a];
//            }
//        }
    }
    
    
    return addItemData;
    
}
- (Item *)getItemDataInfoStoredForParseDate:(NSNumber*)itemId appid:(NSString*)appId
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
	if(appId==nil){
       	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"universal_Id==%@", itemId];
        [request setPredicate:predicate];
    }
    else{
       	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"universal_Id==%@ AND ipAdd==%@", itemId,appId];
        [request setPredicate:predicate];
    }
    
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.class_id integerValue] == [a2.class_id integerValue] && [a1.universal_Id integerValue] == [a2.universal_Id integerValue] && [a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    
#if DEBUG
    NSLog(@"filterResults = %@",filterResults);
#endif
    
    
    Item  * addItemData  = nil;
	if (!error && [filterResults count] > 0) {
		
		addItemData = [filterResults objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    // JIRA 1679
    if ([DELEGATE glbIsStudentSelected] && ![DELEGATE glbIsSyncCalled])
    {
        // do nothing
    }
    else
    {
        //[[DELEGATE marrListedAttachments] removeAllObjects];
    }
    
//    for (Attachment *a in addItemData.itemattachment)
//    {
//        a.displayName = a.displayFileName ;
//        a.localFilePath = a.filePath ;
//        a.fileType = a.type ;
//        [[DELEGATE marrListedAttachments] addObject:a];
//    }
    
    return addItemData;
    
}
-(NSArray*)getStoredAllItemDataTime:(NSDate*)fromdate toDate:(NSDate*)todate assign:(NSDate*)assignDate
{
    
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif
    

    if(fromdate!=nil && todate!=nil && assignDate!=nil)
    {
     
        NSDate *startAssignDate = assignDate;
        NSDate *endAssignDate = assignDate;
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:assignDate];
        [components setHour:0];
        [components setMinute:0];
        [components setSecond:0];
        startAssignDate = [calendar dateFromComponents:components];
        
        [components setHour:23];
        [components setMinute:59];
        [components setSecond:59];
        endAssignDate = [calendar dateFromComponents:components];
        
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
    [request setPropertiesToFetch :[NSArray arrayWithObjects:@"class_id",@"universal_Id",@"itemId",nil]];
    
   
        //---- Predicate modified to support 12/24 format -----//
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@ AND assignedDate>=%@ AND assignedDate<=%@ AND assignTimeDate>=%@ AND assignTimeDate<%@",[NSNumber numberWithBool:NO],startAssignDate,endAssignDate,fromdate,todate];
        [request setPredicate:predicate];
        
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@ AND assignedDate==%@ AND assignTimeDate>=%@ AND assignTimeDate<%@",[NSNumber numberWithBool:NO],assignDate,fromdate,todate];
//        [request setPredicate:predicate];
        //---- Predicate modified to support 12/24 format -----//
        
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    // AMAN CREATED
    results = [[NSSet setWithArray:results] allObjects];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.class_id integerValue] == [a2.class_id integerValue] && [a1.universal_Id integerValue] == [a2.universal_Id integerValue] && [a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy)
            {
                [filterResults addObject:a1];
            }
        }
    }
    
        NSArray *activeItem = [self getActiveItems:filterResults];
        return activeItem;
    }
    else
    {
        return [NSArray array];
    }
    ////////////////////////////
}


-(NSArray*)getStoredAllEventDairyItem:(NSDate*)fromdate toDate:(NSDate*)todate satrtdate:(NSDate*)startDate
{
    
   if(fromdate!=nil && todate!=nil && startDate!=nil)
   {
   
  
       NSDate *startEventDate = startDate;
       NSDate *endEventDate = startDate;
       
       NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
       NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:startDate];
       [components setHour:0];
       [components setMinute:0];
       [components setSecond:0];
       startEventDate = [calendar dateFromComponents:components];
       
       [components setHour:23];
       [components setMinute:59];
       [components setSecond:59];
       endEventDate = [calendar dateFromComponents:components];
   
        //1july
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"EventItem"];
        // NSExpression *s1 = [ NSExpression expressionForConstantValue: fromdate ];
        // NSExpression *s2 = [ NSExpression expressionForConstantValue: todate ];
        // NSArray *limits = [ NSArray arrayWithObjects: s1, s2, nil ];
        ////////////////////////////////////////////////////////////////////
        //   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"startDate == %@ AND startTimeDate BETWEEN %@",startDate,limits];
       
       //---- Predicate modified to support 12/24 format -----//

        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@ AND startDate >= %@ AND startDate <= %@ AND startTimeDate>=%@ AND startTimeDate<%@",[NSNumber numberWithBool:NO],startEventDate,endEventDate,fromdate,todate];
       
//       NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@ AND startDate == %@ AND startTimeDate>=%@ AND startTimeDate<%@",[NSNumber numberWithBool:NO],startDate,fromdate,todate];

       //---- Predicate modified to support 12/24 format -----//
        [request setPredicate:predicate];

       NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        NSMutableArray* filterResults = [[NSMutableArray alloc] init];
        BOOL copy;
        
        
        if (![results count] == 0) {
            for (EventItem *a1 in results) {
                copy = YES;
                for (EventItem *a2 in filterResults) {
                    if ([a1.item_Id integerValue] == [a2.item_Id integerValue] && [a1.universal_id integerValue] == [a2.universal_id integerValue] ) {
                        copy = NO;
                        break;
                    }
                }
                if (copy) {
                    [filterResults addObject:a1];
                }
            }
        }
        
        results = [self getActiveEvents:filterResults];
#if DEBUG
       NSLog(@"getStoredAllEventDairyItem %@",results);
#endif
      
       if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
        return results;
   }
    else
    {
        return [NSArray array];
    }
    
}


-(NSArray*)getTodaysEvents:(NSDate*)startDate
{
        //1july
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"EventItem"];
    ////////////////////////////////////////////////////////////////////
    //   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"startDate == %@ AND startTimeDate BETWEEN %@",startDate,limits];
    
    
    NSCalendar*       calendar = [[[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar] autorelease];
    NSDateComponents* components = [[[NSDateComponents alloc] init] autorelease];
   // components.day = -1;
    NSDate* TodaysDate= [calendar dateByAddingComponents: components toDate: [NSDate date] options: 0];

    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@ AND startDate >= %@ AND startDate <= %@",[NSNumber numberWithBool:NO],startDate,TodaysDate];
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@",[NSNumber numberWithBool:NO]];
    
    [request setPredicate:predicate];

    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    
    if (![results count] == 0) {
        for (EventItem *a1 in results) {
            copy = YES;
            for (EventItem *a2 in filterResults) {
                if ([a1.item_Id integerValue] == [a2.item_Id integerValue] && [a1.universal_id integerValue] == [a2.universal_id integerValue] ) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    
    results = [self getActiveEvents:filterResults];
    
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
}



-(NSArray*)getStoredAllItemData:(NSDate*)date
{
    if(date!=nil)
    {
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
    [request setReturnsDistinctResults:YES];
    
    NSDate *startDate = date;
    NSDate *endDate = date;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    startDate = [calendar dateFromComponents:components];
    
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    endDate = [calendar dateFromComponents:components];
    
    [request setPropertiesToFetch :[NSArray arrayWithObjects:@"class_id",@"universal_Id",@"itemId",nil]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assignedDate>=%@ AND assignedDate<=%@ AND isDelete==%@", startDate,endDate,[NSNumber numberWithBool:NO]];
	[request setPredicate:predicate];
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    
#if DEBUG
    NSLog(@"getStoredAllItemData results%@",results);
#endif
        
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results)
        {
#if DEBUG
            NSLog(@"a1 %@",a1);
#endif
      
            copy = YES;
            for (Item *a2 in filterResults)
            {
                if ([a1.class_id integerValue]== [a2.class_id integerValue] && [a1.universal_Id integerValue]== [a2.universal_Id integerValue]&& [a1.itemId integerValue] == [a2.itemId integerValue])
                {
#if DEBUG
                    NSLog(@"a2 %@",a2);
#endif
      
                    copy = NO;
                    break;
                }
            }
            if (copy)
            {
                [filterResults addObject:a1];
            }
        }
    }
    
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    NSArray *activeItems = [self getActiveItems:filterResults];

    return activeItems;
    }
    else
    {
        return [NSArray array];
    }
    
}

//4julyimportant
-(NSArray*)getStoredAllDueItemData:(NSDate*)date
{
    if(date!=nil)
    {
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
    [request setReturnsDistinctResults:YES];
    
    [request setPropertiesToFetch :[NSArray arrayWithObjects:@"class_id",@"universal_Id",@"itemId",nil]];
    
    NSDate *startDate = date;
    NSDate *endDate = date;

    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    startDate = [calendar dateFromComponents:components];

    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    endDate = [calendar dateFromComponents:components];

    
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dueDate>=%@ AND dueDate<=%@ AND isDelete==%@",startDate,endDate, [NSNumber numberWithBool:NO]];
	[request setPredicate:predicate];

        NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.class_id integerValue] == [a2.class_id integerValue] && [a1.universal_Id integerValue] == [a2.universal_Id integerValue] && [a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    
    
    NSArray *activeItems = [self getActiveItems:filterResults];
    return activeItems;
    }
    else
    {
        return [NSArray array];
    }
    
}
//////////////////////////////////////////////////////////////////////

-(NSArray*)getStoredAllItemDataWithSubjectId:(NSString*)sub_name
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"subject_name==%@ AND isDelete==%@", sub_name,[NSNumber numberWithBool:NO]];
	[request setPredicate:predicate];

    NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.class_id integerValue] == [a2.class_id integerValue] && [a1.universal_Id integerValue]== [a2.universal_Id integerValue] && [a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    
    
    NSArray *activeItems = [self getActiveItems:filterResults];
    return activeItems;
    
}

-(NSArray*)getStoredAllItemDataWithClassId:(NSString*)class_id
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_id==%@ AND isDelete==%@", class_id,[NSNumber numberWithBool:NO]];
	[request setPredicate:predicate];

    NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.class_id integerValue]== [a2.class_id integerValue]&& [a1.universal_Id integerValue]== [a2.universal_Id integerValue] && [a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    
    
    NSArray *activeItems = [self getActiveItems:filterResults];
    return activeItems;
    
}


- (BOOL)isDate:(NSDate *)date inRangeFirstDate:(NSDate *)firstDate lastDate:(NSDate *)lastDate
{
    BOOL res = NO;
    if ([firstDate isEqualToDate:date] || [lastDate isEqualToDate:date])
    {
        res = YES;
    }
    else if ([firstDate compare:date] == NSOrderedAscending && [lastDate compare:date]  == NSOrderedDescending)
    {
        res = YES;
    }
    
    return res;
}

-(NSArray*)getStoredAllItemDataInAppForDueOnly
{
    NSDate *now = [NSDate date];
    NSDate *semStartDate = [now dateByAddingTimeInterval:-16*24*60*60];
    NSDate *semEndDate = [now dateByAddingTimeInterval:+15*24*60*60];
    
    
    // Below Code helpful to get diaryim betwen semester start & end date.
    /* NSDate *CurrentDate = [NSDate date];
     NSDate *semStartDate = nil;
     NSDate *semEndDate = nil;
     
     NSArray *arrTimeTable = [self getStoredAllTimeTableData];
     for (TimeTable *tm in arrTimeTable) {
     
     if ([self isDate:CurrentDate inRangeFirstDate:tm.startDate lastDate:tm.end_date]) {
     semStartDate = tm.startDate;
     semEndDate = tm.end_date;
     }
     }
     */
    
    
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
    //
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@",[NSNumber numberWithBool:NO]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(isDelete==%@ AND dueDate>=%@ AND dueDate<=%@)",[NSNumber numberWithBool:NO],semStartDate,semEndDate,[NSNumber numberWithBool:NO],semStartDate,semEndDate];
    
    
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@ ",@"Note"];
    
    
    [request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.class_id integerValue] == [a2.class_id integerValue] && [a1.universal_Id integerValue] == [a2.universal_Id integerValue] && [a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    
    
    NSArray *activeItems = [self getActiveItems:filterResults];
    return activeItems;
    
}

-(NSArray*)getStoredAllItemDataInApp
{
    NSDate *now = [NSDate date];
    NSDate *semStartDate = [now dateByAddingTimeInterval:-16*24*60*60];
    NSDate *semEndDate = [now dateByAddingTimeInterval:+15*24*60*60];
    
    
    // Below Code helpful to get diaryim betwen semester start & end date.
   /* NSDate *CurrentDate = [NSDate date];
    NSDate *semStartDate = nil;
    NSDate *semEndDate = nil;
    
    NSArray *arrTimeTable = [self getStoredAllTimeTableData];
    for (TimeTable *tm in arrTimeTable) {

        if ([self isDate:CurrentDate inRangeFirstDate:tm.startDate lastDate:tm.end_date]) {
            semStartDate = tm.startDate;
            semEndDate = tm.end_date;
        }
    }
    */
    
    
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
    //
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@",[NSNumber numberWithBool:NO]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(isDelete==%@ AND assignedDate>=%@ AND assignedDate<=%@)",[NSNumber numberWithBool:NO],semStartDate,semEndDate];
    
    
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type = %@ ",@"Note"];
    
    
    [request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.class_id integerValue] == [a2.class_id integerValue] && [a1.universal_Id integerValue] == [a2.universal_Id integerValue] && [a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    
    
    NSArray *activeItems = [self getActiveItems:filterResults];
    
    return activeItems;
    
}

- (NSMutableArray *)getActiveDIWithActiveMeritsFilter:(NSArray *)results
{
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif
    
    
    NSArray *arrOfInActiveMerits = [self getAllInActiveMeritsFromListofValues];
    NSMutableArray *arrayOfAllData = [NSMutableArray arrayWithArray:results];
    
    for(ListofValues *inactiveItemsTypes in arrOfInActiveMerits)
    {
        for(Item *items in results)
        {
            if([items.passTypeId integerValue]!=0)
            {
                if([inactiveItemsTypes.listofvaluesTypeid integerValue]==[items.passTypeId integerValue] )
                {
                    [arrayOfAllData removeObject:items];
                }
            }
        }
    }
    
    return arrayOfAllData;
}

- (NSMutableArray *)getActivePassItems:(NSArray *)results
{
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif
    

    
    NSArray *arrOfInActive = [self getStoredAllInActivePassTypesWithFormTemplate];
    NSMutableArray *arrayOfAllData = [NSMutableArray arrayWithArray:results];

    for(ListofValues *activeItemsTypes in arrOfInActive)
    {
        for(Item *items in results)
        {
            if([items.type isEqualToString:@"OutOfClass"] ||  [items.type isEqualToString:@"Out Of Class"])
            {
                if([items.passTypeId integerValue]!=0)
                {
                if([activeItemsTypes.listofvaluesTypeid integerValue]==[items.passTypeId integerValue] )
                {
                    [arrayOfAllData removeObject:items];
                }
                }
            }
        }
    }
    
    return arrayOfAllData;
}
#pragma mark -
#pragma mark PassType Methods  -

-(NSArray*)getStoredAllActivePassTypesWithFormTemplate
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"ListofValues"];
    
    // Campus Based Start
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active = %@",[NSNumber numberWithInt:1]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active = %@ && (diaryItemType LIKE[c]%@ OR diaryItemType LIKE[c]%@ ) ",[NSNumber numberWithInteger:1],@"OutOfClass",@"Out Of Class"];

    // Campus Based End

	[request setPredicate:predicate];

    NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}
-(NSArray*)getStoredAllInActivePassTypesWithFormTemplate
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"ListofValues"];
    
    // Campus Based Start
   // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active = %@",[NSNumber numberWithInt:0]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"active = %@ && (diaryItemType LIKE[c]%@ OR diaryItemType LIKE[c]%@ )",[NSNumber numberWithInteger:0],@"OutOfClass",@"Out Of Class"];

    // Campus Based End

	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}


-(NSArray *)getActiveItems:(NSArray *)results
{
    // Aman GP
    NSArray *arrOfActive = [self getStoredAllActiveDiaryItemTypesWithFormTemplate];
    
    NSMutableArray *maar = [[NSMutableArray alloc] init];
    
    for(DiaryItemTypes *activeItemsTypes in arrOfActive)
    {
        for(Item *items in results)
        {
            if([activeItemsTypes.formtemplate isEqualToString:items.type] && [activeItemsTypes.diaryitemtypeid integerValue]==[items.itemType_id integerValue])
            {
                [maar addObject:items];
            }
            
        }
    }
    maar = [self getActivePassItems:[NSArray arrayWithArray:maar]];
    
    maar = [self getActiveDIWithActiveMeritsFilter:[NSArray arrayWithArray:maar]];
    
    return maar;
}

-(NSArray *)getActiveEvents:(NSArray *)results
{

    // Aman GP
    NSArray *arrOfActive = [self getStoredAllActiveDiaryItemTypesWithFormTemplate];
    NSMutableArray *maar = [[NSMutableArray alloc] init];
    
    for(DiaryItemTypes *activeItemsTypes in arrOfActive)
    {
        for(EventItem *eventitems in results)
        {
            if([activeItemsTypes.diaryitemtypeid integerValue]==[eventitems.itemType_id integerValue])
            {
                [maar addObject:eventitems];
            }
            
        }
    }
    
    return maar;


}



-(void)getDiaryItems
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_getdairyitems ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        [parameters setObject:profobj.type forKey:@"user_type"];
        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishgetDiaryItems:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}

- (void)netManagerDidFinishgetDiaryItems:(NetManager *)thisNetManager
                                response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
        
         [Flurry logError:FLURRY_EVENT_Error_in_fetching_Diary_Item message:FLURRY_EVENT_Error_in_fetching_Diary_Item error:thisNetManager.error];
        
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_get_Dairy_Items object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}


#pragma mark- Event Item

-(void)parseEventItemDataWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"parseEventItemDataWithDictionary %@",dictionary);
#endif
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init] ;
    [formatter1 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    NSDateFormatter *formatter3 = [[NSDateFormatter alloc] init] ;
    
    EventItem *eventItemData=[self getEventItemDataInfoStored:[dictionary objectForKey:@"AppId"] ipadd:[dictionary objectForKey:@"IpAddress"]];
    
    
    if (!eventItemData) {
        eventItemData = [NSEntityDescription
                         insertNewObjectForEntityForName:@"EventItem"
                         inManagedObjectContext:_managedObjectContext];
    }
    
    
    if ([dictionary objectForKey:@"isSync"]) {
        eventItemData.isSync=[NSNumber numberWithBool:[[dictionary objectForKey:@"isSync"] boolValue]];
    }
    else{
        eventItemData.isSync=[NSNumber numberWithBool:YES];
    }
    if (![[dictionary objectForKey:@"IpAddress"] isKindOfClass:[NSNull class]]) {
        eventItemData.ipadd=[dictionary objectForKey:@"IpAddress"];
    }
    //Aman added
    if (![[dictionary objectForKey:@"Priority"] isKindOfClass:[NSNull class]]) {
        eventItemData.priority=[dictionary objectForKey:@"Priority"];
    }
    
    eventItemData.isDelete=[NSNumber numberWithBool:NO];
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        eventItemData.item_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"ClassId"] isKindOfClass:[NSNull class]]) {
        eventItemData.class_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"ClassId"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
        eventItemData.eventName=[dictionary objectForKey:@"Name"];
    }
    
    // Aman GP
    if (![[dictionary objectForKey:@"type_id"] isKindOfClass:[NSNull class]]) {
        if([[[dictionary objectForKey:@"type_id"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]>0)
        {
            eventItemData.itemType_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"type_id"]integerValue]];
        }
        else
        {
            eventItemData.itemType_id= [NSNumber numberWithInteger:[[self getItemTypeID:@"Event"]integerValue]];
        }
        
    }
    
    
    if (![[dictionary objectForKey:@"StartDate"] isKindOfClass:[NSNull class]]) {
        eventItemData.startDate=[formatter1 dateFromString:[dictionary objectForKey:@"StartDate"]];
    }
    if (![[dictionary objectForKey:@"StartTime"] isKindOfClass:[NSNull class]]) {
        eventItemData.startTime=[dictionary objectForKey:@"StartTime"];
    }
    if (![[dictionary objectForKey:@"StartTime"] isKindOfClass:[NSNull class]]) {
        /*[formatter3 setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
        NSDate *startTimeStr=[formatter3 dateFromString:[dictionary objectForKey:@"StartTime"]];
        [formatter3 setDateFormat:kDateFormatToShowOnUI_hhmma];
        eventItemData.startTimeDate= [formatter3 dateFromString:[formatter3 stringFromDate:startTimeStr]];*/
        //==========================================================================================//
        
        //NSDate *eventStartDate = [formatter1 dateFromString:[dictionary objectForKey:@"StartDate"]];

        // Code Modified for Daisy Data ------------------------------------
        NSDate *eventStartDate = nil;
        if (![[dictionary objectForKey:@"StartDate"] isKindOfClass:[NSNull class]])
        {
            eventStartDate = [formatter1 dateFromString:[dictionary objectForKey:@"StartDate"]];
        }
        // Code Modified End -----------------------------------------------
        
        unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:eventStartDate];
        NSArray *arreventStartTime=nil;
        
        if (![[dictionary objectForKey:@"StartTime"] isKindOfClass:[NSNull class]]) {
            arreventStartTime= [[dictionary objectForKey:@"StartTime"] componentsSeparatedByString:@":"];
        }
        if([arreventStartTime count]>0)
        {
            [dateComponents setHour:[[arreventStartTime objectAtIndex:0] integerValue]];
            [dateComponents setMinute:[[arreventStartTime objectAtIndex:1] integerValue]];
            
            [dateComponents setSecond:[[arreventStartTime objectAtIndex:2] integerValue]];
        }
        NSDate *newEventStartdate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
        NSDateFormatter *lclformatter = [[NSDateFormatter alloc] init];
        [lclformatter setDateStyle:NSDateFormatterNoStyle];
        [lclformatter setTimeStyle:NSDateFormatterShortStyle];
        NSString *startDate = [lclformatter  stringFromDate:newEventStartdate];
        
#if DEBUG
        NSLog(@"[lclformatter dateFromString:startDate] = %@",[lclformatter dateFromString:startDate]);
#endif
      
        eventItemData.startTimeDate = [lclformatter dateFromString:startDate];
        [lclformatter release];
        //===========================================================================================//
        
    }
    
    
    if (![[dictionary objectForKey:@"EndDate"] isKindOfClass:[NSNull class]]) {
        eventItemData.endDate=[formatter1 dateFromString:[dictionary objectForKey:@"EndDate"]];
    }
    if (![[dictionary objectForKey:@"OnTheDay"] isKindOfClass:[NSNull class]]) {
        eventItemData.onTheDay=[NSNumber numberWithBool:[[dictionary objectForKey:@"OnTheDay"]boolValue]];
    }
    
    if (![[dictionary objectForKey:@"EndTime"] isKindOfClass:[NSNull class]]) {
        eventItemData.endTime=[dictionary objectForKey:@"EndTime"];
    }
    
    if (![[dictionary objectForKey:@"Details"] isKindOfClass:[NSNull class]]) {
        eventItemData.details=[dictionary objectForKey:@"Details"];
    }
    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
        eventItemData.school_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"publishToClass"] isKindOfClass:[NSNull class]]) {
        eventItemData.publishToClass=[NSNumber numberWithInteger:[[dictionary objectForKey:@"publishToClass"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"Created"] isKindOfClass:[NSNull class]]) {
        eventItemData.created=[self dateFromMilliSecond:[NSNumber numberWithDouble:[[dictionary objectForKey:@"Created"] doubleValue]]];
    }
    if (![[dictionary objectForKey:@"CreatedBy"] isKindOfClass:[NSNull class]])
    {
        eventItemData.created_by=[dictionary objectForKey:@"CreatedBy"];
    }
    
    if (![[dictionary objectForKey:@"Updated"] isKindOfClass:[NSNull class]])
    {
        eventItemData.udatedTime=[self dateFromMilliSecond:[NSNumber numberWithDouble:[[dictionary objectForKey:@"Updated"] doubleValue]]];
    }
    if (![[dictionary objectForKey:@"AppId"] isKindOfClass:[NSNull class]]) {
        eventItemData.universal_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"AppId"]integerValue]];
    }
    
    eventItemData.isRead=[NSNumber numberWithInteger:0];
    
    //==========================================================================================//
    
    // Code Modified for Daisy Data --------------------------------------------
    NSDate *eventStartDate = nil;
    if (![[dictionary objectForKey:@"StartDate"] isKindOfClass:[NSNull class]])
    {
        eventStartDate = [formatter1 dateFromString:[dictionary objectForKey:@"StartDate"]];
    }
    // Code modification End ---------------------------------------------------
    
    unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:eventStartDate];
    NSArray *arreventStartTime=nil;

    if (![[dictionary objectForKey:@"StartTime"] isKindOfClass:[NSNull class]]) {
        arreventStartTime= [[dictionary objectForKey:@"StartTime"] componentsSeparatedByString:@":"];
    }
    if([arreventStartTime count]>0)
    {
        [dateComponents setHour:[[arreventStartTime objectAtIndex:0] integerValue]];
        [dateComponents setMinute:[[arreventStartTime objectAtIndex:1] integerValue]];
        
        [dateComponents setSecond:[[arreventStartTime objectAtIndex:2] integerValue]];
    }
    NSDate *newEventStartdate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    
#if DEBUG
    NSLog(@"newEventStartdate =%@",[ formatter stringFromDate:newEventStartdate]);
#endif
    
    eventItemData.startDate = newEventStartdate;
    
    //===========================================================================================//
    
    //===========================================================================================//
    
    // Code Modified for Daisy Data --------------------------------------------
    NSDate *eventendDate = nil;
    if (![[dictionary objectForKey:@"EndDate"] isKindOfClass:[NSNull class]])
    {
        eventendDate = [formatter1 dateFromString:[dictionary objectForKey:@"EndDate"]];
    }
    // Code modification End ---------------------------------------------------
    
    unsigned unitFlagsDate1 = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents *dateComponents1 = [[NSCalendar currentCalendar] components:unitFlagsDate1 fromDate:eventendDate];
    NSArray *arreventEndTime=nil;
    
    if (![[dictionary objectForKey:@"EndTime"] isKindOfClass:[NSNull class]]) {
        arreventEndTime= [[dictionary objectForKey:@"EndTime"] componentsSeparatedByString:@":"];
    }
    if([arreventEndTime count]>0)
    {
        [dateComponents1 setHour:[[arreventEndTime objectAtIndex:0] integerValue]];
        [dateComponents1 setMinute:[[arreventEndTime objectAtIndex:1] integerValue]];
        
        [dateComponents1 setSecond:[[arreventEndTime objectAtIndex:2] integerValue]];
    }
    NSDate *newEventEnddate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents1];
    
#if DEBUG
    NSLog(@"newEventEnddate =%@",[ formatter stringFromDate:newEventEnddate]);
#endif
    
    eventItemData.endDate = newEventEnddate;
  
    //===========================================================================================//
    
    
    
    [formatter release];
       [formatter3 release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	}
    
    if (![[dictionary objectForKey:@"attachmentData"] isKindOfClass:[NSNull class]])
    {
        for(NSDictionary *dict in [dictionary objectForKey:@"attachmentData"])
        {
            Attachment *attachmenData=[self getDiaryAttachmentstDataInfoStoredAttachments:[dict objectForKey:@"Id"]];
            
            if (!attachmenData)
            {
                attachmenData = [NSEntityDescription
                                 insertNewObjectForEntityForName:@"Attachment"
                                 inManagedObjectContext:_managedObjectContext];
            }
            
            if (![[dict objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
                attachmenData.aID =[NSNumber numberWithInteger:[[dict objectForKey:@"Id"]integerValue]];
            }
            
            if (![[dict objectForKey:@"dateAttached"] isKindOfClass:[NSNull class]]) {
                attachmenData.dateAttached = [formatter1 dateFromString:[dict objectForKey:@"dateAttached"]];
            }
            
            if (![[dict objectForKey:@"lasyupdatedDate"] isKindOfClass:[NSNull class]]) {
                attachmenData.lastupdatedDate = [formatter1 dateFromString:[dict objectForKey:@"lasyupdatedDate"]];
            }
            
            if (![[dict objectForKey:@"DisplayName"] isKindOfClass:[NSNull class]]) {
                attachmenData.displayFileName = [dict objectForKey:@"DisplayName"];
            }
            
            if (![[dict objectForKey:@"FileType"] isKindOfClass:[NSNull class]]) {
                attachmenData.type = [dict objectForKey:@"FileType"];
            }
            
            if (![[dict objectForKey:@"localFilePath"] isKindOfClass:[NSNull class]]) {
                attachmenData.filePath = [dict objectForKey:@"localFilePath"];
            }
            
            if (![[dict objectForKey:@"FilePath"] isKindOfClass:[NSNull class]]) {
                attachmenData.remoteFilePath = [dict objectForKey:@"FilePath"];
            }
            
            if (![[dict objectForKey:@"CreatedBy"] isKindOfClass:[NSNull class]]) {
                attachmenData.createdBy = [dict objectForKey:@"CreatedBy"];
                
            }
            
            if (![[dict objectForKey:@"size"] isKindOfClass:[NSNull class]]) {
                attachmenData.size = [NSNumber numberWithInteger:[[dict objectForKey:@"size"]integerValue]];
                
            }
            
            if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
                attachmenData.diaryItemId = [NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
                
            }
            
            
            if (![[dictionary objectForKey:@"type_id"] isKindOfClass:[NSNull class]]) {
                if([[[dictionary objectForKey:@"type_id"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]>0)
                {
                    attachmenData.itemtype = [self getFormTemplateOnItemIDBasis:[NSNumber numberWithInteger:[[dictionary objectForKey:@"type_id"]integerValue]]];
                }
            
                
            }

            
            
            //            size
            
            attachmenData.isSync=[NSNumber numberWithBool:YES];
            
            attachmenData.isDeletedOnLocal=[NSNumber numberWithBool:NO];
            
            
            if(attachmenData){
                [eventItemData addEventItemattachmentObject:attachmenData];
            }
            
            //
            
            NSError *error;
            if (![_managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
            }
        }
    }
    
    [formatter1 release];

    
    
    
}



- (EventItem *)getEventItemDataInfoStored:(NSNumber*)itemId ipadd:(NSString*)ipadd
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"EventItem"];
	if(ipadd==nil){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"universal_id == %@", itemId];
        [request setPredicate:predicate];
        
    }
    else{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"universal_id == %@ AND ipadd==%@", itemId,ipadd];
        [request setPredicate:predicate];
        
    }
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results =  [[NSSet setWithArray:results] allObjects];
    
    EventItem  * addEventItemData  = nil;
	if (!error && [results count] > 0) {
		
		addEventItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return addEventItemData;
    
}


-(NSArray*)getStoredAllEventItemDataInApp
{
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"EventItem"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@",[NSNumber numberWithBool:NO]];
        [request setPredicate:predicate];

    NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        results =  [[NSSet setWithArray:results] allObjects];
        NSMutableArray* filterResults = [[NSMutableArray alloc] init];
        BOOL copy;
        
        if (![results count] == 0) {
            for (EventItem *a1 in results) {
                copy = YES;
                for (EventItem *a2 in filterResults) {
                    
                    if ([a1.item_Id integerValue]== [a2.item_Id integerValue] && [a1.universal_id integerValue]== [a2.universal_id integerValue]) {
                        copy = NO;
                        break;
                    }
                }
                if (copy) {
                    [filterResults addObject:a1];
                }
            }
        }
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
    
        results = [self getActiveEvents:filterResults];
    
    
        
        return results;
    
    
}
-(NSArray*)getStoredAllEventItemData:(NSDate*)date
{
        if(date!=nil)
        {
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"EventItem"];
        
        NSDate *startDate = date;
        NSDate *endDate = date;
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
        [components setHour:0];
        [components setMinute:0];
        [components setSecond:0];
        startDate = [calendar dateFromComponents:components];
        
        [components setHour:23];
        [components setMinute:59];
        [components setSecond:59];
        endDate = [calendar dateFromComponents:components];
        
        
        
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"startDate>=%@ AND startDate<=%@ AND isDelete==%@", startDate,endDate,[NSNumber numberWithBool:NO]];
        [request setPredicate:predicate];

            NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        
        NSMutableArray* filterResults = [[NSMutableArray alloc] init];
        BOOL copy;
        
        if (![results count] == 0) {
            for (EventItem *a1 in results) {
                copy = YES;
                for (EventItem *a2 in filterResults) {
                    
                    if ([a1.item_Id integerValue]== [a2.item_Id integerValue] && [a1.universal_id integerValue]== [a2.universal_id integerValue]) {
                        copy = NO;
                        break;
                    }
                }
                if (copy) {
                    [filterResults addObject:a1];
                }
            }
        }
       results = [self getActiveEvents:filterResults];
    
        
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
    
        return results;
        }
    else
    {
        return [NSArray array];
    }
    
    
}

#pragma mark -
#pragma mark Date Converte From GMT to Local  -

- (NSDate *)convertToLocalDateFromGMTDate:(NSDate *)gmtDate
{
    NSDateFormatter *dtFormatter = [[NSDateFormatter alloc] init];
    [dtFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    //Create the date assuming the given string is in GMT
    dtFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *date = [dtFormatter dateFromString:[dtFormatter stringFromDate:gmtDate]];
    
    //Create a date string in the local timezone
    dtFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    NSString *localDateString = [dtFormatter stringFromDate:date];
    
    return [dtFormatter dateFromString:localDateString];
}

#pragma mark- Announcement Item & News
-(void)parseNewsDataWithDictionary:(NSDictionary*)dictionary
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init] ;
    [formatter1 setDateFormat:@"dd/MM/yy"];
    
    News *annoncementsData=[self getNewsDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    if (!annoncementsData) {
        annoncementsData = [NSEntityDescription
                            insertNewObjectForEntityForName:@"News"
                            inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        annoncementsData.news_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"Updated"] isKindOfClass:[NSNull class]])
    {
      //  annoncementsData.cretedDate=[formatter1 dateFromString:[formatter1 stringFromDate:[formatter dateFromString:[dictionary objectForKey:@"Created"]]]];
        //annoncementsData.cretedDate= [dictionary objectForKey:@"Created"];
        
        NSDateFormatter *formatter3 = [[NSDateFormatter alloc] init] ;
        [formatter3 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
        //commented by Mitul - start - For issue #EZEST-2316
        /*
        NSString *createdDatestring = [formatter3 stringFromDate:[formatter dateFromString:[dictionary objectForKey:@"Updated"]]];
        
        annoncementsData.cretedDate =[formatter dateFromString:[dictionary objectForKey:@"Updated"]];
        annoncementsData.createdDate_Str = createdDatestring;
        */
        //Commented  by mitul - End - For issue #EZEST-2316

        formatter.timeZone=[NSTimeZone systemTimeZone];
        
        
        //added by viraj - start - For issue #EZEST-2316
        
//        NSDate *gmtDate  = [formatter dateFromString:[dictionary objectForKey:@"Updated"]];
        
#if DEBUG
        NSLog(@"dictionary objectForKey:Updated : %@",[dictionary objectForKey:@"Updated"]);
#endif
      
        [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
        NSString *updatedStr = [dictionary objectForKey:@"Updated"];
#if DEBUG
        NSLog(@"updatedStr : %@",updatedStr);
#endif
      
        
        NSString *updatedDateStr = [[updatedStr componentsSeparatedByString:@" "] objectAtIndex:0];
        
#if DEBUG
        NSLog(@"updatedDateStr ; %@",updatedDateStr);
#endif
      
        //Added by Mitul - start - For issue #EZEST-2316
        annoncementsData.createdDate_Str =updatedDateStr;
        
        //Added  by mitul - End - For issue #EZEST-2316
        
        
        NSDate *reminderDate = [formatter dateFromString:updatedDateStr];
        unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:reminderDate];
        
#if DEBUG
        NSLog(@"before dateComponents  :%@",dateComponents);
#endif
      
        NSArray *reminderTime=nil;
        if (![[[updatedStr componentsSeparatedByString:@" "] objectAtIndex:1] isKindOfClass:[NSNull class]]) {
            reminderTime= [[[updatedStr componentsSeparatedByString:@" "] objectAtIndex:1] componentsSeparatedByString:@":"];
            
        }
        if([reminderTime count]>0)
        {
            [dateComponents setHour:[[reminderTime objectAtIndex:0] integerValue]];
            [dateComponents setMinute:[[reminderTime objectAtIndex:1] integerValue]];
            
            [dateComponents setSecond:[[reminderTime objectAtIndex:2] integerValue]];
        }
        
#if DEBUG
        NSLog(@"after dateComponents  :%@",dateComponents);
#endif
      
        
        NSDate *gmtDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
        
#if DEBUG
        NSLog(@"News gmtDate : %@",gmtDate);
#endif
      
        
        //Added by Mitul - start - For issue #EZEST-2316
        //TODO: comment below line only for Honeycomb (Migration Change)
        //annoncementsData.cretedDate=gmtDate;
        //Added  by mitul - End - For issue #EZEST-2316
        
        //added by viraj - end - For issue #EZEST-2316
        
        NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:gmtDate];
        NSDate *gmtDate1=[gmtDate dateByAddingTimeInterval:timeZoneOffset];
        annoncementsData.created_time=gmtDate1;
    }
    if (![[dictionary objectForKey:@"Body"] isKindOfClass:[NSNull class]]) {
        annoncementsData.details=[dictionary objectForKey:@"Body"];
    }
    if (![[dictionary objectForKey:@"Tags"] isKindOfClass:[NSNull class]]) {
        annoncementsData.tags=[dictionary objectForKey:@"Tags"];
    }
    if (![[dictionary objectForKey:@"Title"] isKindOfClass:[NSNull class]]) {
        annoncementsData.title=[dictionary objectForKey:@"Title"];
    }
    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
        annoncementsData.school_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolId"]integerValue]];
    }
    
    
    annoncementsData.isReadNews=[NSNumber numberWithBool:NO];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    
    //14AugChange
    if (![[dictionary objectForKey:@"News_Attachments"] isKindOfClass:[NSNull class]]) {
        
        
        for(NSDictionary *dict in [dictionary objectForKey:@"News_Attachments"]){
            News_Attachment *attachmenData=[self getAttachmentstDataInfoStoredAttachments:[dict objectForKey:@"Id"]];
            
            
            if (!attachmenData) {
                attachmenData = [NSEntityDescription
                                 insertNewObjectForEntityForName:@"News_Attachment"
                                 inManagedObjectContext:_managedObjectContext];
            }
            
            if (![[dict objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
                attachmenData.atachment_id=[NSNumber numberWithInteger:[[dict objectForKey:@"Id"]integerValue]];
            }
            if (![[dict objectForKey:@"NewsId"] isKindOfClass:[NSNull class]]) {
                attachmenData.news_id=[NSNumber numberWithInteger:[[dict objectForKey:@"NewsId"]integerValue]];
            }
            
            if (![[dict objectForKey:@"Attachments"] isKindOfClass:[NSNull class]]) {
                attachmenData.atachment_content=[dict objectForKey:@"Attachments"];
            }
            
            
            
            if(annoncementsData){
                [annoncementsData addAttacmentsObject:attachmenData];
            }
            
            NSError *error;
            if (![_managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
            }
        }
    }
    [formatter release];
    [formatter1 release];
    
}


- (News *)getNewsDataInfoStored:(NSNumber*)news_id postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"News"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"news_id == %@", news_id];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    News  * annoncementsmData  = nil;
	if (!error && [results count] > 0) {
		
		annoncementsmData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return annoncementsmData;
    
}

-(void)parseAnnonecementDataWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"parseAnnonecementDataWithDictionary %@",dictionary);
#endif
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init] ;
    [formatter1 setDateFormat:@"dd/MM/yy"];
    Annoncements *annoncementsData=[self getAnnoncementsDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    
    //
    //    "All_Parents" = 1;
    //    "All_Students" = 1;
    //    "All_Teachers" = 1;
    //    "CampusId's" = "<null>";
    //    Created = "2013-05-14 06:41:32";
    //    CreatedBy = 23;
    //    Details = "Test Announcement 2";
    //    Id = 6;
    //    SchoolId = 1;
    //    Updated = "2013-05-14 08:26:01";
    //    UpdatedBy = 23;
    if (!annoncementsData) {
        annoncementsData = [NSEntityDescription
                            insertNewObjectForEntityForName:@"Annoncements"
                            inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        annoncementsData.annuoncement_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"All_Parents"] isKindOfClass:[NSNull class]]) {
        annoncementsData.all_parents=[NSNumber numberWithInteger:[[dictionary objectForKey:@"All_Parents"]integerValue]];
    }
    if (![[dictionary objectForKey:@"All_Students"] isKindOfClass:[NSNull class]]) {
        annoncementsData.all_Student=[NSNumber numberWithInteger:[[dictionary objectForKey:@"All_Students"]integerValue]];
    }
    if (![[dictionary objectForKey:@"All_Teachers"] isKindOfClass:[NSNull class]]) {
        annoncementsData.all_Teacher=[NSNumber numberWithInteger:[[dictionary objectForKey:@"All_Teachers"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
        annoncementsData.created_by=[dictionary objectForKey:@"Name"];
    }
    if (![[dictionary objectForKey:@"campusName"] isKindOfClass:[NSNull class]]) {
        annoncementsData.campuse=[dictionary objectForKey:@"campusName"];
    }
    if (![[dictionary objectForKey:@"Updated"] isKindOfClass:[NSNull class]]) {
      //  annoncementsData.cretedDate=[formatter1 dateFromString:[formatter1 stringFromDate:[formatter dateFromString:[dictionary objectForKey:@"Created"]]]];
        
        NSDateFormatter *formatter3 = [[NSDateFormatter alloc] init] ;
        [formatter3 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
        
        //commented by Mitul - start - For issue #EZEST-2316
        /*
        NSString *createdDatestring = [formatter3 stringFromDate:[formatter dateFromString:[dictionary objectForKey:@"Updated"]]];
        
        annoncementsData.cretedDate=[formatter dateFromString:[dictionary objectForKey:@"Updated"]];
        annoncementsData.createdDate_Str =createdDatestring;
        */
        
        //Commented  by mitul - End - For issue #EZEST-2316
        
        formatter.timeZone=[NSTimeZone systemTimeZone];
        
        
        //added by viraj - start - For issue #EZEST-2316
        
//        NSDate *gmtDate  = [formatter dateFromString:[dictionary objectForKey:@"Updated"]];
        
#if DEBUG
        NSLog(@"dictionary objectForKey:Updated : %@",[dictionary objectForKey:@"Updated"]);
#endif
      
        [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
        NSString *updatedStr = [dictionary objectForKey:@"Updated"];
#if DEBUG
        NSLog(@"updatedStr : %@",updatedStr);
#endif
      
        
        NSString *updatedDateStr = [[updatedStr componentsSeparatedByString:@" "] objectAtIndex:0];
        
#if DEBUG
        NSLog(@"updatedDateStr ; %@",updatedDateStr);
#endif
        
        //Added by Mitul - start - For issue #EZEST-2316
        annoncementsData.createdDate_Str =updatedDateStr;
        
        //Added  by mitul - End - For issue #EZEST-2316
        
        NSDate *reminderDate = [formatter dateFromString:updatedDateStr];
        unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:reminderDate];
        
#if DEBUG
        NSLog(@"before dateComponents  :%@",dateComponents);
#endif
      
        NSArray *reminderTime=nil;
        if (![[[updatedStr componentsSeparatedByString:@" "] objectAtIndex:1] isKindOfClass:[NSNull class]]) {
            reminderTime= [[[updatedStr componentsSeparatedByString:@" "] objectAtIndex:1] componentsSeparatedByString:@":"];
            
        }
        if([reminderTime count]>0)
        {
            [dateComponents setHour:[[reminderTime objectAtIndex:0] integerValue]];
            [dateComponents setMinute:[[reminderTime objectAtIndex:1] integerValue]];
            
            [dateComponents setSecond:[[reminderTime objectAtIndex:2] integerValue]];
        }
        
#if DEBUG
        NSLog(@"after dateComponents  :%@",dateComponents);
#endif
      
        
        NSDate *gmtDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
        
#if DEBUG
        NSLog(@"gmtDate : %@",gmtDate);
#endif
      
    
        //Added by Mitul - start - For issue #EZEST-2316
        //TODO: comment below line only for Honeycomb (Migration Change)
        //annoncementsData.cretedDate=gmtDate;
        //Added  by mitul - End - For issue #EZEST-2316
        
        
        //added by viraj - end - For issue #EZEST-2316
        
    
#if DEBUG
        NSLog(@"[dictionary objectForKey:Updated %@",[dictionary objectForKey:@"Updated"]);
#endif
      
        
        NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:gmtDate];
        
#if DEBUG
        NSLog(@"timeZoneOffset : %f",timeZoneOffset);
#endif
      
        NSDate *gmtDate1=[gmtDate dateByAddingTimeInterval:timeZoneOffset];
        
#if DEBUG
        NSLog(@"gmtDate1 string: %@", [formatter stringFromDate:gmtDate1] );
        NSLog(@"gmtDate1 : %@",gmtDate1);
#endif
        
        annoncementsData.created_time=gmtDate1;
        
#if DEBUG
        NSLog(@"gmtDate1 %@",gmtDate1);
#endif
      
    }
    if (![[dictionary objectForKey:@"Details"] isKindOfClass:[NSNull class]]) {
        annoncementsData.details=[dictionary objectForKey:@"Details"];
    }
    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
        annoncementsData.school_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Updated"] isKindOfClass:[NSNull class]]) {
        annoncementsData.updated_time=[formatter dateFromString:[dictionary objectForKey:@"Updated"]];
    }
    
    annoncementsData.isReadAnn=[NSNumber numberWithBool:NO];
    
#if DEBUG
    NSLog(@"annoncementsData.created_time %@",annoncementsData.created_time);
#endif
    
    
    [formatter release];
    [formatter1 release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	}
}


- (Annoncements *)getAnnoncementsDataInfoStored:(NSNumber*)annoncements_id postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Annoncements"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"annuoncement_id == %@", annoncements_id];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Annoncements  * annoncementsmData  = nil;
	if (!error && [results count] > 0) {
		
		annoncementsmData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return annoncementsmData;
    
}


-(NSArray*)getStoredAllIAnnouncement:(NSString*)date
{

#if DEBUG
    NSLog(@"getStoredAllIAnnouncement Date-%@",date);
#endif
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Annoncements"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"createdDate_Str == %@", date];
	[request setPredicate:predicate];
    
#if DEBUG
    NSLog(@"predicate %@",predicate);
#endif
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }

    return results;
    
}
-(NSArray*)getStoredAllIAnnouncementForInbox
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Annoncements"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"created_time" ascending:NO]];

    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"created_time == %@", date];
    //	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
}


-(NSArray*)getStoredAllINewsForWithDate:(NSString*)date
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"News"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"createdDate_Str == %@",date];
    [request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

-(NSArray*)getStoredAllINewsForInbox
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"News"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"created_time" ascending:NO]];

    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"created_time == %@", date];
    //	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
}

#pragma mark
#pragma mark Profile


-(void)getGradeDetail
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        
        NSString *serviceName = Method_GetGrade ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        // Webservice Logging
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"schoolid"];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetGradeDetail:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}

- (void)netManagerDidFinishGetGradeDetail:(NetManager *)thisNetManager
                                 response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_getGrade_info object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

-(NSArray*)getStoredAllGradeForProfile
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Grade"];
    
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"created_time == %@", date];
    //	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

- (Grade *)getGradeData:(NSString*)gradeName
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Grade"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"grade_name == %@", gradeName];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Grade  * countryData  = nil;
	if (!error && [results count] > 0) {
		
		countryData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return countryData;
    
}



- (Grade *)getGradeDataInfoStored:(NSString*)teacherid postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Grade"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"grade_id == %@", teacherid];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Grade  * gradeData  = nil;
	if (!error && [results count] > 0) {
		
		gradeData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return gradeData;
    
}


-(void)parseGradeDataWithDictionary:(NSDictionary*)dictionary
{
    Grade *gradeData=[self getGradeDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    
    if (!gradeData) {
        gradeData = [NSEntityDescription
                     insertNewObjectForEntityForName:@"Grade"
                     inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        gradeData.grade_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
        gradeData.grade_name=[dictionary objectForKey:@"Name"];
    }
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
	}
}




-(void)getParentDetail:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_Parent_deatil ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
        
        [parameters setObject:[dict objectForKey:@"user_id"]forKey:@"user_id"];
        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        TableUpdateTime *tabl=[self getlastUpdateTableDataInfoStored];
        if(tabl.parentsUpdateTime == nil)
            [parameters setObject:[NSNumber numberWithInteger:0]  forKey:@"LastSync"];
        else
            [parameters setObject:tabl.parentsUpdateTime  forKey:@"LastSync"];
        
#if DEBUG
        NSLog(@"getParentDetail %@",[parameters JSONRepresentation]);
#endif
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetParentDetail:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}

- (void)netManagerDidFinishGetParentDetail:(NetManager *)thisNetManager
                                  response:(NSMutableData *)responseData
{
    
#if DEBUG
    NSLog(@"Parent Details response Called");
#endif
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
#if DEBUG
        NSLog(@"netManagerDidFinishGetParentDetail jsonDictD %@",jsonDictD);
#endif
      
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
        
#if DEBUG
        NSLog(@"netManagerDidFinishGetParentDetail dictionary%@",dictionary);
#endif
      
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_Parent_Detail object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}



//for teachers detail
-(void)getTeacherDetail:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_Teacher_detail ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
        [parameters setObject:[dict objectForKey:@"user_id"]forKey:@"user_id"];
        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        TableUpdateTime *tabl=[self getlastUpdateTableDataInfoStored];
        if(tabl.teacherUpdateTime == nil)
            [parameters setObject:[NSNumber numberWithInteger:0]  forKey:@"LastSync"];
        else
            [parameters setObject:tabl.teacherUpdateTime  forKey:@"LastSync"];
        
#if DEBUG
        NSLog(@"getTeacherDetail %@",[parameters JSONRepresentation]);
#endif
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetTeacherDetail:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
        
		//[AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
	}
}

- (void)netManagerDidFinishGetTeacherDetail:(NetManager *)thisNetManager
                                   response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_Teacher_Detail object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}



//for Parent Invitation
-(void)sendParentInvitation:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        
        
        NSString *serviceName = Method_Parent_Invitaion ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
        
        [parameters setObject:[dict objectForKey:@"user_id"]forKey:@"user_id"];
        [parameters setObject:[dict objectForKey:@"email"]forKey:@"email"];
        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];

        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishsSendParentInvitation:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
	}
}

- (void)netManagerDidFinishsSendParentInvitation:(NetManager *)thisNetManager
                                        response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];

        [Flurry logError:FLURRY_EVENT_Error_in_sending_parent_invitation message:FLURRY_EVENT_Error_in_sending_parent_invitation error:nil];
      
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_Send_Invitaio_To_Parent object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}


-(void)updateUserProfile:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_Update_Profile ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
        TableUpdateTime *tabl=[self getlastUpdateTableDataInfoStored];
        if(tabl.profile == nil)
            [parameters setObject:[NSNumber numberWithInteger:0]  forKey:@"LastSync"];
        else
            [parameters setObject:tabl.profile  forKey:@"LastSync"];

        
        [parameters setObject:[dict objectForKey:@"user_id"]forKey:@"user_id"];
        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishUpdate:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
	}
}

- (void)netManagerDidFinishUpdate:(NetManager *)thisNetManager
                         response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_UpdateProfile object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}


-(void)getCountryDetail:(NSString*)time
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_GetCountry ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];

        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetCountryDetail:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}

- (void)netManagerDidFinishGetCountryDetail:(NetManager *)thisNetManager
                                   response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_GetCountry object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

-(void)insertUserprfile:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_updateprofile ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        [parameters setObject:@"1" forKey:@"isImage"];
        if([dict objectForKey:@"image"]){
            
            [parameters setObject:[dict objectForKey:@"image"]  forKey:@"file"];
        }
        if([dict objectForKey:@"DateOfBirth"]){
            [parameters setObject:[dict objectForKey:@"DateOfBirth"]  forKey:@"DateOfBirth"];
        }
        if([dict objectForKey:@"Email"]){
            [parameters setObject:[dict objectForKey:@"Email"]  forKey:@"Email"];
        }
        if([dict objectForKey:@"LastName"]){
            [parameters setObject:[dict objectForKey:@"LastName"]  forKey:@"LastName"];
        }
        if([dict objectForKey:@"FirstName"]){
            [parameters setObject:[dict objectForKey:@"FirstName"]  forKey:@"FirstName"];
        }
        if([dict objectForKey:@"Telephone"]){
            [parameters setObject:[dict objectForKey:@"Telephone"]  forKey:@"Telephone"];
        }
        if([dict objectForKey:@"Address_line_1"]){
            [parameters setObject:[dict objectForKey:@"Address_line_1"]  forKey:@"Address_line_1"];
        }
        if([dict objectForKey:@"gradeId"]){
            [parameters setObject:[dict objectForKey:@"gradeId"]  forKey:@"gradeId"];
        }
        if([dict objectForKey:@"StateId"]){
            [parameters setObject:[dict objectForKey:@"StateId"]  forKey:@"StateId"];
        }
        if([dict objectForKey:@"CountryId"]){
            [parameters setObject:[dict objectForKey:@"CountryId"]  forKey:@"CountryId"];
        }
        if([dict objectForKey:@"PostalCode"]){
            [parameters setObject:[dict objectForKey:@"PostalCode"]  forKey:@"PostalCode"];
        }
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"]  forKey:@"user_id"];
        
        if([dict objectForKey:@"CityTownSuburb"]){
            [parameters setObject:[dict objectForKey:@"CityTownSuburb"]  forKey:@"CityTownSuburb"];
        }
        
        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishInsert:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
	}
}
- (void)netManagerDidFinishInsert:(NetManager *)thisNetManager
                         response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_updateprofile object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}


-(void)parseProfileDataWithDictionary:(NSDictionary*)dictionary
{
    //5 Aug 2013
    NSDictionary *dictForAddress;
    NSDictionary *dictForUserDetail;
    
    dictForAddress=[dictionary objectForKey:@"address"];
    dictForUserDetail=[dictionary objectForKey:@"user_details"];

#if DEBUG
    NSLog(@"parseProfileDataWithDictionary %@",dictionary);
    NSLog(@"parseProfileDataWithDictionary %@",dictForUserDetail);
#endif
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    
    MyProfile *profileData = nil;
    if ([dictForUserDetail count] > 0 )
    {
        profileData = [self getProfileDataInfoStored:[dictForUserDetail objectForKey:@"Id"] postNotification:NO];
    }
    
    if ([dictForUserDetail count] > 0 )
    {
    if (!profileData) {
        profileData = [NSEntityDescription
                       insertNewObjectForEntityForName:@"MyProfile"
                       inManagedObjectContext:_managedObjectContext];
    }
    }
    if ([dictForUserDetail count] > 0 )
    {
        [AppHelper saveToUserDefaults:[dictForUserDetail objectForKey:@"Id"] withKey:@"user_id"];
        
        [AppHelper saveToUserDefaults:[dictForUserDetail objectForKey:@"SchoolId"] withKey:@"school_Id"];
  
    }
    
    
    if ([AppHelper userDefaultsForKey:@"user_type"]) {
        profileData.type=[AppHelper userDefaultsForKey:@"user_type"];
    }
    
    if ([AppHelper userDefaultsForKey:@"school_Id"]) {
        profileData.school_Id=[NSNumber numberWithInteger:[[AppHelper userDefaultsForKey:@"school_Id"] integerValue]] ;
    }
    
    
    if ([dictForUserDetail count] > 0 )
    {
    if (![[dictForUserDetail objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        profileData.profile_id=[dictForUserDetail objectForKey:@"Id"];
    }
    if (![[dictForUserDetail objectForKey:@"ProfileImage"] isKindOfClass:[NSNull class]]) {
        profileData.image_url=[dictForUserDetail objectForKey:@"ProfileImage"];
    }
    //23AugChanged
    
    if (![[dictForUserDetail objectForKey:@"NormalUserId"] isKindOfClass:[NSNull class]]) {
        profileData.normal_user_id=[dictForUserDetail objectForKey:@"NormalUserId"];
    }

    
    if (![[dictForUserDetail objectForKey:@"FirstName"] isKindOfClass:[NSNull class]]) {
        profileData.name=[dictForUserDetail objectForKey:@"FirstName"];
    }
    
    if (![[dictForUserDetail objectForKey:@"LastName"] isKindOfClass:[NSNull class]]) {
        profileData.last_name=[dictForUserDetail objectForKey:@"LastName"];
    }
    
    if (![[dictForUserDetail objectForKey:@"DateOfBirth"] isKindOfClass:[NSNull class]]) {
        profileData.birthDate=[formatter dateFromString:[dictForUserDetail objectForKey:@"DateOfBirth"]];
    }
    
    
    if (![[dictForUserDetail objectForKey:@"Telephone"] isKindOfClass:[NSNull class]]) {
        profileData.phone=[dictForUserDetail objectForKey:@"Telephone"];
    }
    
    if (![[dictForUserDetail objectForKey:@"grade_name"] isKindOfClass:[NSNull class]]) {
        profileData.year=[dictForUserDetail objectForKey:@"grade_name"];
    }
    if (![[dictForUserDetail objectForKey:@"Email"] isKindOfClass:[NSNull class]]) {
        profileData.email=[dictForUserDetail objectForKey:@"Email"];
    }
    }
    
    if ([dictForAddress count] > 0 )
    {
    //profileData.year=@"2013";
    if (![[dictForAddress objectForKey:@"Address_line_1"] isKindOfClass:[NSNull class]]) {
        profileData.address=[dictForAddress objectForKey:@"Address_line_1"];
    }
    if (![[dictForAddress objectForKey:@"CityTownSuburb"] isKindOfClass:[NSNull class]]) {
        profileData.street=[dictForAddress objectForKey:@"CityTownSuburb"];
    }
    if (![[dictForAddress objectForKey:@"state_name"] isKindOfClass:[NSNull class]]) {
        profileData.state=[dictForAddress objectForKey:@"state_name"];
    }
    if (![[dictForAddress objectForKey:@"country_name"] isKindOfClass:[NSNull class]]) {
        profileData.country=[dictForAddress objectForKey:@"country_name"];
    }
    if (![[dictForAddress objectForKey:@"PostalCode"] isKindOfClass:[NSNull class]]) {
        profileData.zipCode=[dictForAddress objectForKey:@"PostalCode"];
    }
    
    }
    //Important
    profileData.isSync=[NSNumber numberWithBool:YES];
    ////////////////
    [formatter release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}
}


//for parse myprofile data
-(MyParent*)parseMyParentDataWithDictionary:(NSDictionary*)dictionary forStudent:(NSString *)studentId  andStudentName :(NSString *) strStudentName
{
#if DEBUG
    NSLog(@"parseMyParentDataWithDictionary Dict @@@ %@",dictionary);
#endif
    
    
    MyProfile *profileData=[self getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    MyParent *parentData=[self getParenteDataInfoStored:[dictionary objectForKey:@"Id"] studentID:studentId postNotification:NO];
    
    if (!parentData) {
        parentData = [NSEntityDescription
                      insertNewObjectForEntityForName:@"MyParent"
                      inManagedObjectContext:_managedObjectContext];
    }
    if(![[dictionary objectForKey:@"ProfileImage"]isKindOfClass:[NSNull class]])
    {
        if (![[dictionary objectForKey:@"ProfileImage"] isEqualToString:parentData.imageUrl]) {
            parentData.image=nil;
        }
        
        parentData.imageUrl=[dictionary objectForKey:@"image"];
    }
    else
        parentData.image=nil;
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        
//        if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
//        {
//            parentData.parent_id = (NSString*)[NSNumber numberWithInt: [[dictionary objectForKey:@"Id"]intValue]];
//        }
//        else
//        {
//            parentData.parent_id = [dictionary objectForKey:@"Id"];
//        }
        // New Code Aman Migration Prime
        
        parentData.parent_id_str = [dictionary objectForKey:@"Id"];
        
    }
    
    
    if (![[dictionary objectForKey:@"FirstName"] isKindOfClass:[NSNull class]]) {
        NSString *str=nil;
        //5 Aug 2013
        if (![[dictionary objectForKey:@"LastName"] isKindOfClass:[NSNull class]]) {
            str=[dictionary objectForKey:@"LastName"];
        }
        if(str.length>0){
            parentData.name=[NSString stringWithFormat:@"%@ %@",[dictionary objectForKey:@"FirstName"],[dictionary objectForKey:@"LastName"]];
        }
        else{
            parentData.name=[dictionary objectForKey:@"FirstName"];
        }
        
    }
    if (![[dictionary objectForKey:@"Telephone"] isKindOfClass:[NSNull class]]) {
        parentData.phone=[dictionary objectForKey:@"Telephone"];
    }
    if (![[dictionary objectForKey:@"Email"] isKindOfClass:[NSNull class]]) {
        parentData.email=[dictionary objectForKey:@"Email"];
    }
    
    if (studentId.length!=0) {
        parentData.studentId = studentId ;
    }
    
    if (strStudentName.length!=0) {
        parentData.studentName = [@"P/o " stringByAppendingString:strStudentName] ;
    }
    
    [profileData addParentsObject:parentData];
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    return parentData;
}

/*************************New Code for deleting DeletedParents AND UpdatedParents 30 May 2014 *************/

#pragma mark parseToUpdateMyParentDataWithDictionary

-(void)parseToUpdateMyParentDataWithDictionary:(NSDictionary*)dictionary
{
    MyProfile *profileData=[self getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    MyParent *parentData=[self getUpdateParenteDataInfoStored:[dictionary objectForKey:@"Id"]];
    
    if (!parentData) {
        parentData = [NSEntityDescription
                      insertNewObjectForEntityForName:@"MyParent"
                      inManagedObjectContext:_managedObjectContext];
    }
    if(![[dictionary objectForKey:@"ProfileImage"]isKindOfClass:[NSNull class]])
    {
        if (![[dictionary objectForKey:@"ProfileImage"] isEqualToString:parentData.imageUrl]) {
            parentData.image=nil;
        }
        
        parentData.imageUrl=[dictionary objectForKey:@"image"];
    }
    else
        parentData.image=nil;
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        
//        if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
//        {
//            parentData.parent_id = (NSString*)[NSNumber numberWithInt: [[dictionary objectForKey:@"Id"]intValue]];
//        }
//        else
//        {
//            parentData.parent_id = [dictionary objectForKey:@"Id"];
//        }
        // New Code Aman Migration Prime
        
        parentData.parent_id_str = [dictionary objectForKey:@"Id"];
    }
    
    
    if (![[dictionary objectForKey:@"FirstName"] isKindOfClass:[NSNull class]]) {
        NSString *str=nil;
        //5 Aug 2013
        if (![[dictionary objectForKey:@"LastName"] isKindOfClass:[NSNull class]]) {
            str=[dictionary objectForKey:@"LastName"];
        }
        if(str.length>0){
            parentData.name=[NSString stringWithFormat:@"%@ %@",[dictionary objectForKey:@"FirstName"],[dictionary objectForKey:@"LastName"]];
        }
        else{
            parentData.name=[dictionary objectForKey:@"FirstName"];
        }
        
    }
    if (![[dictionary objectForKey:@"Telephone"] isKindOfClass:[NSNull class]]) {
        parentData.phone=[dictionary objectForKey:@"Telephone"];
    }
    if (![[dictionary objectForKey:@"Email"] isKindOfClass:[NSNull class]]) {
        parentData.email=[dictionary objectForKey:@"Email"];
    }
    
    [profileData addParentsObject:parentData];
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}

}


#pragma mark getUpdateParenteDataInfoStored
- (MyParent *)getUpdateParenteDataInfoStored:(NSString*)parentid
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyParent"];
	
    // Modified By Amol G Added student Id in predicates.
 	// New Code Aman Migration Prime
    
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parent_id_str == %@", parentid];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyParent  * parentData  = nil;
	if (!error && [results count] > 0) {
		
		parentData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return parentData;
}

#pragma mark getParenteForParentID
- (NSArray *)getParentForParentID:(NSString *)parentId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyParent"];
	
 	// New Code Aman Migration Prime
    
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parent_id_str == %@", parentId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
      NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

#pragma mark deleteParentWithId
- (void) deleteParentWithId :(NSString *)parentId
{
    NSError *error;
    
    NSArray *objects = [self getParentForParentID:parentId];
    if (objects == nil) {
        // handle error
    } else {
        for (MyParent *object in objects) {
            [_managedObjectContext deleteObject:object];
        }
        [_managedObjectContext save:&error];
    }
}



/*************************New Code for deleting DeletedParents AND UpdatedParents 30 May 2014 *************/





//for parse myteacher data
-(MyTeacher*)parseMyTeacherDataWithDictionary:(NSDictionary*)dictionary{
    
    MyProfile *profileData=[self getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    
    MyTeacher *teacherData=[self getTeacherDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    if (!teacherData) {
        teacherData = [NSEntityDescription
                       insertNewObjectForEntityForName:@"MyTeacher"
                       inManagedObjectContext:_managedObjectContext];
    }
    
    if(![[dictionary objectForKey:@"ProfileImage"]isKindOfClass:[NSNull class]])
    {
        if (![[dictionary objectForKey:@"ProfileImage"] isEqualToString:teacherData.imageUrl]) {
            teacherData.image=nil;
        }
        
        teacherData.imageUrl=[dictionary objectForKey:@"ProfileImage"];
    }
    else
        teacherData.image=nil;
    
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        teacherData.teacher_id=[dictionary objectForKey:@"Id"];
    }
    if (![[dictionary objectForKey:@"Status"] isKindOfClass:[NSNull class]]) {
        teacherData.status=[dictionary objectForKey:@"Status"];
    }
    if (![[dictionary objectForKey:@"salutation"] isKindOfClass:[NSNull class]]) {
        teacherData.salutation=[dictionary objectForKey:@"salutation"];
    }
    if (![[dictionary objectForKey:@"FirstName"] isKindOfClass:[NSNull class]]) {
        NSString *str=nil;
        if (![[dictionary objectForKey:@"LastName"] isKindOfClass:[NSNull class]]) {
            str=[dictionary objectForKey:@"LastName"];
        }
        if(str.length>0){
            teacherData.name=[NSString stringWithFormat:@"%@ %@",[dictionary objectForKey:@"FirstName"],[dictionary objectForKey:@"LastName"]];
        }
        else{
            teacherData.name=[dictionary objectForKey:@"FirstName"];
        }
        
    }
    if (![[dictionary objectForKey:@"subject_id"] isKindOfClass:[NSNull class]]) {
        teacherData.subject_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"subject_id"]integerValue]];
    }
    //22AugChanged
    if (![[dictionary objectForKey:@"subjects"] isKindOfClass:[NSNull class]]) {
        teacherData.subject_name=[dictionary objectForKey:@"subjects"];
    }
    if (![[dictionary objectForKey:@"Telephone"] isKindOfClass:[NSNull class]]) {
        teacherData.phone=[dictionary objectForKey:@"Telephone"];
    }
    if (![[dictionary objectForKey:@"Email"] isKindOfClass:[NSNull class]]) {
        teacherData.email=[dictionary objectForKey:@"Email"];
    }
    [profileData addTeacherObject:teacherData];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	}
    return teacherData;
}



- (MyProfile *)getProfileDataInfoStored:(NSString*)profid postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyProfile"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"profile_id == %@", profid];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyProfile  * profileData  = nil;
	if (!error && [results count] > 0) {
		
		profileData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return profileData;
    
}

- (Report *)getSingleReportDataInfoStored:(NSString*)profid postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Report"];
	
 	//NSPredicate *predicate = [NSPredicate predicateWithFormat:@"profile_id == %@", profid];
	//[request setPredicate:predicate];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"report_id==%@", profid];
	[request setPredicate:predicate];
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Report  * reportData  = nil;
     if (!error && [results count] > 0) {
     
     reportData = [results objectAtIndex:0];
     }
     if (error)
     {
#if DEBUG
         NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
     }
     
     return reportData;
}

- (NSArray *)getListOfValues
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"ListofValues"];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isActive == %@",[NSNumber numberWithInt:1]];
//	[request setPredicate:predicate];

 	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
   
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
}

- (NSArray *)getReportDataInfoStored:(NSString*)profid postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Report"];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isActive == %@",[NSNumber numberWithInteger:1]];
	[request setPredicate:predicate];

    
 	//NSPredicate *predicate = [NSPredicate predicateWithFormat:@"profile_id == %@", profid];
	//[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    NSSortDescriptor *sortBasedOnDueTime;
    sortBasedOnDueTime = [NSSortDescriptor sortDescriptorWithKey:@"report_id"ascending:YES];
    NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortBasedOnDueTime];
    NSArray * sortedArray =[NSArray arrayWithArray:[results sortedArrayUsingDescriptors:sortDescriptorArr]];
    results = sortedArray;

    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}




- (MyParent *)getParenteDataInfoStored:(NSString*)parentid studentID:(NSString*)studentID postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyParent"];
	
    // Modified By Amol G Added student Id in predicates.
 	// New Code Aman Migration Prime
    
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parent_id_str == %@ && studentId == %@", parentid,studentID];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyParent  * parentData  = nil;
	if (!error && [results count] > 0) {
		
		parentData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return parentData;
}




- (NSArray *)getParenteDataForStudent:(NSString *)studentId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyParent"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"studentId == %@", studentId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

- (MyTeacher *)getTeacherDataInfoStored:(NSString*)teacherid postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyTeacher"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"teacher_id == %@", teacherid];
    
 	//NSPredicate *predicate = [NSPredicate predicateWithFormat:@"teacher_id == %@ && status = %@", teacherid,@"1"];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyTeacher  * teacherData  = nil;
	if (!error && [results count] > 0) {
		
		teacherData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return teacherData;
    
}


-(void)parseCountryDataWithDictionary:(NSDictionary*)dictionary{
    
    Country *countryData=[self getCountrymDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    
    if (!countryData) {
        countryData = [NSEntityDescription
                       insertNewObjectForEntityForName:@"Country"
                       inManagedObjectContext:_managedObjectContext];
    }
    
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        countryData.country_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
        countryData.country_name=[dictionary objectForKey:@"Name"];
    }
    NSArray *arr=[dictionary objectForKey:@"States"];
    for(NSDictionary *dict in arr){
        States *state=[self getStateDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
        
        if (!state) {
            state = [NSEntityDescription
                     insertNewObjectForEntityForName:@"States"
                     inManagedObjectContext:_managedObjectContext];
        }
        if (![[dict objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
            state.state_id=[NSNumber numberWithInteger:[[dict objectForKey:@"Id"]integerValue]];
        }
        if (![[dict objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
            state.state_name=[dict objectForKey:@"Name"];
        }
        [countryData addStateObject:state];
        
    }
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}
    
}


- (States *)getStateDataInfoStored:(NSNumber*)state_id postNotification:(BOOL)postNotification
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"States"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"state_id == %@", state_id];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    States  * countryData  = nil;
	if (!error && [results count] > 0) {
		
		countryData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return countryData;
    
}

- (Country *)getCountrymDataInfoStored:(NSNumber*)cuntryId postNotification:(BOOL)postNotification
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Country"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"country_id == %@", cuntryId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Country  * countryData  = nil;
	if (!error && [results count] > 0) {
		
		countryData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
      NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return countryData;
    
}

-(NSArray*)getStoredAllStateData
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Country"];
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assignedDate == %@", date];
	//[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}
- (States *)getStateData:(NSString*)state
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"States"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"state_name == %@", state];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    States  * countryData  = nil;
	if (!error && [results count] > 0) {
		
		countryData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return countryData;
    
}
- (Country *)getCountrymData:(NSString*)country
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Country"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"country_name == %@", country];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Country  * countryData  = nil;
	if (!error && [results count] > 0) {
		
		countryData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return countryData;
    
}

#pragma mark Subject
- (void) deleteObjectOfSubject:(NSString*)subName  {
    
    
    NSError *error;
    NSManagedObject *eventToDelete = [self getSubjectsDataInfoStored:subName postNotification:NO];
    [_managedObjectContext deleteObject:eventToDelete];
    
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif
      
    }
}



-(void)parseSubjectDataWithDictionary:(NSDictionary*)dictionary
{
    Subjects *subjectData=[self getSubjectsDataInfoStored:[dictionary objectForKey:@"Name"] postNotification:NO];
    
    
    if (!subjectData) {
        subjectData = [NSEntityDescription
                       insertNewObjectForEntityForName:@"Subjects"
                       inManagedObjectContext:_managedObjectContext];
    }
    
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        subjectData.subject_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
        subjectData.school_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"CampusId"] isKindOfClass:[NSNull class]]) {
        subjectData.campus_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"CampusId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"IsActive"] isKindOfClass:[NSNull class]]) {
        subjectData.isActive=[NSNumber numberWithInteger:[[dictionary objectForKey:@"IsActive"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
        subjectData.subject_name=[dictionary objectForKey:@"Name"];
    }
    if (![[dictionary objectForKey:@"Code"] isKindOfClass:[NSNull class]]) {
        subjectData.subject_color=[dictionary objectForKey:@"Code"];
    }
    subjectData.user_id=[AppHelper userDefaultsForKey:@"user_id"];
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    
}

- (Subjects *)getSubjectsDataInfoStored:(NSString*)subjectName postNotification:(BOOL)postNotification
{
    
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Subjects"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"subject_name == %@", subjectName];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Subjects  * subjectsData  = nil;
	if (!error && [results count] > 0) {
		
		subjectsData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return subjectsData;
    
}

- (Subjects *)getSubjectDataWithName:(NSString*)subject
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Subjects"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"subject_name == %@", subject];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Subjects  * countryData  = nil;
	if (!error && [results count] > 0) {
		
		countryData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return countryData;
    
}
-(NSArray*)getStoredAllSubjectData
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Subjects"];
    
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user_id == %@", [AppHelper userDefaultsForKey:@"user_id"]];
    //	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

//19 may
#pragma mark SchoolDetails
- (School_detail *)getSchoolDetailDataInfoStored:(NSString*)schoolid postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"School_detail"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"school_Id == %@", schoolid];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    School_detail  * schoolData  = nil;
	if (!error && [results count] > 0) {
		
		schoolData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return schoolData;
    
}
-(void)parseSchoolDetailDataWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"parseSchoolDetailDataWithDictionary dictionary======%@",dictionary);
#endif
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    
    School_detail *schoolData=[self getSchoolDetailDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    if (!schoolData) {
        schoolData = [NSEntityDescription
                      insertNewObjectForEntityForName:@"School_detail"
                      inManagedObjectContext:_managedObjectContext];
    }
    
    /*****  Added By Amol G Allow Parent   ***/
    
    if (![[dictionary objectForKey:@"Allow_Parent"] isKindOfClass:[NSNull class]]) {
        schoolData.allow_parent=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Allow_Parent"]integerValue]];
    }
    
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        schoolData.school_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Theme"] isKindOfClass:[NSNull class]]) {
        schoolData.theme_color=[dictionary objectForKey:@"Theme"];
    }
    if (![[dictionary objectForKey:@"school_bgImage"] isKindOfClass:[NSNull class]]) {
        schoolData.school_BgImageUrl=[dictionary objectForKey:@"school_bgImage"];
    }
    if (![[dictionary objectForKey:@"AddressId"] isKindOfClass:[NSNull class]]) {
        schoolData.address_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"AddressId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Display_Name"] isKindOfClass:[NSNull class]]) {
        schoolData.display_Name=[dictionary objectForKey:@"Display_Name"];
    }
    if (![[dictionary objectForKey:@"Empower_Type"] isKindOfClass:[NSNull class]]) {
        schoolData.empower_type=[dictionary objectForKey:@"Empower_Type"];
    }
    if (![[dictionary objectForKey:@"Regional_Format"] isKindOfClass:[NSNull class]]) {
        schoolData.region=[dictionary objectForKey:@"Regional_Format"];

        // Region Value added in UserDefaults To check which school's user is loggedin
        [AppHelper saveToUserDefaults:[dictionary objectForKey:@"Regional_Format"] withKey:kKeyDefRegion];
    }
    if (![[dictionary objectForKey:@"Diary_Title"] isKindOfClass:[NSNull class]]) {
        schoolData.dairy_Titled=[dictionary objectForKey:@"Diary_Title"];
    }
    if (![[dictionary objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
        schoolData.name=[dictionary objectForKey:@"Name"];
    }
    if (![[dictionary objectForKey:@"DomainName"] isKindOfClass:[NSNull class]]) {
        schoolData.domainName=[dictionary objectForKey:@"DomainName"];
    }
    if (![[dictionary objectForKey:@"HasUploads"] isKindOfClass:[NSNull class]]) {
        schoolData.has_Upload=[NSNumber numberWithInteger:[[dictionary objectForKey:@"HasUploads"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Updated"] isKindOfClass:[NSNull class]]) {
        schoolData.updated=[formatter dateFromString:[dictionary objectForKey:@"Updated"]];
    }
    if (![[dictionary objectForKey:@"Include_Empower"] isKindOfClass:[NSNull class]]) {
        schoolData.include_Impower=[dictionary objectForKey:@"Include_Empower"];
    }
    
    if (![[dictionary objectForKey:@"IsDeleted"] isKindOfClass:[NSNull class]]) {
        schoolData.is_Deleted=[NSNumber numberWithInteger:[[dictionary objectForKey:@"IsDeleted"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Mission_Statement"] isKindOfClass:[NSNull class]]) {
        schoolData.mission_Statement=[dictionary objectForKey:@"Mission_Statement"];
    }
    
    if (![[dictionary objectForKey:@"Motto"] isKindOfClass:[NSNull class]]) {
        schoolData.motto=[dictionary objectForKey:@"Motto"];
    }
    if (![[dictionary objectForKey:@"number_of_Campuses"] isKindOfClass:[NSNull class]]) {
        schoolData.noOfCampus=[NSString stringWithFormat:@"%@",[dictionary objectForKey:@"number_of_Campuses"]];

    }
    if (![[dictionary objectForKey:@"Phone_Number"] isKindOfClass:[NSNull class]]) {
        schoolData.phone_no=[dictionary objectForKey:@"Phone_Number"];
    }
    if (![[dictionary objectForKey:@"SchoolConfig_Id"] isKindOfClass:[NSNull class]]) {
        schoolData.school_conf_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolConfig_Id"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"Website_URL"] isKindOfClass:[NSNull class]]) {
        schoolData.web_Site_Url=[dictionary objectForKey:@"Website_URL"];
    }
    if (![[dictionary objectForKey:@"School_Image"] isKindOfClass:[NSNull class]]) {
        schoolData.school_Image_Url=[dictionary objectForKey:@"School_Image"];
    }
    if (![[dictionary objectForKey:@"School_Logo"] isKindOfClass:[NSNull class]]) {
        schoolData.logo_Image_Url=[dictionary objectForKey:@"School_Logo"];
    }
    if (![[dictionary objectForKey:@"Fax_Number"] isKindOfClass:[NSNull class]]) {
        schoolData.fax_Numbers=[dictionary objectForKey:@"Fax_Number"];
    }
    
    [formatter release];
    
#if DEBUG
    NSLog(@"schoolData : %@",schoolData);
#endif
    
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
}
-(void)updateNumberOfCampus:(NSString*)numberOfCampus
{
    
     School_detail *schoolData = [self getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"]  postNotification:NO];
    
      if(schoolData)
      {
          schoolData.noOfCampus =numberOfCampus;
      }
    
    NSError *error;
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
      
    }
}


#pragma mark Forget Password
-(void)sendPasswordOnMail:(NSString*)email
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_ForgetPassword ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        [parameters setObject:email forKey:@"email_id"];
        // Webservice Logging New both
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishForgetPassword:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
        
		//[AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
	}
}
- (void)netManagerDidFinishForgetPassword:(NetManager *)thisNetManager
                                 response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_ForgetPassword object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}
#pragma mark Login
-(void)registrationWithUser:(NSMutableDictionary*)dict
{
#if DEBUG
    NSLog(@"registrationWithUser %@",dict);
#endif
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_Login ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        [parameters setObject:[dict objectForKey:@"email_id"]  forKey:@"email_id"];
        [parameters setObject:[dict objectForKey:@"password"] forKey:@"password"];
        
        if([[dict objectForKey:@"domain"] length]>0)
        {
        [parameters setObject:[dict objectForKey:@"domain"] forKey:@"domain"];
        }
        [parameters setObject:[dict objectForKey:@"device_type"] forKey:@"device_type"];
        if(![AppHelper userDefaultsForKey:@"Token"]){
            
            [parameters setObject:[NSString stringWithFormat:@"%@",@"hsf3ewra325rf4354dsrw546423df54s43657dd243sdsa32"] forKey:@"device_token"];
        }
        else{
            [parameters setObject:[AppHelper userDefaultsForKey:@"Token"] forKey:@"device_token"];
        }
        if([dict objectForKey:@"user_id"])
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];

        
        NSError *error;
        
        NSString *udid = [SSKeychain passwordForService:@"com.app.daisyApp" account:@"AppUser" error:&error];
        if (!udid)
        {
            [DELEGATE createNewUUID];
            
            
        }
        
        NSString *udid1 = [SSKeychain passwordForService:@"com.app.daisyApp" account:@"AppUser" error:&error];
        [parameters setObject:udid1 forKey:@"macAddress"];


#if DEBUG
        NSLog(@"request URL %@",urlString);
        
        NSLog(@"registrationWithUser [parameters JSONRepresentation] %@",[parameters JSONRepresentation]);
#endif
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishRegistration:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate] hideIndicator];
        
        // New code added to set alert for offline while user is trying to login from login screen
        NSString *strPwd = [AppHelper userDefaultsForKey:kPasswordKey];
        
        // If there is no such password added in user defaults then user is not already login
        // if user is not already login then show the offline alert message
        // if user is already login then he/she can access the app in offline and do not show offline alert
        if([strPwd length]==0)
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
	}
}

- (void)netManagerDidFinishRegistration:(NetManager *)thisNetManager
                               response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_Login object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

#pragma mark Login
-(void)feedBackFromUser:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_FeedBack;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
        [parameters setObject:[dict objectForKey:@"email_id"]  forKey:@"email_id"];
        [parameters setObject:[dict objectForKey:@"message"] forKey:@"message"];
        [parameters setObject:[dict objectForKey:@"device_type"] forKey:@"device_type"];
        if(![AppHelper userDefaultsForKey:@"Token"]){
            
            [parameters setObject:[NSString stringWithFormat:@"%@",@"hsf3ewra325rf4354dsrw546423df54s43657dd243sdsa32"] forKey:@"device_token"];
        }
        else{
            [parameters setObject:[AppHelper userDefaultsForKey:@"Token"] forKey:@"device_token"];
        }
        
        // Webservice Logging New
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishFeedback:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate] hideIndicator];
	}
}

- (void)netManagerDidFinishFeedback:(NetManager *)thisNetManager
                               response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        if (jsonDictD == nil) {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_FEEDBACK delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
            
        }
        if([[jsonDictD objectForKey:@"errorcode"] integerValue]!=0)
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_FEEDBACK delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
            
        }
        if([[jsonDictD objectForKey:@"errorcode"] integerValue]==0)
        {
            [AppHelper showAlertViewWithTag:0 title:APP_NAME message:SUCCESS_FEEDBACK delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
            
        }
    }
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}



#pragma mark My Class
- (void) deleteObjectOfClasUser:(NSString*)Class_id cycle:(NSString*)cycle {
    
    
    NSError *error;
    NSManagedObject *eventToDelete = [self getClassDataInfoStored:Class_id  cycle:cycle  postNotification:NO];
    [_managedObjectContext deleteObject:eventToDelete];
    
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif

    }
}
-(MyClass*)getStoredFilterClass:(NSNumber*)peroid_id cycle:(NSString*)cycleDay timeTable:(NSNumber*)timeTableID
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    //  (cycle_day==%@ OR cycle_day==%@ ) [AppHelper userDefaultsForKey:@"user_id"]  AND  class_userID == %@
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"period_Id == %@ AND cycle_day==%@  AND  class_userID == %@ AND timeTable_id=%@ AND isDelete==%@", peroid_id,cycleDay,[AppHelper userDefaultsForKey:@"user_id"],timeTableID,[NSNumber numberWithBool:NO]];
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return classData;
}


//for get class 19 may
-(void)getClassUserDetail
{
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        
        NSString *serviceName = Method_GetClass_User ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        [parameters setObject:profobj.type forKey:@"user_type"];
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        TableUpdateTime *tabl=[self getlastUpdateTableDataInfoStored];
        if([[NSUserDefaults standardUserDefaults] objectForKey:@"vserionUpdateClassuser"] == nil)
        {
            [parameters setObject:[NSNumber numberWithInteger:0] forKey:@"LastSync"];
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            if (standardUserDefaults) {
                [standardUserDefaults setObject:@"DBUpdated" forKey:@"vserionUpdateClassuser"];
                [standardUserDefaults synchronize];
            }
        }
        else
        {
            [parameters setObject:tabl.studentUpdateTime forKey:@"LastSync"];
        }
        
#if DEBUG
        NSLog(@"url = %@",urlString);
        NSLog(@"parameters getclassuser service = %@",[parameters JSONRepresentation]);
#endif
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetgetClassUserDetail:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}


- (void)netManagerDidFinishGetgetClassUserDetail:(NetManager *)thisNetManager
                                        response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
        
        [Flurry logError:FLURRY_EVENT_Error_in_fetching_Class message:FLURRY_EVENT_Error_in_fetching_Class error:thisNetManager.error];
        
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_Get_Class_User object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

-(void)getSchoolUserClassDetail
{
    // user_id , SchoolId and LastUpdateDate if needed
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_GetSchool_User_Class ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetSchoolUserClassDetail:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
        
		//[AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
	}
}


- (void)netManagerDidFinishGetSchoolUserClassDetail:(NetManager *)thisNetManager
                                           response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_Get_SchoolUser_Class object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}



-(NSArray*)getStoredAllClassData
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_userID==%@",[AppHelper userDefaultsForKey:@"user_id"]];
	[request setPredicate:predicate];
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

-(MyClass*)getStoredClassDatawithClassId:(NSString*)class_id
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_id==%@",class_id];
	[request setPredicate:predicate];
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    return classData;
    
}

- (MyClass *)getClassDataInfoStored:(NSString*)classId  cycle:(NSString*)cycle  postNotification:(BOOL)postNotification;
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_id == %@ AND  class_userID==%@ AND cycle_day == %@", classId,[AppHelper userDefaultsForKey:@"user_id"],cycle];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return classData;
}

-(ClassesInfo *)getClassesInfoDataInfoStored:(NSString*)class_id postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"ClassesInfo"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_id == %@" ,class_id];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    ClassesInfo  * classInfoData  = nil;
	if (!error && [results count] > 0) {
		
		classInfoData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classInfoData;
    
}

//for parse ClassesInfo data



- (MyClass *)getClassDataInfoFromSubjectId:(NSString*)subjId {
    
    
    
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
	
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"subject_d == %@", subjId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classData;
    
}

-(NSArray*)getStoredAllParentsData
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyParent"];
    
    //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_userID==%@",[AppHelper userDefaultsForKey:@"user_id"]];
    //	[request setPredicate:predicate];
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

#pragma mark Students

-(NSArray*)getStoredAllStudentData
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Student"];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"firstname" ascending:YES];
    [request setSortDescriptors:@[sd]];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    NSMutableArray *arrActiveStudent = [[NSMutableArray alloc]init];
    for (Student *s in results) {
        if ([s.status isEqualToString:@"1"]) {
            [arrActiveStudent addObject:s];
        }
    }
    
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }

    return arrActiveStudent;
    
}

-(NSArray*)getStoredAllStudentDataWithTeacherId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Student"];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"firstname" ascending:YES];
    [request setSortDescriptors:@[sd]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"teacher_id == %@ AND status == 1",[AppHelper userDefaultsForKey:@"user_id"]];
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

-(NSArray*)getStoredAllStudentDataWithClassId :(NSString *)classId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Student"];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"firstname" ascending:YES];
    [request setSortDescriptors:@[sd]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_id==%@ AND status == 1",classId];
	[request setPredicate:predicate];

    NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

//for parse student data //07/08/2013
- (Student*)getStudentDataInfoStoredClass:(NSString*)studentId classe1:(NSString*)classe1
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Student"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"student_id==%@ AND class_id==%@", studentId,classe1];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Student  * studentData  = nil;
	if (!error && [results count] > 0)
    {
        //Aman added -- EZMOBILE-46
        
		Student *objStudent = [results objectAtIndex:0];
        if ([objStudent.status isEqualToString:@"1"])
        {
            studentData = [results objectAtIndex:0];
        }
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
    }
    return studentData;
}


- (Student*)getStudentDataInfoStoredOnlyForNote:(NSString*)studentId postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Student"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"student_id == %@", studentId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Student  * studentData  = nil;
    
    for (Student *s in results) {
        if ([s.status isEqualToString:@"1"]) {
            studentData = s;
        }
    }
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return studentData;
    
}


- (Student*)getStudentDataInfoStored:(NSString*)studentId postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Student"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"student_id ==%@", studentId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Student  * studentData  = nil;
	if (!error && [results count] > 0)
    {
        //Aman added -- EZMOBILE-46
		Student *objStudent = [results objectAtIndex:0];
        if ([objStudent.status isEqualToString:@"1"])
        {
            studentData = [results objectAtIndex:0];
            
        }
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return studentData;
    
}


-(BOOL)isMeritStudentActiveOrNot:(NSString*)studentId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Student"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"student_id == %@", studentId];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Student  * studentData  = nil;
    if (!error && [results count] > 0)
    {
        //Aman added -- EZMOBILE-46
        Student *objStudent = [results objectAtIndex:0];
        if ([objStudent.status isEqualToString:@"1"])
        {
            studentData = [results objectAtIndex:0];
            
        }
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    if(studentData)
    {
        return YES;
    }
    return NO;


}


//for parse student data
-(void)parseStudentDataWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"parseStudentDataWithDictionary %@",dictionary);
#endif
    
    NSString *strudentID = nil;
    NSString *strStudentName = nil ;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    
    
    
   Student *studentData=[self getStudentDataInfoStoredClass:[dictionary objectForKey:@"Id"] classe1:[dictionary objectForKey:@"ClassId"]];
    
    
    if (!studentData) {
        studentData = [NSEntityDescription
                       insertNewObjectForEntityForName:@"Student"
                       inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        studentData.student_id=[dictionary objectForKey:@"Id"];
        strudentID =[dictionary objectForKey:@"Id"];
    }
    
    //Aman added -- EZMOBILE-46
    if (![[dictionary objectForKey:@"Status"] isKindOfClass:[NSNull class]]) {
        studentData.status=[dictionary objectForKey:@"Status"];
    }
    
    studentData.teacher_id=[AppHelper userDefaultsForKey:@"user_id"];
    
    if (![[dictionary objectForKey:@"ClassId"] isKindOfClass:[NSNull class]]) {
        studentData.class_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"ClassId"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"FirstName"] isKindOfClass:[NSNull class]]) {
        NSString *str=nil;
        //5 Aug 2013
        if (![[dictionary objectForKey:@"LastName"] isKindOfClass:[NSNull class]]) {
            str=[dictionary objectForKey:@"LastName"];
        }
        if(str.length>0){
            studentData.firstname=[NSString stringWithFormat:@"%@ %@",[dictionary objectForKey:@"LastName"],[dictionary objectForKey:@"FirstName"]];
        }
        else{
            studentData.firstname=[dictionary objectForKey:@"FirstName"];
        }
        
        strStudentName = studentData.firstname ;
        
        // studentData.firstname=[dictionary objectForKey:@"FirstName"];
    }
    if (![[dictionary objectForKey:@"LastName"] isKindOfClass:[NSNull class]]) {
        studentData.lastname=[dictionary objectForKey:@"LastName"];
    }
    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
        studentData.school_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Email"] isKindOfClass:[NSNull class]]) {
        studentData.email_Id=[dictionary objectForKey:@"Email"];
    }
    if (![[dictionary objectForKey:@"Telephone"] isKindOfClass:[NSNull class]]) {
        studentData.phone_no=[dictionary objectForKey:@"Telephone"];
    }
    if (![[dictionary objectForKey:@"ProfileImage"] isKindOfClass:[NSNull class]]) {
        studentData.imageUrl=[dictionary objectForKey:@"ProfileImage"];
    }
    if (![[dictionary objectForKey:@"Updated"] isKindOfClass:[NSNull class]]) {
        studentData.updatedTime=[formatter dateFromString:[dictionary objectForKey:@"Updated"]];
    }
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}
    MyClass *newClas=[self getStoredClassDatawithClassId:[dictionary objectForKey:@"ClassId"]];
    if(newClas){
        [newClas addStudentObject:studentData];
    }
    
    [formatter release];
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}
    else
    {
        // Save parent details if teacher logged - in
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        if([profobj.type isEqualToString:@"Teacher"])
        {
            if (![[dictionary objectForKey:@"parent_details"] isKindOfClass:[NSNull class]])
            {
                NSArray *arrParentDetails = [dictionary objectForKey:@"parent_details"];
                for (NSDictionary *dict in arrParentDetails)
                {
                    [self parseMyParentDataWithDictionary:dict forStudent:strudentID andStudentName:strStudentName];
                }
            }
        }
    }
}

///////
#pragma mark
#pragma mark- For Assigned Student and Remider on MyClass
#pragma mark

-(NSArray*)getStoredAllAssignedItemDataWithItemId:(NSNumber*)itemID
{
#if DEBUG
    NSLog(@"getStoredAllAssignedItemDataWithItemId %d",[itemID integerValue]);
#endif
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DairyItemUsers"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_id == %@", itemID];
	[request setPredicate:predicate];

    NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
#if DEBUG
    NSLog(@"getStoredAllAssignedItemDataWithItemId result%@",results);
#endif
    
    return results;
    
}
// -=-=-=
- (DairyItemUsers*)getAssigneStudentDataInfoStoredForClass:(NSString*)assStudentId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DairyItemUsers"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"student_id == %@ ", assStudentId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    DairyItemUsers  * studentData  = nil;
	if (!error && [results count] > 0) {
		
		studentData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return studentData;
    
}

// -=-=-=

- (DairyItemUsers*)getAssigneStudentDataInfoStored:(NSString*)assStudentId itemId:(NSString*)itemID postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DairyItemUsers"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"student_id == %@ AND item_id==%@", assStudentId,itemID];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    DairyItemUsers  * studentData  = nil;
	if (!error && [results count] > 0) {
		
		studentData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
    }
    
    return studentData;
    
}


-(void)parseAssignedStudentDataWithDictionary:(NSDictionary*)dictionary{
    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    
    
    DairyItemUsers *assStudentData=[self getAssigneStudentDataInfoStored:[dictionary objectForKey:@"UserId"] itemId:[dictionary objectForKey:@"itemId"] postNotification:NO];
    
    
    if (!assStudentData) {
        assStudentData = [NSEntityDescription
                          insertNewObjectForEntityForName:@"DairyItemUsers"
                          inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"itemId"] isKindOfClass:[NSNull class]]) {
        assStudentData.item_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"itemId"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"UserId"] isKindOfClass:[NSNull class]]) {
        assStudentData.student_id=[dictionary objectForKey:@"UserId"];
    }
    
    if (![[dictionary objectForKey:@"Progress"] isKindOfClass:[NSNull class]]) {
        assStudentData.progress=[dictionary objectForKey:@"Progress"];
    }
    /*******************New Methods Added For Out Class********************************/

    if (![[dictionary objectForKey:@"ApproverName"] isKindOfClass:[NSNull class]]) {
        assStudentData.approverName= [dictionary objectForKey:@"ApproverName"];
    }
    
    if (![[dictionary objectForKey:@"StudentName"] isKindOfClass:[NSNull class]]) {
        assStudentData.studentName= [dictionary objectForKey:@"StudentName"];
    }
    
    
    if (![[dictionary objectForKey:@"passTypeStatus"] isKindOfClass:[NSNull class]]) {
        assStudentData.passTypeStatus =[NSNumber numberWithInteger:[[dictionary objectForKey:@"passTypeStatus"] integerValue]];
    }
    /*******************New Methods Added For Out Class********************************/

    if (![[dictionary objectForKey:@"Reminder"] isKindOfClass:[NSNull class]]) {

        [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
        NSString *reminderStr = [dictionary objectForKey:@"Reminder"];
      
        NSString *reminderDateStr = [[reminderStr componentsSeparatedByString:@" "] objectAtIndex:0];
        
        NSDate *reminderDate = [formatter dateFromString:reminderDateStr];
        if (reminderDate != nil )
        {
            unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
            NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:reminderDate];
            
            NSArray *reminderTime=nil;
            if (![[[reminderStr componentsSeparatedByString:@" "] objectAtIndex:1] isKindOfClass:[NSNull class]]) {
                reminderTime= [[[reminderStr componentsSeparatedByString:@" "] objectAtIndex:1] componentsSeparatedByString:@":"];
                
            }
            if([reminderTime count]>0)
            {
                [dateComponents setHour:[[reminderTime objectAtIndex:0] integerValue]];
                [dateComponents setMinute:[[reminderTime objectAtIndex:1] integerValue]];
                
                [dateComponents setSecond:[[reminderTime objectAtIndex:2] integerValue]];
            }
            
          
            
            NSDate *gmtDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
            
            NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:gmtDate];
            
            NSDate *gmtDate1=[gmtDate dateByAddingTimeInterval:timeZoneOffset];
            
            
            assStudentData.reminder=gmtDate1;
        }
        else
        {
            assStudentData.reminder=nil;
        }
    }
    
    assStudentData.isSynch=[NSNumber numberWithBool:NO];
    
    if(![[dictionary objectForKey:@"IsDeleted"] isKindOfClass:[NSNull class]])
    {
        assStudentData.isDeletedOnServer=[NSNumber numberWithInteger:[[dictionary objectForKey:@"IsDeleted"] integerValue]];
        
//        
//            if([[dictionary objectForKey:@"IsDeleted"] integerValue]==1)
//            {
//                assStudentData.meritTitle = @"";
//                
//            }
        
    }
    
    
    if ([AppHelper userDefaultsForKey:@"user_id"]) {
        assStudentData.user_id=[AppHelper userDefaultsForKey:@"user_id"];
    }
    
    //Merit - Start
    
    if(![[dictionary objectForKey:@"passtype_name"] isKindOfClass:[NSNull class]])
    {

            assStudentData.meritTitle = [dictionary objectForKey:@"passtype_name"];
    }
    else
    {
        assStudentData.meritTitle = @"";

    }

    if(![[dictionary objectForKey:@"Description"] isKindOfClass:[NSNull class]])
    {
        assStudentData.meritDescription = [dictionary objectForKey:@"Description"];
    }
    else
    {
        assStudentData.meritDescription = @"";
        
    }

    if(![[dictionary objectForKey:@"IsNotify"] isKindOfClass:[NSNull class]])
        assStudentData.isNotify = [NSNumber numberWithInteger:[[dictionary objectForKey:@"IsNotify"] integerValue]];

    if(![[dictionary objectForKey:@"passtype_id"] isKindOfClass:[NSNull class]])
    {
        assStudentData.listofvaluesTypeid = [NSNumber numberWithInteger:[[dictionary objectForKey:@"passtype_id"] integerValue]];
    }
    else
    {
        assStudentData.listofvaluesTypeid = [NSNumber numberWithInteger:0];
        
    }
    
    if(![[dictionary objectForKey:@"meritAssignedDateAndTime"] isKindOfClass:[NSNull class]])
    {
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init] ;
        [formatter1 setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
        assStudentData.meritAssignedDateAndTime=[formatter1 dateFromString:[dictionary objectForKey:@"meritAssignedDateAndTime"]];
        [formatter1 release];
        formatter1 =nil;
    }
    
    //Merit - End
    
    [formatter release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}
    
}



-(void)sendRemiderToAssinStudent:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        
        NSString *serviceName = Method_sendReminder ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        //  MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        
        
        //{"user_id":"d1fe173d08e959397adf34b1d77e88d7","AppType":"ipad","ReceiverIds":"58a2fc6ed39fd083f55d4182bf88826d,c9f0f895fb98ab9159f51fd0297e236d","DairyitemId":"1004"}
        
        
        // Webservice Logging existing both
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setObject:[dict objectForKey:@"ReceiverIds"] forKey:@"ReceiverIds"];
        [parameters setObject:[dict objectForKey:@"DairyitemId"] forKey:@"DairyitemId"];
        [parameters setObject:[dict objectForKey:@"Description"] forKey:@"Description"];
        
        
#if DEBUG
        NSLog(@"sendRemiderToAssinStudent Reminder Req Dict :%@",[parameters JSONRepresentation]);
#endif
      
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishSendRemiderToAssinStudent:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
        
	}
}


- (void)netManagerDidFinishSendRemiderToAssinStudent:(NetManager *)thisNetManager
                                            response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_sendReminderToStudent object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}



//////

#pragma mark- TimeTable
-(void)getTimeTableDetailsofSchool
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_TimeTable ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:profobj.type forKey:@"type"];
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
        TableUpdateTime *tabl=[self getlastUpdateTableDataInfoStored];
        if([[NSUserDefaults standardUserDefaults] objectForKey:vserionUpdateTimeTable] == nil)
        {
            [parameters setObject:[NSNumber numberWithInteger:0] forKey:@"LastUpdate"];
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            if (standardUserDefaults) {
                [standardUserDefaults setObject:@"DBUpdated" forKey:vserionUpdateTimeTable];
                [standardUserDefaults synchronize];
            }
        }
        else
        {
            [parameters setObject:tabl.timeTble forKey:@"LastUpdate"];
        }

        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        NSError *error;
        
        NSString *udid = [SSKeychain passwordForService:@"com.app.daisyApp" account:@"AppUser" error:&error];
        if (!udid)
        {
            [DELEGATE createNewUUID];
            
            
        }
        
        NSString *udid1 = [SSKeychain passwordForService:@"com.app.daisyApp" account:@"AppUser" error:&error];
        [parameters setObject:udid1 forKey:@"macAddress"];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetTimeTableDetail:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}

#pragma mark- TimeTable
-(void)getReportData
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_GetReport ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
//        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
        [parameters setObject:[AppHelper userDefaultsForKey:@"ipadd"]forKey:@"ipadd"];
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        TableUpdateTime *tabl=[self getlastUpdateTableDataInfoStored];
        if(tabl.report == nil)
            [parameters setObject:[NSNumber numberWithInteger:0]  forKey:@"LastSync"];
        else
            [parameters setObject:tabl.report  forKey:@"LastSync"];
        
#if DEBUG
        NSLog(@"getReportData %@",parameters);
#endif
      
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetReportData:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
        
	}
}

- (void)netManagerDidFinishGetReportData:(NetManager *)thisNetManager
                                     response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
#if DEBUG
        NSLog(@"ERRO BY WEBSERVICE");
#endif
      
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_Report object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}


- (void)netManagerDidFinishGetTimeTableDetail:(NetManager *)thisNetManager
                                     response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_TimeTable object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

- (TimeTable *)getTimeTableDataInfoStored:(NSString*)timetable postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TimeTable"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTable_id == %@", timetable];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    TimeTable  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classData;
    
}

- (TimeTable *)getTimeTableDataFromTimetableId:(NSString*)timetableId andCampusCode:(NSString *)campusCode
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TimeTable"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTable_id == %@ AND campusCode==%@", timetableId,campusCode];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    TimeTable  * classData  = nil;
    if (!error && [results count] > 0) {
        
        classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classData;
    
}

- (NSArray *)getTimeTableDataBasedOnUniqueCampusCode
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TimeTable"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"TimeTable" inManagedObjectContext:_managedObjectContext];
    request.resultType = NSDictionaryResultType;
    request.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"campusCode"]];
    request.returnsDistinctResults = YES;
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
#if DEBUG
    NSLog(@"getTimeTableDataBasedOnUniqueCampusCode results %@",results);
#endif
    
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return results ;
    
}

- (NSArray *)getTimeTableDataFromCampusCode:(NSString *)campusCode
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TimeTable"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES]];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"campusCode==%@",campusCode];
    [request setPredicate:predicate];
    
#if DEBUG
    NSLog(@"getTimeTableDataFromCampusCode predicate %@",predicate);
#endif
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    for (TimeTable *tt in results)
    {
#if DEBUG
        NSLog(@"results tt %ld %@",(long)[tt.timeTable_id integerValue], tt.startDate);
#endif
      
    }
    return results;
    
}




-(void)parseReportDataWithDictionary:(NSDictionary*)dictionary
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    
    // [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    Report *classData = [self getSingleReportDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:nil] ;
    
     if (!classData) {
       classData = [NSEntityDescription
                     insertNewObjectForEntityForName:@"Report"
                     inManagedObjectContext:_managedObjectContext];
            }
 
    
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        classData.report_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    if (![[dictionary objectForKey:@"ReportName"] isKindOfClass:[NSNull class]]) {
        classData.report_name=[dictionary objectForKey:@"ReportName"];
    }
    if (![[dictionary objectForKey:@"ReportDescription"] isKindOfClass:[NSNull class]]) {
        classData.report_description=[dictionary objectForKey:@"ReportDescription"];
    }
    if (![[dictionary objectForKey:@"isStudent"] isKindOfClass:[NSNull class]]) {
        classData.isStudent=[NSNumber numberWithInteger:[[dictionary objectForKey:@"isStudent"]integerValue]];
    }
    if (![[dictionary objectForKey:@"ReportURL"] isKindOfClass:[NSNull class]]) {
        classData.report_url=[dictionary objectForKey:@"ReportURL"];
    }
    if (![[dictionary objectForKey:@"isTeacher"] isKindOfClass:[NSNull class]]) {
      classData.isTeacher=[NSNumber numberWithInteger:[[dictionary objectForKey:@"isTeacher"]integerValue]];
    }
    if (![[dictionary objectForKey:@"isParent"] isKindOfClass:[NSNull class]]) {
        classData.isParent=[NSNumber numberWithInteger:[[dictionary objectForKey:@"isParent"]integerValue]];
    }
    if (![[dictionary objectForKey:@"isAdministrator"] isKindOfClass:[NSNull class]]) {
        classData.isAdministrator=[NSNumber numberWithInteger:[[dictionary objectForKey:@"isAdministrator"]integerValue]];
    }
    if (![[dictionary objectForKey:@"isReportActive"] isKindOfClass:[NSNull class]]) {
        classData.isActive=[NSNumber numberWithInteger:[[dictionary objectForKey:@"isReportActive"]integerValue]];
    }
    if (![[dictionary objectForKey:@"isReportSchoolActive"] isKindOfClass:[NSNull class]]) {
        classData.isReportSchoolActive=[NSNumber numberWithInteger:[[dictionary objectForKey:@"isReportSchoolActive"]integerValue]];
    }
    if (![[dictionary objectForKey:@"schoolId"] isKindOfClass:[NSNull class]]) {
        classData.school_Id=[dictionary objectForKey:@"schoolId"];
    }
    
    [formatter release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}

}

//for parse class data
-(void)parseTimeTableDataWithDictionary:(NSDictionary*)dictionary
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    
    // [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
//    TimeTable *classData=[self getTimeTableDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    
    TimeTable *classData=[self getTimeTableDataFromTimetableId:[dictionary objectForKey:@"Id"] andCampusCode:[dictionary objectForKey:@"CampusCode"]];
    
    if (!classData) {
        classData = [NSEntityDescription
                     insertNewObjectForEntityForName:@"TimeTable"
                     inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        classData.timeTable_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
        classData.semester_name=[dictionary objectForKey:@"Name"];
    }
    if (![[dictionary objectForKey:@"CycleLabel"] isKindOfClass:[NSNull class]]) {
        classData.cycleLable=[dictionary objectForKey:@"CycleLabel"];
    }
    // Campus Based Start

    if (![[dictionary objectForKey:@"CampusCode"] isKindOfClass:[NSNull class]]) {
        classData.campusCode=[dictionary objectForKey:@"CampusCode"];
    }
    // Campus Based End

    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
        classData.school_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"CycleLength"] isKindOfClass:[NSNull class]]) {
        classData.cycle_Length=[dictionary objectForKey:@"CycleLength"];
    }
    if (![[dictionary objectForKey:@"StartDate"] isKindOfClass:[NSNull class]]) {
        classData.startDate=[formatter dateFromString:[dictionary objectForKey:@"StartDate"]];
    }
    
    if (![[dictionary objectForKey:@"EndDate"] isKindOfClass:[NSNull class]]) {
        classData.end_date=[formatter dateFromString:[dictionary objectForKey:@"EndDate"]];
    }
    
    [formatter release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    
    
}
-(NSArray*)getStoredAllTimeTableCycleData
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TimeTableCycle"];
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"school_id == %@", [AppHelper userDefaultsForKey:@"school_Id"]];
	//[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (TimeTableCycle *a1 in results) {
            copy = YES;
            for (TimeTableCycle *a2 in filterResults) {
                
                if ([a1.cycle_day isEqualToString:a2.cycle_day ] && [a1.cycle_name isEqualToString:a2.cycle_name] && [a1.short_lable isEqualToString:a2.short_lable]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    results = filterResults;
    
    
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    
    
    return results;
    
}

- (TimeTableCycle *)getTimeTableCycleDataInfoStored:(NSString*)timetablecycle postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TimeTableCycle"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTableCycle_id == %@", timetablecycle];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    TimeTableCycle  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classData;
    
}

//for parse class data
-(void)parseTimeTableCycleDataWithDictionary:(NSDictionary*)dictionary
{
    TimeTableCycle *classData=[self getTimeTableCycleDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    
    if (!classData) {
        classData = [NSEntityDescription
                     insertNewObjectForEntityForName:@"TimeTableCycle"
                     inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        classData.timeTableCycle_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    if (![[dictionary objectForKey:@"TimetableId"] isKindOfClass:[NSNull class]]) {
        classData.timeTable_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"TimetableId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"CycleLabel"] isKindOfClass:[NSNull class]]) {
        classData.cycle_name=[dictionary objectForKey:@"CycleLabel"];
    }
    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
        classData.school_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"CampusId"] isKindOfClass:[NSNull class]]) {
        classData.campus_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"CampusId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"CycleDays"] isKindOfClass:[NSNull class]]) {
        classData.cycle_day=[dictionary objectForKey:@"CycleDays"];
    }
    if (![[dictionary objectForKey:@"ShortLabel"] isKindOfClass:[NSNull class]]) {
        classData.short_lable=[dictionary objectForKey:@"ShortLabel"];
    }
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    
    
}
-(TimeTableCycle*)getCurentTimeTableCycle:(NSNumber*)table_id  cycle:(NSString*)cycleno
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TimeTableCycle"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTable_id == %@ AND cycle_day == %@", table_id,cycleno];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    TimeTableCycle  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classData;
}


-(NSArray*)getStoredAllTimeTableData
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TimeTable"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES]];
    
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

#pragma mark room
- (Room *)getRoomDataInfoStored:(NSString*)room postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Room"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"room_id == %@", room];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Room  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classData;
    
}

//for parse class data
-(void)parseRoomDataWithDictionary:(NSDictionary*)dictionary
{
    
    Room *classData=[self getRoomDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    
    if (!classData) {
        classData = [NSEntityDescription
                     insertNewObjectForEntityForName:@"Room"
                     inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        classData.room_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
        classData.room_name=[dictionary objectForKey:@"Name"];
    }
    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
        classData.school_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"CampusId"] isKindOfClass:[NSNull class]]) {
        classData.campus_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"CampusId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Code"] isKindOfClass:[NSNull class]]) {
        classData.code=[dictionary objectForKey:@"Code"];
    }
    
    
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    
    
}
#pragma  mark peroid Data
- (Period *)getPeriodDataInfoStored:(NSString*)period postNotification:(BOOL)postNotification
{
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    if([profobj.type isEqualToString:@"Teacher"])
    {
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"Period"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"period_id == %d", [period integerValue]];
        [request setPredicate:predicate];
        
        NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        Period  * classData  = nil;
        if (!error && [results count] > 0) {
            
            classData = [results objectAtIndex:0];
        }
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
        
        return classData;
    }
    else
    {
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"Period"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"period_id == %@ && forteacher == 0 ", period];
        [request setPredicate:predicate];
        
        NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        Period  * classData  = nil;
        if (!error && [results count] > 0) {
            
            classData = [results objectAtIndex:0];
        }
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
        
        return classData;
    }
    
    
    
}

//for parse class data
-(void)parsePeriodDataWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"parsePeriodDataWithDictionary %@",dictionary);
#endif
    
    
    
    // Code commented to parse Start_Time and End_Time as NSDate without any formatter
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
//    [formatter setDateFormat:@"HH:mm:ss"];
//    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    
    Period *classData=[self getPeriodDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    
    if (!classData) {
        classData = [NSEntityDescription
                     insertNewObjectForEntityForName:@"Period"
                     inManagedObjectContext:_managedObjectContext];
    }

    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        
        classData.period_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    

    //Aman added
    if(![[dictionary objectForKey:@"HasBreak"] isKindOfClass:[NSNull class]])
    {
        classData.has_Break =[NSNumber numberWithInteger:[[dictionary objectForKey:@"HasBreak"]integerValue]];
    }

    if (![[dictionary objectForKey:@"TimetableId"] isKindOfClass:[NSNull class]]) {
        classData.timeTable_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"TimetableId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"CycleDays"] isKindOfClass:[NSNull class]]) {
        classData.cycle_day=[dictionary objectForKey:@"CycleDays"];
    }
    

    if (![[dictionary objectForKey:@"Title"] isKindOfClass:[NSNull class]]) {
        classData.title=[dictionary objectForKey:@"Title"];
    }
    if (![[dictionary objectForKey:@"ForTeacher"] isKindOfClass:[NSNull class]]) {
        classData.forteacher=[NSNumber numberWithInteger:[[dictionary objectForKey:@"ForTeacher"]integerValue]];
    }
    

    
    if (![[dictionary objectForKey:@"StartTime"] isKindOfClass:[NSNull class]]) {
        
        NSDate *periodStartDate = [NSDate date];
        unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:periodStartDate];
        NSArray *arrassigTime=nil;
        arrassigTime= [[dictionary objectForKey:@"StartTime"] componentsSeparatedByString:@":"];
        if([arrassigTime count]>0)
        {
            [dateComponents setHour:[[arrassigTime objectAtIndex:0] integerValue]];
            [dateComponents setMinute:[[arrassigTime objectAtIndex:1] integerValue]];
            
            [dateComponents setSecond:[[arrassigTime objectAtIndex:2] integerValue]];

        }
        NSDate *newPeriodStartDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];


        classData.start_time=newPeriodStartDate;
        
    }
    
    if (![[dictionary objectForKey:@"EndTime"] isKindOfClass:[NSNull class]]) {
        NSDate *periodStartDate = [NSDate date];
        unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:periodStartDate];
        NSArray *arrassigTime=nil;
        arrassigTime= [[dictionary objectForKey:@"EndTime"] componentsSeparatedByString:@":"];
        if([arrassigTime count]>0)
        {
            [dateComponents setHour:[[arrassigTime objectAtIndex:0] integerValue]];
            [dateComponents setMinute:[[arrassigTime objectAtIndex:1] integerValue]];
            
            [dateComponents setSecond:[[arrassigTime objectAtIndex:2] integerValue]];
            
        }

        NSDate *newPeriodEndDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
        

        classData.end_time=newPeriodEndDate;
        
    }
    
    classData.school_id=[NSNumber numberWithInteger:[[AppHelper userDefaultsForKey:@"school_Id"]integerValue]];

    
    classData.user_id=[AppHelper userDefaultsForKey:@"user_id"];

    
    
//    [formatter release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	}
    
    
}
-(NSArray*)getStoredAllPeriodTableData:(NSNumber*)table_id cycle:(NSString*)cycleDay
{
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    if([profobj.type isEqualToString:@"Teacher"])
    {
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"Period"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTable_id == %d AND cycle_day==%@", [table_id integerValue],cycleDay];
        [request setPredicate:predicate];
        
        NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        
        NSMutableArray *marrResults = [[NSMutableArray alloc] init];
        for (Period *periodObj in results)
        {
            if (periodObj.start_time != nil && periodObj.end_time != nil)
            {
                [marrResults addObject:periodObj];
            }
        }
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
        return marrResults;
    }
    else
    {
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"Period"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTable_id == %d AND cycle_day==%@ AND forteacher ==0", [table_id integerValue],cycleDay];
        [request setPredicate:predicate];

        NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        
        NSMutableArray *marrResults = [[NSMutableArray alloc] init];
        for (Period *periodObj in results)
        {
            if (periodObj.start_time != nil && periodObj.end_time != nil)
            {
                [marrResults addObject:periodObj];
            }
        }

        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
        return marrResults;
    }

}

-(Period*)getStoredFilterPeriodTableData:(NSNumber*)table_id cycle:(NSString*)cycleDay startTime:(NSDate*)time
{
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    if([profobj.type isEqualToString:@"Teacher"])
    {
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"Period"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
        [formatter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTable_id == %@ AND  start_time==%@ AND  (cycle_day==%@ OR cycle_day==%@ )", table_id,time,cycleDay,@"0"];
        [request setPredicate:predicate];
        NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        Period  * classData  = nil;
        if (!error && [results count] > 0) {
            
            classData = [results objectAtIndex:0];
        }
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
        
        [formatter release];
        return classData;
    }
    else
    {
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"Period"];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
        [formatter setDateFormat:kDateFormatOnlyTimeToSaveFetchInFromDB];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTable_id == %@ AND  start_time==%@ AND  (cycle_day==%@ OR cycle_day==%@ ) AND forteacher ==0", table_id,time,cycleDay,@"0"];
        [request setPredicate:predicate];
        NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        Period  * classData  = nil;
        if (!error && [results count] > 0) {
            
            classData = [results objectAtIndex:0];
        }
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
        
        [formatter release];
        return classData;
    }
    
    
    
}

//TODO: Rohit
-(void)addPeriodOnTimeTable:(NSDictionary*)contentType
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_AddPeriod ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        
        // Webservice Logging
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        // New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
        [parameters setObject:[contentType objectForKey:@"room_name"] forKey:@"room_name"];
        [parameters setObject:[contentType objectForKey:@"sub_name"] forKey:@"subject_name"];
        [parameters setObject:[contentType objectForKey:@"PeriodId"] forKey:@"PeriodId"];
        [parameters setObject:[contentType objectForKey:@"Code"] forKey:@"code"];
        [parameters setObject:[contentType objectForKey:@"CycleDays"] forKey:@"CycleDays"];
        [parameters setObject:profobj.type forKey:@"user_type"];
        
        //setdairyitems and keys is
        // created  and updated
        double milliseconds = 1000.0 * [[NSDate date] timeIntervalSince1970];
        
        
        NSString *strcre=[NSString stringWithFormat:@"%f",milliseconds];
        NSArray *aa=[strcre componentsSeparatedByString:@"."];
        
        [parameters setObject:[aa objectAtIndex:0]forKey:@"created"];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishAddPeriod:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
        [dictionary setObject:contentType forKey:@"Class_details"];
        [dictionary setObject:@"1" forKey:@"ErrorCode"];
        [dictionary setObject:@"Class added successfully." forKey:@"Status"];
        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_AddPeriod object:nil userInfo:dictionary];
        [dictionary release];
	}
}

- (void)netManagerDidFinishAddPeriod:(NetManager *)thisNetManager
                            response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_AddPeriod object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

//TODO: Rohit
-(void)editPeriodOnTimeTable:(NSDictionary*)contentType
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_editPeriod ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        //
        // Webservice Logging
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        // New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
        
        [parameters setObject:[contentType objectForKey:@"room_name"] forKey:@"room_name"];
        [parameters setObject:[contentType objectForKey:@"sub_name"] forKey:@"subject_name"];
        
        [parameters setObject:[contentType objectForKey:@"PeriodId"] forKey:@"PeriodId"];
        [parameters setObject:[contentType objectForKey:@"Code"] forKey:@"code"];
        [parameters setObject:[contentType objectForKey:@"CycleDays"] forKey:@"CycleDays"];
        // [parameters setObject:[contentType objectForKey:@"Id"] forKey:@"ClassId"];
        if ([contentType objectForKey:@"Id"]) {
            [parameters setObject:[contentType objectForKey:@"Id"] forKey:@"ClassId"];
        }
        [parameters setObject:profobj.type forKey:@"user_type"];
        
        //////
        //setdairyitems and keys is
        // created  and updated
        double milliseconds = 1000.0 * [[NSDate date] timeIntervalSince1970];
        
        NSString *strcre=[NSString stringWithFormat:@"%f",milliseconds];
        NSArray *aa=[strcre componentsSeparatedByString:@"."];
        
        [parameters setObject:[aa objectAtIndex:0]forKey:@"updated"];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishEditPeriod:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else//ofline mode
    {
        NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
        [dictionary setObject:contentType forKey:@"Class_details"];
        [dictionary setObject:@"1" forKey:@"ErrorCode"];
        [dictionary setObject:[NSNumber numberWithBool:NO]forKey:@"isSync"];
        [dictionary setObject:@"Diaryitem updated successfully." forKey:@"Status"];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EditPeriodData object:nil userInfo:dictionary];
        [dictionary release];
	}
}



- (void)netManagerDidFinishEditPeriod:(NetManager *)thisNetManager
                             response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSMutableDictionary *jsonDictD = [decodedString JSONValue];
        
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_EditPeriodData object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

//TODO: Rohit
-(void)deltePeriodOnTimeTable:(NSDictionary*)contentType
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_deletPeroid ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        // Webservice Logging
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        // New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
        [parameters setObject:[contentType objectForKey:@"roomId"] forKey:@"roomId"];
        [parameters setObject:[contentType objectForKey:@"periodId"] forKey:@"periodId"];
        [parameters setObject:[contentType objectForKey:@"cycle"] forKey:@"CycleDays"];
        [parameters setObject:[contentType objectForKey:@"ClassId"] forKey:@"ClassId"];
        
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishDeletePeriod:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
        
	}
}

- (void)netManagerDidFinishDeletePeriod:(NetManager *)thisNetManager
                               response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_DeletePeriodData object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

#pragma mark School-Info
-(void)getSchoolInformation:(NSString*)contentType
{
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_SchoolInfo ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        //        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        //
        //        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        //[parameters setObject:@"56" forKey:@"SchoolId"];///////////////////
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
        
        [parameters setObject:contentType forKey:@"content_type"];
        TableUpdateTime *tabl=[self getlastUpdateTableDataInfoStored];
        
        if([[NSUserDefaults standardUserDefaults] objectForKey:vserionUpdateSchoolContents] == nil)
        {
            [parameters setObject:[NSNumber numberWithInteger:0] forKey:@"lastSynch"];
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            if (standardUserDefaults) {
                [standardUserDefaults setObject:@"DBUpdated" forKey:vserionUpdateSchoolContents];
                [standardUserDefaults synchronize];
            }
        }
        else
        {
            [parameters setObject:tabl.about forKey:@"lastSynch"];
        }
        
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        
        [parameters setObject:profobj.type forKey:@"user_type"];
        
        // Webservice Logging New Both
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
#if DEBUG
        NSLog(@"getSchoolInformation urlString %@ \n Parameters : %@",urlString,[parameters JSONRepresentation]);
#endif
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetSchoolDetail:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}


- (void)netManagerDidFinishGetSchoolDetail:(NetManager *)thisNetManager
                                  response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_SchoolInfo object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}
- (School_Information *)getSchoolInfoDataInfoStored:(NSString*)schol postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"School_Information"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_id == %@", schol];
	[request setPredicate:predicate];
	
#if DEBUG
    NSLog(@"getSchoolInfoDataInfoStored predicate %@",predicate);
#endif
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    School_Information  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
        
    }
    
    return classData;
    
}

-(void)ParseSchoolInformatio:(NSDictionary*)dictionary
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init] ;
    [formatter1 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    School_Information *classData=[self getSchoolInfoDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    
    if (!classData) {
        classData = [NSEntityDescription
                     insertNewObjectForEntityForName:@"School_Information"
                     inManagedObjectContext:_managedObjectContext];
    }
    
    
    if (![[dictionary objectForKey:@"StartDate"] isKindOfClass:[NSNull class]]) {
        classData.startDate=[formatter1 dateFromString:[formatter1 stringFromDate:[formatter dateFromString:[dictionary objectForKey:@"StartDate"]]]];
    }
    if (![[dictionary objectForKey:@"EndDate"] isKindOfClass:[NSNull class]]) {
        classData.endDate=[formatter1 dateFromString:[formatter1 stringFromDate:[formatter dateFromString:[dictionary objectForKey:@"EndDate"]]]];
    }
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        classData.item_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    if (![[dictionary objectForKey:@"ParentId"] isKindOfClass:[NSNull class]]) {
        classData.parent_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"ParentId"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"ContentType"] isKindOfClass:[NSNull class]]) {
        classData.content_type=[dictionary objectForKey:@"ContentType"];
    }
    if (![[dictionary objectForKey:@"Title"] isKindOfClass:[NSNull class]]) {
        classData.title=[dictionary objectForKey:@"Title"];
    }
    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
        classData.school_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"OrderBy"] isKindOfClass:[NSNull class]]) {
        classData.order_by=[dictionary objectForKey:@"OrderBy"];
    }
    if (![[dictionary objectForKey:@"OrderNo"] isKindOfClass:[NSNull class]]) {
        classData.order_no=[NSNumber numberWithInteger:[[dictionary objectForKey:@"OrderNo"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Status"] isKindOfClass:[NSNull class]]) {
        classData.status=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Status"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Content"] isKindOfClass:[NSNull class]]) {
        classData.content=[dictionary objectForKey:@"Content"];
    }
    
    [formatter release];
    [formatter1 release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
	}
}


//----- Added By Mitul For Content Logic -----//

- (NSArray *)parseManifestFile:(NSString *)contentType
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"manifest" ofType:@"ddm"];
    NSData *manifestData = [NSData dataWithContentsOfFile:filePath];
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:manifestData options:NSJSONReadingMutableLeaves error:&error];
    
    ///////////////////
    
    NSMutableArray *finalResults = [[[NSMutableArray alloc] init] autorelease];
    
    NSArray *arrPackage = [dict valueForKey:@"calendarBanner"];
    
    for (NSDictionary *dict1 in arrPackage )
    {
        
        if ([[dict1 valueForKey:@"packageId"] isEqualToString:contentType])
        {
            SchoolInformation *about = [[[SchoolInformation alloc] init] autorelease];
            
            // Get Html File Content
            NSError* error = nil;
            
            NSString *strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"Content/%@",contentType]];
            
            NSString *finalPath = [strBundlePath stringByAppendingPathComponent:[dict1 valueForKey:@"article"]];
            
            NSString *res = [NSString stringWithContentsOfFile:finalPath encoding:NSUTF8StringEncoding error: &error];
            
            about.content = res;
            about.content_type = contentType;
            about.title = [dict1 valueForKey:@"title"];
            
            about.startDate = (NSDate*)[self dateWithJSONString:[dict1 objectForKey:@"startDate"] ];
            
            about.endDate = (NSDate*)[self dateWithJSONString:[dict1 objectForKey:@"endDate"]];
            
            [finalResults addObject:about];
        }
    }
    return (NSArray*)finalResults;
}

- (NSDate*)dateWithJSONString:(NSString*)dateStr
{
    // Convert string to date object
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSDate *date = [dateFormat dateFromString:dateStr];
    // This is for check the output
    [dateFormat release];
    dateFormat = nil;
    
    return date;
}
//--------- By Mitul ---------//

//getDaisyAbout
-(NSArray*)getStoredDaisyPaneltData:(NSDate*)Date
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"School_Information"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"content_type == %@ AND  content!=%@ AND startDate<=%@ AND endDate>=%@", @"Panel",@"",Date,Date];
	[request setPredicate:predicate];

    NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return results;
    
}

-(NSArray*)getStoredDaisyAboutData:(NSString*)type
{
    //----- Added By Mitul on 23-Dec Content Logic -----//
    
    if ([type isEqualToString:@"Empower"])
    {
        // Get the path of Bundle To fetch HTML files array
//        NSString *strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];
        
        // Get the path of Bundle To fetch HTML files array
        NSString *EducationContentFolder = [[Service sharedInstance] getContentTypeForEducationContent];
        NSString *strBundlePath;
        // For Daisy
        if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
        {
            strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];
        }
        // For Prime
        // For Teacher And Student Same content
        else
        {
            strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content"];
            strBundlePath = [strBundlePath stringByAppendingPathComponent:EducationContentFolder];
        }

        NSArray *arrFiles = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:strBundlePath error: nil];
        NSMutableArray *marrFiles = [[NSMutableArray alloc] init];
        NSMutableArray *marrSort = [[NSMutableArray alloc] init];
        
        for (int i=0; i<[arrFiles count]; i++)
        {
            if ([[arrFiles objectAtIndex:i] rangeOfString:@".html"].location == NSNotFound)
            {
            }
            else
            {
                [marrFiles addObject:[arrFiles objectAtIndex:i]];
                NSArray *arrr = [[arrFiles objectAtIndex:i] componentsSeparatedByString:@" "];
                [marrSort addObject:[NSDictionary dictionaryWithObjectsAndKeys:[arrFiles objectAtIndex:i],@"Data",[NSNumber numberWithInteger:[[arrr objectAtIndex:0]integerValue]],@"Index", nil]];
            }
        }
        
        NSSortDescriptor *sortBasedOnDueTime;
        sortBasedOnDueTime = [NSSortDescriptor sortDescriptorWithKey:@"Index"ascending:YES];
        NSArray *sortDescriptorArr = [NSArray arrayWithObject:sortBasedOnDueTime];
        NSArray * sortedArray =[NSArray arrayWithArray:[marrSort sortedArrayUsingDescriptors:sortDescriptorArr]];
        marrFiles = [self arrayOfEducationContent:[sortedArray valueForKey:@"Data"]];
        
        return marrFiles;
    }
    //--------- By Mitul ---------//
    else
    {
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"School_Information"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"content_type == %@ AND  content!=%@ AND parent_id=0 AND status=1", type,@""];
        
        [request setPredicate:predicate];
        NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        
        // Sorting array on the bases of Order_No
        NSSortDescriptor *orderNoDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order_no" ascending:YES];
        NSArray *sortDescriptors = @[orderNoDescriptor];
        NSArray *sortedArray = [results sortedArrayUsingDescriptors:sortDescriptors];
        
        // Fetching childs of parents
        
        NSMutableArray *arrForSchoolInfo = [[NSMutableArray alloc] init];
        
        for (School_Information *schoolInfo in sortedArray)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"content_type == %@ AND  content!=%@ AND parent_id==%@ AND status=1", type,@"",schoolInfo.item_id];
            [request setPredicate:predicate];
            
            NSError *error = nil;
            NSArray *resultsForChild = [_managedObjectContext executeFetchRequest:request error:&error];
            
            // Sorting array on the bases of Order_No
            NSSortDescriptor *orderNoDescriptorForChild = [[NSSortDescriptor alloc] initWithKey:@"order_no" ascending:YES];
            NSArray *sortDescriptorsForChild = @[orderNoDescriptorForChild];
            NSArray *sortedArrayForChild = [resultsForChild sortedArrayUsingDescriptors:sortDescriptorsForChild];

            [arrForSchoolInfo addObject:schoolInfo];
            [arrForSchoolInfo addObjectsFromArray:sortedArrayForChild];
        }
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
        
        if ([arrForSchoolInfo count]>0)
        {
            return (NSArray *)arrForSchoolInfo;
        }
        else
        {
            return [NSArray array];
        }
    }
    
}
-(NSArray*)getStoredAllSchoolData
{
    //----- Added By Mitul For Content Logic -----//
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"School_Information"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"content!=%@ && content_type=%@",@"",@"SchoolContent"];
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    NSArray *arr = [self getStoredDaisyAboutData:@"Empower"];
    NSMutableArray *arrOfAllSchoolData = [[NSMutableArray alloc] initWithArray:results];
    [arrOfAllSchoolData addObjectsFromArray:arr];
    
    
    NSString *strContentType = [self getContentType];
    NSArray *arrContentOfPullbar = [self parseManifestFile:strContentType];
    [arrOfAllSchoolData addObjectsFromArray:arrContentOfPullbar];
    
    results = (NSArray*)arrOfAllSchoolData;
    return results;
    //--------- By Mitul ---------//
}
- (School_Images *)getSchoolImageDataInfoStored:(NSString*)image postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"School_Images"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"image_url == %@", image];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    School_Images  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classData;
    
}

-(void)ParseSchoolImageData:(NSDictionary*)dictionary
{
    School_Images *classData=[self getSchoolImageDataInfoStored:[dictionary objectForKey:@"url"] postNotification:NO];
    
    if (!classData) {
        classData = [NSEntityDescription
                     insertNewObjectForEntityForName:@"School_Images"
                     inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"image"] isKindOfClass:[NSNull class]]) {
        classData.image_name=[dictionary objectForKey:@"image"];
    }
    if (![[dictionary objectForKey:@"url"] isKindOfClass:[NSNull class]]) {
        classData.image_url=[dictionary objectForKey:@"url"];
    }
    
    if (![[dictionary objectForKey:@"type"] isKindOfClass:[NSNull class]]) {
        classData.image_type=[dictionary objectForKey:@"type"];
    }
    
    if ([classData.isDownload integerValue]==0) {
        classData.isDownload=[NSNumber numberWithBool:NO];
    }
    //
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    
    
}
-(NSArray*)getNotDownloadedImage:(NSString*)type
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"School_Images"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDownload==%@",[NSNumber numberWithBool:NO]];
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}


//
-(void)getSchoolImage
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_SchoolImages ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        //[parameters setObject:@"56" forKey:@"SchoolId"];/////////////
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
        // [parameters setObject:contentType forKey:@"content_type"];
        TableUpdateTime *tabl=[self getlastUpdateTableDataInfoStored];
        [parameters setObject:tabl.empowerContent forKey:@"lastSynch"];
        
        // Webservice Logging New Both
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:APP_Key forKey:kKeyAppType];

#if DEBUG
        NSLog(@"getSchoolImage : parameters : %@",parameters);
#endif
      
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetSchoolImage:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}


- (void)netManagerDidFinishGetSchoolImage:(NetManager *)thisNetManager
                                 response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_SchoolImage object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}
#pragma mark Inbox
#pragma mark Inbox
-(void)getInboxMessages
{
#if DEBUG
    NSLog(@"Inbox getInboxMessages method called... ");
#endif
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_getInboxMsz ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
        // Webservice Logging
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        // New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
#if DEBUG
        NSLog(@"getInboxMessages parameters%@",parameters);
#endif
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetInboxMsz:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
        
	}
}


- (void)netManagerDidFinishGetInboxMsz:(NetManager *)thisNetManager
                              response:(NSMutableData *)responseData
{
#if DEBUG
    NSLog(@"Inbox netManagerDidFinishGetInboxMsz Method called...");
#endif
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
        
        [Flurry logError:FLURRY_EVENT_Error_in_fetching_Inbox_Item message:FLURRY_EVENT_Error_in_fetching_Inbox_Item error:thisNetManager.error];
   
        
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_getInboxMsz object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

-(void)sendInboxMessages:(NSString*)sub des:(NSString*)des reciver:(NSString*)reciver
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_sendInboxMsz ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        // Webservice Logging Existing both
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setObject:reciver forKey:@"ReceiverId"];
        [parameters setObject:sub forKey:@"Subject"];
        [parameters setObject:des forKey:@"Description"];
        [parameters setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[self getStoredAllMessage] count]] forKey:@"AppId"];
        
#if DEBUG
        NSLog(@"Inbox sendInboxMessages %@",parameters);
#endif
      
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishSendInboxMsz:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}


- (void)netManagerDidFinishSendInboxMsz:(NetManager *)thisNetManager
                               response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_sendInboxMsz object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}
-(void)replyInboxMessages:(NSString*)sub des:(NSString*)des reciver:(NSString*)reciver parent:(NSNumber*)parent_id
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_replyInboxMsz ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        [parameters setObject:parent_id forKey:@"ParentId"];        //
        // Webservice Logging existing both
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setObject:reciver forKey:@"ReceiverId"];
        [parameters setObject:sub forKey:@"Subject"];
        [parameters setObject:des forKey:@"Description"];
        [parameters setObject:[NSString stringWithFormat:@"%lu",(unsigned long)[[self getStoredAllMessage] count]] forKey:@"AppId"];
        
        
        
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishReplyInboxMsz:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
        
	}
}


- (void)netManagerDidFinishReplyInboxMsz:(NetManager *)thisNetManager
                                response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
        
        [Flurry logError:FLURRY_EVENT_Error_in_replying_Inbox_Item message:FLURRY_EVENT_Error_in_replying_Inbox_Item error:thisNetManager.error];
        

        
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_sendInboxMsz object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}
-(void)readInboxMessages:(NSString*)msz_id
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_readInboxMsz ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        // Webservice Logging
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        // New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setObject:msz_id forKey:@"message_id"];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishReadInboxMsz:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}


- (void)netManagerDidFinishReadInboxMsz:(NetManager *)thisNetManager
                               response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
        
           [Flurry logError:FLURRY_EVENT_Error_in_fetching_Inbox_Item message:FLURRY_EVENT_Error_in_fetching_Inbox_Item error:thisNetManager.error];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_readnboxMsz object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}
-(void)deleteInboxMessages:(NSNumber*)msz_id
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_deleteInboxMsz ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        // Webservice Logging
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        // New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setObject:msz_id forKey:@"message_id"];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishDeleteInboxMsz:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}


- (void)netManagerDidFinishDeleteInboxMsz:(NetManager *)thisNetManager
                                 response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
        
        [Flurry logError:FLURRY_EVENT_Error_in_deleting_Inbox_Item message:FLURRY_EVENT_Error_in_deleting_Inbox_Item error:thisNetManager.error];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_deleteInboxMsz object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

- (Inbox *)getInboxDataInfoStored:(NSNumber*)message_id postNotification:(BOOL)postNotification
{
#if DEBUG
    NSLog(@"Inbox getInboxDataInfoStored");
#endif
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"message_id == %@", message_id];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Inbox  * schoolData  = nil;
	if (!error && [results count] > 0) {
		
		schoolData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
    }
    return schoolData;
    
}

- (NSArray *)getInboxDataStored:(NSNumber*)message_id postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"message_id == %@", message_id];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return results;
    
}


-(Inbox*)parseInboxMessagesWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"Inbox parseInboxMessagesWithDictionary");
#endif
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    
    Inbox *schoolData=[self getInboxDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    if (!schoolData) {
        schoolData = [NSEntityDescription
                      insertNewObjectForEntityForName:@"Inbox"
                      inManagedObjectContext:_managedObjectContext];
    }
    
    
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        schoolData.message_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"ParentId"] isKindOfClass:[NSNull class]]) {
        schoolData.parent_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"ParentId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"UserId"] isKindOfClass:[NSNull class]]) {
        schoolData.sender_id=[dictionary objectForKey:@"UserId"];
    }
    if (![[dictionary objectForKey:@"ReceiverId"] isKindOfClass:[NSNull class]]) {
        schoolData.reciver_id=[dictionary objectForKey:@"ReceiverId"];
    }
    if (![[dictionary objectForKey:@"Subject"] isKindOfClass:[NSNull class]]) {
        schoolData.subject=[dictionary objectForKey:@"Subject"];
    }
    if (![[dictionary objectForKey:@"Description"] isKindOfClass:[NSNull class]]) {
        schoolData.discrptin=[dictionary objectForKey:@"Description"];
    }
    if (![[dictionary objectForKey:@"Created"] isKindOfClass:[NSNull class]]) {
        //added by viraj - start - For issue #EZEST-2316
        
        [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
        NSString *createdStr = [dictionary objectForKey:@"Created"];
        
        NSString *createdDateStr = [[createdStr componentsSeparatedByString:@" "] objectAtIndex:0];
        
        NSDate *reminderDate = [formatter dateFromString:createdDateStr];
        unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
        NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:reminderDate];
        
        NSArray *reminderTime=nil;
        if (![[[createdStr componentsSeparatedByString:@" "] objectAtIndex:1] isKindOfClass:[NSNull class]]) {
            reminderTime= [[[createdStr componentsSeparatedByString:@" "] objectAtIndex:1] componentsSeparatedByString:@":"];
            
        }
        if([reminderTime count]>0)
        {
            [dateComponents setHour:[[reminderTime objectAtIndex:0] integerValue]];
            [dateComponents setMinute:[[reminderTime objectAtIndex:1] integerValue]];
            
            [dateComponents setSecond:[[reminderTime objectAtIndex:2] integerValue]];
        }
        
        
        NSDate *createdDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
        
        schoolData.created_time=createdDate;
        //added by viraj - end - For issue #EZEST-2316
    }
    schoolData.inbox_user=[AppHelper userDefaultsForKey:@"user_id"];
    [formatter release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    return schoolData;
}
-(NSArray*)getStoredMessageForLocal:(NSNumber*)mszId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox"];
    // change (local_parent_id == %@ OR parent_id==%@)
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"local_parent_id == %@ AND inbox_user==%@ AND isDel == %@", mszId,[AppHelper userDefaultsForKey:@"user_id"],[NSNumber numberWithBool:NO]];
    
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}

-(NSArray*)getStoredMessage:(NSNumber *)parent_id
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"local_parent_id == %@ AND inbox_user==%@ AND isDel == %@", parent_id,[AppHelper userDefaultsForKey:@"user_id"],[NSNumber numberWithBool:NO]];
    
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}
-(NSArray*)getStoredAllMessage
{
#if DEBUG
    NSLog(@"Inbox getStoredAllMessage");
#endif
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox"];
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}
- (void) deleteObjectOfInBoxMessge:(NSNumber*)messgeid{
    
    
    NSError *error;
    NSManagedObject *eventToDelete = [self getInboxDataInfoStored:messgeid   postNotification:NO];
    if(eventToDelete){
        [_managedObjectContext deleteObject:eventToDelete];
        
        if (![_managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Error deleting  - error:%@",error);
#endif
        }
    }
}
#pragma mark DayOfNotes
- (DayOfNotes *)getIDayOfNotesDataInfoStored:(NSNumber*)day_id postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DayOfNotes"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dayOfNotes_id == %@ AND user_id==%@", day_id,[AppHelper userDefaultsForKey:@"user_id"]];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    DayOfNotes  * schoolData  = nil;
	if (!error && [results count] > 0) {
		
		schoolData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return schoolData;
    
}
-(void*)parseDayOfNotesWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"parseDayOfNotesWithDictionary======%@",dictionary);
#endif
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    
    DayOfNotes *schoolData=[self getIDayOfNotesDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    if (!schoolData) {
        schoolData = [NSEntityDescription
                      insertNewObjectForEntityForName:@"DayOfNotes"
                      inManagedObjectContext:_managedObjectContext];
    }
    
    
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        schoolData.dayOfNotes_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"IncludeInCycle"] isKindOfClass:[NSNull class]]) {
        schoolData.includeInCycle=[NSNumber numberWithInteger:[[dictionary objectForKey:@"IncludeInCycle"]integerValue]];
    }
    if (![[dictionary objectForKey:@"IsGreyedOut"] isKindOfClass:[NSNull class]]) {
        schoolData.isGreyOut=[NSNumber numberWithBool:[[dictionary objectForKey:@"IsGreyedOut"] boolValue]];
    }
    if (![[dictionary objectForKey:@"OverrideCycle"] isKindOfClass:[NSNull class]]) {
        schoolData.overrideCycle=[NSNumber numberWithInteger:[[dictionary objectForKey:@"OverrideCycle"]integerValue]];
    }
    if (![[dictionary objectForKey:@"OverrideCycleDay"] isKindOfClass:[NSNull class]]) {
        schoolData.overrideCycleDay=[NSNumber numberWithInteger:[[dictionary objectForKey:@"OverrideCycleDay"]integerValue]];
    }
    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
        schoolData.schoolId=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SchoolId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Title"] isKindOfClass:[NSNull class]]) {
        schoolData.title=[dictionary objectForKey:@"Title"];
    }
    if (![[dictionary objectForKey:@"Date"] isKindOfClass:[NSNull class]]) {
        schoolData.dateOfNote=[formatter dateFromString:[dictionary objectForKey:@"Date"]];
    }
    schoolData.isWeekend=[NSNumber numberWithBool:[self isWeekend:[formatter dateFromString:[dictionary objectForKey:@"Date"]]]];
    schoolData.user_id=[AppHelper userDefaultsForKey:@"user_id"];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    [formatter release];
    return schoolData;
}
-(BOOL)isWeekend:(NSDate*)date{
    NSCalendar *calendar=[NSCalendar currentCalendar];
    NSDateComponents *dayComponents = [calendar
                                       components:NSDayCalendarUnit |kCFCalendarUnitWeekday fromDate:date];
    if([dayComponents weekday]==1 || [dayComponents weekday]==7){
        return YES;
    }
    else{
        return NO;
    }
}
-(BOOL)dayOfNotesHaveWeekend:(NSDate*)date
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DayOfNotes"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isWeekend == %@ AND user_id==%@ AND dateOfNote==%@ AND (includeInCycle==%@ OR isGreyOut==%@)", [NSNumber numberWithBool:NO],[AppHelper userDefaultsForKey:@"user_id"],date,[NSNumber numberWithInteger:0],[NSNumber numberWithBool:YES]];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    NSSortDescriptor *indexSort = [[NSSortDescriptor alloc] initWithKey:@"dateOfNote" ascending:YES];
    NSArray *array = [[NSArray alloc] initWithObjects:indexSort, nil];
    [request setSortDescriptors:array];
    [array release], array = nil;
    [indexSort release], indexSort = nil;

    if (!error && [results count] > 0)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(DayOfNotes*)dayOfNotesHaveHollyDay:(NSDate*)date semDate:(NSDate*)dateOfsem{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DayOfNotes"];
	if(dateOfsem==nil){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@" user_id==%@ AND dateOfNote<=%@ AND overrideCycle>=%@",[AppHelper userDefaultsForKey:@"user_id"],date,[NSNumber numberWithInteger:1]];
        [request setPredicate:predicate];
    }
    else{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@" dateOfNote>=%@ AND user_id==%@ AND dateOfNote<=%@ AND overrideCycle>=%@",dateOfsem,[AppHelper userDefaultsForKey:@"user_id"],date,[NSNumber numberWithInteger:1]];
        [request setPredicate:predicate];
    }
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    NSSortDescriptor *indexSort = [[NSSortDescriptor alloc] initWithKey:@"dateOfNote" ascending:YES];
    NSArray *array = [[NSArray alloc] initWithObjects:indexSort, nil];
    [request setSortDescriptors:array];
    [array release], array = nil;
    [indexSort release], indexSort = nil;
    DayOfNotes *dayNotes=nil;
    if (!error && [results count] > 0)
    {
        dayNotes = [results lastObject];
    }
    return dayNotes;
}
-(NSArray*)dayOfNotesCycleLength:(NSDate*)date semDate:(NSDate*)dateOfsem{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DayOfNotes"];
	
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@" dateOfNote>=%@ AND user_id==%@ AND dateOfNote<=%@ AND overrideCycleDay>=%@",dateOfsem,[AppHelper userDefaultsForKey:@"user_id"],date,[NSNumber numberWithInteger:1]];
    [request setPredicate:predicate];
    
    
    
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    NSSortDescriptor *indexSort = [[NSSortDescriptor alloc] initWithKey:@"dateOfNote" ascending:YES];
    NSArray *array = [[NSArray alloc] initWithObjects:indexSort, nil];
    [request setSortDescriptors:array];
    [array release], array = nil;
    [indexSort release], indexSort = nil;
    DayOfNotes *dayNotes=nil;
    if (!error && [results count] > 0)
    {
        dayNotes = [results lastObject];
    }
    return results;
}
-(DayOfNotes*)dayOfNotesIncludeCycleDay:(NSDate*)date note:(NSNumber*)day_id{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DayOfNotes"];
    //    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    //    [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
 	//NSPredicate *predicate = [NSPredicate predicateWithFormat:@" user_id==%@ AND dateOfNote<=%@ AND dayOfNotes_id>=%@ AND includeInCycle==%@",[AppHelper userDefaultsForKey:@"user_id"],date,day_id,[NSNumber numberWithBool:YES]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@" user_id==%@ AND dateOfNote==%@ AND overrideCycleDay>=%@",[AppHelper userDefaultsForKey:@"user_id"],date,[NSNumber numberWithInteger:1]];
    [request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    NSSortDescriptor *indexSort = [[NSSortDescriptor alloc] initWithKey:@"dateOfNote" ascending:YES];
    NSArray *array = [[NSArray alloc] initWithObjects:indexSort, nil];
    [request setSortDescriptors:array];
    [array release], array = nil;
    [indexSort release], indexSort = nil;
    DayOfNotes *dayNotes=nil;
    if (!error && [results count] > 0)
    {
        dayNotes = [results objectAtIndex:0];
    }
    
    return dayNotes;
}


#pragma mark
#pragma  mark For Note Entry
- (NoteEntry *)getNoteEntryDataInfoStoredForDelete:(NSNumber*)itemId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"NoteEntry"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"noteEntry_id == %@", itemId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
   
    
    NoteEntry  * addEventItemData  = nil;
	if (!error && [results count] > 0) {
		
		addEventItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return addEventItemData;
    
}

- (NoteEntry *)getNoteEntryDataInfoStoredInDb:(NSNumber*)uniqueId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"NoteEntry"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"unique_id == %@", uniqueId];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    
    
    NoteEntry  * addEventItemData  = nil;
    if (!error && [results count] > 0) {
        
        addEventItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
        
    }
    
    return addEventItemData;
    
}


- (void) deleteObjectOfNoteEntryItem:(NSNumber*)Item_id  {
    
    
    NSError *error;
    NSManagedObject *eventToDelete = [self getNoteEntryDataInfoStoredForDelete:Item_id ];
    if (eventToDelete)
    {
        [_managedObjectContext deleteObject:eventToDelete];
    }
    
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif
      
    }
}
-(void)parseNoteEntryDataWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif
    
#if DEBUG
    NSLog( @"parseNoteEntryDataWithDictionary = %@", dictionary );
#endif
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init] ;
    [formatter1 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    NoteEntry *itemData=nil;
    
    itemData = [self getNoteEntryDataInfoStoredForDelete:[NSNumber numberWithInteger:[[dictionary objectForKey:@"uniqueid"]integerValue]]];
    
    if(itemData ==nil)
    {
        itemData = [self getNoteEntryDataInfoStoredInDb:[NSNumber numberWithInteger:[[dictionary objectForKey:@"appId"]integerValue]]];
    }
    
    NSDateFormatter *formatter3 = [[NSDateFormatter alloc] init] ;
    
    
    if (!itemData) {
        itemData = [NSEntityDescription
                    insertNewObjectForEntityForName:@"NoteEntry"
                    inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"uniqueid"] isKindOfClass:[NSNull class]]) {
        itemData.noteEntry_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"uniqueid"]integerValue]];
    }
    
    if ([dictionary objectForKey:@"isSync"])
    {
        itemData.isSync=[NSNumber numberWithBool:[[dictionary objectForKey:@"isSync"] boolValue]];
    }
    else
    {
        itemData.isSync=[NSNumber numberWithBool:YES];
    }
    ///EZEST TEST  1733
     itemData.isDelete=[NSNumber numberWithBool:NO];
    
    if (![[dictionary objectForKey:@"IpAddress"] isKindOfClass:[NSNull class]]) {
        itemData.ipadd=[dictionary objectForKey:@"IpAddress"];
    }
    if (![[dictionary objectForKey:@"noteId"] isKindOfClass:[NSNull class]]) {
        itemData.note_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"noteId"]integerValue]];
    }
    
    ////17june
    
    
    if (![[dictionary objectForKey:@"text"] isKindOfClass:[NSNull class]]) {
        itemData.text=[dictionary objectForKey:@"text"];
    }
    if (![[dictionary objectForKey:@"appId"] isKindOfClass:[NSNull class]]) {
        itemData.unique_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"appId"]integerValue]];
    }
    
    if ([dictionary objectForKey:@"localnoteid"] ) {
        itemData.note_localId=[NSNumber numberWithInteger:[[dictionary objectForKey:@"localnoteid"]integerValue]];
    }
    
    else{
        itemData.note_localId=[NSNumber numberWithInteger:[[AppHelper userDefaultsForKey:@"noteid"]integerValue]];
    }
    if (![[dictionary objectForKey:@"userid"] isKindOfClass:[NSNull class]]) {
        itemData.user_id=[dictionary objectForKey:@"userid"];
    }
    if (![[dictionary objectForKey:@"createddate"] isKindOfClass:[NSNull class]]) {
        itemData.created=[self dateFromMilliSecond:[NSNumber numberWithDouble:[[dictionary objectForKey:@"createddate"] doubleValue]]];
    }
    
    
    [formatter release];
    [formatter1 release];
    [formatter3 release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}
    
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_insertNoteEntry object:nil userInfo:dictionary];

}


- (NoteEntry *)getNoteEntryDataInfoStoredForParseDate:(NSNumber*)itemId ipadd:(NSString*)ipadd
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"NoteEntry"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"unique_id == %@ AND ipadd==%@", itemId,ipadd];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    NoteEntry  * addItemData  = nil;
	if (!error && [results count] > 0) {
		
		addItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return addItemData;
    
}
- (NSArray *)getAllNoteEntryDataI:(NSNumber*)itemId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"NoteEntry"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"note_localId == %@ AND isDelete==%@", itemId,[NSNumber numberWithBool:NO]];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    NoteEntry  * addItemData  = nil;
	if (!error && [results count] > 0) {
		
		addItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return results;
    
}
- (NSArray *)getAllNoteEntryDataIwithUniqueId:(NSNumber*)itemId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"NoteEntry"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"note_Id == %@", itemId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    NoteEntry  * addItemData  = nil;
	if (!error && [results count] > 0) {
		
		addItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
    }
    
    return results;
    
}

-(void)insertUpadteNoteEntry:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        
        
        NSString *serviceName = Method_insertupdatenotentry ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
        
        if ([dict objectForKey:@"det"] ) {
            [parameters setObject:[dict objectForKey:@"det"]forKey:@"details"];
        }
        if ([dict objectForKey:@"not"]) {
            [parameters setObject:[dict objectForKey:@"title"]forKey:@"Title"];
        }
        if([dict objectForKey:@"deletedAttachmentId"]){
            [parameters setObject:[dict objectForKey:@"deletedAttachmentId"]forKey:@"deletedAttachmentId"];
        }

        // Webservice Logging
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"userId"];
        // New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
#if DEBUG
        NSLog(@"parameters = %@", [parameters JSONRepresentation]);
#endif
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishInsertUpadteNoteEntry:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
	}
}

- (void)netManagerDidFinishInsertUpadteNoteEntry:(NetManager *)thisNetManager
                                        response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_insertNoteEntry object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}
#pragma  mark For syncronizeDiaryItem
//29Junework
- (NSArray *)getEvnetsDataInfoStoredwithNoSync:(NSNumber*)isSyncVal
{
   
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"EventItem"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSync == %@ AND isDelete==%@", isSyncVal,[NSNumber numberWithBool:YES]];	[request setPredicate:predicate];
        
        NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        results =  [[NSSet setWithArray:results] allObjects];
        NSMutableArray* filterResults = [[NSMutableArray alloc] init];
        BOOL copy;
        
        if (![results count] == 0) {
            for (EventItem *a1 in results) {
                copy = YES;
                for (EventItem *a2 in filterResults) {
                    
                    if ([a1.item_Id integerValue]== [a2.item_Id integerValue] && [a1.universal_id integerValue]== [a2.universal_id integerValue]) {
                        copy = NO;
                        break;
                    }
                }
                if (copy) {
                    [filterResults addObject:a1];
                }
            }
        }
        results = [self getActiveEvents:filterResults];

        
        EventItem  * addItemData  = nil;
        if (!error && [results count] > 0) {
            
            addItemData = [results objectAtIndex:0];
        }
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
    
        return results;
        
    
}

#pragma  mark

- (NSArray *)getTaskBasedMeritStoredwithNoSync:(NSNumber*)isSyncVal
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DairyItemUsers"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSynch == %@", isSyncVal];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (!error && [results count] > 0) {
        return results;
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return [NSArray array];
    
}


- (NSArray *)getNoteEntryDataInfoStoredwithNoSync:(NSNumber*)isSyncVal
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"NoteEntry"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSync == %@", isSyncVal];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    NoteEntry  * addItemData  = nil;
	if (!error && [results count] > 0) {
		
		addItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return results;
    
}

- (NSArray *)getOutOfClassDataInfoStoredwithNoSync:(NSNumber*)itemId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSync==%@ AND isDelete==%@ AND (type ==%@ OR type ==%@)", itemId,[NSNumber numberWithBool:NO],@"OutOfClass",@"Out Of Class"];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    Item  * addItemData  = nil;
	if (!error && [results count] > 0) {
		
		addItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    
    
    return filterResults;
    
}

- (NSArray *)getItemDataInfoStoredwithNoSync:(NSNumber*)itemId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSync==%@ AND isDelete==%@ AND (type !=%@ OR type !=%@)", itemId,[NSNumber numberWithBool:NO],@"OutOfClass",@"Out Of Class"];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    Item  * addItemData  = nil;
	if (!error && [results count] > 0) {
		
		addItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.class_id integerValue] == [a2.class_id integerValue] && [a1.universal_Id integerValue] == [a2.universal_Id integerValue] && [a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    
    
    return filterResults;
    
}
- (NSArray *)getItemDataInfoStoredwithdeletedItem
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Item"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@", [NSNumber numberWithBool:YES]];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    Item  * addItemData  = nil;
	if (!error && [results count] > 0) {
		
		addItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Item *a1 in results) {
            copy = YES;
            for (Item *a2 in filterResults) {
                if ([a1.class_id integerValue] == [a2.class_id integerValue] && [a1.universal_Id integerValue] == [a2.universal_Id integerValue] && [a1.itemId integerValue] == [a2.itemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    NSArray *activeItems = [self getActiveItems:filterResults];
    
    return activeItems;
    
}
-(void)synchronizeDiaryItem:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        
        
        NSString *serviceName = Method_syncronizeDiaryItem ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        if ([dict objectForKey:@"diaryitems"] ) {
            [parameters setObject:[dict objectForKey:@"diaryitems"]forKey:@"diaryitems"];
        }
        if ([dict objectForKey:@"events"]) {
            [parameters setObject:[dict objectForKey:@"events"]forKey:@"events"];
        }
        if ([dict objectForKey:@"deleteDairyitem"]) {
            [parameters setObject:[dict objectForKey:@"deleteDairyitem"]forKey:@"deleteDairyitem"];
        }
        if ([dict objectForKey:@"details"]) {
            [parameters setObject:[dict objectForKey:@"details"]forKey:@"details"];
        }
        
        if ([dict objectForKey:@"deletedTaskBasedMerits"]) {
            [parameters setObject:[dict objectForKey:@"deletedTaskBasedMerits"]forKey:@"deletedTaskBasedMerits"];
        }
        
        // Webservice Logging existing both
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
       [parameters setObject:[AppHelper userDefaultsForKey:@"ipadd"]forKey:@"ipadd"];
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
        TableUpdateTime *tabl=[self getlastUpdateTableDataInfoStored];
        
        if([[NSUserDefaults standardUserDefaults] objectForKey:vserionUpdate] == nil)
        {
            [self deleteObjectOfDairyItemOnTheBasisOfUniversalID];

            [parameters setObject:[NSNumber numberWithInteger:0] forKey:@"lastSynch"];
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            if (standardUserDefaults) {
                [standardUserDefaults setObject:@"DBUpdated" forKey:vserionUpdate];
                [standardUserDefaults synchronize];
            }
        }
        else
        {
            [parameters setObject:tabl.diaryItem forKey:@"lastSynch"];
        }

        //tabl.diaryItem
        MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
        
        [parameters setObject:profobj.type forKey:@"user_type"];
        
        
#if DEBUG
        NSLog(@"InsertUpdate SYNC Parameters %@",[parameters JSONRepresentation ]);
#endif
      
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishSynchronizeDiaryItem:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
	}
}

- (void)netManagerDidFinishSynchronizeDiaryItem:(NetManager *)thisNetManager
                                       response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        // JIRA 1679
        if ([DELEGATE glbIsStudentSelected])
        {
            [DELEGATE setGlbIsSyncCalled:YES];
        }
        else
        {
            [DELEGATE setGlbIsSyncCalled:NO];
        }
        
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
        
        //Aman Gupta
        [[Service sharedInstance]parseDiaryItemTypesDataWithDictionary:dictionary];
        
        /*******************New Methods Added For Out Class********************************/
        [[Service sharedInstance] parseListOfValuesDataWithDictionary:dictionary];
        
        /*******************New Methods Added For Out Class********************************/

        
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_syncronizeDiaryItem object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}
#pragma mark LastUpdateTimeOfTable
-(void)parseLastUpdateTableDictionary:(NSDictionary*)dictionary
{
    
    TableUpdateTime *annoncementsData=[self getlastUpdateTableDataInfoStored];
    if (!annoncementsData) {
        annoncementsData = [NSEntityDescription
                            insertNewObjectForEntityForName:@"TableUpdateTime"
                            inManagedObjectContext:_managedObjectContext];
    }
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        annoncementsData.table_id= [dictionary objectForKey:@"Id"];
    }
    if (![[dictionary objectForKey:@"diary"] isKindOfClass:[NSNull class]]) {
        annoncementsData.diaryItem= [NSNumber numberWithDouble:[[dictionary objectForKey:@"diary"] doubleValue]];
    }
    //    if (![[dictionary objectForKey:@"note"] isKindOfClass:[NSNull class]]) {
    //        annoncementsData.= [NSNumber numberWithInt:[[dictionary objectForKey:@"note"] doubleValue]];
    //    }
    if (![[dictionary objectForKey:@"inbox"] isKindOfClass:[NSNull class]])
    {
#if DEBUG
        NSLog(@"parseLastUpdateTableDictionary called for Inbox ...");
#endif
      
        annoncementsData.inbox= [NSNumber numberWithDouble:[[dictionary objectForKey:@"inbox"] doubleValue]];
    }
    
    if (![[dictionary objectForKey:@"class"] isKindOfClass:[NSNull class]]) {
        annoncementsData.classUpdateTime= [NSNumber numberWithDouble:[[dictionary objectForKey:@"class"] doubleValue]];
    }
    
    if (![[dictionary objectForKey:@"class"] isKindOfClass:[NSNull class]]) {
        annoncementsData.studentUpdateTime= [NSNumber numberWithDouble:[[dictionary objectForKey:@"class"] doubleValue]];
    }
    //12/AUG/2013
    if (![[dictionary objectForKey:@"class"] isKindOfClass:[NSNull class]]) {
        annoncementsData.about= [NSNumber numberWithDouble:[[dictionary objectForKey:@"class"] doubleValue]];
    }
    if (![[dictionary objectForKey:@"class"] isKindOfClass:[NSNull class]]) {
        annoncementsData.schoolInfo= [NSNumber numberWithDouble:[[dictionary objectForKey:@"class"] doubleValue]];
    }
    if (![[dictionary objectForKey:@"class"] isKindOfClass:[NSNull class]]) {
        annoncementsData.timeTble= [NSNumber numberWithDouble:[[dictionary objectForKey:@"class"] doubleValue]];
    }
    //for image
    if (![[dictionary objectForKey:@"class"] isKindOfClass:[NSNull class]]) {
        annoncementsData.empowerContent= [NSNumber numberWithDouble:[[dictionary objectForKey:@"class"] doubleValue]];
    }
    
    //    if (![[dictionary objectForKey:@"Created"] isKindOfClass:[NSNull class]]) {
    //        annoncementsData.created_time=[formatter1 dateFromString:[formatter1 stringFromDate:[formatter dateFromString:[dictionary objectForKey:@"Created"]]]];
    //    }
    //    if (![[dictionary objectForKey:@"Body"] isKindOfClass:[NSNull class]]) {
    //        annoncementsData.details=[dictionary objectForKey:@"Body"];
    //    }
    //    if (![[dictionary objectForKey:@"Tags"] isKindOfClass:[NSNull class]]) {
    //        annoncementsData.tags=[dictionary objectForKey:@"Tags"];
    //    }
    //    if (![[dictionary objectForKey:@"Title"] isKindOfClass:[NSNull class]]) {
    //        annoncementsData.title=[dictionary objectForKey:@"Title"];
    //    }
    //    if (![[dictionary objectForKey:@"SchoolId"] isKindOfClass:[NSNull class]]) {
    //        annoncementsData.school_id=[NSNumber numberWithInt: [[dictionary objectForKey:@"SchoolId"]intValue]];
    //    }
    
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    
    
}


- (TableUpdateTime *)getlastUpdateTableDataInfoStored
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TableUpdateTime"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"table_id == %@", @"1"];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    TableUpdateTime  * annoncementsmData  = nil;
	if (!error && [results count] > 0) {
		
		annoncementsmData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return annoncementsmData;
    
}
#pragma mark CycleAnd Day
-(void)parseCycleDaywithDictionary:(NSDictionary*)dictionary{
    
    NSDateFormatter *formater=[[NSDateFormatter alloc]init];
    [formater setDateFormat:@"dd-MM-yyyy"];
    
    TableForCycle *annoncementsData=[self getlastcycleandDayTableDataInfoStored:[formater dateFromString:[dictionary objectForKey:@"date"]]];
    if (!annoncementsData) {
        annoncementsData = [NSEntityDescription
                            insertNewObjectForEntityForName:@"TableForCycle"
                            inManagedObjectContext:_managedObjectContext];
    }
    if (![[dictionary objectForKey:@"date"] isKindOfClass:[NSNull class]]) {
        annoncementsData.dateRef= [formater dateFromString:[dictionary objectForKey:@"date"]];
    }
    if (![[dictionary objectForKey:@"cycle"] isKindOfClass:[NSNull class]]) {
        annoncementsData.cycle= [dictionary objectForKey:@"cycle"];
    }
    
    if (![[dictionary objectForKey:@"day"] isKindOfClass:[NSNull class]]) {
        annoncementsData.cycleDay= [dictionary objectForKey:@"day"];
    }
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    [formater release];
    
}


- (TableForCycle *)getlastcycleandDayTableDataInfoStored:(NSDate*)date
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TableForCycle"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateRef == %@",date];
   
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    TableForCycle  * annoncementsmData  = nil;
	if (!error && [results count] > 0) {
		
		annoncementsmData = [results objectAtIndex:0];
    }
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return annoncementsmData;
    
}

#pragma mark Rohit changes
- (MyClass*)getClassDataInfoStoredForAppid:(NSString*)appId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"app_id==%@",appId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass  * addItemData  = nil;
	if (!error && [results count] > 0) {
		
		addItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return addItemData;
    
}

-(NSArray*)getStoredAllClassDataWithSubjecName:(NSString*)subjectname
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@ AND subject_name==%@",[NSNumber numberWithBool:NO],subjectname];
	[request setPredicate:predicate];

    NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}


-(NSArray*)getStoredAllClassDataInApp:(NSNumber*)table_id
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@ AND timeTable_id==%@",[NSNumber numberWithBool:NO],table_id];
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];

#if DEBUG
    NSLog(@"getStoredAllClassDataInApp %@",results);
#endif


    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}
#pragma mark get class via class ID
- (MyClass *)getClassDataFromClassId:(NSString*)classId {
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
	
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_id == %@", classId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classData;
    
}

- (NSArray *)getClassDataInfoStoredwithNoSync:(NSNumber*)sync
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSync == %@", sync];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass  * addItemData  = nil;
	if (!error && [results count] > 0) {
		
		addItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return results;
    
}
#pragma mark delete class object
- (void) deleteClassObject:(MyClass*)Object  {
    
    NSError *error;
    
    if(Object)
        [_managedObjectContext deleteObject:Object];
    
    if (![_managedObjectContext save:&error])
    {
     
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif
      
    }
}
-(void)SyncMyClassDataWithServer:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"SyncMyClassDataWithServer %@",dictionary);
#endif
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    
    MyClass *classData=[[Service sharedInstance] getClassDataRoom:[dictionary objectForKey:@"room_name"] Subject:[dictionary objectForKey:@"sub_name"] period:[dictionary objectForKey:@"PeriodId"] cycle:[dictionary objectForKey:@"CycleDays"]];
    
    if (!classData)
    {
        classData = [NSEntityDescription
                     insertNewObjectForEntityForName:@"MyClass"
                     inManagedObjectContext:_managedObjectContext];
    }
    Period *perObj=[self getPeriodDataInfoStored:[dictionary objectForKey:@"PeriodId"] postNotification:NO];
    
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        classData.class_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"AppId"] isKindOfClass:[NSNull class]]) {
        classData.app_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"AppId"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
        classData.class_name=[dictionary objectForKey:@"Name"];
    }
    if (![[dictionary objectForKey:@"CycleDays"] isKindOfClass:[NSNull class]]) {
        classData.cycle_day=[NSNumber numberWithInteger:[[dictionary objectForKey:@"CycleDays"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Code"] isKindOfClass:[NSNull class]]) {
        classData.code=[dictionary objectForKey:@"Code"];
    }
    if (![[dictionary objectForKey:@"PeriodId"] isKindOfClass:[NSNull class]]) {
        classData.period_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"PeriodId"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"RoomId"] isKindOfClass:[NSNull class]]) {
        classData.room_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"RoomId"]integerValue]];
    }
    // Campus Based Start
    if (![[dictionary objectForKey:@"CampusId"] isKindOfClass:[NSNull class]]) {
        
        classData.campusId =[NSNumber numberWithInteger:[[dictionary objectForKey:@"CampusId"] integerValue]];
    }
#if DEBUG
    NSLog(@"classData.campusId %@",classData.campusId);
#endif
    
    if (![[dictionary objectForKey:@"CampusCode"] isKindOfClass:[NSNull class]]) {
        
        classData.campusCode=[dictionary objectForKey:@"CampusCode"];
    }
#if DEBUG
    NSLog(@"classData.campusCode %@",classData.campusCode);
#endif
    
    // Campus Based End
    
    
    
    if (![[dictionary objectForKey:@"room_name"] isKindOfClass:[NSNull class]]) {
        classData.room_name=[dictionary objectForKey:@"room_name"];
    }
    
    if (![[dictionary objectForKey:@"sub_name"] isKindOfClass:[NSNull class]]) {
        classData.subject_name=[dictionary objectForKey:@"sub_name"];
    }
    
    if (![[dictionary objectForKey:@"SubjectId"] isKindOfClass:[NSNull class]]) {
        classData.subject_d=[NSNumber numberWithInteger:[[dictionary objectForKey:@"SubjectId"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"Updated"] isKindOfClass:[NSNull class]]) {
        classData.updatedTime=[self dateFromMilliSecond:[NSNumber numberWithDouble:[[dictionary objectForKey:@"Updated"] doubleValue]]];
    }
    classData.class_userID=[AppHelper userDefaultsForKey:@"user_id"];
    
    //Important
    if (![[dictionary objectForKey:@"CreatedBy"] isKindOfClass:[NSNull class]]) {
        classData.createdBy=[dictionary objectForKey:@"CreatedBy"];
    }
    
    //new changes ß
    if (![[dictionary objectForKey:@"Created"] isKindOfClass:[NSNull class]]) {
        classData.createdTime=[self dateFromMilliSecond:[NSNumber numberWithDouble:[[dictionary objectForKey:@"Created"] doubleValue]]];
    }
    
    if(perObj.timeTable_id)
        classData.timeTable_id=perObj.timeTable_id;
    
    if ([dictionary objectForKey:@"isSync"]) {
        classData.isSync=[NSNumber numberWithBool:[[dictionary objectForKey:@"isSync"] boolValue]];
    }
    else{
        classData.isSync=[NSNumber numberWithBool:YES];
    }
    
    classData.isDelete=[NSNumber numberWithBool:NO];
    
#if DEBUG
    NSLog(@"classData %@",classData);
#endif
    
    
    //save subject data
    Subjects *subjectData=[self getSubjectDataWithName:[dictionary objectForKey:@"sub_name"] ];
    
    if (!subjectData) {
        subjectData = [NSEntityDescription
                       insertNewObjectForEntityForName:@"Subjects"
                       inManagedObjectContext:_managedObjectContext];
    }
    if (![[dictionary objectForKey:@"Code"] isKindOfClass:[NSNull class]]) {
        subjectData.subject_color=[dictionary objectForKey:@"Code"];
    }
    
    if (![[dictionary objectForKey:@"sub_name"] isKindOfClass:[NSNull class]]) {
        subjectData.subject_name=[dictionary objectForKey:@"sub_name"];
    }
    
    //save room data
    Room *roomData=[self getRoomDataInfoStored:[dictionary objectForKey:@"RoomId"] postNotification:NO];
    
    if (!roomData) {
        roomData = [NSEntityDescription
                    insertNewObjectForEntityForName:@"Room"
                    inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"RoomId"] isKindOfClass:[NSNull class]]) {
        roomData.room_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"RoomId"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"room_name"] isKindOfClass:[NSNull class]]) {
        roomData.room_name=[dictionary objectForKey:@"room_name"];
    }
    
    [formatter release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"SyncMyClassDataWithServer Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}
    
}
#pragma mark class update last Sync time
-(void)updateClassTime:(TableUpdateTime*)table
{
    table.classUpdateTime=[NSNumber numberWithDouble:[[self miliSecondFromDateMethod:[NSDate date]]doubleValue]];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
}



#pragma mark get my class
- (MyClass *)getClassDataRoom:(NSString*)roomName Subject:(NSString*)subName period:(NSString*)periodID cycle:(NSString*)cycle;
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
	
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"period_Id == %@ AND  room_name==%@ AND subject_name== %@ AND cycle_day==%@", periodID,roomName,subName,cycle];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass  * classData  = nil;
	if (!error && [results count] > 0) {
		
		classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classData;
    
}

- (MyClass *)getClassDataRoom:(NSNumber*)roomID period:(NSNumber*)periodID cycle:(NSNumber*)cycleday classID:(NSNumber*)classID
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"period_Id ==%@ AND room_Id ==%@ AND cycle_day ==%@ AND class_id ==%@", periodID,roomID,cycleday,classID];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass  * classData  = nil;
    if (!error && [results count] > 0) {
        
        classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classData;
    
}



-(void)parseMyClassDataWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"parseMyClassDataWithDictionary %@",dictionary);
#endif
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    
    MyClass *classData=[[Service sharedInstance] getClassDataInfoStored:[dictionary objectForKey:@"Id"]  cycle:[dictionary objectForKey:@"CycleDays"] postNotification:NO];
    
    if (!classData)
    {
        classData = [NSEntityDescription
                     insertNewObjectForEntityForName:@"MyClass"
                     inManagedObjectContext:_managedObjectContext];
    }
    Period *perObj=[self getPeriodDataInfoStored:[dictionary objectForKey:@"PeriodId"] postNotification:NO];
    
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        classData.class_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"AppId"] isKindOfClass:[NSNull class]]) {
        classData.app_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"AppId"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"Name"] isKindOfClass:[NSNull class]]) {
        classData.class_name=[dictionary objectForKey:@"Name"];
    }
    if (![[dictionary objectForKey:@"CycleDays"] isKindOfClass:[NSNull class]]) {
        classData.cycle_day=[NSNumber numberWithInteger:[[dictionary objectForKey:@"CycleDays"]integerValue]];
    }
    if (![[dictionary objectForKey:@"Code"] isKindOfClass:[NSNull class]]) {
        classData.code=[dictionary objectForKey:@"Code"];
    }
    if (![[dictionary objectForKey:@"PeriodId"] isKindOfClass:[NSNull class]]) {
        classData.period_Id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"PeriodId"]integerValue]];
    }
    if (![[dictionary objectForKey:@"room_name"] isKindOfClass:[NSNull class]]) {
        classData.room_name=[dictionary objectForKey:@"room_name"];
    }
    
    if (![[dictionary objectForKey:@"sub_name"] isKindOfClass:[NSNull class]]) {
        classData.subject_name=[dictionary objectForKey:@"sub_name"];
    }
    
    if (![[dictionary objectForKey:@"Updated"] isKindOfClass:[NSNull class]]) {
        classData.updatedTime=[formatter dateFromString:[dictionary objectForKey:@"Updated"]];
    }
    classData.class_userID=[AppHelper userDefaultsForKey:@"user_id"];
    
    //Important
    
    if (![[dictionary objectForKey:@"CreatedBy"] isKindOfClass:[NSNull class]]) {
        classData.createdBy=[dictionary objectForKey:@"CreatedBy"];
    }
    
    //new changes ß
    if (![[dictionary objectForKey:@"Created"] isKindOfClass:[NSNull class]]) {
        classData.createdTime=[formatter dateFromString:[dictionary objectForKey:@"Created"]];
    }
    
    if(perObj.timeTable_id)
        classData.timeTable_id=perObj.timeTable_id;
    
    
    if ([dictionary objectForKey:@"isSync"])
    {
        classData.isSync=[NSNumber numberWithBool:[[dictionary objectForKey:@"isSync"] boolValue]];
    }
    else{
        classData.isSync=[NSNumber numberWithBool:YES];
    }
    
    [formatter release];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"parseMyClassDataWithDictionary Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	
        
	}
    
}

-(void)synchronizeClass:(NSMutableDictionary*)dict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_syncronizeClass ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        if ([dict objectForKey:@"details"] ) {
            [parameters setObject:[dict objectForKey:@"details"]forKey:@"details"];
        }
        if ([dict objectForKey:@"deleteClasses"] ) {
            [parameters setObject:[dict objectForKey:@"deleteClasses"]forKey:@"deleteClasses"];
        }
        
        // Webservice Logging
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        // New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_type"]forKey:@"user_type"];
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
        
        TableUpdateTime *tabl=[self getlastUpdateTableDataInfoStored];
        [parameters setObject:tabl.classUpdateTime forKey:@"lastSynch"];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self finishLoadingSelector:@"netManagerDidFinishSynchronizeClass:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
	}
}
- (void)netManagerDidFinishSynchronizeClass:(NetManager *)thisNetManager
                                   response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        NSDictionary *jsonDictD = [decodedString JSONValue];
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_syncronizeClass object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}
- (void)deleteSubjectForClass:(MyClass*)class  {
    
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    // Aman Changed this 23Rd April 2014
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_id == %@ AND period_Id == %@", [class.class_id stringValue],[class.period_Id stringValue]];

    // Aman Changed Before this below is the predicate 23Rd April 2014

    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"subject_name == %@", class.subject_name];
    
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
	if (!error && [results count]>0)
    {
        [_managedObjectContext deleteObject:[results objectAtIndex:0]];
        
        if (![_managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Error deleting  - error:%@",error);
#endif
        }
    }
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
}

- (NSArray *)getClassDeleteObject:(NSNumber*)del
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isDelete==%@",del];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass  * addItemData  = nil;
	if (!error && [results count] > 0) {
		
		addItemData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return results;
    
}
#pragma mark
#pragma mark My Add-Ons-Ishan

-(void)storeInboxMessages:(NSString*)sub des:(NSString*)des reciver:(NSString*)reciver parentId:(NSNumber *)parentId localParentId:(NSNumber *)localParentId parentMailId:(NSString *) parentMailId
{
#if DEBUG
    NSLog(@"Inbox storeInboxMessages 1");
#endif
    
    Inbox *message = [NSEntityDescription
                      insertNewObjectForEntityForName:@"Inbox"
                      inManagedObjectContext:_managedObjectContext];
    
    message.subject=sub;
    message.discrptin=des;
    message.reciver_id=reciver;
    message.message_id=[NSNumber numberWithInteger:0];
    message.inbox_user=[AppHelper userDefaultsForKey:@"user_id"];
    message.created_time=[NSDate date];
    message.sender_id=[AppHelper userDefaultsForKey:@"user_id"];
    message.ipAdd=[AppHelper userDefaultsForKey:@"ipadd"];
    
    message.email_id = parentMailId ;
    
    if(parentId)
        message.parent_id=parentId;
    else
        message.parent_id=[NSNumber  numberWithInteger:0];
    
    message.isSync=[NSNumber numberWithBool:NO];
    message.isDel=[NSNumber numberWithBool:NO];
    message.appId=[[Service sharedInstance] nextAvailble:@"appId" forEntityName:@"Inbox"];
    
    
    if(localParentId)
        message.local_parent_id=localParentId;
    else
        message.local_parent_id=[NSNumber numberWithInteger:0];
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}
    else
    {
        
    }
    
}

-(void)synchronizeMails
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_sendmessagesync ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
        TableUpdateTime *tabl=[[Service sharedInstance] getlastUpdateTableDataInfoStored];
        
        
        [parameters setValue:tabl.inbox forKey:@"LastSynch"];
       
        // Webservice Logging existing both
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        [parameters setValue:[self getUnsentMail] forKey:@"details"];

        [parameters setValue:[self getdeletedMessageUsers] forKey:@"deleteMessage"];
        
        [parameters setObject:[AppHelper userDefaultsForKey:@"ipadd"]forKey:@"IpAddress"];
        
#if DEBUG
        NSLog(@"Inbox url = %@",urlString);
        NSLog(@"Inbox parameters = %@",[parameters JSONRepresentation]);
#endif
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishSynchronizeMailItem:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
	}
    
}


-(NSArray *)getUnsentMail
{
#if DEBUG
    NSLog(@"Inbox getUnsentMail");
#endif
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox"];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isSync==%@",[NSNumber numberWithBool:NO]];
    [request setPredicate:pred];
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    NSDateFormatter *dateFor=[[NSDateFormatter alloc]init];
    [dateFor setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
    [dateFor setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    if (!error && [results count] > 0)
    {
        
	}
    
    for (Inbox *info in results)
    {
        NSArray *arrParentMailId = nil;
        
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        if(info.discrptin!=nil)
            [dict setValue:info.discrptin forKey:@"Description"];
        if(info.subject!=nil)
            [dict setValue:info.subject forKey:@"Subject"];
        if(info.reciver_id!=nil)
            [dict setValue:info.reciver_id forKey:@"ReceiverId"];
        if(info.appId!=nil)
            [dict setValue:info.appId forKey:@"AppId"];
        if(info.created_time!=nil)
            [dict setValue:[dateFor stringFromDate:info.created_time] forKey:@"created"];
        if(info.parent_id!=nil)
            [dict setValue:info.parent_id forKey:@"ParentId"];
        if(info.local_parent_id!=nil)
            [dict setValue:info.local_parent_id forKey:@"LocalParentId"];
        if (info.email_id != nil)
        {
            arrParentMailId = [info.email_id componentsSeparatedByString:@","];
            [dict setValue:arrParentMailId forKey:@"parentEmails"];
            
        }
        [arr addObject:dict];
        [dict release];
    }
    [dateFor release];
    return [arr autorelease];
    
}
-(NSArray *)getdeletedMessage
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox"];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isDel==%@",[NSNumber numberWithBool:YES]];
    [request setPredicate:pred];
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    if (!error && [results count] > 0)
    {
        
	}
    
    for (Inbox *info in results)
    {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:info.message_id forKey:@"message_id"];
        [arr addObject:dict];
        [dict release];
    }
    return arr;
    
}

-(NSArray *)getdeletedMessageUsers
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox_Users"];
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"isDel==%@",[NSNumber numberWithBool:YES]];
    [request setPredicate:pred];
    
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    if (!error && [results count] > 0)
    {
        
	}
    
    for (Inbox_Users *info in results)
    {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        // [dict setValue:info.iD forKey:@"message_id"];
        [arr addObject:info.iD];
        [dict release];
    }
    return arr;
    
}
-(void)setStatusOfMail:(NSArray *)responseArray :(NSArray *)deletedMessage
{
    for (NSDictionary *dictionary in responseArray)
    {
        Inbox *message;
        
        if(![[dictionary objectForKey:@"AppId"] isKindOfClass:[NSNull class]])
        {
            message=[self getInboxDataInfoStoredForAppId:[NSNumber numberWithInteger:[[dictionary objectForKey:@"AppId"] integerValue] ] :[dictionary objectForKey:@"IpAddress"] postNotification:NO];
        }
        else
        {
            message=[self getInboxDataInfoStored:[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"] integerValue] ] postNotification:NO];
        }
        
        if (!message)
        {
#if DEBUG
            NSLog(@"Inbox message setStatusOfMail");
#endif

            message = [NSEntityDescription
                       insertNewObjectForEntityForName:@"Inbox"
                       inManagedObjectContext:_managedObjectContext];
        }
        
        
        if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]])
        {
            message.message_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
        }
        if (![[dictionary objectForKey:@"IpAddress"] isKindOfClass:[NSNull class]]) {
            message.ipAdd=[dictionary objectForKey:@"IpAddress"];
        }
        if (![[dictionary objectForKey:@"ParentId"] isKindOfClass:[NSNull class]])
        {
            message.parent_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"ParentId"]integerValue]];
        }
        if (![[dictionary objectForKey:@"UserId"] isKindOfClass:[NSNull class]]) {
            message.sender_id=[dictionary objectForKey:@"UserId"];
        }
        if (![[dictionary objectForKey:@"SenderName"] isKindOfClass:[NSNull class]]) {
            message.senderName=[dictionary objectForKey:@"SenderName"];
        }
        if (![[dictionary objectForKey:@"Subject"] isKindOfClass:[NSNull class]]) {
            message.subject=[dictionary objectForKey:@"Subject"];
        }
        if (![[dictionary objectForKey:@"Description"] isKindOfClass:[NSNull class]]) {
            message.discrptin=[dictionary objectForKey:@"Description"];
        }
        if (![[dictionary objectForKey:@"Created"] isKindOfClass:[NSNull class]])
        {
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
            [formatter setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];

#if DEBUG
            NSLog(@"[dictionary objectForKey:Created] %@",[dictionary objectForKey:@"Created"]);
#endif
      
            formatter.timeZone=[NSTimeZone systemTimeZone];
            
            // 2316

            
            [formatter setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
            NSString *createdStr = [dictionary objectForKey:@"Created"];
#if DEBUG
            NSLog(@"createdStr : %@",createdStr);
#endif
      
            
            NSString *createdDateStr = [[createdStr componentsSeparatedByString:@" "] objectAtIndex:0];
            
#if DEBUG
            NSLog(@"updatedDateStr ; %@",createdDateStr);
#endif
      
            
            NSDate *reminderDate = [formatter dateFromString:createdDateStr];
            unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
            NSDateComponents *dateComponents = [[NSCalendar currentCalendar] components:unitFlagsDate fromDate:reminderDate];
            
#if DEBUG
            NSLog(@"before dateComponents  :%@",dateComponents);
#endif

            NSArray *createdTime=nil;
            if (![[[createdStr componentsSeparatedByString:@" "] objectAtIndex:1] isKindOfClass:[NSNull class]]) {
                createdTime= [[[createdStr componentsSeparatedByString:@" "] objectAtIndex:1] componentsSeparatedByString:@":"];
                
            }
            if([createdTime count]>0)
            {
                [dateComponents setHour:[[createdTime objectAtIndex:0] integerValue]];
                [dateComponents setMinute:[[createdTime objectAtIndex:1] integerValue]];
                
                [dateComponents setSecond:[[createdTime objectAtIndex:2] integerValue]];
            }
            
#if DEBUG
            NSLog(@"after dateComponents  :%@",dateComponents);
#endif
      
            
            NSDate *gmtDate = [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
            
#if DEBUG
            NSLog(@"gmtDate : %@",gmtDate);
#endif
      

            //2316
            
            NSTimeInterval timeZoneOffset = [[NSTimeZone systemTimeZone] secondsFromGMTForDate:gmtDate];
            NSDate *gmtDate1=[gmtDate dateByAddingTimeInterval:timeZoneOffset];

            message.created_time=gmtDate1;
            [formatter release];
            
        }
        
        if (![[dictionary objectForKey:@"AppId"] isKindOfClass:[NSNull class]])
        {
            message.appId=[NSNumber numberWithInteger:[[dictionary objectForKey:@"AppId"] integerValue] ];
            if (![[dictionary objectForKey:@"ParentId"] isKindOfClass:[NSNull class]])
                message.local_parent_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"ParentId"] integerValue]];
            else{
                message.local_parent_id=[NSNumber numberWithInteger:0];
            }
            
        }
        else
        {
            message.appId=[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"]integerValue]];
            
            message.local_parent_id=[NSNumber numberWithInteger:[[dictionary objectForKey:@"ParentId"] integerValue]];
        }
        message.isSync=[NSNumber numberWithBool:YES];
        message.isDel=[NSNumber numberWithBool:NO];
        
        message.inbox_user=[AppHelper userDefaultsForKey:@"user_id"];
        NSError *error;
        if (![_managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
      
            
        }else{
            if (![[dictionary objectForKey:@"message_users"] isKindOfClass:[NSNull class]]) {
            NSArray *arrmessageUsers = [dictionary objectForKey:@"message_users"];
            NSMutableArray *arrReciverId = [[NSMutableArray alloc]init];
            NSMutableArray *arrReciverName = [[NSMutableArray alloc]init];
            NSString *receiverIds = @"";
            NSString *receiverNames = @"";
            // Store Entry in Message_Users table
                //ReceiverName
            for (NSDictionary *dict in arrmessageUsers) {
                    [self parseInboxUserWithDictionary:dict];
                    [arrReciverId addObject:[dict objectForKey:@"ReceiverId"]];
                    [arrReciverName addObject:[dict objectForKey:@"ReceiverName"]];
                }
                receiverIds=[arrReciverId componentsJoinedByString:@","];
                receiverNames=[arrReciverName componentsJoinedByString:@","];
                arrReciverId = nil ;
                arrReciverName = nil ;
                message.reciver_id = receiverIds ;
                message.reciverName = receiverNames ;
                if (![_managedObjectContext save:&error])
                {
#if DEBUG
                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
                }
            }
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Inbox Message Save" object:message userInfo:nil];
    }
    
    for (NSDictionary *dictionary in deletedMessage)
    {
        [self deleteObjectOfInBoxMessge:[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"] integerValue] ]];
    }
    
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Mail synch complete" object:nil];
}


- (void)netManagerDidFinishSynchronizeMailItem:(NetManager *)thisNetManager
                                      response:(NSMutableData *)responseData
{
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    if([[dictionary objectForKey:@"ErrorCode"]integerValue]==1)
    {
        TableUpdateTime *tabl=[[Service sharedInstance] getlastUpdateTableDataInfoStored];
        
        tabl.inbox=[NSNumber numberWithDouble:[[dictionary valueForKey:@"LastSynchTime"]doubleValue]];
        [self setStatusOfMail:[dictionary objectForKey:@"details"] :[dictionary objectForKey:@"deleted"] ];
    }
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

- (Inbox *)getInboxDataInfoStoredForAppId:(NSNumber *)app_id :(NSString *)ipAddaress postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appId == %@ AND ipAdd == %@", app_id,ipAddaress];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Inbox  * schoolData  = nil;
	if (!error && [results count] > 0) {
		
		schoolData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return schoolData;
    
}
- (Inbox *)getInboxDataInfoStoredForAppId:(NSNumber *)app_id postNotification:(BOOL)postNotification
{
#if DEBUG
    NSLog(@"Inbox getInboxDataInfoStoredForAppId");
#endif
    
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"appId == %@", app_id];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Inbox  * schoolData  = nil;
	if (!error && [results count] > 0) {
		
		schoolData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }

    return schoolData;
    
}

- (void)messageDeletedWithAppId:(NSNumber *)appId ipAddress:(NSString *)ipAdd
{
    
    Inbox *schoolData=[self getInboxDataInfoStoredForAppId:appId :ipAdd postNotification:NO];
    
    schoolData.isDel=[NSNumber numberWithBool:YES];
    
    NSError *error;
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
    }
    
    [self deleteObjectOfInboxUsers:schoolData.message_id];
    
}
- (void)messageDeletedWithAppId:(NSNumber *)msgId
{
    
    Inbox *schoolData=[self getInboxDataInfoStored:msgId postNotification:NO];
    
    schoolData.isDel=[NSNumber numberWithBool:YES];
    
    NSError *error;
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
      
        
    }
    [self deleteObjectOfInboxUsers:msgId];
}
- (void)deleteObjectOfInBoxMessgefromAppId:(NSNumber *)appId
{
    NSError *error;
    NSManagedObject *eventToDelete = [self getInboxDataInfoStoredForAppId:appId postNotification:NO];
    Inbox *inbox = (Inbox *)eventToDelete ;
    if(eventToDelete)
    {
        [_managedObjectContext deleteObject:eventToDelete];
        
        if (![_managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Error deleting  - error:%@",error);
#endif
      
        }
    }
    [self deleteObjectOfInboxUsers:inbox.message_id];
}

- (void)deleteObjectOfInBoxMessgefromAppId:(NSNumber *)appId ipAddress:(NSString *)ipAdd
{
    NSError *error;
    NSManagedObject *eventToDelete = [self getInboxDataInfoStoredForAppId:appId :ipAdd postNotification:NO];
    Inbox *inbox = (Inbox *)eventToDelete ;
    if(eventToDelete)
    {
        [_managedObjectContext deleteObject:eventToDelete];
        
        if (![_managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Error deleting  - error:%@",error);
#endif
        }
    }
    [self deleteObjectOfInboxUsers:inbox.message_id];
}

//Ishan//////////////////////
//31 july
#pragma mark
#pragma mark MyClass Service Call
-(void)getSearchClasses:(NSMutableDictionary*)dict
{
    
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_SearchClasses ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
        [parameters setObject:[dict objectForKey:@"code"]forKey:@"code"];
        [parameters setObject:[dict objectForKey:@"SchoolId"]forKey:@"SchoolId"];
        
        // Webservice Logging new both
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetgetSearchClasses:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
	}
}

- (void)netManagerDidFinishGetgetSearchClasses:(NetManager *)thisNetManager
                                      response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_Search_Classes object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}


-(void)getAssociatesClasses:(NSMutableDictionary*)dict
{
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
	if (networkReachable)
    {
        NSString *serviceName = Method_AssociateClass ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        //ClassId ,user_id,user_type and created
        
        
        [parameters setObject:[dict objectForKey:@"ClassId"]forKey:@"ClassId"];
        [parameters setObject:[dict objectForKey:@"user_id"]forKey:@"user_id"];
        [parameters setObject:[dict objectForKey:@"user_type"]forKey:@"user_type"];
        [parameters setObject:[dict objectForKey:@"created"]forKey:@"created"];
        
        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishGetAssociatesClasses:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
		[netManager startRequest];
		[parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
	}
}

- (void)netManagerDidFinishGetAssociatesClasses:(NetManager *)thisNetManager
                                       response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_Associate_Class object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}






#pragma mrk
#pragma mark For New attachments
//14AugChange
- (News_Attachment*)getAttachmentstDataInfoStoredAttachments:(NSString*)attachmentId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"News_Attachment"];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"atachment_id == %@",attachmentId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    News_Attachment  * attachmentData  = nil;
	if (!error && [results count] > 0) {
		
		attachmentData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return attachmentData;
    
}


#pragma mark
#pragma mark EventUsers for asssigned students
// Aman added
-(void)parseAssignedStudentDataForEventUserWithDictionary:(NSDictionary*)dictionary
{
    
    Events_Users *assStudentData = [self getAssigneStudentForEventUsersDataInfoStored:[dictionary objectForKey:@"UserId"] eventId:[dictionary objectForKey:@"itemId"] postNotification:NO];
    
    if (!assStudentData) {
        assStudentData = [NSEntityDescription
                          insertNewObjectForEntityForName:@"Events_Users"
                          inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"itemId"] isKindOfClass:[NSNull class]]) {
        assStudentData.eventId=[dictionary objectForKey:@"itemId"];
    }
    
    if (![[dictionary objectForKey:@"UserId"] isKindOfClass:[NSNull class]]) {
        assStudentData.userId=[dictionary objectForKey:@"UserId"];
    }
    
    if (![[dictionary objectForKey:@"Isdeleted"] isKindOfClass:[NSNull class]]) {
        assStudentData.status=[dictionary objectForKey:@"Isdeleted"];
    }
    
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}
}



- (Events_Users *)getAssigneStudentForEventUsersDataInfoStored:(NSString*)assStudentId eventId:(NSString*)eventID postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Events_Users"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId == %@ AND eventId==%@", assStudentId,eventID];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Events_Users  * userData  = nil;
	if (!error && [results count] > 0) {
		
		userData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return userData;
    
}

- (NSArray *)getEventUserDataInfoStored:(NSString*)eventId postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Events_Users"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"eventId == %@", eventId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
   // Events_Users  * addEventItemData  = nil;
//	if (!error && [results count] > 0) {
//		
//		addEventItemData = [results objectAtIndex:0];
//    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return results;
    
}

- (void) deleteObjectOfEventUsersItem:(NSNumber*)Item_id
{
}

#pragma mark -
#pragma mark Inbox_Users

- (Inbox_Users *)getInboxUsersDataInfoStoredForAppId:(NSNumber *)app_id postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox_Users"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"localMessageId == %@", app_id];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Inbox_Users  * inbox_user  = nil;
	if (!error && [results count] > 0) {
		
		inbox_user = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return inbox_user;
    
}


- (NSArray *)getInboxUserDataInfoStoredForDelete:(NSNumber*)messageId postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox_Users"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageId == %@", messageId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];

    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return results;
    
}

- (Inbox_Users *)getInboxUserDataInfoStored:(NSString*)userId messageId:(NSString*)messageId postNotification:(BOOL)postNotification
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Inbox_Users"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId == %@ AND messageId==%@", userId,messageId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Inbox_Users  * userData  = nil;
	if (!error && [results count] > 0) {
		
		userData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return userData;
    
}


- (void) deleteObjectOfInboxUsers:(NSNumber*)messageId  {
    
    NSArray *arrUserToDelete = [self getInboxUserDataInfoStoredForDelete:messageId postNotification:NO];
    for (Inbox_Users *u in arrUserToDelete) {
        u.isDel = [NSNumber numberWithBool:YES];
        NSError *error;
        if (![_managedObjectContext save:&error])
        {
#if DEBUG
                NSLog(@"Error deleting  - error:%@",error);
#endif
        }
        
    }
}



-(void)parseInboxUserWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"Inbox parseInboxUserWithDictionary");
#endif
    
    Inbox_Users *inboxUser = [self getInboxUserDataInfoStored:[dictionary objectForKey:@"ReceiverId"]
                                                    messageId:[dictionary objectForKey:@"message_id"]
                                             postNotification:NO];
    
    if (!inboxUser) {
        inboxUser = [NSEntityDescription
                     insertNewObjectForEntityForName:@"Inbox_Users"
                     inManagedObjectContext:_managedObjectContext];
    }
    
    if (![[dictionary objectForKey:@"id"] isKindOfClass:[NSNull class]]) {
        inboxUser.iD = [NSNumber numberWithInteger:[[dictionary objectForKey:@"id"]integerValue]];
    }
    
    if (![[dictionary objectForKey:@"ReceiverId"] isKindOfClass:[NSNull class]]) {
        inboxUser.userId = [dictionary objectForKey:@"ReceiverId"];
    }
    
    if (![[dictionary objectForKey:@"message_id"] isKindOfClass:[NSNull class]]) {
        inboxUser.messageId = [NSNumber numberWithInteger:[[dictionary objectForKey:@"message_id"]integerValue]];
    }
    inboxUser.isDel = [NSNumber numberWithBool:NO];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
	}
}


#pragma mrk
#pragma mark For Dairy Item New attachments
//14AugChange

- (Attachment*)getDiaryAttachmentstDataInfoStoredAttachments:(NSString*)attachmentId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Attachment"];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"aID == %@",attachmentId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Attachment  * attachmentData  = nil;
	if (!error && [results count] > 0) {
		
		attachmentData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return attachmentData;
    
}

- (NSArray *) getAttachmentsForItemId:(NSNumber *)itemId withType:(NSString *)DItype {
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Attachment"];
	
    NSPredicate *predicate = nil;
    predicate =[NSPredicate predicateWithFormat:@"diaryItemId ==%@ AND isDeletedOnLocal == 0 AND itemtype =%@",itemId,DItype];
	[request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = nil;
    results =[_managedObjectContext executeFetchRequest:request error:&error];
    
    if([results count]==0)
    {
        predicate =[NSPredicate predicateWithFormat:@"diaryItemId ==%@ AND isDeletedOnLocal == 0",itemId];
        [request setPredicate:predicate];
        results =[_managedObjectContext executeFetchRequest:request error:&error];

    }

    
    NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Attachment *a1 in results) {
            copy = YES;
            for (Attachment *a2 in filterResults) {
                
                // Attachment offline
                
                if([a1.aID isEqualToNumber:[NSNumber numberWithInteger:0]])
                {
                    if ([a1.localAttachmentId integerValue]== [a2.localAttachmentId integerValue] && [a1.diaryItemId integerValue]== [a2.diaryItemId integerValue]) {
                        // Attachment offline
                        
                        copy = NO;
                        break;
                    }
                }
                else
                {
                    if ([a1.aID integerValue]== [a2.aID integerValue] && [a1.diaryItemId integerValue]== [a2.diaryItemId integerValue]){
                        copy = NO;
                        break;
                    }
                }
                // Attachment offline

            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }

    
    results = (NSArray*)filterResults;
    
    
    
    return results ;
}

- (NSArray *) getNoSyncAttachmentsForItemId:(NSNumber *)itemId {
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Attachment"];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"diaryItemId == %@ AND isDeletedOnLocal == 0 AND isSync == 0",itemId];
	[request setPredicate:predicate];
    
    NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
   /* NSMutableArray* filterResults = [[NSMutableArray alloc] init];
    BOOL copy;
    
    if (![results count] == 0) {
        for (Attachment *a1 in results) {
            copy = YES;
            for (Attachment *a2 in filterResults) {
                
                if ([a1.aID integerValue]== [a2.aID integerValue] && [a1.diaryItemId integerValue]== [a2.diaryItemId integerValue]) {
                    copy = NO;
                    break;
                }
            }
            if (copy) {
                [filterResults addObject:a1];
            }
        }
    }
    
    
    results = filterResults;*/
    
    return results ;
}


- (void) updateIsDeletedFlagForAttachment :(Attachment *)attachment {
    
    attachment.isDeletedOnLocal = [NSNumber numberWithBool:YES];
    attachment.isSync = [NSNumber numberWithBool:NO];

    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}
    
}

- (void) updateIsReadFlagForEventItem :(EventItem *)eventItem
{
    eventItem.isRead = [NSNumber numberWithBool:YES];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}

}

- (void) updateIsReadFlagForDiaryItem :(Item *)item
{
    item.isRead = [NSNumber numberWithBool:YES];
    NSError *error;
    if (![_managedObjectContext save:&error])
	{
#if DEBUG
    	NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
	}

}

- (void) insertAtatchmentLocally :(NSDictionary *)dict {
    
    
    Attachment *attachmenData=nil;
    if (!attachmenData) {
        attachmenData = [NSEntityDescription
                    insertNewObjectForEntityForName:@"Attachment"
                    inManagedObjectContext:_managedObjectContext];
    }
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init] ;
    [formatter1 setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];
    
    attachmenData.localAttachmentId =[[Service sharedInstance] nextAvailble:@"localAttachmentId" forEntityName:@"Attachment"];
    
    
    if (![[dict objectForKey:@"dateAttached"] isKindOfClass:[NSNull class]]) {
        attachmenData.dateAttached = [dict objectForKey:@"dateAttached"];
    }
    
    if (![[dict objectForKey:@"lasyupdatedDate"] isKindOfClass:[NSNull class]]) {
        attachmenData.lastupdatedDate = [formatter1 dateFromString:[dict objectForKey:@"lasyupdatedDate"]];
    }
    
    if (![[dict objectForKey:@"displayName"] isKindOfClass:[NSNull class]]) {
        attachmenData.displayFileName = [dict objectForKey:@"displayName"];
    }
    
    if (![[dict objectForKey:@"fileType"] isKindOfClass:[NSNull class]]) {
        attachmenData.type = [dict objectForKey:@"fileType"];
    }
    
    if (![[dict objectForKey:@"localFilePath"] isKindOfClass:[NSNull class]]) {
        attachmenData.filePath = [dict objectForKey:@"localFilePath"];
    }
    
    if (![[dict objectForKey:@"remoteFilePath"] isKindOfClass:[NSNull class]]) {
        attachmenData.remoteFilePath = [dict objectForKey:@"remoteFilePath"];
    }
    
    if (![[dict objectForKey:@"dairyItemId"] isKindOfClass:[NSNull class]]) {
        attachmenData.diaryItemId = [NSNumber numberWithInteger:[[dict objectForKey:@"diaryItemId"]integerValue]];
        
    }
    if(![[dict objectForKey:@"itemType"] isKindOfClass:[NSNull class]])
    {
        attachmenData.itemtype =[dict objectForKey:@"itemType"];
    }
    
    if ([dict objectForKey:@"isSync"]) {
        attachmenData.isSync=[NSNumber numberWithBool:[[dict objectForKey:@"isSync"] boolValue]];
    }
    else{
        attachmenData.isSync=[NSNumber numberWithBool:YES];
    }
    
    if ([dict objectForKey:@"isDeletedOnLocal"]) {
        attachmenData.isDeletedOnLocal=[NSNumber numberWithBool:[[dict objectForKey:@"isDeletedOnLocal"] boolValue]];
    }
    else{
        attachmenData.isDeletedOnLocal=[NSNumber numberWithBool:YES];
    }
    
    
    NSError *error;
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
      
        
    }
    [formatter1 release];
}

- (void) parseAttachmentData :(NSDictionary *)jsonDictD
{
    NSArray *arrStatus = [jsonDictD objectForKey:@"status"];
    for (NSDictionary *dict in arrStatus)
    {
        if (![[dict objectForKey:@"LocalAttachmentId"] isKindOfClass:[NSNull class]])
        {
            NSString *localKey = [dict objectForKey:@"LocalAttachmentId"];
            NSArray * arr = [localKey componentsSeparatedByString:@"_"];
            NSString *localAttachmentId = [arr objectAtIndex:0];
            NSString *diaryItemId = [arr objectAtIndex:1];
            Attachment *attachmentData = [self getDiaryAttachmentstDataInfoStoredAttachments:localAttachmentId andDiaryItemId:diaryItemId];
            if (!attachmentData)
            {
                attachmentData = [NSEntityDescription
                                  insertNewObjectForEntityForName:@"Attachment"
                                  inManagedObjectContext:_managedObjectContext];
            }
            
            if (![[dict objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
                attachmentData.aID = [NSNumber numberWithInteger:[[dict objectForKey:@"Id"]integerValue]];
            }
            
            if (![[dict objectForKey:@"FilePath"] isKindOfClass:[NSNull class]]) {
                attachmentData.remoteFilePath = [dict objectForKey:@"FilePath"];
            }
            
            if (![[dict objectForKey:@"itemType"] isKindOfClass:[NSNull class]]) {
                attachmentData.itemtype = [dict objectForKey:@"itemType"];
            }
 
            
            if (![[dict objectForKey:@"FileType"] isKindOfClass:[NSNull class]]) {
                attachmentData.type = [dict objectForKey:@"FileType"];
            }
            
            if (![[dict objectForKey:@"DisplayName"] isKindOfClass:[NSNull class]]) {
                attachmentData.displayFileName = [dict objectForKey:@"DisplayName"];
            }
            
            if (![[dict objectForKey:@"CreatedBy"] isKindOfClass:[NSNull class]]) {
                attachmentData.createdBy = [dict objectForKey:@"CreatedBy"];
            }
            
            if (![[dict objectForKey:@"size"] isKindOfClass:[NSNull class]]) {
                attachmentData.size = [NSNumber numberWithInteger:[[dict objectForKey:@"size"]integerValue]];
            }
            
            if (![[dict objectForKey:@"Status"] isKindOfClass:[NSNull class]]) {
                NSString *str = [[dict objectForKey:@"Status"] lowercaseString];
                if ([str isEqualToString:@"success"]) {
                    attachmentData.isSync = [NSNumber numberWithBool:YES];
                }else{
                    attachmentData.isSync = [NSNumber numberWithBool:NO];
                }
            }
            
            attachmentData.isDeletedOnLocal = [NSNumber numberWithBool:NO];
            
            NSError *error;
            if (![_managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
                
            }
        }
    }
    [[DELEGATE glb_mdict_for_Attachement] removeAllObjects];
}


- (Attachment*)getDiaryAttachmentstDataInfoStoredAttachments:(NSString*)localAttachmentId andDiaryItemId:(NSString *)diaryItemId
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Attachment"];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"diaryItemId == %@ AND localAttachmentId == %@",diaryItemId,localAttachmentId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    Attachment  * attachmentData  = nil;
	if (!error && [results count] > 0) {
		
		attachmentData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return attachmentData;
    
}

- (void) deleteAttachmentForDiaryItemId :(NSNumber *)diaryItemId withType:(NSString*)DItype {
    
    NSError *error;
    NSArray *arrToDeletAttachment = [self getAttachmentsForItemId:diaryItemId withType:DItype];
    
    for (NSManagedObject *a in arrToDeletAttachment) {
        [_managedObjectContext deleteObject:a];
    }
    
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif
      
    }
    
}
// Attachment offline

-(void)updateAttachement:(NSNumber *)diaryItemId universalid:(NSNumber *)universalID withType:(NSString*)DItype
{
    
    NSError *error;
    NSArray *arrToDeletAttachment = nil;
    arrToDeletAttachment = [self getAttachmentsForItemId:diaryItemId withType:DItype];
    
    if([arrToDeletAttachment count] ==0)
    {
        arrToDeletAttachment = [self getAttachmentsForItemId:universalID withType:DItype];
        
        for (Attachment *a in arrToDeletAttachment) {
            
            if(![a.diaryItemId isEqualToNumber:[NSNumber numberWithInteger:0]])
            {
            a.diaryItemId = diaryItemId;
            if (![_managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
            }
            }
        }
    }
    else
    {
        for (Attachment *a in arrToDeletAttachment) {
            
            a.diaryItemId = diaryItemId;
            if (![_managedObjectContext save:&error])
            {
#if DEBUG
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
            }
        }
    }
    
}

-(NSArray*)getStoredDiaryItemIDForAttachementWithNosync
{
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Attachment" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSync == 0 AND isDeletedOnLocal == 0"];
    [request setPredicate:predicate];
    
    request.entity = entity;
    request.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"diaryItemId"]];
    request.returnsDistinctResults = YES;
    request.resultType = NSDictionaryResultType;
    
    
    NSError *error = nil;
    NSArray *distincResults = [_managedObjectContext executeFetchRequest:request error:&error];
    // Use the results
    [request release];
    
#if DEBUG
    NSLog(@"getStoredAttachementWithNosync:%@",distincResults);
#endif
    
    return distincResults;
    
    
    
}

-(NSArray*)getStoredDiaryItemIDForAttachementWithNosyncAndIsDeleteOne
{
    
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Attachment" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isSync == 0 AND isDeletedOnLocal == 1 AND aID>%@",[NSNumber numberWithInteger:0]];
    [request setPredicate:predicate];
    
    request.entity = entity;
    request.propertiesToFetch = [NSArray arrayWithObject:[[entity propertiesByName] objectForKey:@"aID"]];
    request.returnsDistinctResults = YES;
    request.resultType = NSDictionaryResultType;
    
    
    NSError *error = nil;
    NSArray *distincResults = [_managedObjectContext executeFetchRequest:request error:&error];
    // Use the results
    [request release];
    
#if DEBUG
    NSLog(@"getStoredAttachementWithNosync:%@",distincResults);
#endif
    
    return distincResults;
    
    
    
}

// Attachment offline

- (void) deleteDiaryItemUserForDiaryItemId :(NSNumber *)diaryItemId {
    
    NSError *error;
    NSArray *arrToDeletAttachment = [self getStoredAllAssignedItemDataWithItemId:diaryItemId];
    
    for (NSManagedObject *a in arrToDeletAttachment) {
        [_managedObjectContext deleteObject:a];
    }
    
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif

    }
    
}


- (DairyItemUsers*)getDiaryItemUserForDiaryItemId :(NSNumber*)itemID
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DairyItemUsers"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_id==%@",itemID];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    DairyItemUsers  * studentData  = nil;
    if (!error && [results count] > 0) {
        
        studentData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return studentData;
    
}




- (void) deleteAttachmentForId :(NSString *)attachmentId {
    
    NSError *error;
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Attachment"];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"aID == %d",[attachmentId integerValue]];
	[request setPredicate:predicate];
    
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if([results count]>0)
    {
        [_managedObjectContext deleteObject:[results objectAtIndex:0]];
        
        if (![_managedObjectContext save:&error]) {
            
#if DEBUG
            NSLog(@"Error deleting  - error:%@",error);
#endif
      
        }
    }
    
}


#pragma mark -
#pragma mark return EducationContent from Bundle  -

//----- Added By Mitul on 23-Dec Content Logic -----//

- (NSString *)getContentType
{
    NSString *contentType;
    
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    if ([schoolDetailObj.include_Impower isEqualToString:@"0"])
    {
        contentType = @"CatholicPullbar";
    }
    // 2 means Do not show content Panel
    else if ([schoolDetailObj.include_Impower isEqualToString:@"2"])
    {
        contentType = @"None";
    }
    // else 1 means Empower Content Jr/Sr get from Empower_Type Field
    else
    {
        // Senior Empower Content
        if ([schoolDetailObj.empower_type isEqualToString:@"senior"])
        {
            contentType = @"EmpowerPullbarSr";
            
        }
        // Elementry Empower Content
        
        else if ([[schoolDetailObj.empower_type lowercaseString] isEqualToString:@"elementry"] ||
                 [[schoolDetailObj.empower_type lowercaseString] isEqualToString:@"elementary"] )
        {
            contentType = @"EmpowerPullbarElem";
            
        }
        else // Junior Empower Content
        {
            contentType = @"EmpowerPullbarJr";
            
        }
    }
    
    return contentType;
}
/*
- (NSString *)getContentType
{
    NSString *contentType;
    
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    if ([schoolDetailObj.include_Impower isEqualToString:@"0"])
    {
        contentType = @"CatholicPullbar";
    }
    // 2 means Do not show content Panel
    else if ([schoolDetailObj.include_Impower isEqualToString:@"2"])
    {
        contentType = @"None";
    }
    // else 1 means Empower Content Jr/Sr get from Empower_Type Field
    else
    {
        // Senior Empower Content
        if ([schoolDetailObj.empower_type isEqualToString:@"senior"])
        {
            contentType = @"EmpowerPullbarSr";
            
        }
        
        else // Junior Empower Content
        {
            contentType = @"EmpowerPullbarJr";
            
        }
    }
    
    return contentType;
}*/

- (NSString *)getContentTypeForEducationContent
{
    NSString *contentType;
    
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];
    
    
    // else 1 means Empower Content Jr/Sr get from Empower_Type Field
    
    // Senior Empower Content
    if ([schoolDetailObj.empower_type isEqualToString:@"senior"])
    {
        contentType = @"EducationalContentSr";
        
    }
    // Elementry Empower Content
    else if ([[schoolDetailObj.empower_type lowercaseString] isEqualToString:@"elementry"]||
             [[schoolDetailObj.empower_type lowercaseString] isEqualToString:@"elementary"])
    {
        contentType = @"EducationalContentElem";
        
    }
    else // Junior Empower Content
    {
        contentType = @"EducationalContentJr";
        
    }
    
    return contentType;
}

-(NSMutableArray *)arrayOfEducationContent:(NSMutableArray*)educationBundledata
{
    
    NSMutableArray *finalResults = [[[NSMutableArray alloc] init] autorelease];
    int inIndex =0;
    for(NSString *strFile in educationBundledata)
    {
        SchoolInformation *about = [[[SchoolInformation alloc] init] autorelease];
        // Get Html File Content
        NSError* error = nil;
        NSArray *arrFileName = [[educationBundledata objectAtIndex:inIndex] componentsSeparatedByString:@"."];
        
//        NSString *strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];
        // Get the path of Bundle To fetch HTML files array
        NSString *EducationContentFolder = [[Service sharedInstance] getContentTypeForEducationContent];
        NSString *strBundlePath;
        // For Daisy
        if ([[AppHelper userDefaultsForKey:kKeyDefRegion] isEqualToString:kKeyRegionAUS])
        {
            strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content/EducationalContent"];
        }
        // For Prime
        // For Teacher And Student Same content
        else
        {
            strBundlePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Content"];
            strBundlePath = [strBundlePath stringByAppendingPathComponent:EducationContentFolder];
        }

        NSString *finalPath = [strBundlePath stringByAppendingPathComponent:strFile];
        
        //        NSString *path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@/%@",strBundlePath,[arrFileName objectAtIndex:0]] ofType: @"html" inDirectory:nil];
        //
        
        NSString *res = [NSString stringWithContentsOfFile:finalPath encoding:NSUTF8StringEncoding error: &error];
        
        about.content = res;
        about.content_type = @"Empower";
        about.title = [arrFileName objectAtIndex:0];
        
        [finalResults addObject:about];
        
        inIndex ++;
    }
    
    return finalResults;
}
//--------- By Mitul ---------//


#pragma mark
#pragma mark Delete Students

- (NSArray *) getStudentsForClassId:(NSString *)classId {
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Student"];
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_id == %@",classId];
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"firstname" ascending:YES];
    [request setSortDescriptors:@[sd]];
	[request setPredicate:predicate];
    
    NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    return results ;
}


- (void) deleteStudentForClassID :(NSString *)classID {
    
    NSError *error;
    //  NSFetchRequest *request = [self getBasicRequestForEntityName:@"Student"];
    
    NSArray *objects = [self getStudentsForClassId:classID];
    if (objects == nil) {
        // handle error
    } else {
        for (Student *object in objects) {
            [_managedObjectContext deleteObject:object];
        }
        [_managedObjectContext save:&error];
    }
}

- (void) deleteStudent {
    
    NSError *error;
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"Student"];
    
    NSArray *objects = [_managedObjectContext executeFetchRequest:request error:&error];
    if (objects == nil) {
        // handle error
    } else {
        for (NSManagedObject *object in objects) {
            [_managedObjectContext deleteObject:object];
        }
        [_managedObjectContext save:&error];
    }
}

#pragma Delete Attachment From Attachment Table
- (void) deleteObjectOfAttachment:(NSString*)attachmentId  {
    
    
    NSError *error;
    NSManagedObject *eventToDelete = [self getDiaryAttachmentstDataInfoStoredAttachments:attachmentId];
    if (eventToDelete) {
        [_managedObjectContext deleteObject:eventToDelete];
    }
    
    
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif
      
    }
}

/*******************New Methods Added For Out Class********************************/

-(NSString*)getNameForPassOnPassTypeIDBasis:(NSNumber*)formPassID
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"ListofValues"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"listofvaluesTypeid = %@",formPassID ];
	[request setPredicate:predicate];
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    if ([results count]>0)
    {
        ListofValues *list = [results objectAtIndex:0];
        
        return list.type;
    }
    return @"";
    
    
}


-(void)SendRequestForPassApproval:(NSDictionary*)dictionary
{
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate]netManager]networkReachable];
    if (networkReachable)
    {
        
        BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
        if (networkReachable)
        {
            NSString *serviceName = Method_For_PassApproval;
            NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
            urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
            
            // Webservice Logging
            [parameters setObject:APP_Key forKey:kKeyAppType];
            
            [parameters setObject:[dictionary valueForKey:@"Id"] forKey:@"Id"];
            [parameters setObject:[dictionary valueForKey:@"user_id"] forKey:@"user_id"];
            [parameters setObject:[dictionary valueForKey:@"SchoolId"] forKey:@"SchoolId"];
            [parameters setObject:[dictionary valueForKey:@"PassCode"] forKey:@"PassCode"];
            
            self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                        finishLoadingSelector:@"netManagerDidFinishPassApproval:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
            [netManager startRequest];
            [parameters release];
        }
        else
        {
            [[AppDelegate getAppdelegate] hideIndicator];
        }
   
    
}
}

- (void)netManagerDidFinishPassApproval:(NetManager *)thisNetManager
response:(NSMutableData *)responseData
    {
        
        
        NSMutableDictionary *dictionary=nil;
        if (thisNetManager.error)
        {
            [dictionary setObject:netManager.error forKey:@"error"];
            
        }
        else
        {
            NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
            
            decodedString = [StringTransformer transformDecodedString:decodedString];
            
            NSDictionary *jsonDictD = [decodedString JSONValue];
            
            dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_SendPass_For_Approval object:nil userInfo:dictionary];
        
        [dictionary release];
        [thisNetManager release];
        self.netManager = nil;
    }
   
-(void)updateDiaryItemIsApprovedStatus:(NSDictionary*)dictionary
{
    Item *item =[self getItemDataInfoStored:[dictionary objectForKey:@"Id"] postNotification:NO];
    item.isApproved=[NSNumber numberWithInteger:[[dictionary objectForKey:@"PassStatus"]integerValue]];
    NSError *error;
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
    }
    
}

-(void)updateDiaryItemForMeritDescription:(NSNumber*)itemId meritdesription:(NSString*)descriptionmerit
{
    Item *item =[self getItemDataInfoStored:itemId postNotification:NO];
    
    if([[item.type lowercaseString] isEqualToString:@"merits"] || [[item.type lowercaseString] isEqualToString:@"merit"])
    {
    if (![descriptionmerit isKindOfClass:[NSNull class]] || [[descriptionmerit stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]length ]>0)
    {
        item.itemDescription = descriptionmerit;

    }
    
    NSError *error;
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
      
    }
    }
}


-(void)addAssignedUserAfterApproval:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif
    
    for(NSDictionary *dictAss in [dictionary objectForKey:@"assignUserId"])
    {
         DairyItemUsers *assStudentData=[self getAssigneStudentDataInfoStored:[dictAss objectForKey:@"UserId"] itemId:[dictAss objectForKey:@"itemId"] postNotification:NO];
        
        
        if (!assStudentData) {
            assStudentData = [NSEntityDescription
                              insertNewObjectForEntityForName:@"DairyItemUsers"
                              inManagedObjectContext:_managedObjectContext];
        }
        
        if (![[dictAss objectForKey:@"itemId"] isKindOfClass:[NSNull class]]) {
            assStudentData.item_id=[NSNumber numberWithInteger:[[dictAss objectForKey:@"itemId"]integerValue]];
        }
        
        
        if (![[dictAss objectForKey:@"UserId"] isKindOfClass:[NSNull class]]) {
            assStudentData.student_id= [dictAss objectForKey:@"UserId"];
        }
        if (![[dictAss objectForKey:@"CreatedBy"] isKindOfClass:[NSNull class]]) {
            assStudentData.user_id= [dictAss objectForKey:@"CreatedBy"];
        }
        
        /*******************New Methods Added For Out Class********************************/
        
        ///// Here Approver Name Acts in Two ways
        // 1) If Teacher Logins Then ApproverName Hold  those Student Names for which passes has been created by the teacher.
        // 2) If Student Logins Then ApproverName Hold  those teacher Names for which passes has been approved by the teacher.
        
        if (![[dictAss objectForKey:@"ApproverName"] isKindOfClass:[NSNull class]]) {
            assStudentData.approverName= [dictAss objectForKey:@"ApproverName"];
        }
        if (![[dictAss objectForKey:@"StudentName"] isKindOfClass:[NSNull class]]) {
            assStudentData.studentName= [dictAss objectForKey:@"StudentName"];
        }
        
        if (![[dictAss objectForKey:@"passTypeStatus"] isKindOfClass:[NSNull class]]) {
            assStudentData.passTypeStatus =[NSNumber numberWithInteger:[[dictAss objectForKey:@"passTypeStatus"] integerValue]];
        }
        
        if(![[dictAss objectForKey:@"meritAssignedDateAndTime"] isKindOfClass:[NSNull class]])
        {
            NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init] ;
            [formatter1 setDateFormat:kDateFormatDateWithTimeToSaveFetchFromDB];
            assStudentData.meritAssignedDateAndTime=[formatter1 dateFromString:[dictAss objectForKey:@"meritAssignedDateAndTime"]];
            [formatter1 release];
            formatter1 =nil;
        }
        
        /*******************New Methods Added For Out Class********************************/
        
        
        assStudentData.isSynch=[NSNumber numberWithBool:NO];
        assStudentData.isDeletedOnServer=[NSNumber numberWithBool:NO];
        
        
        
        NSError *error;
        if (![_managedObjectContext save:&error])
        {
#if DEBUG
            NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
      
        }
    }
}

-(NSString *)getApprovalNameFromLocalDB:(NSString*)itemID
{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DairyItemUsers"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_id==%@",itemID];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    DairyItemUsers  * studentData  = nil;
	if (!error && [results count] > 0) {
		
		studentData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return  studentData.approverName;
}

-(NSString *)getStudentName:(Item*)diaryitem
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DairyItemUsers"];
	
 	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"item_id==%@",diaryitem.itemId];
	[request setPredicate:predicate];
	
	NSError *error = nil;
	NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    DairyItemUsers  * studentData  = nil;
	if (!error && [results count] > 0) {
		
		studentData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return  [studentData studentName];//[self passstudentName:studentData.student_id];



}

-(void)updateLocalPass:(NSDictionary*)dict
{
    Item *itemDiary = nil;
    
    
    if (![[dict objectForKey:@"AppType"] isEqualToString:APP_Key]) {
        itemDiary=[self getItemDataInfoStored:[dict objectForKey:@"Id"] postNotification:NO];
    }
    /*******************New Methods Added For Out Class********************************/
    
    if (![[dict objectForKey:@"pass_status"] isKindOfClass:[NSNull class]])
    {
        itemDiary.isApproved=[NSNumber numberWithInteger:[[dict objectForKey:@"pass_status"] integerValue]];
    }
    itemDiary.isSync = [NSNumber numberWithBool:FALSE];
    itemDiary.isDelete = [NSNumber numberWithBool:FALSE];
    
    NSError *error;
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
      
        
    }
}

/*******************New Methods Added For Out Class********************************/

-(void)updateTableForCycle:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"Uodate For Table Cycle %@",dictionary);
#endif

        NSDateFormatter *formater=[[NSDateFormatter alloc]init];
    [formater setDateFormat:kDateFormatOnlyDateToSaveFetchInFromDB];

    NSDate *dats = [formater dateFromString:[dictionary objectForKey:@"Date"]];
    [formater setDateFormat:@"dd-MM-yyyy"];
    NSString *dateStr = [formater stringFromDate:dats];
 
    
    TableForCycle *tableForCycle = nil;
    tableForCycle=[self getlastcycleandDayTableDataInfoStored:[formater dateFromString:dateStr]];

    
    if (![[dictionary objectForKey:@"OverrideCycle"] isKindOfClass:[NSNull class]]) {
        tableForCycle.cycle= [dictionary objectForKey:@"OverrideCycle"];
    }
    
    NSError *error;
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
    }
}





#pragma mark -
#pragma mark Campusbased Timetable Methods -

// Campus Based Start

-(MyClass*)getStoredAllClassesTableData:(NSNumber*)table_id cycle:(NSString*)cycleDay
{
#if DEBUG
    NSLog(@"Crash Service Class 1");
#endif

    MyClass *classObj = nil;
    MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
    if([profobj.type isEqualToString:@"Teacher"])
    {
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTable_id == %@ AND cycle_day==%@", table_id,cycleDay];
        [request setPredicate:predicate];

        NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        if([results count]>0)
            classObj = [results objectAtIndex:0];
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }
        return classObj;
        
    }
    else
    {
        NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTable_id==%d AND cycle_day==%d", [table_id integerValue],[cycleDay integerValue]];
        [request setPredicate:predicate];

        NSError *error = nil;
        NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
        if([results count]>0)
            classObj = [results objectAtIndex:0];
        if (error)
        {
#if DEBUG
            NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
        }

        return classObj;
    }
#if DEBUG
    NSLog(@"Crash Service Class 2");
#endif

    
    
}

-(NSString*)getTimeTableCodeOnthebasisofClass:(MyClass*)classselected
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_id==%d",[classselected.class_id integerValue]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass *tt = [results objectAtIndex:0];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return tt.campusCode;
    
    
}

-(NSArray*)getStoredAllClassData:(MyClass*)classe
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_id==%d",[classe.class_id integerValue]];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}
-(NSArray*)getUniqueClassOntimeTableId:(TimeTable *)timeTableObj classes:(MyClass*)classeObj
{
    
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTable_id ==%d && class_id ==%d ",[timeTableObj.timeTable_id integerValue],[classeObj.class_id integerValue]];
    [request setPredicate:predicate];
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}



- (MyClass *)getClassDataFromClassIdAndCycleDay:(NSString*)classId cycleDay:(NSString *)cycleDay {
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"MyClass"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"class_id == %@ AND cycle_day == %@", classId,cycleDay];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    MyClass  * classData  = nil;
    if (!error && [results count] > 0) {
        
        classData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return classData;
    
}

-(NSArray*)getStoredCalendarCycleDayForGivenDate:(NSDate *)date
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
//    NSString *strGivenDate = [formatter stringFromDate:date];
    
    //new Logic added to support differnt timezone by checking date range to avoid time stamp issue

    NSDate *startDate = date;
//    NSDate *endDate = date;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    startDate = [calendar dateFromComponents:components];
    
//    [components setHour:23];
//    [components setMinute:59];
//    [components setSecond:59];
//    endDate = [calendar dateFromComponents:components];

    
    /*NSDate *startDate = date;
    NSTimeInterval length = 60*60*24;
    [[NSCalendar currentCalendar] rangeOfUnit:NSDayCalendarUnit
                                    startDate:&startDate
                                     interval:&length
                                      forDate:startDate];
    
    NSDate *endDate = [startDate dateByAddingTimeInterval:length];
    */
    
    // New logic to compare dates as a string to avoid time stamp of given date and DB date
    // As while changing the time zone from mumbai/aus to UK/london date comparision is not working properly
    // B'coz of time stamp difference in DB and in Given Date
//---------------------------------------------------------------------------------------------------
//    NSFetchRequest *request = [self getBasicRequestForEntityName:@"CalendarCycleDay"];
//    
//    NSError *error = nil;
//    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
//    
//    if (error)
//    {
//        NSLog(@"getAllStoredCalenderCycleDays fetch request error = %@", [error localizedDescription]);
//    }
//    
//    NSMutableArray *marrCalcycleDayObj = [[NSMutableArray alloc] init];
//
//    for (CalendarCycleDay *calCycleDay in results)
//    {
//        NSString *strDateRef = [formatter stringFromDate:calCycleDay.dateRef];
//        if ([strDateRef isEqualToString:strGivenDate])
//        {
//            [marrCalcycleDayObj addObject:calCycleDay];
//            break;
//        }
//    }
//
//    return (NSArray *)marrCalcycleDayObj;
//----------------------------------------------------------------------------------------------------
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"CalendarCycleDay"];
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateRef == %@",date];
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateRef>=%@ AND dateRef<=%@",startDate,endDate];
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(dateRef >= %@) AND (dateRef < %@)", startDate, endDate];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dateRef == %@",startDate];
    
    [request setPredicate:predicate];
    
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    return results;
    
}


-(NSArray*)getStoredCalendarCycleDay:(MyClass*)classeselected
{
    
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"CalendarCycleDay"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cycleDay==%d AND campusId==%d ",[classeselected.cycle_day integerValue],[classeselected.campusId integerValue]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return results;
}

-(TimeTable*)getTimeTableonthebasisOfClassTimeTableId:(MyClass*)classeselected
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TimeTable"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"timeTable_id==%d",[classeselected.timeTable_id integerValue]];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    TimeTable *tt = [results objectAtIndex:0];
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    
    return tt;
}

-(void)getCalendarCycleDays
{
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        
        NSString *serviceName = Method_getcalcycledays ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];

#if DEBUG
        NSLog(@"School ID %@",[AppHelper userDefaultsForKey:@"SchoolId"]);
        NSLog(@"School ID %@",[AppHelper userDefaultsForKey:@"school_Id"]);
#endif
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        // Webservice Logging New
        [parameters setObject:APP_Key forKey:kKeyAppType];
        [parameters setObject:[AppHelper userDefaultsForKey:@"school_Id"] forKey:@"SchoolId"];
    
        [parameters setObject:[AppHelper userDefaultsForKey:@"ipadd"]forKey:@"ipadd"];
        //
        
        TableUpdateTime *tabl=[self getlastUpdateTableDataInfoStored];
        if([[NSUserDefaults standardUserDefaults] objectForKey:vserionCalenderCycleUpdate] == nil)
        {
            [parameters setObject:[NSNumber numberWithInteger:0] forKey:@"LastSync"];
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            
            if (standardUserDefaults) {
                [standardUserDefaults setObject:@"DBUpdated" forKey:vserionCalenderCycleUpdate];
                [standardUserDefaults synchronize];
            }
        }
        else
        {
            if(tabl.calendarCycleUpdate == nil || [self isNumberOfCampusUpdated])
            {
            [self deleteAllObjects:@"CalendarCycleDay"];
            [parameters setObject:[NSNumber numberWithInteger:0]  forKey:@"LastSync"];
            
            }
                else
                {
                    [parameters setObject:tabl.calendarCycleUpdate  forKey:@"LastSync"];
                }
            
        }
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishCalendarCycleDaysDetail:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
        [netManager startRequest];
        [parameters release];
    }
    else
    {
        [[AppDelegate getAppdelegate]hideIndicator];
    }
}
- (void)netManagerDidFinishCalendarCycleDaysDetail:(NetManager *)thisNetManager
                                          response:(NSMutableData *)responseData
{
#if DEBUG
    NSLog(@"netManagerDidFinishCalendarCycleDaysDetail...");
#endif

    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
        
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_CalendarCycleDays object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}

-(CalendarCycleDay *)getCalendarCycleDayDataInfoStoredForId:(NSNumber *)Id
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"CalendarCycleDay"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"calendarCycleDayId == %@",Id];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    results = [[NSSet setWithArray:results] allObjects];
    CalendarCycleDay  * addCalendarCycleData  = nil;
    if (!error && [results count] > 0) {
        
        addCalendarCycleData = [results objectAtIndex:0];
    }
    if (error)
    {
#if DEBUG
        NSLog(@"fetch request error = %@", [error localizedDescription]);
#endif

    }
    
    return addCalendarCycleData;
}


-(void)parseCalendarCycleDayWithDictionary:(NSDictionary*)dictionary
{
#if DEBUG
    NSLog(@"CalendarCycleDay %@",dictionary );
#endif
    
    CalendarCycleDay *calendarCycleDayObj=nil;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init] ;
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    calendarCycleDayObj=[self getCalendarCycleDayDataInfoStoredForId:[dictionary objectForKey:@"Id"]];
    
    
    if (!calendarCycleDayObj) {
        calendarCycleDayObj = [NSEntityDescription
                               insertNewObjectForEntityForName:@"CalendarCycleDay"
                               inManagedObjectContext:_managedObjectContext];
    }
    
    
    if (![[dictionary objectForKey:@"Created"] isKindOfClass:[NSNull class]])
    {
        
        calendarCycleDayObj.created=[self dateFromMilliSecond:[NSNumber numberWithDouble:[[dictionary objectForKey:@"Created"] doubleValue]]];
        
    }
    if (![[dictionary objectForKey:@"CreatedBy"] isKindOfClass:[NSNull class]])
    {
        calendarCycleDayObj.createdBy=[dictionary objectForKey:@"CreatedBy"];
    }
    if (![[dictionary objectForKey:@"Updated"] isKindOfClass:[NSNull class]])
    {
        
        calendarCycleDayObj.updated=[self dateFromMilliSecond:[NSNumber numberWithDouble:[[dictionary objectForKey:@"Updated"] doubleValue]]];
        
    }
    if (![[dictionary objectForKey:@"UpdatedBy"] isKindOfClass:[NSNull class]])
    {
        calendarCycleDayObj.updatedBy=[dictionary objectForKey:@"UpdatedBy"];
    }
    
    
    if (![[dictionary objectForKey:@"CycleDayLabel"] isKindOfClass:[NSNull class]])
    {
        calendarCycleDayObj.cycleDayLabel=[dictionary objectForKey:@"CycleDayLabel"];
    }
    
        //Cycle labels are not displaying in calendar Month and Glance views - start
    if (![[dictionary objectForKey:@"formatedCycleDayLabel"] isKindOfClass:[NSNull class]])
    {
        calendarCycleDayObj.formatedCycleDayLabel=[dictionary objectForKey:@"formatedCycleDayLabel"];
    }
        //Cycle labels are not displaying in calendar Month and Glance views - end
    
    if (![[dictionary objectForKey:@"Date"] isKindOfClass:[NSNull class]]) {
        calendarCycleDayObj.dateRef=[formatter dateFromString:[dictionary objectForKey:@"Date"]];
    }
    
    /*if (![[dictionary objectForKey:@"Date"] isKindOfClass:[NSNull class]])
     {
     calendarCycleDayObj.dateRef=[self dateFromMilliSecond:[NSNumber numberWithDouble:[[dictionary objectForKey:@"Date"] doubleValue]]];
     }*/
    
    if (![[dictionary objectForKey:@"Id"] isKindOfClass:[NSNull class]]) {
        calendarCycleDayObj.calendarCycleDayId =[NSNumber numberWithInteger:[[dictionary objectForKey:@"Id"] integerValue]];
    }
    
    if (![[dictionary objectForKey:@"CampusId"] isKindOfClass:[NSNull class]]) {
        calendarCycleDayObj.campusId =[NSNumber numberWithInteger:[[dictionary objectForKey:@"CampusId"] integerValue]];
    }
    
    if (![[dictionary objectForKey:@"CycleDay"] isKindOfClass:[NSNull class]]) {
        calendarCycleDayObj.cycleDay =[NSNumber numberWithInteger:[[dictionary objectForKey:@"CycleDay"] integerValue]];
    }
    
    calendarCycleDayObj.isDelete=[NSNumber numberWithBool:NO];
    
    
    
    NSError *error;
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
        
    }
    
    
    
}

- (void) deleteObjectCalendarCycleDay:(NSNumber*)CalendarCycleDay_id  {
    
    
    NSError *error;
    NSManagedObject *calendarCycleDayToDelete = [self getCalendarCycleDayDataInfoStoredForId:CalendarCycleDay_id ];
    if (calendarCycleDayToDelete) {
        [_managedObjectContext deleteObject:calendarCycleDayToDelete];
    }
    
    
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Error deleting  - error:%@",error);
#endif
      
    }
}

// Campus Based End

#pragma mark -
#pragma mark Other Methods  - Campus Based Migration
// Campus Based Migration Start
-(void)deleteAllTablesData
{
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif
   

    [self deleteAllObjects:@"Annoncements"];
    [self deleteAllObjects:@"Attachment"];
    [self deleteAllObjects:@"CalendarCycleDay"];
    [self deleteAllObjects:@"ClassesInfo"];
    [self deleteAllObjects:@"Country"];
    [self deleteAllObjects:@"DairyItemUsers"];
    [self deleteAllObjects:@"DayOfNotes"];
    [self deleteAllObjects:@"DiaryItemTypes"];
    [self deleteAllObjects:@"EventItem"];
    [self deleteAllObjects:@"Events_Users"];
    [self deleteAllObjects:@"Grade"];
    [self deleteAllObjects:@"Inbox"];
    [self deleteAllObjects:@"Inbox_Users"];
    [self deleteAllObjects:@"Item"];
    [self deleteAllObjects:@"ListofValues"];
    [self deleteAllObjects:@"MyClass"];
    [self deleteAllObjects:@"MyParent"];
    [self deleteAllObjects:@"MyTask"]; // Not in use
    [self deleteAllObjects:@"MyTeacher"];
    [self deleteAllObjects:@"News"];
    [self deleteAllObjects:@"News_Attachment"];
    [self deleteAllObjects:@"NoteEntry"];
    [self deleteAllObjects:@"Notifications"]; // Not in use
    [self deleteAllObjects:@"Period"];
    [self deleteAllObjects:@"Report"];
    [self deleteAllObjects:@"Room"];
    [self deleteAllObjects:@"School_Images"];
    [self deleteAllObjects:@"School_Information"];
    [self deleteAllObjects:@"States"];
    [self deleteAllObjects:@"Student"];
    [self deleteAllObjects:@"Subjects"];
    [self deleteAllObjects:@"TableForCycle"];
    [self deleteAllObjects:@"TimeTable"];
    [self deleteAllObjects:@"TimeTableCycle"];
    [self updateTableupdateTimeForCampusBasedMigration];
    
}

-(void)updateTableupdateTimeForCampusBasedMigration
{
#if DEBUG
    NSLog( @"TRACE %@ METHOD %s:%d ", @"DescriptionGoesHere", __func__, __LINE__ );
#endif

    TableUpdateTime *tabl=[[Service sharedInstance] getlastUpdateTableDataInfoStored];
    tabl.studentUpdateTime=[NSNumber numberWithDouble:0];
    tabl.about = [NSNumber numberWithDouble:0];
    tabl.calenderPanel = [NSNumber numberWithDouble:0];
    tabl.classUpdateTime = [NSNumber numberWithDouble:0];
    tabl.diaryItem = [NSNumber numberWithDouble:0];
    tabl.empowerContent = [NSNumber numberWithDouble:0];
    tabl.inbox = [NSNumber numberWithDouble:0];
    tabl.profile = [NSNumber numberWithDouble:0];
    tabl.schoolInfo = [NSNumber numberWithDouble:0];
    tabl.studentUpdateTime = [NSNumber numberWithDouble:0];
    tabl.timeTble = [NSNumber numberWithDouble:0];
    tabl.calendarCycleUpdate = [NSNumber numberWithDouble:0];
    tabl.teacherUpdateTime = [NSNumber numberWithDouble:0];
    tabl.parentsUpdateTime = [NSNumber numberWithDouble:0];

    NSError *error;
    if(tabl)
    {
    if (![_managedObjectContext save:&error])
    {
#if DEBUG
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
#endif
      
    }
    }
    LoginViewController  *login=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    [login userLoginAgain];
    
}
// Campus Based Migration End

- (BOOL)isMeritActive
{
    NSArray *arrActiveDITypes = [[NSArray alloc]initWithArray:[self getStoredAllActiveDiaryItemTypesWithFormTemplate]];
    
    for(DiaryItemTypes *types in arrActiveDITypes)
    {
#if DEBUG
        NSLog(@"types.name %@",types.name);
#endif
      
        
        if ( [[types.name lowercaseString] isEqualToString:@"merits"] || [[types.name lowercaseString] isEqualToString:@"merit"] )
        {
            return YES;
        }
    }
    return NO;
}

- (NSArray *)getActiveLOVWithDIU:(NSArray *)arrDIU
{

    
    if ([self isMeritActive])
    {
        NSMutableArray *marrDIUsers = [[NSMutableArray alloc] init];
        
        NSArray *arrActiveMeritsSubTypes = [self getAllActiveMeritsFromListofValues];
        for (DairyItemUsers *assinUserobj in arrDIU)
        {
#if DEBUG
            NSLog(@"assinUserobj %@",assinUserobj);
#endif
      
            if ([assinUserobj.listofvaluesTypeid integerValue] != 0 )
            {
                BOOL isLOVIdMatched = NO;
                for (ListofValues *lov in arrActiveMeritsSubTypes)
                {
#if DEBUG
                    NSLog(@"[lov.listofvaluesTypeid integerValue] %ld",(long)[lov.listofvaluesTypeid integerValue]);
                    NSLog(@"[assinUserobj.listofvaluesTypeid integerValue] %ld",(long)[assinUserobj.listofvaluesTypeid integerValue]);
#endif
                    
                    if ([lov.listofvaluesTypeid integerValue] == [assinUserobj.listofvaluesTypeid integerValue])
                    {
                        isLOVIdMatched = YES;
                        break;
                    }
                }
                if (isLOVIdMatched)
                {
                    [marrDIUsers addObject:assinUserobj];
                }
            }
            else
            {
                [marrDIUsers addObject:assinUserobj];
            }
        }
#if DEBUG
        NSLog(@"marrDIUsers %@",marrDIUsers);
#endif
      
        return (NSArray *)marrDIUsers;
    }
    else
    {
        return [NSArray array];
    }
    
}

/*************Start Methods added for calender crash ( when user click repetedly on calender tab jsut after logging before getting all data from server)***********************/

- (NSInteger )getCountforAllStoredCalenderCycleDays{
    
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"CalendarCycleDay"];
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    
    if (error)
    {
#if DEBUG
        NSLog(@"getAllStoredCalenderCycleDays fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    if([results count]>0)
    {
    return [results count];
    }
    else
    {
         return 0;
    }
}

-(NSInteger)getCountForStoredAllTimeTableData
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"TimeTable"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"startDate" ascending:YES]];
    
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"getStoredAllTimeTableDataCount fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    if([results count]>0)
    {
        return [results count];
    }
    else
    {
        return 0;
    }
}

-(NSInteger)getCountForStoredDiaryItemTypes
{
    NSFetchRequest *request = [self getBasicRequestForEntityName:@"DiaryItemTypes"];
    
    
    NSError *error = nil;
    NSArray *results = [_managedObjectContext executeFetchRequest:request error:&error];
    if (error)
    {
#if DEBUG
        NSLog(@"getStoredAllTimeTableDataCount fetch request error = %@", [error localizedDescription]);
#endif
      
    }
    if([results count]>0)
    {
        return [results count];
    }
    else
    {
        return 0;
    }
}


- (BOOL)isNumberOfCampusUpdated
{
    School_detail *schoolDetailObj=[[Service sharedInstance]getSchoolDetailDataInfoStored:[AppHelper userDefaultsForKey:@"school_Id"] postNotification:NO];

    if([schoolDetailObj.noOfCampus integerValue] != [[AppHelper userDefaultsForKey:@"NumberOfCampus"] integerValue])
    {
        return YES;
    }
    
    return NO;
}




/*************End Methods added for calender crash ( when user click repetedly on calender tab jsut after logging before getting all data from server)***********************/


-(void)addTaskBasedMerit:(NSMutableDictionary*)dict
{
#if DEBUG
    NSLog(@"addTaskBasedMerit %@",dict);
#endif
    
    
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        //...............................................
        //http://productdynamics.expensetrackingapplication.com/Webservice/setdairyitems
        //and keys is  Type , SchoolId, SubjectId, Title,Description,  Priority ,AssignedDate, AssignedTime,DueDate,DueTime,EstimatedHours, EstimatedMins, Completed , Progress,AssignedtoMe ,All_Students and user_id
        
        NSString *serviceName = Method_addtaskbasedmerit ;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
#if DEBUG
        NSLog(@"addTaskBasedMerit urlString %@",urlString);
#endif
      
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        if ([dict objectForKey:@"Type"] ) {
            [parameters setObject:[dict objectForKey:@"Type"] forKey:@"Type"];
        }
        
        if ([dict objectForKey:@"Id"] ) {
            [parameters setObject:[dict objectForKey:@"Id"]forKey:@"diaryItemId"];
        }
        
        // Aman GP
        if (![[dict objectForKey:@"type_id"] isKindOfClass:[NSNull class]]) {
            [parameters setObject:[dict objectForKey:@"type_id"]forKey:@"type_id"];
        }
        
        if (![[dict objectForKey:@"type_name"] isKindOfClass:[NSNull class]]) {
            [parameters setObject:[dict objectForKey:@"type_name"]forKey:@"type_name"];
        }
        
        if ([dict objectForKey:@"Title"]) {
            [parameters setObject:[dict objectForKey:@"Title"]forKey:@"Title"];
        }
        
        if ([dict objectForKey:@"WebLink"]) {
            [parameters setObject:[dict objectForKey:@"WebLink"]forKey:@"WebLink"];
        }
        
        if ([dict objectForKey:@"OnTheDay"]) {
            [parameters setObject:[dict objectForKey:@"OnTheDay"]forKey:@"OnTheDay"];
        }
        if ([dict objectForKey:@"Priority"]) {
            [parameters setObject:[dict objectForKey:@"Priority"]forKey:@"Priority"];
        }
        if ([dict objectForKey:@"AssignedDate"]) {
            [parameters setObject:[dict objectForKey:@"AssignedDate"]forKey:@"AssignedDate"];
        }
        if ([dict objectForKey:@"AssignedTime"] ) {
            [dict setObject:[self convert12to24format:[dict objectForKey:@"AssignedTime"]] forKey:@"AssignedTime"];
            [parameters setObject:[dict objectForKey:@"AssignedTime"]forKey:@"AssignedTime"];
        }
        /*******************New Methods Added For Out Class********************************/
        
        if([dict objectForKey:@"passtype_id"]){
            [parameters setObject:[dict objectForKey:@"passtype_id"] forKey:@"passtype_id"];
            [parameters setObject:[dict objectForKey:@"passtype_name"] forKey:@"passtype_name"];
            
        }
        
        if([dict objectForKey:@"isnotify"]){
            [parameters setObject:[dict objectForKey:@"isnotify"] forKey:@"isnotify"];

        }
        
        if([dict objectForKey:@"comment"]){
            [parameters setObject:[dict objectForKey:@"comment"] forKey:@"comment"];

        }
        if([dict objectForKey:@"merit_type"]){
            [parameters setObject:[dict objectForKey:@"merit_type"] forKey:@"merit_type"];

        }
       
        
        //Merit - End
        
        if([[dict objectForKey:@"Type"]  isEqualToString:@"OutOfClass"] || [[dict objectForKey:@"Type"]  isEqualToString:@"Out Of Class"])
        {
            MyProfile *profobj=[[Service sharedInstance]getProfileDataInfoStored:[AppHelper userDefaultsForKey:@"user_id"] postNotification:NO];
            if ([profobj.type isEqualToString:@"Student"])
            {
                if([[dict objectForKey:@"PassCode"] length]>0)
                    [parameters setObject:[dict objectForKey:@"PassCode"] forKey:@"PassCode"];
                
            }
        }
        
        
        if([dict objectForKey:@"pass_status"]){
            [parameters setObject:[dict objectForKey:@"pass_status"] forKey:@"pass_status"];
            
        }
        ////
        if ([dict objectForKey:@"assignUserId"] ) {
            [parameters setObject:[dict objectForKey:@"assignUserId"]forKey:@"diaryItemUserId"];
        }
        ////
        if ([dict objectForKey:@"DueDate"]) {
            [parameters setObject:[dict objectForKey:@"DueDate"]forKey:@"DueDate"];
        }
        if ([dict objectForKey:@"DueTime"]) {
            [dict setObject:[self convert12to24format:[dict objectForKey:@"DueTime"]] forKey:@"DueTime"];
            [parameters setObject:[dict objectForKey:@"DueTime"]forKey:@"DueTime"];
            
        }
        if ([dict objectForKey:@"EstimatedHours"]) {
            [parameters setObject:[dict objectForKey:@"EstimatedHours"]forKey:@"EstimatedHours"];
        }
        
        if ([dict objectForKey:@"EstimatedMins"]) {
            [parameters setObject:[dict objectForKey:@"EstimatedMins"]forKey:@"EstimatedMins"];
        }
        if ([dict objectForKey:@"Completed"]) {
            [parameters setObject:[dict objectForKey:@"Completed"]forKey:@"Completed"];
        }
        if ([dict objectForKey:@"Progress"]) {
            [parameters setObject:[dict objectForKey:@"Progress"] forKey:@"Progress"];
        }
        if ([dict objectForKey:@"AssignedtoMe"]) {
            [parameters setObject:[dict objectForKey:@"AssignedtoMe"] forKey:@"AssignedtoMe"];
        }
        if ([dict objectForKey:@"Description"]) {
            [parameters setObject:[dict objectForKey:@"Description"]forKey:@"Description"];
        }
        if ([dict objectForKey:@"sub_name"]) {
            [parameters setObject:[dict objectForKey:@"sub_name"]forKey:@"subjectName"];
        }
        
        if ([dict objectForKey:@"Weight"]) {
            [parameters setObject:[dict objectForKey:@"Weight"]forKey:@"Weight"];
        }
        
        if ([dict objectForKey:@"ClassId"]) {
            [parameters setObject:[dict objectForKey:@"ClassId"]forKey:@"ClassId"];
        }
        
        
        [parameters setObject:[dict objectForKey:@"created"] forKey:@"created"];
        [parameters setObject:[dict objectForKey:@"updated"] forKey:@"updated"];
        
        //////
        [parameters setObject:[dict objectForKey:@"SchoolId"] forKey:@"schoolId"];
        [parameters setObject:[dict objectForKey:@"user_id"] forKey:@"userID"];
        [parameters setObject:[dict objectForKey:@"AppId"] forKey:@"AppId"];
        // Webservice Logging existing
        [parameters setObject:APP_Key forKey:kKeyAppType];
        [parameters setObject:[AppHelper userDefaultsForKey:@"ipadd"]forKey:@"ipadd"];
        
#if DEBUG
        NSLog(@"JSON Parameters : %@",[parameters JSONRepresentation]);
#endif
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishsAddTaskBasedMerit:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
        [netManager startRequest];
        [parameters release];
    }
    else
    {
        
        //Merit - Start
        
        if(![[dict objectForKey:@"Type"] isEqualToString:k_MERIT_CHECK])     //Merit - End
        {
            NSMutableDictionary *dictionary=[[NSMutableDictionary alloc]init];
            [dictionary setObject:@"1" forKey:@"ErrorCode"];
            [dictionary setObject:@"Dairy items add successfully." forKey:@"Status"];
            NSArray *arr=[[dict objectForKey:@"assignUserId"] componentsSeparatedByString:@","];
            // [self parseItemDataWithDictionary:dict];
            if(arr.count>0){
                NSMutableArray *arr1=[[NSMutableArray alloc]init];
                for(NSString *str in arr){
                    NSMutableDictionary *dict1=[[NSMutableDictionary alloc]init];
                    [dict1 setObject:str forKey:@"UserId"];
                    [dict1 setObject:[dict objectForKey:@"AppId"] forKey:@"itemId"];
                    [dict1 setObject:@"0000-00-00 00:00:00" forKey:@"Reminder"];
                    [dict1 setObject:@"0" forKey:@"Progress"];
                    
                    [arr1 addObject:dict1];
                    [dict1 release];
                    
                }
                [dict setObject:arr1 forKey:@"assignUserId"];
                [arr1 release];
            }
            [dictionary setObject:dict forKey:@"dairy_item"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EditItem object:nil userInfo:dictionary];
            [dictionary release];
            //[AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
        }
        
    }
}


- (void)netManagerDidFinishsAddTaskBasedMerit:(NetManager *)thisNetManager
                                 response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
        
#if DEBUG
        NSLog(@"netManagerDidFinishsAddTaskBasedMerit error dictionary %@",dictionary);
#endif
        
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
#if DEBUG
        NSLog(@"netManagerDidFinishsAddTaskBasedMerit %@",jsonDictD);
#endif
      
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
        
#if DEBUG
        NSLog(@"netManagerDidFinishsAddTaskBasedMerit %@",dictionary);
#endif
        
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_EditItem object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}


-(void)deleteAttachmentsOnServer:(NSArray*)arrayOfDict
{
    BOOL networkReachable = [[[AppDelegate getAppdelegate] netManager] networkReachable];
    if (networkReachable)
    {
        NSString *serviceName = Method_DeleteAttachments;
        NSString *urlString =[NSString stringWithFormat:@"%@%@",BaseUrl,serviceName];
        urlString =[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        
        
            NSMutableArray *arrForDeleteAttachment=[[NSMutableArray alloc]init];
            for (NSDictionary *atc in arrayOfDict) {
                
                [arrForDeleteAttachment addObject:[atc valueForKey:@"aID"]];
            }
            
            NSString *strDeletedIds=[arrForDeleteAttachment componentsJoinedByString:@","];
            [parameters setObject:strDeletedIds forKey:@"deletedAttachmentId"];
            
            [arrForDeleteAttachment release];
        
        
        
        
        if ([parameters objectForKey:@"deletedAttachmentId"] ) {
            [parameters setObject:[parameters objectForKey:@"deletedAttachmentId"]forKey:@"deletedAttachmentId"];
        }
        
        
        // Webservice Logging
        [parameters setObject:[AppHelper userDefaultsForKey:@"user_id"] forKey:@"user_id"];
        
        // New
        [parameters setObject:APP_Key forKey:kKeyAppType];
#if DEBUG
        NSLog(@"urlString %@",urlString);
        NSLog(@"deleteAttachmentsOnServer -parameters-%@",[parameters JSONRepresentation]);
#endif
        
        self.netManager = [[NetManager alloc] initWithURL:[NSURL URLWithString:urlString]target:self
                                    finishLoadingSelector:@"netManagerDidFinishdeleteAttachmentsOnServer:response:"postParameters:parameters showErrorMessage:YES storedObject:nil];
        [netManager startRequest];
        [parameters release];
    }
    else
    {
        //[AppHelper showAlertViewWithTag:0 title:APP_NAME message:ERROR_INTERNET delegate:nil cancelButtonTitle:nil otherButtonTitles:Alert_Ok_Button];
    }
}

- (void)netManagerDidFinishdeleteAttachmentsOnServer:(NetManager *)thisNetManager
                                   response:(NSMutableData *)responseData
{
    
    
    NSMutableDictionary *dictionary=nil;
    if (thisNetManager.error)
    {
        [dictionary setObject:netManager.error forKey:@"error"];
        
        
    }
    else
    {
        NSString *decodedString = [[[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding] autorelease];
        
        decodedString = [StringTransformer transformDecodedString:decodedString];
        
        NSDictionary *jsonDictD = [decodedString JSONValue];
        
        dictionary = [[NSMutableDictionary dictionaryWithDictionary:jsonDictD]retain];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName: NOTIFICATION_DeleteAttachments object:nil userInfo:dictionary];
    
    [dictionary release];
    [thisNetManager release];
    self.netManager = nil;
}



@end

//
//  AppHelper.h
//  IntroduceThem
//
//  Created by Jitendra Singh on 13/06/11.
//  Copyright 2011 Tata Consultant Services. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface AppHelper : NSObject {
    
}

+ (UIImage*)appLogoImage;
+(void)saveToUserDefaults:(id)value withKey:(NSString*)key;
+(NSString*)userDefaultsForKey:(NSString*)key;
+(void)removeFromUserDefaultsWithKey:(NSString*)key;
//+ (NSString*)displayStringForKey:(NSString*)key;
//+ (BOOL)imageExist:(NSString*)imageName;
+ (void) showAlertViewWithTag:(NSInteger)tag title:(NSString*)title message:(NSString*)msg delegate:(id)delegate 
            cancelButtonTitle:(NSString*)CbtnTitle otherButtonTitles:(NSString*)otherBtnTitles;
+ (NSString *)getCurrentLanguage;
+(UIColor *)colorFromHexString:(NSString *)hexString;
 /************** Code Added For Push Notification  By Aman **********************/
+(void)saveToUserDefaultforPushNotification:(id)obje withkey:(NSString*)key;

@end

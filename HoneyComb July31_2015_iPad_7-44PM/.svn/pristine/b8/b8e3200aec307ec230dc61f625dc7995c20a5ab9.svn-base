﻿<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Content Panel.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="UTF-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>Writing and Researching</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="../../../Support/content-style.css" type="text/css"/>
</head>

<body class="panel">
	<!-- InstanceBeginEditable name="content" -->
	<div style="width:845px; clear:both;color:#575757;font-family: Arial;">
	<h1 style="padding-bottom:5px;font-size:16pt;"><strong>Writing and Researching...<br>
	<span style="text-align:right"><em>...Reports and Essays</em></span></strong></h1>
    <img src="Images/research&writing.jpg" alt="" width="188" style="float:left;padding-right:20px;padding-bottom:0px;">
    <p>Some major assessment tasks this year may be projects that will involve research and organisation. These are generally handed out well before the due date, so you will have plenty of time to research and put the project together. Before starting, check with your teacher that you have understood the topic and its requirements, particularly content, format and style. Will the project be simply words or also pictures, charts and graphs? Four factors are critical to its management: <strong>timing, research, synthesis and evaluation,</strong> and finally <strong>presentation</strong>.</p>
    <h2 style="font-size:14pt;"><strong>Timing</strong></h2>
    <p>Leaving too much to the end is risky without a time management plan to ensure you meet the final deadline. Here are two possible methods:</p>
    <ul style="margin-left:-20px; margin-top:0;">
   	<li style="list-style-position:outside; margin-left:205px;"><strong>A time line</strong> Design a timeline with dates for each section to be completed as well as a deadline for putting the project together in its final format.</li>
    <li style="list-style-position:outside"><strong>Time allocation for specific processes</strong> Rather than allocating a period of time for each question or section, work out a time for each activity. For example, you might allow two weeks for research, one week for sifting and organising information, a week for planning and drafting and so on.</li>
    </ul>
    <h2 style="font-size:14pt;"><strong>Research</strong></h2>
    <p>The sources selected will depend on the subject matter. You’ve had experience researching books, journals, encyclopaedias and newspapers. Other useful sources are:</p>
    <ul style="margin-left:-20px; margin-top:0;">
   	<li style="list-style-position:outside"><strong>Specific subject-based websites</strong> If you were doing a geography project for example, you could look at CSIRO websites, a Bureau of Meteorology website or a university website for its Geography department.</li>
    <li style="list-style-position:outside"><strong>Search Engines</strong> Search engines (e.g. Google) are invaluable and quick. Type in specific words rather than a general term and the search is narrowed down.</li>
    <li style="list-style-position:outside"><strong>Educational Packages</strong> Newspapers, museums, government departments and large companies publish excellent information packages on topics. Why not check their websites to see if one is available for your topic?</li>
    <li style="list-style-position:outside"><strong>Interviews</strong> You may know someone with experience or knowledge in the field your project covers. Your Google search might provide names of people you can email for information.</li>
    </ul>
    <p><strong><em>Don’t forget to keep a list of the sources you explore for your bibliography – even if you do not use quotes or examples from these in the project itself.</em></strong></p>
    <p><strong>Synthesis and Evaluation</strong></p>
    <p>You will save time, and find it easier to use all information you collect by creating a filing system before you start. Why not organise folders based on each question or section of the project? File into the appropriate folder at the time you examine a source. Chances are you will need less than half of your collected material. Choosing the most effective examples is a skill in itself. As a guide, decide:</p>
    <ol style="margin-left:-20px; margin-top:0;">
   	<li style="list-style-position:outside">Which examples are the most relevant and convincing.</li>
    <li style="list-style-position:outside">Whether to paraphrase the information or quote directly. Either way, you must acknowledge the source of your information.</li>
    <li style="list-style-position:outside">Whether you need to provide an analytical comparison if you have used several sources for a section.</li>
    </ol>
    <p>This stage is like sifting through a box of tomatoes. You pull out the best and discard what’s left.</p>
    <h2 style="font-size:14pt;"><strong>Presentation</strong></h2>
    <p>Well done! You’ve completed the groundwork – now for the final presentation! An organised, attractive project conveys confidence and care. Don’t forget:</p>
    <ul style="margin-left:-20px; margin-top:0;">
   	<li style="list-style-position:outside">A title page or front cover with topic, your name and form, your teacher’s name</li>
    <li style="list-style-position:outside">A Contents page</li>
    <li style="list-style-position:outside">Make titles/subtitles stand out. Use a new page to start each section.</li>
    <li style="list-style-position:outside">Information attractively presented in various forms (e.g. graphs, maps, pictures, boxed details)</li>
    <li style="list-style-position:outside">Project stapled and enclosed in a secure folder</li>
    <li style="list-style-position:outside">Check all questions/sections are covered thoroughly. Check spelling, grammar and vocabulary are accurate and pages are numbered.</li>
    <li style="list-style-position:outside">Accurate bibliography that acknowledges all resources.</li>
    </ul>
    </div>
	<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>

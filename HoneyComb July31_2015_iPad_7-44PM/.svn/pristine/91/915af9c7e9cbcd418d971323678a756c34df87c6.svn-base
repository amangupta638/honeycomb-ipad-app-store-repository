﻿<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Content Panel.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="UTF-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>20 Questions to ask...</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="../../../Support/content-style.css" type="text/css"/>
</head>

<body class="panel">
	<!-- InstanceBeginEditable name="content" -->
	<div style="width:845px; clear:both;color:#575757;font-family: Arial;">
	<h1 style="padding-bottom:5px;font-size:16pt;"><strong>20 Questions to ask...<br>
	<span style="text-align:right">...your careers counsellor</span></strong></h1>
    <img src="Images/20question2ask.jpg" alt="" width="188" height="133" style="float:left;padding-right:20px;padding-bottom:20px;">
    <p>Leaving school and going onto further education or entering the workforce can be a scary yet exhilarating experience. Your future depends to a large degree on the choices you make before you leave school. Look at all your options. Don’t be afraid to ask questions. Get answers you need to help you make informed decisions. Your first stop should be with your Careers teacher/counsellor.</p>
    <ol style="margin-left:-20px; margin-top:0; clear:both">
        <li style="list-style-position:outside">Where do I find out about course applications and deadlines?</li>
        <li style="list-style-position:outside">What are the admission requirements for the course I wish to take?</li>
        <li style="list-style-position:outside">What are my other options? Apprenticeship, Traineeship, Defence Forces, Workforce?</li>
        <li style="list-style-position:outside">How flexible is my course? Can I change one or more subjects or will I be locked in?</li>
        <li style="list-style-position:outside">Could I take a year off before attending University? Does my course choice allow this?</li>
        <li style="list-style-position:outside">Do I have information about Open Days of the institutions I am interested in?</li>
        <li style="list-style-position:outside">How much will my course(s) cost?</li>
        <li style="list-style-position:outside">What Scholarships or Bursaries can I apply for?</li>
        <li style="list-style-position:outside">How can I find out about financial aid? What is Youth Allowance, Austudy, Abstudy?</li>
        <li style="list-style-position:outside">What is HECS-HELP?</li>
        <li style="list-style-position:outside">How do I cope with a part-time job and going to University or TAFE?</li>
        <li style="list-style-position:outside">Can I go to a University or TAFE in another State or Country?</li>
        <li style="list-style-position:outside">What options do I have if I don’t get into the course of my first choice?</li>
        <li style="list-style-position:outside">Are there bridging courses that will allow me to do the course of my choice later?</li>
        <li style="list-style-position:outside">Does my course choice offer full-time or part-time study?</li>
        <li style="list-style-position:outside">What type of jobs would I be able to get at the end of my studies?</li>
        <li style="list-style-position:outside">How are my results delivered to me?</li>
        <li style="list-style-position:outside">What extra-curricular activities could help me with my future choices?</li>
        <li style="list-style-position:outside">Would references help? Who should I ask and how many references should I seek?</li>
        <li style="list-style-position:outside">Am I on track to achieve my goals?</li>
    </ol>
    <p style="text-align:center"><strong>10 QUESTIONS TO ASK YOURSELF ABOUT YOUR FUTURE</strong></p>
    <ol style="margin-left:-20px; margin-top:0;">
        <li style="list-style-position:outside">Are my choices realistic?</li>
        <li style="list-style-position:outside">Am I really interested in further education?</li>
        <li style="list-style-position:outside">Is this what I <strong>really</strong> want to do?</li>
        <li style="list-style-position:outside">Have I done all the necessary research I need to make this decision?</li>
        <li style="list-style-position:outside">Can I afford my choice in terms of time and costs?</li>
        <li style="list-style-position:outside">Should I defer my course for a year</li>
        <li style="list-style-position:outside">Am i doing enough now to achieve my career goals?</li>
        <li style="list-style-position:outside">Have I discussed my options with my family, friends or counsellor?</li>
        <li style="list-style-position:outside">What type of job or business do I want in my future?</li>
        <li style="list-style-position:outside">Will what I do now get me the future I want?</li>
    </ol>
    <p>Consult your Careers Counsellor or student advisor. Speak to the University or TAFE you are looking to attend. Go to Open Days. Talk to other students who have the same interests as you. Talk to students who are taking the course you want to do. If you want to do an apprenticeship or traineeship, talk to industry professionals. If you are thinking about getting a job, get advice from employment agencies or professionals in the industry you want to join.</p>
    <p>Now that you are armed with the important information about your options, it is time to ask yourself the serious questions about your hopes, dreams, goals and desires for your future. Get all of the input, advice and information you possibly can.</p>
	</div>
    
    
    

<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>

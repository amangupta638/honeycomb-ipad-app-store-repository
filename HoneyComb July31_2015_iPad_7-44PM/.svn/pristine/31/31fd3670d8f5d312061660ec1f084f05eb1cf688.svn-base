﻿<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Content Panel.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="UTF-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>BELIEVE it and you will SEE it - Part 2</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="../../../Support/content-style.css" type="text/css"/>
</head>

<body class="panel">
	<!-- InstanceBeginEditable name="content" -->
	<div style="width:845px; clear:both;color:#575757;font-family: Arial;">
	<h1 style="padding-bottom:5px; font-size:16pt;"><strong>BELIEVE it and you will SEE it! </strong></h1>
    <h2 style="padding-bottom:5px; font-size:14pt;"><strong>Part 2</strong></h2>
	<img src="Images/believeitandyouwillseeit_80.JPG" alt="" width="188" style="float:left;padding-right:20px;padding-bottom:20px;">
    <p><strong>YOU WON’T FAIL if you DON’T FAIL TO PLAN.</strong></p>
      <p><strong>Make a plan.</strong> Most people who fail have simply failed to plan.</p>
      <p><strong>Stick to your plan.</strong> If you face a setback, overcome it and get back on track.</p>
    <p><strong>See yourself completing each task.</strong> The power of positive thinking powers your engine to succeed.</p>
    <p><strong>Ask for help along the way.</strong> Don’t be afraid to seek assistance. Your family, friends and teachers are there to support you. If you highlight a problem early, chances are the problem can be resolved. Remember, ‘together we achieve more.’</p>
    <p>&nbsp;</p>
    <table style="border:#000 2px solid;clear:both;" cellspacing="0" cellpadding="1">
      <tr>
    <td colspan="3" style="border:#000 2px solid; text-align:center; background:#AAA"><strong>Obstacles to your plan and ways to overcome them…</strong></td>
    </tr>
    <tr>
    <td style="border:#000 2px solid; text-align:center;"><strong>Obstacle</strong></td>
    <td colspan="2"style="border:#000 2px solid; text-align:center;"><strong>How To Overcome The Obstacle</strong></td>
  </tr>
  <tr>
    <td style="border:#000 2px solid; padding:5px 15px;"><strong>Becoming Ill</strong></td>
    <td style="border:#000 2px solid; background:#CCC; padding:5px 15px;">Sometimes illness strikes but you can take the following steps to avoid becoming ill.
    <ul style="margin-left:-20px; margin-top:0;">
    <li style="list-style-position:outside">Get plenty of sleep</li>
    <li style="list-style-position:outside">Don’t stay up late and get up early</li>
    <li style="list-style-position:outside">Eat healthy food. Drink plenty of water</li>
    <li style="list-style-position:outside">Seek medical advice if you become ill</li>
    </ul></td>
  </tr>
  <tr>
    <td style="border:#000 2px solid; padding:5px 15px;"><strong>Family Commitments</strong></td>
    <td style="border:#000 2px solid; background:#CCC; padding:5px 15px;">Your family is an important part of your life and so is your education. Try to plan for unexpected events. Functions you must attend can throw you off course if you do not plan for them. These important commitments are part of a healthy life balance. By keeping up to date or keeping ahead of your plan, you will be ready for the unexpected. Factor in some spare time at various intervals in your plan.</td>
  </tr>
  <tr>
    <td style="border:#000 2px solid; padding:5px 15px;"><strong>Physical Overload</strong></td>
   <td style="border:#000 2px solid; background:#CCC; padding:5px 15px;">Trying to fit too many things into your life will make it difficult for you to stick to your plan. Be realistic about what you can fit into each day, remembering to allow for unexpected events.</td>
  </tr>
  <tr>
    <td style="border:#000 2px solid; padding:5px 15px;"><strong>Emotional Overload</strong></td>
    <td style="border:#000 2px solid; background:#CCC; padding:5px 15px;">It is normal to feel a little pressure when you are in the process of achieving your goals. The most experienced executives in large companies feel the pressure to meet deadlines. The trick is to convert this feeling of pressure into a motivational surge much like a turbo boost in an engine. Take regular time-out to switch off the pressure. Constant, unrelieved stress will lead to exhaustion.</td>
  </tr>
  <tr>
    <td style="border:#000 2px solid; padding:5px 15px;"><strong>Negative &quot;Self Talk&quot;</strong></td>
    <td style="border:#000 2px solid; background:#CCC; padding:5px 15px;">Positive thinking is paramount to achieving your goals. You have made a plan. See yourself achieving your goals and you will gear your mind and body to achieve.</td>
  </tr>
</table>
</div>

    
	<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>

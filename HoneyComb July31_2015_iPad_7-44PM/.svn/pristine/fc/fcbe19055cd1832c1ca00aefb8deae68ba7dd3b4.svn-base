﻿<!DOCTYPE HTML>
<html><!-- InstanceBegin template="/Templates/Content Panel.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="UTF-8">
<!-- InstanceBeginEditable name="doctitle" -->
<title>12 Ways to Combat Stress</title>
<!-- InstanceEndEditable -->
<!-- InstanceBeginEditable name="head" -->
<!-- InstanceEndEditable -->
<link rel="stylesheet" href="../../../Support/content-style.css" type="text/css"/>
</head>

<body class="panel">
	<!-- InstanceBeginEditable name="content" -->
	<div style="width:845px; clear:both;color:#575757;font-family: Arial;">
	<h1 style="padding-bottom:5px;font-size:16pt;"><strong>12 Ways to Combat Stress</strong></h1>
    <img src="Images/12wayscombat-stress.jpg" alt="" width="188" style="float:left;padding-right:20px;padding-bottom: 20px;">
    <p>Like it or not, you probably will get pretty stressed as key events in your school year loom closer. You might come to a realisation that you haven’t worked hard enough through the year or are stressing out because of a subject you absolutely hate. When you have been under stress for a long time, you may find that you are less able to think clearly and rationally. The good news is that stress is not all bad. There is <Strong>good stress</strong> and <strong>bad stress</strong>. Stress for some people can be exciting, even stimulating. It can energise you to achieve more. Good stress can motivate you to prioritise.  Bad stress for too long can lead to poor performance, both physical and mental. Bad stress can make you feel angry, depressed or a failure. It can affect your
	concentration, and make you feel tired, angry, confused or negative. Worst of all,
it’s a known fact that long term exposure to stress can cause your health to suffer.</p>
    <p>Stress is going to be a part of your life whatever you do. The thing to remember is that if you control it and not allow it to overtake you, you may find that stress is just the motivation you need.</p>
    <ol style="margin-left:-20px; margin-top:0; clear:both; font-weight: bold;">
        <li style="list-style-position:outside">Prioritise your tasks<span style="font-weight: normal;">. Don’t let your work pile up. Doing assignments or last minute swatting causes unnecessary stress. Let your policy be <em><strong>work first, play later</strong></em>.</li>
        <li style="list-style-position:outside">Make lists<span style="font-weight: normal;">. A major cause of stress is being overwhelmed and swamped with work. Make a list of what needs to be done. Allocate<em> time estimates</em> and <em>deadlines</em> for each task. Just writing this list will help you feel you’ve done something.</span></li>
        <li style="list-style-position:outside">Set realistic goals<span style="font-weight: normal;">. Don’t expect the impossible for yourself. Set goals you can achieve. Set smaller, easier goals and prioritise them.</span></li>
        <li style="list-style-position:outside">Be your own time keeper<span style="font-weight: normal;">. Managing your time increases productivity. Using that list you created, spend quality time on the most urgent tasks.</span></li>
        <li style="list-style-position:outside">Learn to say “no”<span style="font-weight: normal;"> especially in the crazy times towards the middle and end of semester. Know your limits and respect them.</span></li>
        <li style="list-style-position:outside">Don’t let one thing dominate you<span style="font-weight: normal;"> such as a subject you find difficult. Your other subjects will suffer and getting behind in your work will stress you out. Prioritise!</span></li>
        <li style="list-style-position:outside">Don’t keep things bottled up<span style="font-weight: normal;">. When worries or stress builds up, talk it over with a friend. You don’t have to cope alone.</span></li>
        <li style="list-style-position:outside">Take care of yourself<span style="font-weight: normal;">. Get lots of shut-eye (at least 8 hours of sleep a night), avoid alcohol, nicotine, excess caffeine and other stimulants.</span></li>
        <li style="list-style-position:outside">Eat THREE well balanced meals a day<span style="font-weight: normal;">. When you stress out, your body uses up nutrients faster than it normally does. Eating breakfast is an absolute must. Eat plenty of fruits, vegies and foods that are high in complex carbohydrates such as wholemeal bread and brown rice. If you can, eat small amounts often through the day to keep your blood sugar level steady.</span></li>
        <li style="list-style-position:outside">Exercise<span style="font-weight: normal;">. Using your study planner, build 20 minutes into your day for some exercise. Regular, moderate exercise helps boost your energy, clear your mind and release pent-up emotions. Oh, and it’s fun too!</span></li>
        <li style="list-style-position:outside">Have some ‘You’ time<span style="font-weight: normal;">. Put away the books for a few hours and do something that makes you happy. Watch your favourite TV show or movie. Go for a coffee with a friend. Take your dog for a walk. Whatever it is, make sure it’s fun and non-work related.</span></li>
        <li style="list-style-position:outside">Exercise your sense of humour<span style="font-weight: normal;">. Laughing eases pent-up emotions and releases good chemicals in your brain. Next time you feel stressed, look for the humorous side of the situation and find something to laugh at.</span></li>
    </ol>
    </div>
    
<!-- InstanceEndEditable -->
</body>
<!-- InstanceEnd --></html>

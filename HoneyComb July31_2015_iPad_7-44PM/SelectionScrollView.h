//
//  SelectionScrollView.h
//  StudyPlaner
//
//  Created by Viraj Harhare on 12/29/14.
//  Copyright (c) 2014 Swatantra Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Merit.h"

@protocol SelectionScrollViewDelegate <NSObject>

- (void)didChangeSelectionToNewValue:(id)newSelection;

@end


@interface SelectionScrollView : UIView

@property (nonatomic, retain) NSArray *arrContent;
@property (nonatomic, assign) id <SelectionScrollViewDelegate>delegate;
@property (nonatomic, strong)UIButton *leftNavButton;
@property (nonatomic, strong) UIButton *rightNavButton;
- (id)initWithFrame:(CGRect)frame contentArray:(NSArray *)contentArray;

- (void)navigateToSelectedMerit:(Merit *)selectedMerit;

@end

//
//  Inbox.m
//  StudyPlaner
//
//  Created by Ishan Gupta on 04/07/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "Inbox.h"


@implementation Inbox

@dynamic created_time;
@dynamic discrptin;
@dynamic inbox_user;
@dynamic message_id;
@dynamic parent_id;
@dynamic reciver_id;
@dynamic sender_id;
@dynamic subject;
@dynamic appId;
@dynamic ipAdd;
@dynamic isDel;
@dynamic isSync;
@dynamic local_parent_id;
@dynamic email_id ;
@dynamic reciverName ;
@dynamic senderName ;

@end

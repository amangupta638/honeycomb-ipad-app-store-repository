//
//  DiaryItemTypes.m
//  StudyPlaner
//
//  Created by Aman Gupta on 12/5/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "DiaryItemTypes.h"


@implementation DiaryItemTypes

@dynamic diaryitemtypeid;
@dynamic name;
@dynamic formtemplate;
@dynamic school_Id;
@dynamic icon;
@dynamic active;
@dynamic isonlyteacher;
@end

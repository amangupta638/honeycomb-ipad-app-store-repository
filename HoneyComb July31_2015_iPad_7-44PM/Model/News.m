//
//  News.m
//  StudyPlaner
//
//  Created by Dhirendra on 22/08/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "News.h"
#import "News_Attachment.h"


@implementation News

@dynamic created_time;
@dynamic cretedDate;
@dynamic details;
@dynamic isReadNews;
@dynamic news_id;
@dynamic school_id;
@dynamic tags;
@dynamic title;
@dynamic attacments;
@dynamic createdDate_Str;
@end

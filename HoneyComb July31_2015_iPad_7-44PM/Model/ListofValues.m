//
//  ListofValues.m
//  StudyPlaner
//
//  Created by Apple on 21/06/14.
//  Copyright (c) 2014 Swatantra Singh. All rights reserved.
//

#import "ListofValues.h"

//
@implementation ListofValues

@dynamic active;
@dynamic created;
@dynamic createdBy;
@dynamic descriptions;
@dynamic diaryItemType;
@dynamic icon;
@dynamic listofvaluesTypeid;
@dynamic school_Id;
@dynamic sortedOrder;
@dynamic type;
@dynamic updated;
@dynamic updatedBy;
@dynamic value;
@dynamic iconsmall;

@end

//
//  News.h
//  StudyPlaner
//
//  Created by Dhirendra on 22/08/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class News_Attachment;

@interface News : NSManagedObject

@property (nonatomic, retain) NSDate * created_time;
@property (nonatomic, retain) NSDate * cretedDate;
@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSNumber * isReadNews;
@property (nonatomic, retain) NSNumber * news_id;
@property (nonatomic, retain) NSNumber * school_id;
@property (nonatomic, retain) NSString * tags;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *attacments;
@property (nonatomic, retain) NSString * createdDate_Str;

@end

@interface News (CoreDataGeneratedAccessors)

- (void)addAttacmentsObject:(News_Attachment *)value;
- (void)removeAttacmentsObject:(News_Attachment *)value;
- (void)addAttacments:(NSSet *)values;
- (void)removeAttacments:(NSSet *)values;

@end

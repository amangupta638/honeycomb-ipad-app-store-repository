//
//  Events_Users.h
//  StudyPlaner
//
//  Created by Aman Gupta on 11/29/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Events_Users : NSManagedObject

@property (nonatomic, retain) NSString * eventId;
@property (nonatomic, retain) NSNumber * iD;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString *status;
@end

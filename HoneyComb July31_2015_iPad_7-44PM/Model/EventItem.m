//
//  EventItem.m
//  StudyPlaner
//
//  Created by Aman gupta on 10/07/15.
//  Copyright (c) 2015 Swatantra Singh. All rights reserved.
//

#import "EventItem.h"
#import "Attachment.h"


@implementation EventItem

@dynamic assingedUserId;
@dynamic class_id;
@dynamic created;
@dynamic created_by;
@dynamic details;
@dynamic endDate;
@dynamic endTime;
@dynamic eventName;
@dynamic ipadd;
@dynamic isDelete;
@dynamic isRead;
@dynamic isSync;
@dynamic item_Id;
@dynamic itemType_id;
@dynamic onTheDay;
@dynamic priority;
@dynamic publishToClass;
@dynamic school_Id;
@dynamic startDate;
@dynamic startTime;
@dynamic startTimeDate;
@dynamic udatedTime;
@dynamic universal_id;
@dynamic eventItemattachment;

@end

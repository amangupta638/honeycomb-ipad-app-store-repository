//
//  DayOfNotes.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 18/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "DayOfNotes.h"


@implementation DayOfNotes

@dynamic dateOfNote;
@dynamic dayOfNotes_id;
@dynamic details;
@dynamic includeInCycle;
@dynamic isGreyOut;
@dynamic isWeekend;
@dynamic overrideCycle;
@dynamic overrideCycleDay;
@dynamic schoolId;
@dynamic title;
@dynamic type;
@dynamic uniqueId;
@dynamic updateDate;
@dynamic user_id;

@end

//
//  NoteEntry.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 01/07/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "NoteEntry.h"


@implementation NoteEntry

@dynamic created;
@dynamic ipadd;
@dynamic isSync;
@dynamic note_Id;
@dynamic note_localId;
@dynamic noteEntry_id;
@dynamic text;
@dynamic unique_id;
@dynamic updated;
@dynamic user_id;
@dynamic isDelete;

@end

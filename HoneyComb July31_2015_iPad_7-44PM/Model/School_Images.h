//
//  School_Images.h
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface School_Images : NSManagedObject

@property (nonatomic, retain) NSString * image_name;
@property (nonatomic, retain) NSString * image_type;
@property (nonatomic, retain) NSString * image_url;
@property (nonatomic, retain) NSNumber * isDownload;

@end

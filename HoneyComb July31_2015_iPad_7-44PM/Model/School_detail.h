//
//  School_detail.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 05/09/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface School_detail : NSManagedObject


@property (nonatomic, retain) NSNumber * allow_parent; // Added By Amol G

@property (nonatomic, retain) NSNumber * address_Id;
@property (nonatomic, retain) NSString * dairy_Titled;
@property (nonatomic, retain) NSString * display_Name;
@property (nonatomic, retain) NSString * fax_Numbers;
@property (nonatomic, retain) NSNumber * has_Upload;
@property (nonatomic, retain) NSString * include_Impower;
@property (nonatomic, retain) NSNumber * is_Deleted;
@property (nonatomic, retain) NSData * logo_Image;
@property (nonatomic, retain) NSString * logo_Image_Url;
@property (nonatomic, retain) NSString * mission_Statement;
@property (nonatomic, retain) NSString * motto;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * noOfCampus;
@property (nonatomic, retain) NSString * phone_no;
@property (nonatomic, retain) NSString * region;
@property (nonatomic, retain) NSNumber * school_conf_Id;
@property (nonatomic, retain) NSNumber * school_Id;
@property (nonatomic, retain) NSData * school_Image;
@property (nonatomic, retain) NSString * school_Image_Url;
@property (nonatomic, retain) NSString * theme_color;
@property (nonatomic, retain) NSDate * updated;
@property (nonatomic, retain) NSString * web_Site_Url;
@property (nonatomic, retain) NSData * school_BgImge;
@property (nonatomic, retain) NSString * school_BgImageUrl;
@property (nonatomic, retain) NSString * empower_type;
@property (nonatomic,retain) NSString * domainName;
@end

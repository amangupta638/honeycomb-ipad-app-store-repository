//
//  EventItem.h
//  StudyPlaner
//
//  Created by Aman gupta on 10/07/15.
//  Copyright (c) 2015 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Attachment;

@interface EventItem : NSManagedObject

@property (nonatomic, retain) NSString * assingedUserId;
@property (nonatomic, retain) NSNumber * class_id;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * created_by;
@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSDate * endDate;
@property (nonatomic, retain) NSString * endTime;
@property (nonatomic, retain) NSString * eventName;
@property (nonatomic, retain) NSString * ipadd;
@property (nonatomic, retain) NSNumber * isDelete;
@property (nonatomic, retain) NSNumber * isRead;
@property (nonatomic, retain) NSNumber * isSync;
@property (nonatomic, retain) NSNumber * item_Id;
@property (nonatomic, retain) NSNumber * itemType_id;
@property (nonatomic, retain) NSNumber * onTheDay;
@property (nonatomic, retain) NSString * priority;
@property (nonatomic, retain) NSNumber * publishToClass;
@property (nonatomic, retain) NSNumber * school_Id;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSString * startTime;
@property (nonatomic, retain) NSDate * startTimeDate;
@property (nonatomic, retain) NSDate * udatedTime;
@property (nonatomic, retain) NSNumber * universal_id;
@property (nonatomic, retain) NSSet *eventItemattachment;
@end

@interface EventItem (CoreDataGeneratedAccessors)

- (void)addEventItemattachmentObject:(Attachment *)value;
- (void)removeEventItemattachmentObject:(Attachment *)value;
- (void)addEventItemattachment:(NSSet *)values;
- (void)removeEventItemattachment:(NSSet *)values;

@end

//
//  TableForCycle.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 29/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TableForCycle : NSManagedObject

@property (nonatomic, retain) NSDate * dateRef;
@property (nonatomic, retain) NSString * cycle;
@property (nonatomic, retain) NSString * cycleDay;

@end

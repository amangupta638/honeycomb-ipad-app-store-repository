//
//  CalendarCycleDay.h
//  StudyPlaner
//
//  Created by Apple on 09/08/14.
//  Copyright (c) 2014 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CalendarCycleDay : NSManagedObject

@property (nonatomic, retain) NSDate * dateRef;
@property (nonatomic, retain) NSString * cycleDayLabel;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSDate * updated;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSNumber * isDelete;
@property (nonatomic, retain) NSNumber * calendarCycleDayId;
@property (nonatomic, retain) NSNumber * cycleDay;
@property (nonatomic, retain) NSNumber * campusId;
@property (nonatomic, retain) NSString *formatedCycleDayLabel;

@end

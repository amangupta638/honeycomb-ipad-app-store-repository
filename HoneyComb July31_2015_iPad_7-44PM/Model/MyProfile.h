//
//  MyProfile.h
//  StudyPlaner
//
//  Created by Dhirendra on 23/08/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ClassesInfo, MyParent, MyTeacher;

@interface MyProfile : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSDate * birthDate;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * image_url;
@property (nonatomic, retain) NSNumber * isSync;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * normal_user_id;
@property (nonatomic, retain) NSString * profile_id;
@property (nonatomic, retain) NSNumber * school_Id;
@property (nonatomic, retain) NSString * state;
@property (nonatomic, retain) NSString * street;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * year;
@property (nonatomic, retain) NSString * zipCode;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) NSSet *myClasses;
@property (nonatomic, retain) NSSet *parents;
@property (nonatomic, retain) NSSet *teacher;
@end

@interface MyProfile (CoreDataGeneratedAccessors)

- (void)addMyClassesObject:(ClassesInfo *)value;
- (void)removeMyClassesObject:(ClassesInfo *)value;
- (void)addMyClasses:(NSSet *)values;
- (void)removeMyClasses:(NSSet *)values;

- (void)addParentsObject:(MyParent *)value;
- (void)removeParentsObject:(MyParent *)value;
- (void)addParents:(NSSet *)values;
- (void)removeParents:(NSSet *)values;

- (void)addTeacherObject:(MyTeacher *)value;
- (void)removeTeacherObject:(MyTeacher *)value;
- (void)addTeacher:(NSSet *)values;
- (void)removeTeacher:(NSSet *)values;

@end

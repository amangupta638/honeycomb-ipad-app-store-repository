//
//  News_Attachment.h
//  StudyPlaner
//
//  Created by Dhirendra on 22/08/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class News;

@interface News_Attachment : NSManagedObject

@property (nonatomic, retain) NSString * atachment_content;
@property (nonatomic, retain) NSNumber * atachment_id;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSNumber * news_id;
@property (nonatomic, retain) NSDate * updated;
@property (nonatomic, retain) News *news;

@end

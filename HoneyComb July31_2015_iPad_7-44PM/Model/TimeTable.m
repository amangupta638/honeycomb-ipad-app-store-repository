//
//  TimeTable.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 01/08/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "TimeTable.h"


@implementation TimeTable

@dynamic cycle_Length;
@dynamic end_date;
@dynamic school_id;
@dynamic semester_name;
@dynamic startDate;
@dynamic timeTable_id;
@dynamic cycleLable;
// Campus Based Start
@dynamic campusCode;
// Campus Based End



@end

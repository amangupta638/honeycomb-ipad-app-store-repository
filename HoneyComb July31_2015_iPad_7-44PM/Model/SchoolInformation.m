//
//  SchoolInformation.m
//  StudyPlaner
//
//  Created by Mitul Trivedi on 12/20/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "SchoolInformation.h"

@implementation SchoolInformation

@synthesize content;
@synthesize content_type;
@synthesize item_id;
@synthesize order_by;
@synthesize order_no;
@synthesize parent_id;
@synthesize school_Id;
@synthesize status;
@synthesize title;
@synthesize update_date;
@synthesize startDate;
@synthesize endDate;

@end

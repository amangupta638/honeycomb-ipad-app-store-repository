//
//  Merit.m
//  StudyPlaner
//
//  Created by Viraj Harhare on 12/29/14.
//  Copyright (c) 2014 Swatantra Singh. All rights reserved.
//

#import "Merit.h"

@implementation Merit

@synthesize name;
@synthesize image;
@synthesize listofvaluesTypeid;
@synthesize subType;

- (void)dealloc
{
    [name release];
    [image release];
    [subType release];
    [listofvaluesTypeid release];
    
    [super dealloc];
}

@end

//
//  Country.m
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "Country.h"
#import "States.h"


@implementation Country

@dynamic country_id;
@dynamic country_name;
@dynamic upade_time;
@dynamic state;

@end

//
//  MyClass.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 03/07/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "MyClass.h"
#import "Student.h"


@implementation MyClass

@dynamic app_id;
@dynamic class_id;
@dynamic class_name;
@dynamic class_userID;
@dynamic code;
@dynamic createdBy;
@dynamic createdTime;
@dynamic cycle_day;
@dynamic ipaddress;
@dynamic isDelete;
@dynamic isSync;
@dynamic period_Id;
@dynamic room_Id;
@dynamic room_name;
@dynamic subject_d;
@dynamic subject_name;
@dynamic timeTable_id;
@dynamic updatedTime;
@dynamic student;
// Campus Based Start
@dynamic campusId;
@dynamic campusCode;
// Campus Based End
@synthesize arrRegisteredStudents ;

@end

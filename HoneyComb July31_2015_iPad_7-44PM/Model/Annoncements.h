//
//  Annoncements.h
//  StudyPlaner
//
//  Created by Dhirendra on 27/07/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Annoncements : NSManagedObject

@property (nonatomic, retain) NSNumber * all_parents;
@property (nonatomic, retain) NSNumber * all_Student;
@property (nonatomic, retain) NSNumber * all_Teacher;
@property (nonatomic, retain) NSNumber * annuoncement_id;
@property (nonatomic, retain) NSString * campuse;
@property (nonatomic, retain) NSString * created_by;
@property (nonatomic, retain) NSDate * created_time;
@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSNumber * isReadAnn;
@property (nonatomic, retain) NSNumber * school_id;
@property (nonatomic, retain) NSDate * updated_time;
@property (nonatomic, retain) NSDate * cretedDate;
@property (nonatomic, retain) NSString * createdDate_Str;

@end

//
//  Room.h
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Room : NSManagedObject

@property (nonatomic, retain) NSNumber * campus_id;
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSNumber * room_id;
@property (nonatomic, retain) NSString * room_name;
@property (nonatomic, retain) NSNumber * school_id;

@end

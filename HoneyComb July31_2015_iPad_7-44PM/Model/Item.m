//
//  Item.m
//  StudyPlaner
//
//  Created by Aman Gupta on 12/16/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "Item.h"
#import "Attachment.h"


@implementation Item

@dynamic assignedDate;
@dynamic assignedTime;
@dynamic assignedtoMe;
@dynamic assignTimeDate;
@dynamic assingedUserId;
@dynamic class_id;
@dynamic created;
@dynamic dueDate;
@dynamic dueTime;
@dynamic dueTimeDate;
@dynamic estimat_Minuts;
@dynamic estimate_hours;
@dynamic ipAdd;
@dynamic isCompelete;
@dynamic isDelete;
@dynamic isSync;
@dynamic itemDescription;
@dynamic itemId;
@dynamic lastUpdated;
@dynamic onTheDay;
@dynamic priority;
@dynamic progress;
@dynamic school_id;
@dynamic subject_name;
@dynamic title;
@dynamic type;
@dynamic universal_Id;
@dynamic weight;
@dynamic itemattachment;
@dynamic weblink;
@dynamic createdBy ;
//Aman GP
@dynamic itemType_id;
@dynamic isRead;
/*******************New Methods Added For Out Class********************************/

@dynamic isApproved;
@dynamic passTypeId;
/*******************New Methods Added For Out Class********************************/
@end

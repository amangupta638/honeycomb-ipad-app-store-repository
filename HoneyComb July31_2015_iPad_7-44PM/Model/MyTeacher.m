//
//  MyTeacher.m
//  StudyPlaner
//
//  Created by Swatantra Singh on 05/09/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "MyTeacher.h"
#import "MyProfile.h"


@implementation MyTeacher

@dynamic email;
@dynamic image;
@dynamic imageUrl;
@dynamic name;
@dynamic phone;
@dynamic subject_id;
@dynamic subject_name;
@dynamic teacher_id;
@dynamic salutation;
@dynamic profile;
@dynamic status;
@end

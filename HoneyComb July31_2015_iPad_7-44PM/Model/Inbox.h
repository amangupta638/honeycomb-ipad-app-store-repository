//
//  Inbox.h
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Inbox : NSManagedObject

@property (nonatomic, retain) NSDate * created_time;
@property (nonatomic, retain) NSString * discrptin;
@property (nonatomic, retain) NSString * inbox_user;
@property (nonatomic, retain) NSNumber * message_id;
@property (nonatomic, retain) NSNumber * parent_id;
@property (nonatomic, retain) NSString * reciver_id;
@property (nonatomic, retain) NSString * sender_id;
@property (nonatomic, retain) NSString * subject;

@end

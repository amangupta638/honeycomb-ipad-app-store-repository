//
//  TimeTableCycle.h
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TimeTableCycle : NSManagedObject

@property (nonatomic, retain) NSNumber * campus_id;
@property (nonatomic, retain) NSString * cycle_day;
@property (nonatomic, retain) NSString * cycle_name;
@property (nonatomic, retain) NSNumber * school_id;
@property (nonatomic, retain) NSString * short_lable;
@property (nonatomic, retain) NSNumber * timeTable_id;
@property (nonatomic, retain) NSNumber * timeTableCycle_id;

@end

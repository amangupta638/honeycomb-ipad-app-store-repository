//
//  Notifications.m
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "Notifications.h"


@implementation Notifications

@dynamic notificatio_details;
@dynamic notification_id;
@dynamic notification_title;
@dynamic notification_type;
@dynamic readStatus;

@end

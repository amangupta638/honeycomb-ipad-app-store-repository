//
//  MyParent.h
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MyProfile;

@interface MyParent : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * imageUrl;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * parent_id;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * studentId;
@property (nonatomic, retain) NSString * studentName;
@property (nonatomic, retain) MyProfile *profile;
// New Code Aman Migration Prime
@property (nonatomic, retain) NSString * parent_id_str;


@end

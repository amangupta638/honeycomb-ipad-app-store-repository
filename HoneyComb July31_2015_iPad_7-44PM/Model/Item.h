//
//  Item.h
//  StudyPlaner
//
//  Created by Aman Gupta on 12/16/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Attachment;

@interface Item : NSManagedObject

@property (nonatomic, retain) NSDate * assignedDate;
@property (nonatomic, retain) NSString * assignedTime;
@property (nonatomic, retain) NSNumber * assignedtoMe;
@property (nonatomic, retain) NSDate * assignTimeDate;
@property (nonatomic, retain) NSString * assingedUserId;
@property (nonatomic, retain) NSNumber * class_id;
@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSDate * dueDate;
@property (nonatomic, retain) NSString * dueTime;
@property (nonatomic, retain) NSDate * dueTimeDate;
@property (nonatomic, retain) NSString * estimat_Minuts;
@property (nonatomic, retain) NSString * estimate_hours;
@property (nonatomic, retain) NSString * ipAdd;
@property (nonatomic, retain) NSNumber * isCompelete;
@property (nonatomic, retain) NSNumber * isDelete;
@property (nonatomic, retain) NSNumber * isSync;
@property (nonatomic, retain) NSString * itemDescription;
@property (nonatomic, retain) NSNumber * itemId;
@property (nonatomic, retain) NSDate * lastUpdated;
@property (nonatomic, retain) NSNumber * onTheDay;
@property (nonatomic, retain) NSString * priority;
@property (nonatomic, retain) NSString * progress;
@property (nonatomic, retain) NSNumber * school_id;
@property (nonatomic, retain) NSString * subject_name;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * weblink;
@property (nonatomic, retain) NSString *createdBy ;
//Aman GP
@property (nonatomic, retain) NSNumber * itemType_id;

@property (nonatomic, retain) NSNumber * isRead;

@property (nonatomic, retain) NSNumber * universal_Id;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) NSSet *itemattachment;
/*******************New Methods Added For Out Class********************************/
@property (nonatomic, retain) NSNumber * isApproved;
@property (nonatomic, retain) NSNumber * passTypeId;

/*******************New Methods Added For Out Class********************************/
@end

@interface Item (CoreDataGeneratedAccessors)

- (void)addItemattachmentObject:(Attachment *)value;
- (void)removeItemattachmentObject:(Attachment *)value;
- (void)addItemattachment:(NSSet *)values;
- (void)removeItemattachment:(NSSet *)values;

@end

//
//  DiaryItemTypes.h
//  StudyPlaner
//
//  Created by Aman Gupta on 12/5/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DiaryItemTypes : NSManagedObject

@property (nonatomic, retain) NSNumber * diaryitemtypeid;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * formtemplate;
@property (nonatomic, retain) NSNumber * school_Id;
@property (nonatomic, retain) NSString * icon;
@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) NSNumber * isonlyteacher;

@end

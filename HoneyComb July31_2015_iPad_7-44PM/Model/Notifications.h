//
//  Notifications.h
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Notifications : NSManagedObject

@property (nonatomic, retain) NSString * notificatio_details;
@property (nonatomic, retain) NSNumber * notification_id;
@property (nonatomic, retain) NSString * notification_title;
@property (nonatomic, retain) NSString * notification_type;
@property (nonatomic, retain) NSNumber * readStatus;

@end

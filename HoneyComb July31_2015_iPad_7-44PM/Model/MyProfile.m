//
//  MyProfile.m
//  StudyPlaner
//
//  Created by Dhirendra on 23/08/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "MyProfile.h"
#import "ClassesInfo.h"
#import "MyParent.h"
#import "MyTeacher.h"


@implementation MyProfile

@dynamic address;
@dynamic birthDate;
@dynamic country;
@dynamic email;
@dynamic image;
@dynamic image_url;
@dynamic isSync;
@dynamic name;
@dynamic phone;
@dynamic profile_id;
@dynamic school_Id;
@dynamic state;
@dynamic street;
@dynamic type;
@dynamic year;
@dynamic zipCode;
@dynamic last_name;
@dynamic myClasses;
@dynamic parents;
@dynamic teacher;
@dynamic normal_user_id;
@end

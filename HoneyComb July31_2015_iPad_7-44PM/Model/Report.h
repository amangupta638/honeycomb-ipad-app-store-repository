//
//  Report.h
//  StudyPlaner
//
//  Created by Amol Gaikwad on 20/06/14.
//  Copyright (c) 2014 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Report : NSManagedObject

@property (nonatomic, retain) NSNumber * report_id;
@property (nonatomic, retain) NSString * school_Id;
@property (nonatomic, retain) NSNumber * isReportSchoolActive;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSNumber * isAdministrator;
@property (nonatomic, retain) NSNumber * isParent;
@property (nonatomic, retain) NSNumber * isTeacher;
@property (nonatomic, retain) NSNumber * isStudent;
@property (nonatomic, retain) NSString * report_url;
@property (nonatomic, retain) NSString * report_description;
@property (nonatomic, retain) NSString * report_name;

@end

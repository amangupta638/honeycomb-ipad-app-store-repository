//
//  TableUpdateTime.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 26/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TableUpdateTime : NSManagedObject

@property (nonatomic, retain) NSNumber * about;
@property (nonatomic, retain) NSNumber * calenderPanel;
@property (nonatomic, retain) NSNumber * classUpdateTime;
@property (nonatomic, retain) NSNumber * diaryItem;
@property (nonatomic, retain) NSNumber * empowerContent;
@property (nonatomic, retain) NSNumber * inbox;
@property (nonatomic, retain) NSNumber * profile;
@property (nonatomic, retain) NSNumber * schoolInfo;
@property (nonatomic, retain) NSNumber * studentUpdateTime;
@property (nonatomic, retain) NSString * table_id;
@property (nonatomic, retain) NSNumber * timeTble;
@property (nonatomic, retain) NSNumber * report;
// Campus Based Start
@property (nonatomic, retain) NSNumber * calendarCycleUpdate;
// Campus Based End
@property (nonatomic, retain) NSNumber * teacherUpdateTime;
@property (nonatomic, retain) NSNumber * parentsUpdateTime;


@end

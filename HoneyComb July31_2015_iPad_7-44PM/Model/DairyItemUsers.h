//
//  DairyItemUsers.h
//  StudyPlaner
//
//  Created by Dhirendra on 18/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DairyItemUsers : NSManagedObject

@property (nonatomic, retain) NSNumber * item_id;
@property (nonatomic, retain) NSString * student_id;
@property (nonatomic, retain) NSString * progress;
@property (nonatomic, retain) NSDate * reminder;
@property (nonatomic, retain) NSDate * update_date;
@property (nonatomic, retain) NSNumber * unique_id;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * approverName;
@property (nonatomic, retain) NSNumber * passTypeStatus;
@property (nonatomic, retain) NSNumber * isSynch;
@property (nonatomic, retain) NSNumber * isDeletedOnServer;
@property (nonatomic, retain) NSString * studentName;

//Merit - Start
@property (nonatomic, retain) NSString *meritTitle;
@property (nonatomic, retain) NSString *meritDescription;
@property (nonatomic, retain) NSNumber *isNotify;
@property (nonatomic, retain) NSNumber *listofvaluesTypeid;
@property (nonatomic, retain) NSDate   *meritAssignedDateAndTime;
//Merit - End

@end

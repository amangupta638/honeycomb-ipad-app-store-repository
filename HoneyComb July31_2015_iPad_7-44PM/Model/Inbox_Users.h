//
//  Inbox_Users.h
//  StudyPlaner
//
//  Created by Nandini Tomke on 13/12/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Inbox_Users : NSManagedObject

@property (nonatomic, retain) NSNumber * messageId;
@property (nonatomic, retain) NSNumber * localMessageId;
@property (nonatomic, retain) NSNumber * iD;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSNumber * isDel;

@end

//
//  Country.h
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class States;

@interface Country : NSManagedObject

@property (nonatomic, retain) NSNumber * country_id;
@property (nonatomic, retain) NSString * country_name;
@property (nonatomic, retain) NSDate * upade_time;
@property (nonatomic, retain) NSSet *state;
@end

@interface Country (CoreDataGeneratedAccessors)

- (void)addStateObject:(States *)value;
- (void)removeStateObject:(States *)value;
- (void)addState:(NSSet *)values;
- (void)removeState:(NSSet *)values;

@end

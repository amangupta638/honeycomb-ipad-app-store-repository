//
//  ClassesInfo.h
//  StudyPlaner
//
//  Created by Dhirendra on 23/08/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MyProfile, Student;

@interface ClassesInfo : NSManagedObject

@property (nonatomic, retain) NSNumber * class_id;
@property (nonatomic, retain) NSString * class_name;
@property (nonatomic, retain) NSString * class_UserId;
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSNumber * period_id;
@property (nonatomic, retain) NSNumber * room_id;
@property (nonatomic, retain) NSNumber * schoo_id;
@property (nonatomic, retain) NSNumber * subject_id;
@property (nonatomic, retain) NSString * subject_name;
@property (nonatomic, retain) NSDate * updateTime;
@property (nonatomic, retain) MyProfile *profile;
@property (nonatomic, retain) NSSet *students;
@end

@interface ClassesInfo (CoreDataGeneratedAccessors)

- (void)addStudentsObject:(Student *)value;
- (void)removeStudentsObject:(Student *)value;
- (void)addStudents:(NSSet *)values;
- (void)removeStudents:(NSSet *)values;

@end

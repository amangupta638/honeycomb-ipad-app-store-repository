//
//  MyTeacher.h
//  StudyPlaner
//
//  Created by Swatantra Singh on 05/09/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MyProfile;

@interface MyTeacher : NSManagedObject

@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * imageUrl;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSNumber * subject_id;
@property (nonatomic, retain) NSString * subject_name;
@property (nonatomic, retain) NSString * teacher_id;
@property (nonatomic, retain) NSString * salutation;
@property (nonatomic, retain) NSString * status;

@property (nonatomic, retain) MyProfile *profile;

@end

//
//  Room.m
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "Room.h"


@implementation Room

@dynamic campus_id;
@dynamic code;
@dynamic room_id;
@dynamic room_name;
@dynamic school_id;

@end

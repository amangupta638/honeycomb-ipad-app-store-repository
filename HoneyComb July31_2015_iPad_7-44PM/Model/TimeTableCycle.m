//
//  TimeTableCycle.m
//  StudyPlaner
//
//  Created by Dhirendra on 10/06/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import "TimeTableCycle.h"


@implementation TimeTableCycle

@dynamic campus_id;
@dynamic cycle_day;
@dynamic cycle_name;
@dynamic school_id;
@dynamic short_lable;
@dynamic timeTable_id;
@dynamic timeTableCycle_id;

@end

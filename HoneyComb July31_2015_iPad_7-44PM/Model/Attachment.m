//
//  Attachment.m
//  StudyPlaner
//
//  Created by Aman gupta on 10/07/15.
//  Copyright (c) 2015 Swatantra Singh. All rights reserved.
//

#import "Attachment.h"
#import "EventItem.h"
#import "Item.h"


@implementation Attachment

@dynamic aID;
@dynamic createdBy;
@dynamic dateAttached;
@dynamic diaryItemId;
@dynamic displayFileName;
@dynamic filePath;
@dynamic isDeletedOnLocal;
@dynamic isSync;
@dynamic itemtype;
@dynamic lastupdatedDate;
@dynamic localAttachmentId;
@dynamic remoteFilePath;
@dynamic size;
@dynamic type;
@dynamic eventItem;
@dynamic item;

@synthesize driveName;
@synthesize indexPath;
@synthesize localFolderName;
@synthesize displayName;
@synthesize fileType;
@synthesize localFilePath;
@synthesize fileData ;

@end

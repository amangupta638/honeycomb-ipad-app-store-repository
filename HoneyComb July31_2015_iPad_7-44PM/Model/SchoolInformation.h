//
//  SchoolInformation.h
//  StudyPlaner
//
//  Created by Mitul Trivedi on 12/20/13.
//  Copyright (c) 2013 Swatantra Singh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SchoolInformation : NSObject

@property (nonatomic, retain) NSString * content;
@property (nonatomic, retain) NSString * content_type;
@property (nonatomic, retain) NSNumber * item_id;
@property (nonatomic, retain) NSString * order_by;
@property (nonatomic, retain) NSNumber * order_no;
@property (nonatomic, retain) NSNumber * parent_id;
@property (nonatomic, retain) NSNumber * school_Id;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSDate * update_date;
@property (nonatomic, retain) NSDate * startDate;
@property (nonatomic, retain) NSDate * endDate;

@end
